package db.service;

import db.domain.Task;
import db.response.TaskListResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Service that uses rest call to complete its goals.
 * <p>
 * Created by vtymkiv on 17.08.2016.
 */
@Component
public class TaskServiceImpl implements TaskService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskServiceImpl.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${rest.provider.protocol}://${rest.provider.host}")
    private String restHost;

    @Value("${rest.provider.port}")
    private String restPort;

    @Value("${rest.provider.endpoint}")
    private String restEndPoint;


    @Override
    public List<String> getTaskListNames() {
        LOGGER.info("Getting task list names...");
        String endPoint = restHost + ":" + restPort + restEndPoint + "/getTaskListNames";

        LOGGER.debug("Rest Endpoint: {}", endPoint);
        List<String> tasks = restTemplate.getForObject(endPoint, List.class);
        return tasks;
    }

    @Override
    public List<Task> getTaskList() {
        LOGGER.info("Getting task list ...");
        TaskListResponse taskListResponse = restTemplate.getForObject(restHost + ":" + restPort + restEndPoint + "/getTaskList", TaskListResponse.class);
        LOGGER.debug("Response {}", taskListResponse);
        return taskListResponse.getTasks();
    }
}
