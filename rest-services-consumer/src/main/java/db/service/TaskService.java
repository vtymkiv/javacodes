package db.service;

import db.domain.Task;

import java.util.List;

/**
 * Created by vtymkiv on 17.08.2016.
 */
public interface TaskService {
    List<String> getTaskListNames();

    List<Task> getTaskList();
}
