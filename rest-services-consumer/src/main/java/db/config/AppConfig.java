package db.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Responsible for configuring access to external config files, like properties files
 * Created by vtymkiv on 22.08.2016.
 */
@PropertySource(value = "classpath:properties/application.properties")
@Configuration
public class AppConfig {
}
