package db;

import db.domain.Task;
import db.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

/**
 *
 */
@SpringBootApplication
public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(App.class, args);
        TaskService bean = context.getBean(TaskService.class);

        List<String> taskListNames = bean.getTaskListNames();
        LOGGER.info("Task list names: {}", taskListNames);

        List<Task> taskList = bean.getTaskList();
        LOGGER.info("Task list : {}", taskList);
    }
}
