package org.example.service.mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public interface JavaMailSender {
    MimeMessage createMimeMessage();

    void send(MimeMessage message) throws MessagingException;

    Properties getJavaMailProperties();
}
