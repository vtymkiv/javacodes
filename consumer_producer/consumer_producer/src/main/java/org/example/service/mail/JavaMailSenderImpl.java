package org.example.service.mail;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class JavaMailSenderImpl implements JavaMailSender {
    private Session session;
    private Properties javaMailProps;

    public JavaMailSenderImpl() {
        session = initSession();
        javaMailProps = new Properties();
    }

    @Override
    public MimeMessage createMimeMessage() {
        return new MimeMessage(session);
    }

    @Override
    public void send(MimeMessage message) throws MessagingException {
        Transport.send(message);
    }

    @Override
    public Properties getJavaMailProperties() {
        return javaMailProps;
    }

    private Session initSession() {
        return Session.getDefaultInstance(getJavaMailProperties());
    }
}
