package org.example.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class AsyncService {
    private final ExecutorService executorService;
    private long timeout;
    private TimeUnit timeUnit;

    public AsyncService(int numOfThreads, long timeout,  TimeUnit timeUnit) {
        this.executorService = Executors.newFixedThreadPool(numOfThreads);
        this.timeout = timeout;
        this.timeUnit = timeUnit;
    }

    public void submit(Runnable runnable) {
        this.executorService.submit(runnable);
    }

    public void awaitTerminationAfterShutdown() {
        this.executorService.shutdown();
        try {
            if (!this.executorService.awaitTermination(timeout, timeUnit)) {
                this.executorService.shutdownNow();
            }
        } catch (InterruptedException ex) {
            this.executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
}
