package org.example.service.mail;


import lombok.extern.slf4j.Slf4j;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.util.List;

/**
 * Responsible for sending email with attachment.
 *
 * @author Vitaliy Tymkiv
 */
@Slf4j
public class MailSender {

    private static final String CONTENT_TYPE = "text/html; charset=utf-8";

    private  MailUtils mailUtils;

    private JavaMailSender javaMailSender;

    public MailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    /**
     * Send email to recipient without attachments with text
     *
     * @param text
     * @throws Exception
     */
    public void sendEmail(String subject, String text, List<String> to, String from) throws MessagingException {
        sendEmail(subject, text, mailUtils.convertToAddressList(to), mailUtils.convertToAddress(from));
    }

    /**
     * Sending email to recipients with text in body and without attachment .
     *
     * @param subject
     * @param text
     * @param to
     * @param from
     * @throws MessagingException
     */
    public void sendEmail(String subject, String text, List<InternetAddress> to, InternetAddress from) throws MessagingException {
        log.debug("Sending email with subject [{}], text [{}], to recipients [{}] and from [{}]", subject, text, to, from);

        MimeMessage message = javaMailSender.createMimeMessage();
        message.setRecipients(Message.RecipientType.TO, to.toArray(new InternetAddress[to.size()]));
        message.setFrom(from);
        message.setSubject(subject);
        message.setContent(text, CONTENT_TYPE);
        javaMailSender.send(message);

        log.debug("Email was send to {} with subject {}.", to, subject);
    }


    /**
     * Sending email to recipients. All info required for email are passed explicitly.
     *
     * @param data     The document that will be attach to email
     * @param to       List of recipients
     * @param from     From whom this message would be send
     * @param subject  The subject of email
     * @param fileName Name of the attached document
     * @param fileType The type of the attached document
     * @throws MessagingException
     * @throws IOException
     */
    public void sendEmail(byte[] data, List<InternetAddress> to, InternetAddress from, String subject, String fileName, String fileType) throws MessagingException, IOException {
        log.debug("Sending email to {} with subject: {}, file name: {} and file type {} from {}.", to, subject, fileName, fileType, from);

        MimeMessage message = javaMailSender.createMimeMessage();
        message.setRecipients(Message.RecipientType.TO, to.toArray(new InternetAddress[to.size()]));
        message.setSubject(subject);
        message.setFrom(from);

        BodyPart bodyPart = initBodyPart(data, fileName, fileType);
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(bodyPart);
        message.setContent(multipart);
        javaMailSender.send(message);

        log.debug("Email was send to {} with subject {}.", to, subject);
    }


    /**
     * Initializing body with data, file name and file type
     *
     * @param data
     * @param fileType
     * @param fileName
     * @return
     * @throws MessagingException
     */
    public BodyPart initBodyPart(byte[] data, String fileName, String fileType) throws MessagingException {
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setFileName(fileName);
        DataSource dataSource = new ByteArrayDataSource(data, fileType);
        messageBodyPart.setDataHandler(new DataHandler(dataSource));
        return messageBodyPart;
    }

}
