package org.example.service.mail;

import lombok.extern.slf4j.Slf4j;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Utility for sending email
 *
 * @author Vitaliy Tymkiv
 */
@Slf4j
public class MailUtils {

    public List<InternetAddress> convertToAddressList(List<String> recipients) throws AddressException {
        log.debug("Initializing list of recipients: {}", recipients);
        Objects.requireNonNull(recipients, "Empty list of recipients"); //notEmpty(recipients, "Empty list of recipients");
        int n = recipients.size();
        List<InternetAddress> internetAddressList = new ArrayList<>(n);
        for (String recipient : recipients) {
            internetAddressList.add(convertToAddress(recipient));
        }
        return internetAddressList;
    }

    public InternetAddress convertToAddress(String recipient) throws AddressException {
        log.info("Initializing recipient: {}", recipient);
        Objects.requireNonNull(recipient, "Empty recipient");
        return new InternetAddress(recipient.trim());
    }
}
