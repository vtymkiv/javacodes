package org.example;

import lombok.extern.slf4j.Slf4j;
import org.example.service.AsyncService;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 */
@Slf4j
public class ADMReportCLI {
    public static void main(String[] args) {
        String test = "ttttt|";
        String[] split = org.apache.commons.lang.StringUtils.split(test, "|");
        System.out.println(String.format("%s %s %s %s", 1/2, 2/2, 3/2, 4/2));
        System.out.println(split[0]);
        System.out.println("Hello World!");
        run();
    }

    private static void run() {

        List<String> regionList = Arrays.asList("ASIA1", "ASIA2", "ASIA3");
        //regionList.forEach(region-> produceReportByRegion(region));
        produceReportByRegion("ASIA3");
    }

    private static void produceReportByRegion(String region) {
        log.info("Producing report by region {}", region);
        AsyncService asyncService = new AsyncService(Runtime.getRuntime().availableProcessors(),1, TimeUnit.MILLISECONDS);
        List<Runnable> runnableList = Arrays.asList(
                () -> {
                    int delay = 100;
                    System.out.println("Fast thread started");
                    AsyncService asyncService1 = new AsyncService(1, 2, TimeUnit.MILLISECONDS);

                    asyncService1.submit( ()-> {
                        System.out.println("Fast Sub thread started");
                        for(int i = 0; i < 200; i++) {
                            System.out.println("Fast Sub thread:" + i);
                            putToSleepOn(delay + 10);
                        }
                        System.out.println("Fast Sub thread ended");
                    });
                    asyncService1.awaitTerminationAfterShutdown();

                    for(int i = 0; i < 100; i++) {
                        System.out.println("Fast thread:" + i);
                        putToSleepOn(delay);
                    }
                    System.out.println("Fast thread ended");
                },

                () -> {
                    int delay = 1000;
                    System.out.println("Slow thread started");
                    for(int i = 0; i < 100; i++) {
                        System.out.println("Slow thread:" + i);

                        putToSleepOn(delay);
                    }
                    System.out.println("Slow thread ended");
                }

               );


        long startProcessingTime = System.currentTimeMillis();
        for( Runnable runnable : runnableList) {
            asyncService.submit(runnable);
        }
        //runnableList.forEach(
        //);

        asyncService.awaitTerminationAfterShutdown();

        long totalProcessingTime = System.currentTimeMillis() - startProcessingTime;
        System.out.println("Precessing time: " + totalProcessingTime);

    }

    private static void putToSleepOn(long sleepMillis) {
        try {
            Thread.sleep(sleepMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

