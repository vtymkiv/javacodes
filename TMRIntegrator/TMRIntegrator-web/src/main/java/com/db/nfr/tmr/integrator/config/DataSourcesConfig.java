package com.db.nfr.tmr.integrator.config;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jndi.JndiObjectFactoryBean;

/**
 * Created by tymkvit on 02/02/2017.
 */
@Configuration
public class DataSourcesConfig {
    @ConfigurationProperties(prefix = "data-sources.stg")
    @Bean
    public JndiPropertyHolder jndiStagePropertyHolder() {
        return new JndiPropertyHolder();
    }


    @ConfigurationProperties(prefix = "data-sources.tms")
    @Bean
    public JndiPropertyHolder jndiTmsPropertyHolder() {
        return new JndiPropertyHolder();
    }


    @ConfigurationProperties(prefix = "data-sources.adm")
    @Bean
    public JndiPropertyHolder jndiAdmAppPropertyHolder() {
        return new JndiPropertyHolder();
    }


    @Bean(name = "admJdbcTemplate")
    public JdbcTemplate admJdbcTemplate() throws NamingException {
        return new JdbcTemplate(admAppDataSource());
    }

    @Bean
    public SimpleJdbcCall admSimpleJdbcCall() throws NamingException {
        return new SimpleJdbcCall(admJdbcTemplate());
    }

    @Bean(name = "tmsJdbcTemplate")
    public JdbcTemplate tmsJdbcTemplate() throws NamingException {
        return new JdbcTemplate(tmsFeedDataSource());
    }

    @Bean
    public SimpleJdbcCall tmsSimpleJdbcCall() throws NamingException {
        return new SimpleJdbcCall(tmsJdbcTemplate());
    }

    @Bean(name = "stgJdbcTemplate")
    public JdbcTemplate stgJdbcTemplate() throws NamingException {
        return new JdbcTemplate(stgFeedDataSource());
    }

    @Bean(name = "stgSimpleJdbcCall")
    public SimpleJdbcCall stgSimpleJdbcCall() throws NamingException {
        return new SimpleJdbcCall(stgJdbcTemplate());
    }


    @Primary
    @Bean(name = "stgFeedDS", destroyMethod = "")
    public DataSource stgFeedDataSource() throws NamingException {
        JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
        jndiObjectFactoryBean.setJndiName(jndiStagePropertyHolder().getJndiName());
        jndiObjectFactoryBean.setProxyInterface(DataSource.class);
        jndiObjectFactoryBean.setLookupOnStartup(false);
        jndiObjectFactoryBean.afterPropertiesSet();
        return (DataSource) jndiObjectFactoryBean.getObject();
    }

    @Bean(name = "admAppDS", destroyMethod = "")
    public DataSource admAppDataSource() throws NamingException {
        JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
        jndiObjectFactoryBean.setJndiName(jndiAdmAppPropertyHolder().getJndiName());
        jndiObjectFactoryBean.setProxyInterface(DataSource.class);
        jndiObjectFactoryBean.setLookupOnStartup(false);
        jndiObjectFactoryBean.afterPropertiesSet();
        return (DataSource) jndiObjectFactoryBean.getObject();
    }

    @Bean(name = "tmsFeedDS", destroyMethod = "")
    public DataSource tmsFeedDataSource() throws NamingException {
        JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
        jndiObjectFactoryBean.setJndiName(jndiTmsPropertyHolder().getJndiName());
        jndiObjectFactoryBean.setProxyInterface(DataSource.class);
        jndiObjectFactoryBean.setLookupOnStartup(false);
        jndiObjectFactoryBean.afterPropertiesSet();
        return (DataSource) jndiObjectFactoryBean.getObject();
    }

    private static class JndiPropertyHolder {
        private String jndiName;

        public String getJndiName() {
            return jndiName;
        }

        public void setJndiName(String jndiName) {
            this.jndiName = jndiName;
        }
    }


}
