package com.db.nfr.tmr.integrator.quantstruts.common;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by tymkvit on 20/02/2017.
 */
@Slf4j
public enum Status {
    EQT_LOAD_COMPLETED("EQT Load Completed"),
    DEBT_LOAD_COMPLETED("DEBT Load Completed"),
    COMPLETED("Completed"),
    STARTED("Started");

    @Getter
    private final String statusCode;

    Status(String statusCode) {
        this.statusCode = statusCode;
    }

    public static Status findByCode(String name) {
        Status foundStatus = null;
        for (Status status : values()) {
            if (status.getStatusCode().equalsIgnoreCase(name)) {
                foundStatus = status;
                break;
            }
        }
        return foundStatus;
    }
}
