package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.PkgLoadMandatesRepoDAO;
import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.quantstruts.common.FileRepoStreamProcessor;
import com.db.nfr.tmr.integrator.quantstruts.common.Header;
import com.db.nfr.tmr.integrator.quantstruts.common.MandateType;
import com.db.nfr.tmr.integrator.quantstruts.common.RecAllFilesProcessor;
import com.db.nfr.tmr.integrator.quantstruts.common.Status;


import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.RISK_2_PM;
import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.RISK_TO_PM_ID;
import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.TRADER_2_RISK;
import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.TRADER_TO_RISK_ID;

/**
 * @author gasiork
 */
@Component
public class EqtRouteBuilderFactory {
    private String routeId;
    private String cronExpr;

    @Value("${camel.route.scheduler}")
    private String routeScheduler;

    @Autowired
    private RecAllFilesProcessor recAllFilesProcessor;
    @Autowired
    private FileRepoStreamProcessor fileRepoStreamProcessor;
    @Autowired
    private EqtAggregationStrategy eqtAggregationStrategy;
    @Autowired
    private EqtProdLogProcessor eqtProdLogProcessor;
    @Autowired
    private EqtProdProcessor eqtProdProcessor;
    @Autowired
    private EqtMandateProcessor eqtMandateProcessor;
    @Autowired
    private EqtLoadProcessor eqtLoadProcessor;
    @Autowired
    private EqtSmallFilesProcessor eqtSmallFilesProcessor;
    @Autowired
    private EqtDeleteSmallFilesProcessor eqtDeleteSmallFilesProcessor;

    @Autowired
    private PkgLoadMandatesRepoDAO pkgLoadMandatesRepoDAO;

    public RouteBuilder getRouteBuilder() {
        return new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                String scheduler = routeScheduler + cronExpr.replaceAll(" ", "+");

                from("{{ftp-eqt-master-file}}" + TRADER_2_RISK + "&" + scheduler)
                        .process(exchange -> {
                            exchange.getProperties().put(Header.EQT_PROCESSING_STATUS, Status.STARTED.getStatusCode());
                        })
                        // Call process to get feedConfigId based on feed subname(get file info)
                        .setHeader(Header.FEED_CONFIG_ID, constant(TRADER_TO_RISK_ID))
                        // Get info about master file for processing
                        .process(recAllFilesProcessor)
                        // Process master file
                        .process(fileRepoStreamProcessor)
                        // Mark that loading process has been started
                        .process(exchange -> {
                            TypeRecAllFiles masterFileRec = exchange.getIn().getHeader(Header.REC_ALL_FILES, TypeRecAllFiles.class);
                            pkgLoadMandatesRepoDAO.startLoading(MandateType.EQUITY, masterFileRec.getFileDate());
                            exchange.getIn().setHeader(Header.REC_ALL_FILES, masterFileRec);
                            exchange.getProperties().put(Header.EQT_MASTER_FILE_PROPERTY, masterFileRec);
                        })
                        .log("Master file has been processed, starting to process small files...")
                        // Get List of files from directory on ftp server and process it
                        .process(eqtSmallFilesProcessor)
                        .log("Processing small files is done.")
                        // Process master file
                        .process(eqtMandateProcessor)
                        .log(new StringBuilder().append("EqtRouteBuilderFactory finished ").append(TRADER_2_RISK).toString())
                        // Process second master file
                        .pollEnrich("{{ftp-eqt-master-file}}" + RISK_2_PM, eqtAggregationStrategy)
                        .setHeader(Header.FEED_CONFIG_ID, constant(RISK_TO_PM_ID))
                        .process(recAllFilesProcessor)
                        .process(fileRepoStreamProcessor)
                        .process(eqtProdLogProcessor)
                        .process(eqtProdProcessor)
                        .process(eqtLoadProcessor)
                        .process(eqtDeleteSmallFilesProcessor)
                        .log("EqtRouteBuilderFactory completely finished")
                        .setBody(constant("Import of equities is done."))
                        .to("{{emailTo}}" + "{{subjectForEqt}}")
                        .routeId(getRouteId());
            }
        };
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getCronExpr() {
        return cronExpr;
    }

    public void setCronExpr(String cron) {
        this.cronExpr = cron;
    }
}
