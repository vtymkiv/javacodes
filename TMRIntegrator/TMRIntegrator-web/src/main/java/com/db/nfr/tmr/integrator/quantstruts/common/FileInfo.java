package com.db.nfr.tmr.integrator.quantstruts.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by tymkvit on 03/02/2017.
 */
@Getter
@Setter
@ToString
public class FileInfo {
    private int feedConfigId;
    private String fileMask;
    private String imageType;
    private double retention;
}
