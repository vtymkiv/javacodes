package com.db.nfr.tmr.integrator.quantstruts.quant.transformers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 * @author gasiork
 */
public class DefaultTransformer implements Transformer {

    @Override
    public List<String> transformLine(CSVRecord line) {
        List<String> data = new ArrayList<>();
        for (String s : line) {
            if (s != null) {
                s = s.replaceAll("\"", "");
            }
            data.add(s);
        }
        return data;
    }

    @Override
    public Map<String, String> transformLineToMap(String line) {
        String[] array = line.split(",");
        List<String> data = new ArrayList();
        for (int i = 0; i < array.length; i++) {
            try {
                String s = array[i];
                if (!s.isEmpty()) {
                    if (s.startsWith("\"") && s.endsWith("\"")) {
                        s = s.substring(1, s.length() - 1);
                    } else if (s.startsWith("\"")) {
                        s = s.substring(1, s.length());
                        s += array[i + 1].substring(0, array[i + 1].length() - 1);
                    } else if (s.endsWith("\"")) {
                        continue;
                    }
                }
                data.add(s.trim());
            } catch (Exception ex) {
                data.add("");
            }
        }
        Map<String, String> map = new LinkedHashMap<>();
        for (String key : data) {
            map.put(key, null);
        }
        return map;
    }

    @Override
    public List<String> transformRow(Row row) {
        Iterator<Cell> cells = row.cellIterator();
        List<String> list = new ArrayList();
        while (cells.hasNext()) {
            Cell cell = cells.next();
            list.add(getCellValue(cell));
        }
        return list;
    }

    @Override
    public List<String> transformRow(Row row, int columnCount) {
        //columnCount is -1 by default if not provided in client
        if (columnCount < 0) {
            return transformRow(row);
        } else {
            List<String> list = new ArrayList();
            for (int i = 0; i < columnCount; i++) {
                Cell cell = row.getCell(i);
                list.add(getCellValue(cell));
            }
            return list;
        }
    }

    private String getCellValue(Cell cell) {
        String value = null;
        if (cell != null) {
            if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
                value = cell.getStringCellValue();
            } else if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
                value = cell.getNumericCellValue() + "";
            } else if (Cell.CELL_TYPE_BLANK == cell.getCellType()) {
                value = "";
            }
            value = value != null ? value.replaceAll("\"", "") : StringUtils.EMPTY;
        }
        return value;
    }
}
