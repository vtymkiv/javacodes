package com.db.nfr.tmr.integrator.quantstruts.quant.filereaders;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Row;

import com.db.nfr.tmr.integrator.persistence.common.DAO;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.Client;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.OracleMapping;
import com.db.nfr.tmr.integrator.quantstruts.quant.transformers.DefaultTransformer;
import com.db.nfr.tmr.integrator.quantstruts.quant.transformers.Transformer;

import lombok.extern.slf4j.Slf4j;


import static com.db.nfr.tmr.integrator.utils.PropertyUtils.getConfigProperties;

/**
 * @author gasiork
 */
@Slf4j
public abstract class LocalFileReader extends FileReader {
    private static int BULK_COUNT;
    private DAO dao;
    private final Transformer TRANSFORMER = new DefaultTransformer();
    private final List<List<String>> BULK_DATA = new ArrayList<>();

    static {
        BULK_COUNT = Integer.parseInt(getConfigProperties().getProperty("bulk.count", "500"));
    }

    public LocalFileReader(DAO dao) {
        this.dao = dao;
    }

    public void setDao(DAO dao) {
        this.dao = dao;
    }

    public DAO getDao() {
        return dao;
    }

    @Override
    public void cleanUp(Client client) throws SQLException {
        String sql = client.getOracleMapping().getCleanUpSql();
        if (sql != null && !sql.isEmpty()) {
            dao.cleanUp(client.getOracleMapping().getCleanUpSql());
        }
    }

    @Override
    protected void gatherStatistics(Client client) {
        dao.gatherStatistics(client.getOracleMapping().getTableName());
    }

    @Override
    public void processLine(CSVRecord line, Client client) {
        this.processLine(line, client, -1);
    }

    @Override
    public void processLine(CSVRecord line, Client client, int columnCount) {
        if (line == null) {
            processLastLines(client);
            return;
        }
        List<String> dataLine = TRANSFORMER.transformLine(line);
        if (columnCount > -1) {
            while (dataLine.size() > columnCount) {
                dataLine.remove(columnCount);
            }
        }
        BULK_DATA.add(dataLine);
        if (BULK_DATA.size() >= BULK_COUNT) {
            persistBulk(client);
            BULK_DATA.clear();
        }
    }

    protected void processXlsLine(Row row, Client client) {

        List<String> dataLine = TRANSFORMER.transformRow(row, getColumnCount());
        BULK_DATA.add(dataLine);
        if (BULK_DATA.size() >= BULK_COUNT) {
            persistBulk(client);
            BULK_DATA.clear();
        }
    }

    private void processLastLines(Client client) {
        if (!BULK_DATA.isEmpty()) {
            persistBulk(client);
            BULK_DATA.clear();
        }
    }

    private void persistBulk(Client client) {
        List<List<Object>> persistenceData = transformBulkData(BULK_DATA);
        OracleMapping oracleMapping = client.getOracleMapping();
        String bulkType = oracleMapping.getBulkType();
        String type = oracleMapping.getType();
        String sql = oracleMapping.getSql();
        if (bulkType != null && !bulkType.isEmpty()) {
            if (type != null && !type.isEmpty()) {
                if (sql != null && !sql.isEmpty()) {
                    dao.persist(persistenceData, oracleMapping.getBulkType(), oracleMapping.getType(), sql);
                }
            }
        }
    }

    private List<List<Object>> transformBulkData(List<List<String>> bulkData) {
        List<List<Object>> res = new ArrayList();
        for (List<String> list : bulkData) {
            List data = transformData(list);
            if (data != null) {
                res.add(data);
            }
        }
        return res;
    }

    public abstract List<Object> transformData(List<String> bulkData);

    @Override
    public void finishOfParsing(Client client) throws SQLException {
        LOGGER.debug("{}.finishOfParsing", client);
        OracleMapping oracleMapping = client.getOracleMapping();
        dao.markFinish(oracleMapping.getValidateSql(), oracleMapping.getUpdateSql());
    }

    @Override
    public boolean makeLogCall(Client client, int lineNumber) throws SQLException {
        return dao.logCall(client, lineNumber);
    }

    @Override
    public void makeStartCall(Client client) {
        dao.markStartCall(client);
    }

    protected void makeExceptionLogCall(Client client) throws SQLException {
        dao.logCall(client, Client.NO_FILE_DATA_AMOUNT);
    }

}
