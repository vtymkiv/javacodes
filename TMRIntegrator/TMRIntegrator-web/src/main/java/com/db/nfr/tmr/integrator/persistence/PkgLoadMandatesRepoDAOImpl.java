package com.db.nfr.tmr.integrator.persistence;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.db.nfr.tmr.integrator.persistence.common.CustomOracleTypes;
import com.db.nfr.tmr.integrator.quantstruts.common.MandateFile;
import com.db.nfr.tmr.integrator.quantstruts.common.MandateType;

import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleTypes;

/**
 * Created by tymkvit on 03/02/2017.
 */
@Slf4j
@Repository
public class PkgLoadMandatesRepoDAOImpl implements PkgLoadMandatesRepoDAO {
    @Autowired
    private CustomOracleTypes customOracleTypes;
    private final JdbcTemplate tmsJdbcTemplate;
    private SimpleJdbcCall logStartLoadingFilesSP;
    private SimpleJdbcCall loadMandatesSP;
    private SimpleJdbcCall loadMandateFileSP;
    private SimpleJdbcCall saveErrorSP;
    private SimpleJdbcCall gatherStatisticsSP;

    @Autowired
    public PkgLoadMandatesRepoDAOImpl(@Qualifier("tmsJdbcTemplate") JdbcTemplate tmsJdbcTemplate) {
        this.tmsJdbcTemplate = tmsJdbcTemplate;
        createProcedures();
    }

    /**
     * Create procedures that would be used
     */
    private void createProcedures() {
        logStartLoadingFilesSP =
                new SimpleJdbcCall(this.tmsJdbcTemplate)
                        .withSchemaName("tms_owner")
                        .withCatalogName("pkg_load_mandates")
                        .withProcedureName("p_start_loading")
                        .useInParameterNames(
                                "ip_mandate_date",
                                "ip_mandate_type")
                        .declareParameters(
                                new SqlOutParameter("op_error_code", OracleTypes.NUMBER),
                                new SqlOutParameter("op_cursor", OracleTypes.CURSOR),
                                new SqlParameter("ip_mandate_date", OracleTypes.DATE),
                                new SqlParameter("ip_mandate_type", OracleTypes.VARCHAR)
                        );


        loadMandateFileSP = new SimpleJdbcCall(this.tmsJdbcTemplate)
                .withSchemaName("tms_owner")
                .withCatalogName("pkg_load_mandates")
                .withProcedureName("p_load_mandate_file")
                .useInParameterNames("ip_trader_email",
                        "ip_mandate_date",
                        "ip_mandate_type",
                        "ip_mandate_file",
                        "ip_file_name",
                        "ip_file_date",
                        "ip_file_size",
                        "ip_file_type")
                .declareParameters(
                        new SqlOutParameter("op_error_code", OracleTypes.NUMBER),
                        new SqlOutParameter("op_cursor", OracleTypes.CURSOR),
                        new SqlParameter("ip_trader_email", OracleTypes.VARCHAR),
                        new SqlParameter("ip_mandate_date", OracleTypes.DATE),
                        new SqlParameter("ip_mandate_type", OracleTypes.VARCHAR),
                        new SqlParameter("ip_mandate_file", OracleTypes.BLOB),
                        new SqlParameter("ip_file_name", OracleTypes.VARCHAR),
                        new SqlParameter("ip_file_date", OracleTypes.DATE),
                        new SqlParameter("ip_file_size", OracleTypes.NUMBER),
                        new SqlParameter("ip_file_type", OracleTypes.VARCHAR)
                );

        saveErrorSP = new SimpleJdbcCall(this.tmsJdbcTemplate)
                .withSchemaName("tms_owner")
                .withCatalogName("pkg_load_mandates")
                .withProcedureName("p_save_error")
                .useInParameterNames(
                        "ip_trader_email",
                        "ip_mandate_date",
                        "ip_mandate_type",
                        "ip_file_name",
                        "ip_file_date",
                        "ip_file_size",
                        "ip_file_type")
                .declareParameters(
                        new SqlOutParameter("op_error_code", OracleTypes.NUMBER),
                        new SqlOutParameter("op_cursor", OracleTypes.CURSOR),
                        new SqlParameter("ip_trader_email", OracleTypes.VARCHAR),
                        new SqlParameter("ip_mandate_date", OracleTypes.DATE),
                        new SqlParameter("ip_mandate_type", OracleTypes.VARCHAR),
                        new SqlParameter("ip_file_name", OracleTypes.VARCHAR),
                        new SqlParameter("ip_file_date", OracleTypes.DATE),
                        new SqlParameter("ip_file_size", OracleTypes.NUMBER),
                        new SqlParameter("ip_file_type", OracleTypes.VARCHAR)

                );

        loadMandatesSP = new SimpleJdbcCall(this.tmsJdbcTemplate)
                .withSchemaName("tms_owner")
                .withCatalogName("pkg_load_mandates")
                .withProcedureName("p_load_mandates")
                .useInParameterNames(
                        "ip_mandate_date",
                        "ip_mandate_type"
                )
                .declareParameters(
                        new SqlOutParameter("op_error_code", OracleTypes.INTEGER),
                        new SqlOutParameter("op_cursor", OracleTypes.CURSOR),
                        new SqlParameter("ip_mandate_date", OracleTypes.DATE),
                        new SqlParameter("ip_mandate_type", OracleTypes.VARCHAR)
                );


        gatherStatisticsSP = new SimpleJdbcCall(this.tmsJdbcTemplate)
                .withSchemaName("tms_owner")
                .withCatalogName("pkg_load_mandates")
                .withProcedureName("p_gather_stats");
    }

    /**
     * Start the loading process and log it.
     * It will call pkg_load_mandates.p_start_loading
     */
    @Override
    public void startLoading(MandateType mandateType, Date mandateDate) throws SQLException, ParseException {
        LOGGER.info("Log that small files start loading");
        Assert.notNull(mandateType, "mandateType cannot be null");
        Assert.notNull(mandateDate, "mandateDate cannot be null");

        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_mandate_type", mandateType.getCode())
                .addValue("ip_mandate_date", new java.sql.Date(mandateDate.getTime()));
        logStartLoadingFilesSP.execute(inParams);
        LOGGER.info("Logging into the database is done.");
    }

    //@Transactional("tmsAppEntityManager")
    @Override
    public void loadMandateFile(MandateFile mandateFile) throws SQLException, ParseException {
        LOGGER.info("Starting to load small file {}", mandateFile);
        Assert.notNull(mandateFile, "mandateFile cannot be null");

        MandateFile.Mandate mandate = mandateFile.getMandate();
        Assert.notNull(mandate, "mandate cannot be null");

        Assert.hasText(mandate.getTraderEmail(), "traderEmail is empty");
        Assert.notNull(mandate.getMandateType(), "mandateType cannot be null");
        Assert.notNull(mandate.getMandateDate(), "mandateDate cannot be null");

        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_trader_email", mandate.getTraderEmail())
                .addValue("ip_mandate_date", new java.sql.Date(mandate.getMandateDate().getTime()))
                .addValue("ip_mandate_type", mandate.getMandateType())
                .addValue("ip_mandate_file", new SqlLobValue(mandateFile.getFileImage()))
                .addValue("ip_file_name", mandateFile.getFileName())
                .addValue("ip_file_date", new java.sql.Date(mandateFile.getFileDate().getTime()))
                .addValue("ip_file_size", mandateFile.getFileSize())
                .addValue("ip_file_type", mandateFile.getFileType());

        loadMandateFileSP.execute(inParams);
        LOGGER.info("Loading small file {} is finished", mandateFile);
    }

    /**
     * @param fileDate
     * @param mandateType
     * @throws SQLException
     * @throws ParseException
     */
    @Override
    public void loadMandates(Date fileDate, MandateType mandateType) throws SQLException, ParseException {
        LOGGER.info("Starting to load mandates with date {} and mandate type {}", fileDate, mandateType);
        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_mandate_date", new java.sql.Date(fileDate.getTime()))
                .addValue("ip_mandate_type", mandateType.getCode());

        loadMandatesSP.execute(inParams);
        LOGGER.info("Load mandates is done.");
    }

    /**
     * Logging error in database
     * It will call the pkg_load_mandates.p_save_error
     * stored procedure.
     *
     * @param mandateFile mandate file
     * @param errorMsg    error tha happened during processing the file
     */
    @Override
    public void saveError(MandateFile mandateFile, String errorMsg) throws SQLException, ParseException {
        LOGGER.info("Starting to save error for file {}", mandateFile);
        Assert.notNull(mandateFile, "mandateFile cannot be null");

        MandateFile.Mandate mandate = mandateFile.getMandate();
        Assert.notNull(mandate, "mandate cannot be null");
        Assert.hasText(mandate.getTraderEmail(), "traderEmail is empty");
        Assert.notNull(mandate.getMandateType(), "mandateType cannot be null");
        Assert.notNull(mandate.getMandateDate(), "mandateDate cannot be null");

        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_trader_email", mandate.getTraderEmail())
                .addValue("ip_mandate_date", new java.sql.Date(mandate.getMandateDate().getTime()))
                .addValue("ip_mandate_type", mandate.getMandateType())
                .addValue("ip_file_name", mandateFile.getFileName())
                .addValue("ip_file_date", new java.sql.Date(mandateFile.getFileDate().getTime()))
                .addValue("ip_file_size", mandateFile.getFileSize())
                .addValue("ip_file_type", mandateFile.getFileType());

        saveErrorSP.execute(inParams);
        LOGGER.info("Error for file {} was saved.", mandateFile);
    }

    @Override
    public void gatherStatistics() {
        LOGGER.info("Gathering statistics...");
        gatherStatisticsSP.execute();
        LOGGER.info("Gathering statistics is done.");
    }
}
