package com.db.nfr.tmr.integrator.quantstruts.quant.common;

import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.component.file.GenericFileOperationFailedException;
import org.apache.camel.component.file.remote.RemoteFileOperations;
import org.apache.camel.component.file.remote.SftpEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.jcraft.jsch.ChannelSftp;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by tymkvit on 20/02/2017.
 */
@Slf4j
@Component
public class CommonDeleteSmallFilesProcessor {
    /**
     * Processes the message exchange
     *
     * @throws Exception if an internal processing error has occurred.
     */
    @Autowired
    private CamelContext camelContext;

    public void process(String pathToFolder, String endPoint, TypeRecAllFiles masterFileRec) throws Exception {
        LOGGER.info("Deleting small files in progress...");
        SftpEndpoint sftpEndpoint = camelContext.getEndpoint(endPoint, SftpEndpoint.class);
        RemoteFileOperations<ChannelSftp.LsEntry> remoteFileOperations = sftpEndpoint.createRemoteFileOperations();
        boolean isConnected = remoteFileOperations.connect(sftpEndpoint.getConfiguration());
        if (!isConnected) {
            LOGGER.debug("Not able to connect to {}", sftpEndpoint.getConfiguration());
            return;
        }

        try {
            List<ChannelSftp.LsEntry> lsEntries = remoteFileOperations.listFiles(pathToFolder);
            StringBuilder pathToFile = new StringBuilder()
                    .append(pathToFolder)
                    .append("/");
            StringBuilder fileName = new StringBuilder();

            for (ChannelSftp.LsEntry lsEntry : lsEntries) {
                if (lsEntry.getAttrs().isDir()) {
                    continue;
                }
                fileName.setLength(0);
                fileName.append(pathToFile).append(lsEntry.getFilename());
                long fileModificationTime = lsEntry.getAttrs().getMTime() * 1000L;
                if (fileModificationTime > masterFileRec.getFileDate().getTime()) {
                    LOGGER.info("The file {} has not been deleted because of its modification date is more than modification date of master file {}", fileName, masterFileRec);
                } else {
                    remoteFileOperations.deleteFile(fileName.toString());
                    LOGGER.info("The file {} should be deleted by now.", fileName);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception happened during deleting small files.", e);
        } finally {
            try {
                remoteFileOperations.disconnect();
            } catch (GenericFileOperationFailedException foEx) {
                LOGGER.error("Failed to disconnect from sftp.", foEx);
            }
        }
        LOGGER.info("Deleting small files is done.");
    }
}
