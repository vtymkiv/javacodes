package com.db.nfr.tmr.integrator.persistence;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;

import com.db.nfr.tmr.integrator.quantstruts.common.MandateFile;
import com.db.nfr.tmr.integrator.quantstruts.common.MandateType;

/**
 * This class represents the data access object that would be
 * used for accessing to the database's pkg_mandate_load package functions
 * <p>
 * Created by tymkvit on 01/02/2017.
 */
public interface PkgLoadMandatesRepoDAO {

    /**
     * Start the loading process and log it.
     * It will call pkg_load_mandates.p_start_loading
     */
    void startLoading(MandateType mandateType, Date mandateDate) throws SQLException, ParseException;

    /**
     * Start to load a file
     * It will call the pkg_load_mandates.p_load_mandate_file
     * stored procedure.
     *
     * @param mandateFile mandate file
     */
    void loadMandateFile(MandateFile mandateFile) throws SQLException, ParseException;

    /**
     * @param fileDate
     * @param mandateType
     * @throws SQLException
     * @throws ParseException
     */
    void loadMandates(Date fileDate, MandateType mandateType) throws SQLException, ParseException;

    /**
     * Logging error in database
     * It will call the pkg_load_mandates.p_save_error
     * stored procedure.
     *
     * @param mandateFile mandate file
     * @param errorMsg    error tha happened during processing the file
     */
    void saveError(MandateFile mandateFile, String errorMsg) throws SQLException, ParseException;

    void gatherStatistics();

}
