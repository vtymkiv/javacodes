package com.db.nfr.tmr.integrator.quantstruts.common;

import org.springframework.util.Assert;

/**
 * Utility class responsible for extracting information
 * from the string in predefined format.
 * Created by tymkvit on 10/02/2017.
 */
public final class Utils {
    // Default private constructor
    private Utils() {
    }

    // TODO: Use regexp instead or split

    /**
     * Parses string that came in format: [MANDATE_TYPE]_[DATE]_[TRADER_EMAIL]_[TMR].[EXT] .
     * The behaviour of method would be not as expected if format would be different
     *
     * @param fileName XXXX_DATE_aaron.doyle_TMR.xlsx
     * @return
     */
    public static String getEmailFromString(String fileName) {
        Assert.hasText(fileName, "The file name can not be null or empty");

        StringBuffer email = new StringBuffer();
        int firstUnderscoreIdx = fileName.indexOf("_");
        int secondUnderscoreIdx = fileName.indexOf("_", firstUnderscoreIdx + 1);
        int thirdUnderscoreIdx = fileName.indexOf("_", secondUnderscoreIdx + 1);

        if (firstUnderscoreIdx < 0 || secondUnderscoreIdx < 0 || thirdUnderscoreIdx < 0) {
            throw new IllegalArgumentException(String.format("The %s does not correspond format [MANDATE_TYPE]_[DATE]_[TRADER_EMAIL]_[TMR].[EXT]", fileName));
        }
        return email.append(fileName.substring(secondUnderscoreIdx + 1, thirdUnderscoreIdx)).append("@db.com").toString();
    }

    public static String getExtFromFileName(String fileName) {
        StringBuffer fileExt = new StringBuffer();
        int firstUnderscoreIdx = fileName.lastIndexOf(".");

        if (firstUnderscoreIdx < 0) {
            fileExt.append("undefined");
        } else {
            fileExt.append(fileName.substring(firstUnderscoreIdx + 1, fileName.length()));
        }
        return fileExt.toString();
    }
}
