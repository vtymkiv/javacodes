package com.db.nfr.tmr.integrator.quantstruts.quant.debt;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.quantstruts.common.MandateType;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.CommonSmallFilesProcessor;

import lombok.extern.slf4j.Slf4j;

/**
 * Class is responsible for the processing of small(slave) files of master file.
 * These files are complements of major(master) file
 * <p>
 * Created by tymkvit on 09/02/2017.
 */
@Slf4j
@Component
public class DebtSmallFilesProcessor implements Processor {

    @Autowired
    private CommonSmallFilesProcessor commonSmallFilesProcessor;

    /**
     * Processes the message exchange
     *
     * @param exchange the message exchange
     * @throws Exception if an internal processing error has occurred.
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.debug("Starting to process small files for debt...");
        commonSmallFilesProcessor.process("{{ftp-debt-slave-files}}", exchange, MandateType.DEBT);
        LOGGER.debug("Processing small files for debt is done.");
    }
}
