package com.db.nfr.tmr.integrator.persistence.common;

import java.sql.SQLException;
import java.util.List;

import com.db.nfr.tmr.integrator.quantstruts.quant.common.Client;

//TODO: Requires refactoring

/**
 * @author gasiork
 */
public interface DAO {
    void cleanUp(String sql) throws SQLException;

    void persist(final List<List<Object>> data, String bulkType, String type, String sql);

    void markFinish(String validateSql, String updateSql) throws SQLException;

    boolean logCall(Client client, int lineNumber) throws SQLException;

    void logCall(Client client, Exception ex) throws SQLException;

    void markStartCall(Client client);

    void gatherStatistics(String tableName);
}
