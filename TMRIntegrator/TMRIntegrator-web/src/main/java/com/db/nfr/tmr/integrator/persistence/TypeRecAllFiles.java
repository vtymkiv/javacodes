package com.db.nfr.tmr.integrator.persistence;

import java.util.Date;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author gasiork
 */
@ToString
@Getter
public class TypeRecAllFiles {
    private final int feedConfigId;
    private final String fileName;
    @Getter(AccessLevel.NONE)
    private final Date fileDate;
    private final long fileSize;
    @Setter
    private int id;

    public TypeRecAllFiles(final int feedConfigId, final String fileName, final Date fileDate, final long fileSize) {
        this.feedConfigId = feedConfigId;
        this.fileName = fileName;
        this.fileDate = new Date(fileDate.getTime());
        this.fileSize = fileSize;
    }

    public Date getFileDate() {
        return new Date(fileDate.getTime());
    }
}