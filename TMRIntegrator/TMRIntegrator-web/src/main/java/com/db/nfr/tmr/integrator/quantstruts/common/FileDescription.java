package com.db.nfr.tmr.integrator.quantstruts.common;

import java.util.Date;

import lombok.Getter;
import lombok.ToString;

/**
 * Represents information about existing file like:
 * <code>
 * <li>name</li>
 * <li>modification date</li>
 * <li>size</li>
 * </code>
 * Created by tymkvit on 09/02/2017.
 */
@ToString
public class FileDescription {
    @Getter
    private String name;
    @Getter
    private long size;
    private Date date;


    public FileDescription(String filename, long time, long size) {
        this(filename, new Date(time), size);
    }

    private FileDescription(String name, Date date, long size) {
        this.name = name;
        this.date = date;
        this.size = size;
    }

    public Date getDate() {
        return new Date(date.getTime());
    }
}
