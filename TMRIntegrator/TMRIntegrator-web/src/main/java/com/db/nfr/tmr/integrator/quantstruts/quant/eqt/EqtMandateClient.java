package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.persistence.common.DAO;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.AbstractClient;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.OracleMapping;
import com.monitorjbl.xlsx.StreamingReader;

/**
 * @author gasiork
 */
public class EqtMandateClient extends AbstractClient {

    private final OracleMapping mapping = new EqtMandateMapping();

    public EqtMandateClient(DAO dao) {
        super(dao);
    }

    @Override
    public List transformData(List<String> bulkData) {
        return bulkData;
    }

    @Override
    public OracleMapping getOracleMapping() {
        return mapping;
    }

    @Override
    public String getName() {
        return "trader2risk";
    }

    @Override
    public int getSheetNumber() {
        return 0;
    }

    @Override
    public void execute(TypeRecAllFiles rec, InputStream in) throws IOException {
        try (Workbook workbook = StreamingReader.builder().open(in)) {
            execute(rec, workbook);
        }
    }

    @Override
    public int getColumnCount() {
        return 3;
    }
}
