package com.db.nfr.tmr.integrator.quantstruts.quant.common;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.Exchange;
import org.apache.camel.Service;
import org.apache.camel.component.file.GenericFile;
import org.apache.camel.component.file.remote.SftpEndpoint;
import org.apache.camel.spi.IdempotentRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.PkgLoadMandatesRepoDAO;
import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.quantstruts.common.FileDescription;
import com.db.nfr.tmr.integrator.quantstruts.common.Header;
import com.db.nfr.tmr.integrator.quantstruts.common.MandateFile;
import com.db.nfr.tmr.integrator.quantstruts.common.MandateType;
import com.db.nfr.tmr.integrator.quantstruts.common.Utils;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import lombok.extern.slf4j.Slf4j;


import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.FAILED_TO_PROCESS_FILES;
import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.FILES_FOR_PROCESSING;
import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.NOT_PROCESSED_FILES;
import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.PROCESSED_FILES;

/**
 * Responsible for processing quant struts files(Debt and Equities)
 * <p>
 * Created by tymkvit on 14/02/2017.
 */
@Slf4j
@Component
public final class CommonSmallFilesProcessor {
    @Autowired
    private PkgLoadMandatesRepoDAO pkgLoadMandatesRepoDAO;

    @Autowired
    private CamelContext camelContext;

    @Value("${polling.consumer.timeout:300000}")
    private long pollingConsumerTimeout;

    public void process(String endPoint, Exchange exchange, MandateType mandateType) throws Exception {
        // Get the list of files from SFTP
        LOGGER.info("Starting to process small files...");
        validateInputParameters(endPoint, exchange);

        TypeRecAllFiles masterFileRec = exchange.getIn().getHeader(Header.REC_ALL_FILES, TypeRecAllFiles.class);
        LOGGER.info("Master file: {}", masterFileRec);

        Map<String, List<FileDescription>> fileListMap = getFileStatusMap();
        SftpEndpoint sftpEndpoint = camelContext.getEndpoint(endPoint, SftpEndpoint.class);
        cleanRepositories(sftpEndpoint);
        ConsumerTemplate consumer = camelContext.createConsumerTemplate();
        consumer.start();
        Exchange ex;
        try {
            while ((ex = consumer.receive(sftpEndpoint, pollingConsumerTimeout)) != null) {
                GenericFile file = ex.getIn().getBody(GenericFile.class);
                FileDescription inputFile = new FileDescription(file.getFileName(), file.getLastModified(), file.getFileLength());
                FileStatus fileStatus = processFile(mandateType, masterFileRec, ex, inputFile);
                populateFileStatusMap(fileListMap, inputFile, fileStatus);
                // This is should be called to mark the file as processed
                consumer.doneUoW(ex);
            }
            LOGGER.info("Processing small files has been finished. Files that have not been processed: {}, Files that have been failed to process: {}",
                    fileListMap.get(NOT_PROCESSED_FILES), fileListMap.get(FAILED_TO_PROCESS_FILES));
        } catch (Exception e) {
            LOGGER.error("Exception happened during processing small files.", e);
        } finally {
            // We should stop consumer if we started it.
            consumer.cleanUp();
            stopConsumer(consumer);
            //sftpEndpoint.shutdown();
        }

        if (fileListMap.get(PROCESSED_FILES).isEmpty()) {
            LOGGER.debug("List of processed files was empty. The route with id {} would be stopped.", exchange.getFromRouteId());
            stopRoute(exchange);
        }
    }

    private void validateInputParameters(String endPoint, Exchange exchange) {
        Objects.requireNonNull(exchange, "exchange can not be null.");
        if (StringUtils.isBlank(endPoint)) {
            throw new IllegalArgumentException("The endPoint cannot be blank.");
        }
    }

    private void cleanRepositories(SftpEndpoint sftpEndpoint) {
        IdempotentRepository<String> inProgressRepository = sftpEndpoint.getInProgressRepository();
        IdempotentRepository<String> idempotentRepository = sftpEndpoint.getIdempotentRepository();
        if (inProgressRepository != null) {
            inProgressRepository.clear();
        }
        if (idempotentRepository != null) {
            idempotentRepository.clear();
        }
    }

    private void populateFileStatusMap(Map<String, List<FileDescription>> fileListMap, FileDescription inputFile, FileStatus fileStatus) {
        fileListMap.get(FILES_FOR_PROCESSING).add(inputFile);
        switch (fileStatus) {
            case NOT_PROCESSED:
                fileListMap.get(NOT_PROCESSED_FILES).add(inputFile);
                break;
            case PROCESSED:
                fileListMap.get(PROCESSED_FILES).add(inputFile);
                break;
            case FAILED_TO_PROCESS:
                fileListMap.get(FAILED_TO_PROCESS_FILES).add(inputFile);
                break;
        }
    }

    private Map<String, List<FileDescription>> getFileStatusMap() {
        HashMap<String, List<FileDescription>> fileListMap = new HashMap<>();
        fileListMap.put(FILES_FOR_PROCESSING, new ArrayList<>());
        fileListMap.put(NOT_PROCESSED_FILES, new ArrayList<>());
        fileListMap.put(FAILED_TO_PROCESS_FILES, new ArrayList<>());
        fileListMap.put(PROCESSED_FILES, new ArrayList<>());
        return fileListMap;
    }

    private void stopConsumer(Service consumer) {
        try {
            LOGGER.debug("Stopping consumer...");
            consumer.stop();
            LOGGER.debug("Consumer has been stopped.");
        } catch (Exception ex) {
            LOGGER.error("Exception happened during stopping consumer.", ex);
        }
    }

    private void stopRoute(Exchange exchange) {
        // mark the exchange to stop continue routing
        exchange.setProperty(Exchange.ROUTE_STOP, Boolean.TRUE);
    }

    private FileStatus processFile(MandateType mandateType, TypeRecAllFiles masterFileRec, Exchange ex, FileDescription inputFile)
            throws IOException, SftpException, JSchException, ParseException {
        LOGGER.debug("Processing file {} ....", inputFile);
        if (inputFile.getDate().getTime() > masterFileRec.getFileDate().getTime()) {
            LOGGER.debug("The file: {} will not be processed because it's last modification time is more than master " +
                    "file {}.", inputFile, masterFileRec);
            return FileStatus.NOT_PROCESSED;
        }

        // Process all slave files which modification time is less or equals to modification time of master file
        MandateFile mandateFile = getMandateFile(mandateType, masterFileRec, inputFile);
        try (InputStream is = ex.getIn().getBody(InputStream.class)) {
            mandateFile.setFileImage(IOUtils.toByteArray(is));
        }

        try {
            pkgLoadMandatesRepoDAO.loadMandateFile(mandateFile);
            LOGGER.debug("The file: {} has been processed.", inputFile);
            return FileStatus.PROCESSED;
        } catch (SQLException e) {
            LOGGER.error("Was not able to process file {} due to error: {}.", inputFile, e);
            // Log error into the database
            try {
                pkgLoadMandatesRepoDAO.saveError(mandateFile, e.getMessage());
            } catch (SQLException sqlEx) {
                LOGGER.error("Exception happened when trying to save error in database.", sqlEx);
            }
            return FileStatus.FAILED_TO_PROCESS;
        }
    }

    private MandateFile getMandateFile(MandateType mandateType, TypeRecAllFiles masterFile, FileDescription
            slaveFile) throws IOException, SftpException, JSchException {
        MandateFile mandateFile = new MandateFile();
        MandateFile.Mandate mandate = new MandateFile.Mandate();
        mandate.setMandateType(mandateType.getCode());
        mandate.setMandateDate(masterFile.getFileDate());
        mandate.setTraderEmail(Utils.getEmailFromString(slaveFile.getName()));
        mandateFile.setFileName(slaveFile.getName());
        mandateFile.setFileType(Utils.getExtFromFileName(slaveFile.getName()));
        mandateFile.setFileSize(slaveFile.getSize());
        mandateFile.setFileDate(slaveFile.getDate());
        mandateFile.setMandate(mandate);
        return mandateFile;
    }
}
