package com.db.nfr.tmr.integrator.quantstruts.common;

/**
 * Created by danyvik on 28/12/2016.
 */
public final class Constant {
    private Constant() {
    }

    public static final int ROWCACHESIZE = 10000;
    public static final int BUFFERSIZE = 16384;

    public static final String FEED_APP_NAME = "TMR";
    // Equity constants
    public static final String TRADER_2_RISK = "trader2risk.xlsx";
    public static final String RISK_2_PM = "risk2pm.csv";
    public static final int TRADER_TO_RISK_ID = 62;
    public static final int RISK_TO_PM_ID = 61;

    public static final String EQT_APP_SUB_NAME = "EQT_MANDATES";

    // Debt
    public static final String DEBT_FILE_NAME = "DEBT_QS_Data.xlsx";

    public static final String DEBT_APP_SUB_NAME = "DEBT";
    public static final int DEBT_FILE_ID = 58;

    public static final String FILES_FOR_PROCESSING = "filesForProcessing";
    public static final String NOT_PROCESSED_FILES = "notProcessedFiles";
    public static final String PROCESSED_FILES = "processedFiles";
    public static final String FAILED_TO_PROCESS_FILES = "failedToProcessFiles";

}
