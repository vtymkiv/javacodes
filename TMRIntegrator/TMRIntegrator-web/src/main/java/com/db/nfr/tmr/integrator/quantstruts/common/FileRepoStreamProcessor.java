package com.db.nfr.tmr.integrator.quantstruts.common;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.PkgFileRepoDAO;
import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.quantstruts.quant.eqt.EqtLoadProcessor;

/**
 * @author gasiork
 * @author Vitaliy Tymkiv
 */
@Component
public class FileRepoStreamProcessor implements Processor {
    public static TypeRecAllFiles traderRec;

    @Autowired
    private PkgFileRepoDAO pkgFileRepoDAO;

    @Autowired
    private EqtLoadProcessor eqtLoadProcessor;

    @Override
    public void process(Exchange exchange) throws Exception {

        TypeRecAllFiles rec = exchange.getIn().getHeader(Header.REC_ALL_FILES, TypeRecAllFiles.class);

        List<FileRecord> fileRecordList = pkgFileRepoDAO.checkFiles(Arrays.asList(rec), FileRecord.class);

        if (!fileRecordList.isEmpty()) {
            FileRecord masterFileRec = fileRecordList.get(0);
            rec.setId(masterFileRec.getId());
            InputStream is = exchange.getIn().getBody(InputStream.class);
            pkgFileRepoDAO.loadFile(masterFileRec, is);
            is.close();

            exchange.getOut().setHeader(Header.REC_ALL_FILES, rec);
            if (rec.getFeedConfigId() == Constant.TRADER_TO_RISK_ID) {
                traderRec = rec;
            }
            exchange.getOut().setBody(exchange.getIn().getBody());
        } else {
            if (rec.getFeedConfigId() == Constant.RISK_TO_PM_ID) {// if RISK_2_PM
                eqtLoadProcessor.process(exchange, traderRec);//process TRADER_2_RISK
            }
            exchange.setProperty(Exchange.ROUTE_STOP, Boolean.TRUE);
        }
    }

}
