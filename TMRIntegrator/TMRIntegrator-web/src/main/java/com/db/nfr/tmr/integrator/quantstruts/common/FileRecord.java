package com.db.nfr.tmr.integrator.quantstruts.common;

import java.util.Arrays;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents the record about file that stored procedure like stg_package.check_file would
 * return in op_cursor
 * <p>
 * Created by tymkvit on 03/02/2017.
 */

@Setter
@Getter
@ToString(exclude = "fileImage")
public class FileRecord {
    private int id;
    private String fileName;
    private String imageType;
    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private byte[] fileImage;

    public byte[] getFileImage() {
        return (fileImage == null ? null : Arrays.copyOf(fileImage, fileImage.length));
    }

    public void setFileImage(byte[] fileImage) {
        this.fileImage = (fileImage == null ? null : Arrays.copyOf(fileImage, fileImage.length));
    }
}
