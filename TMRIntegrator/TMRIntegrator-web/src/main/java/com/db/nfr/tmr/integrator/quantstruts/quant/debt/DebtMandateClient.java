package com.db.nfr.tmr.integrator.quantstruts.quant.debt;

import java.util.List;

import com.db.nfr.tmr.integrator.persistence.common.DAO;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.AbstractClient;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.OracleMapping;

/**
 * @author gasiork
 */
public class DebtMandateClient extends AbstractClient {

    private final OracleMapping mapping = new DebtMandateMapping();

    public DebtMandateClient(DAO dao) {
        super(dao);
    }

    @Override
    public List transformData(List<String> bulkData) {
        if (bulkData.size() > 2) {
            bulkData.remove(0);
            bulkData.remove(0);
            return bulkData;
        } else {
            return null;
        }
    }

    @Override
    public OracleMapping getOracleMapping() {
        return mapping;
    }

    @Override
    public String getName() {
        return "debtmandates";
    }

    @Override
    public int getSheetNumber() {
        return 0;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

}
