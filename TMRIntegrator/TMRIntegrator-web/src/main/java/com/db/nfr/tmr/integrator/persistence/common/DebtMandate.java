package com.db.nfr.tmr.integrator.persistence.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * This class represents debt mandate
 * Created by tymkvit on 07/02/2017.
 */
@Getter
@Setter
@ToString
public class DebtMandate {
    private String trader;
    private String bookFamily;
    private String productFamily;
}
