package com.db.nfr.tmr.integrator.quantstruts.quant.debt;

import com.db.nfr.tmr.integrator.quantstruts.quant.common.OracleMapping;

/**
 * @author gasiork
 */
public class DebtMandateMapping extends OracleMapping {
    public DebtMandateMapping() {
        this.setCleanUpSql("{call stg_owner.PKG_STG_MANDATES.p_clean_stg_mandates_debt()}");
        this.setBulkType("tt_stg_debt_mandates".toUpperCase());
        this.setType("to_stg_debt_mandates".toUpperCase());
        this.setSql("{call stg_owner.PKG_STG_MANDATES.p_fill_stg_mandates_debt(?)}");
        this.setTableName("STG_DEBT_MANDATES");
    }
}
