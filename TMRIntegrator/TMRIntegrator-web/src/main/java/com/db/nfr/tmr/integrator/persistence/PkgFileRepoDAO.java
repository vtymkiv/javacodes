package com.db.nfr.tmr.integrator.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.db.nfr.tmr.integrator.quantstruts.common.FileInfo;
import com.db.nfr.tmr.integrator.quantstruts.common.FileRecord;

/**
 * This class represents the data access object that would be
 * used for accessing to the database's pkg_file_repo package functions
 * Created by tymkvit on 01/02/2017.
 */
public interface PkgFileRepoDAO {
    /**
     * Get list of new files. Record with status NEW will be created.
     * It will call pkg_file_repo.check_files
     *
     * @return List of new files
     */
    <T> List<T> checkFiles(List<TypeRecAllFiles> fileList, Class<T> returnType) throws SQLException;

    /**
     * Get information about file by feedConfigId
     *
     * @param feedConfigId
     * @param <T>
     * @return
     * @throws SQLException
     */

    /**
     * Get the file name mask by feed id.
     * It will call pkg_file_repo.get_file_info
     *
     * @param feedConfigId
     * @return file name
     */
    FileInfo getFileInfo(int feedConfigId) throws SQLException;

    /**
     * Master file will be loaded(saved) into database.
     *
     * @param fileRecord
     * @throws SQLException
     */
    void loadFile(FileRecord fileRecord) throws SQLException;

    void setStatus(FileRecord fileRecord, String status) throws SQLException;

    void setStatus(int fileId, String status) throws SQLException;

    void loadFile(FileRecord fileRecord, Workbook workbook) throws SQLException, IOException;

    void loadFile(FileRecord fileRecord, InputStream is) throws SQLException, IOException;
}
