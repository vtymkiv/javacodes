package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author gasiork
 */
@Component
public class EqtAggregationStrategy implements AggregationStrategy {
    private static Logger LOGGER = LoggerFactory.getLogger(EqtAggregationStrategy.class);

    @Override
    public Exchange aggregate(Exchange trader2risk, Exchange risk2pm) {
        try {
            //here we come only if trader2risk is processed successfully
            return risk2pm;
        } catch (Exception ex) {
            LOGGER.error("Aggregating Eqt products and mandates has failed.", ex);
            throw new RuntimeException(ex);
        }
    }

}
