package com.db.nfr.tmr.integrator.quantstruts.quant.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.persistence.common.DAO;
import com.db.nfr.tmr.integrator.quantstruts.quant.filereaders.LocalFileReader;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gasiork
 */
@Slf4j
public abstract class AbstractClient extends LocalFileReader implements Client {
    protected List<List<String>> bulkData = new ArrayList<>();
    private long fileSize;
    private String fileDateText;

    public AbstractClient(DAO dao) {
        super(dao);
    }

    public void execute(TypeRecAllFiles rec, InputStream in) throws IOException {
        this.execute(rec, in, -1);
    }

    public void execute(TypeRecAllFiles rec, InputStream in, int columnCount) {
        try {
            initFileMetaData(rec);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, Charset.defaultCharset()));
            cleanUp(this);
            processFile(reader, this, columnCount);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            logExceptionToDb(ex);
        }
    }

    public void execute(TypeRecAllFiles rec, Workbook workbook) {
        try {
            initFileMetaData(rec);
            cleanUp(this);
            processFile(workbook, this);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            logExceptionToDb(ex);
        }
    }

    private void logExceptionToDb(Exception ex) {
        try {
            DAO dao = getDao();
            dao.logCall(this, ex);
            throw new RuntimeException(ex);
        } catch (Exception ex1) {
            LOGGER.error(ex1.getMessage(), ex1);
            throw new RuntimeException(ex1);
        }
    }

    protected void processFile(Workbook workbook, Client client) throws Exception {
        Sheet sheet = workbook.getSheetAt(getSheetNumber());
        Iterator<Row> rows = sheet.iterator();
        boolean isFirst = true;
        while (rows.hasNext()) {
            Row row = rows.next();
            if (isFirst) {
                isFirst = false;
                continue;
            }
            processXlsLine(row, client);
        }
        processLine(null, client);
        gatherStatistics(client);
        finishOfParsing(client);
    }

    public boolean makeLogCall(InputStream in) throws Exception {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, Charset.defaultCharset()));
            CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT);
            int lineNumber = 0;

            // TODO: This is wrong approach to get the number of records in csv file. Should be refactored
            for (CSVRecord line : parser) {
                lineNumber++;
            }
            return makeLogCall(this, lineNumber - 1);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return makeLogCall(this, -1);
        }
    }

    public boolean makeLogCall(Workbook workbook, int sheetNumber) throws Exception {
        try {
            Sheet sheet = workbook.getSheetAt(sheetNumber);
            Iterator<Row> rows = sheet.iterator();
            int lineNumber = 0;
            while (rows.hasNext()) {
                rows.next();
                lineNumber++;
            }
            return makeLogCall(this, lineNumber - 1);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return makeLogCall(this, -1);
        }
    }

    public abstract int getSheetNumber();

    protected void initFileMetaData(TypeRecAllFiles rec) throws SQLException {
        try {
            fileDateText = rec.getFileDate().toString();
            fileSize = rec.getFileSize();
        } catch (Exception ex) {
            makeExceptionLogCall(this);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public long getFileSize() {
        return fileSize;
    }

    @Override
    public String getFileDateText() {
        return fileDateText;
    }

}
