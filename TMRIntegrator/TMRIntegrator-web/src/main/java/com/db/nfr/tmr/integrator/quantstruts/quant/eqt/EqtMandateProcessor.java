package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import java.io.InputStream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.PkgFileRepoDAO;
import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.persistence.common.DAO;
import com.db.nfr.tmr.integrator.quantstruts.common.Header;

import lombok.extern.slf4j.Slf4j;


/**
 * @author gasiork
 */
@Slf4j
@Component
public class EqtMandateProcessor implements Processor {
    @Autowired
    private DAO dao;

    @Autowired
    private PkgFileRepoDAO pkgFileRepoDAO;

    @Override
    public void process(Exchange exchng) throws Exception {
        LOGGER.info("Processing Eqt Mandates started");
        TypeRecAllFiles rec = exchng.getIn().getHeader(Header.REC_ALL_FILES, TypeRecAllFiles.class);
        InputStream in = exchng.getIn().getBody(InputStream.class);
        EqtMandateClient client = new EqtMandateClient(dao);
        client.execute(rec, in);
        exchng.getOut().setHeader(Header.REC_ALL_FILES, rec);
        exchng.getOut().setBody(exchng.getIn().getBody());
        LOGGER.info("Processing Eqt Mandates finished");
    }

}
