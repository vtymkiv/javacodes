package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.PkgFileRepoDAO;
import com.db.nfr.tmr.integrator.persistence.PkgLoadMandatesRepoDAO;
import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.quantstruts.common.Constant;
import com.db.nfr.tmr.integrator.quantstruts.common.FileRepoStreamProcessor;
import com.db.nfr.tmr.integrator.quantstruts.common.Header;
import com.db.nfr.tmr.integrator.quantstruts.common.MandateType;
import com.db.nfr.tmr.integrator.quantstruts.common.Status;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gasiork
 * @author Vitaliy Tymkiv
 */
@Slf4j
@Component
public class EqtLoadProcessor implements Processor {
    @Autowired
    private PkgFileRepoDAO pkgFileRepoDAO;

    @Autowired
    private PkgLoadMandatesRepoDAO pkgLoadMandatesRepoDAO;

    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.debug("EqtLoadProcessor processing trader rec [{}] ", FileRepoStreamProcessor.traderRec);
        process(exchange, FileRepoStreamProcessor.traderRec);
    }

    public void process(Exchange exchange, TypeRecAllFiles rec) throws Exception {
        LOGGER.debug("Preparing Eqt Load procedure for rec [{}]", rec);
        pkgLoadMandatesRepoDAO.loadMandates(rec.getFileDate(), MandateType.EQUITY);
        pkgLoadMandatesRepoDAO.gatherStatistics();

        // Loading finished successfully. We mark it only for trader to risk data.
        if (Constant.TRADER_TO_RISK_ID == rec.getFeedConfigId()) {
            LOGGER.info("Setting status to completed...");
            pkgFileRepoDAO.setStatus(rec.getId(), "Completed");
            LOGGER.info("Setting status to completed is done.");
            exchange.getProperties().put(Header.EQT_PROCESSING_STATUS, Status.EQT_LOAD_COMPLETED.getStatusCode());
            // Copying properties
            exchange.getOut().copyFrom(exchange.getIn());
        }
        LOGGER.info("EQT load finished");
    }
}
