package com.db.nfr.tmr.integrator.quantstruts.quant.filereaders;

import java.io.BufferedReader;
import java.sql.SQLException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.db.nfr.tmr.integrator.quantstruts.quant.common.Client;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Vitaliy Tymkiv
 */
@Slf4j
public abstract class FileReader {

    public abstract void cleanUp(Client client) throws SQLException;

    protected void processFile(BufferedReader reader, Client client, int columnCount) throws Exception {
        CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT);
        boolean isFirst = true;
        for (CSVRecord line : parser) {
            if (isFirst) {
                isFirst = false;
                continue;
            }
            processLine(line, client, columnCount);
        }
        processLine(null, client, columnCount);
        gatherStatistics(client);
        finishOfParsing(client);
    }

    public abstract void makeStartCall(Client client);

    public abstract boolean makeLogCall(Client client, int lineNumber) throws SQLException;

    public abstract void processLine(CSVRecord line, Client client);

    public abstract void processLine(CSVRecord line, Client client, int columnCount);

    public abstract void finishOfParsing(Client client) throws SQLException;

    protected abstract void gatherStatistics(Client client);

    public int getColumnCount() {
        return -1;
    }
}
