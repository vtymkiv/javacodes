package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import com.db.nfr.tmr.integrator.quantstruts.quant.common.OracleMapping;

/**
 * @author gasiork
 */
public class EqtMandateMapping extends OracleMapping {
    public EqtMandateMapping() {
        this.setCleanUpSql("{call stg_owner.PKG_STG_MANDATES.p_clean_stg_mandates_eqt()}");
        this.setBulkType("tt_stg_eqt_mandates".toUpperCase());
        this.setType("to_stg_eqt_mandates".toUpperCase());
        this.setSql("{call stg_owner.PKG_STG_MANDATES.p_fill_stg_mandates_eqt(?)}");
        this.setTableName("STG_EQT_MANDATES");
    }
}
