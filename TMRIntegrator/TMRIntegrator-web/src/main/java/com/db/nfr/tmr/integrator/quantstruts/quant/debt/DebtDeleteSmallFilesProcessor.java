package com.db.nfr.tmr.integrator.quantstruts.quant.debt;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.quantstruts.common.Header;
import com.db.nfr.tmr.integrator.quantstruts.common.Status;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.CommonDeleteSmallFilesProcessor;

/**
 * Created by tymkvit on 20/02/2017.
 */
@Component
public class DebtDeleteSmallFilesProcessor implements Processor {
    @Value("${common-folder}/${debt-folder}")
    private String debtFolder;
    @Autowired
    private CommonDeleteSmallFilesProcessor commonDeleteSmallFilesProcessor;

    /**
     * Processes the message exchange
     *
     * @param exchange the message exchange
     * @throws Exception if an internal processing error has occurred.
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        String status = exchange.getProperty(Header.DEBT_PROCESSING_STATUS, String.class);
        TypeRecAllFiles masterFileRec = exchange.getProperty(Header.DEBT_MASTER_FILE_PROPERTY, TypeRecAllFiles.class);
        if (Status.DEBT_LOAD_COMPLETED.getStatusCode().equalsIgnoreCase(status)) {
            commonDeleteSmallFilesProcessor.process(debtFolder, "{{ftp-debt-slave-files-delete}}", masterFileRec);
        }
    }
}
