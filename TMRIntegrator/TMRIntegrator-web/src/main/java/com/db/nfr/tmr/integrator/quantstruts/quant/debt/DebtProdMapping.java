package com.db.nfr.tmr.integrator.quantstruts.quant.debt;

import com.db.nfr.tmr.integrator.quantstruts.quant.common.OracleMapping;

/**
 * @author gasiork
 */
public class DebtProdMapping extends OracleMapping {
    public DebtProdMapping() {
        this.setCleanUpSql("{call stg_owner.PKG_STG_MANDATES.p_clean_stg_mandates_prod_debt()}");
        this.setBulkType("tt_stg_debt_mandates_prod".toUpperCase());
        this.setType("to_stg_debt_mandates_prod".toUpperCase());
        this.setSql("{call stg_owner.PKG_STG_MANDATES.p_fill_stg_mandates_prod_debt(?)}");
        this.setTableName("STG_DEBT_MANDATES_PROD_DEF");
    }
}
