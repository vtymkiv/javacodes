package com.db.nfr.tmr.integrator.quantstruts.common;

import java.util.Arrays;
import java.util.Date;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents the file that will be loaded via pkg_load_mandates.p_load_mandate_file
 * stored procedure
 * <p>
 * Created by tymkvit on 03/02/2017.
 */
@Getter
@Setter
@ToString(exclude = "fileImage")
public class MandateFile {
    private String fileName;
    private String fileType;
    private long fileSize;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private byte[] fileImage;
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private Date fileDate;

    private Mandate mandate;

    @Getter
    @Setter
    @ToString
    public static class Mandate {
        private String traderEmail;
        private String mandateType;

        @Getter(AccessLevel.NONE)
        @Setter(AccessLevel.NONE)
        private Date mandateDate;

        public Date getMandateDate() {
            return new Date(mandateDate.getTime());
        }

        public void setMandateDate(Date mandateDate) {
            this.mandateDate = new Date(mandateDate.getTime());
        }

    }

    public void setFileDate(Date fileDate) {
        this.fileDate = new Date(fileDate.getTime());
    }

    public Date getFileDate() {
        return new Date(fileDate.getTime());
    }

    public byte[] getFileImage() {
        return (fileImage == null ? null : Arrays.copyOf(fileImage, fileImage.length));
    }

    public void setFileImage(byte[] fileImage) {
        this.fileImage = (fileImage == null ? null : Arrays.copyOf(fileImage, fileImage.length));
    }
}
