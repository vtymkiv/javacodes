package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import com.db.nfr.tmr.integrator.quantstruts.quant.common.OracleMapping;

/**
 * @author gasiork
 */
public class EqtProdMapping extends OracleMapping {
    public EqtProdMapping() {
        this.setCleanUpSql("{call stg_owner.PKG_STG_MANDATES.p_clean_stg_mandates_prod_eqt()}");
        this.setBulkType("tt_stg_eqt_mandates_prod".toUpperCase());
        this.setType("to_stg_eqt_mandates_prod".toUpperCase());
        this.setSql("{call stg_owner.PKG_STG_MANDATES.p_fill_stg_mandates_prod_eqt(?)}");
        this.setTableName("STG_EQT_PRODFAMILY_DEF");
    }
}
