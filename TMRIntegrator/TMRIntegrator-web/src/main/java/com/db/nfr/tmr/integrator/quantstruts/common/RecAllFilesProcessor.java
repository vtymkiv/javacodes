package com.db.nfr.tmr.integrator.quantstruts.common;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.file.GenericFile;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gasiork
 */
@Slf4j
@Component
public class RecAllFilesProcessor implements Processor {

    @Override
    public void process(Exchange exchng) throws Exception {
        LOGGER.debug("Processing RecAllFiles...");
        // Getting file read from endpoint in our case sftp
        GenericFile gf = exchng.getIn().getBody(GenericFile.class);
        int feedConfigId = exchng.getIn().getHeader(Header.FEED_CONFIG_ID, Integer.class);
        TypeRecAllFiles rec = new TypeRecAllFiles(feedConfigId, gf.getFileName(), new Date(gf.getLastModified()), gf.getFileLength());
        exchng.getOut().setHeader(Header.REC_ALL_FILES, rec);
        exchng.getOut().setBody(exchng.getIn().getBody());
        LOGGER.debug("Processing RecAllFiles is done. TypeRecAllFiles = {}", rec);
    }
}
