package com.db.nfr.tmr.integrator.persistence;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.stereotype.Repository;

import com.db.nfr.tmr.integrator.persistence.common.CustomOracleTypes;
import com.db.nfr.tmr.integrator.quantstruts.common.FileInfo;
import com.db.nfr.tmr.integrator.quantstruts.common.FileRecord;

import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleTypes;

/**
 * Created by tymkvit on 02/02/2017.
 */
@Slf4j
@Repository("pkgFileRepoDAOImpl")
public class PkgFileRepoDAOImpl implements PkgFileRepoDAO {

    @Autowired
    private CustomOracleTypes customOracleTypes;

    private SimpleJdbcCall checkFilesSP;
    private SimpleJdbcCall getFileInfoSP;
    private SimpleJdbcCall loadFileSP;
    private SimpleJdbcCall logFileStatusSP;

    public PkgFileRepoDAOImpl(@Qualifier("stgJdbcTemplate") JdbcTemplate jdbcTemplate) {
        checkFilesSP = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName("stg_owner")
                .withCatalogName("pkg_file_repo")
                .withProcedureName("check_files")
                .useInParameterNames()
                .declareParameters(
                        new SqlParameter("ip_files", OracleTypes.ARRAY),
                        new SqlOutParameter("op_cursor", OracleTypes.CURSOR)
                );

        getFileInfoSP = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName("stg_owner")
                .withCatalogName("pkg_file_repo")
                .withProcedureName("get_file_info")
                .useInParameterNames()
                .declareParameters(
                        new SqlParameter("ip_feed_config_id", OracleTypes.NUMBER),
                        new SqlOutParameter("op_file_mask", OracleTypes.VARCHAR),
                        new SqlOutParameter("op_image_type", OracleTypes.VARCHAR),
                        new SqlOutParameter("op_retention", OracleTypes.VARCHAR)

                );

        loadFileSP = new SimpleJdbcCall(jdbcTemplate).withSchemaName("stg_owner")
                .withCatalogName("pkg_file_repo").withProcedureName("load_file")
                .withoutProcedureColumnMetaDataAccess()
                .declareParameters(
                        new SqlParameter("ip_file_id", Types.INTEGER),
                        new SqlParameter("ip_file_image", Types.BLOB)
                );

        logFileStatusSP = new SimpleJdbcCall(jdbcTemplate).withSchemaName("stg_owner")
                .withCatalogName("pkg_file_repo").withProcedureName("log_file_status")
                .useInParameterNames()
                .withoutProcedureColumnMetaDataAccess()
                .declareParameters(
                        new SqlParameter("ip_file_id", OracleTypes.NUMBER),
                        new SqlParameter("ip_file_status", OracleTypes.VARCHAR)
                );
    }


    /**
     * Get list of new files. Record with status NEW will be created.
     * It will call pkg_file_repo.check_files
     *
     * @param fileList   List of all files
     * @param returnType The type that will be populate with values from database and return as list of returned type
     * @return List of new files(returned types)
     */
    @Override
    public <T> List<T> checkFiles(List<TypeRecAllFiles> fileList, Class<T> returnType) throws SQLException {
        LOGGER.debug("Calling check files with file list {} ", fileList);
        checkFilesSP.returningResultSet("op_cursor", BeanPropertyRowMapper.newInstance(returnType));
        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_files", customOracleTypes.getRecAllFilesStructArray(checkFilesSP.getJdbcTemplate().getDataSource(), fileList));
        Map<String, Object> result = checkFilesSP.execute(inParams);
        return (List<T>) result.get("op_cursor");
    }

    /**
     * Get information about file by feedConfigId
     *
     * @param feedConfigId
     * @return
     * @throws SQLException
     */
    @Override
    public FileInfo getFileInfo(int feedConfigId) throws SQLException {
        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_feed_config_id", feedConfigId);

        Map<String, Object> result = getFileInfoSP.execute(inParams);
        FileInfo fileInfo = new FileInfo();
        fileInfo.setFeedConfigId(feedConfigId);
        fileInfo.setFileMask(String.valueOf(result.get("op_file_mask")));
        fileInfo.setImageType(String.valueOf(result.get("op_image_type")));
        fileInfo.setRetention(Double.parseDouble(String.valueOf(result.get("op_retention"))));
        return fileInfo;
    }

    /**
     * Master file will be loaded(saved) into database.
     *
     * @param fileRecord
     * @throws SQLException
     */
    @Override
    public void loadFile(FileRecord fileRecord) throws SQLException {
        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_file_id", fileRecord.getId())
                .addValue("ip_file_image", new SqlLobValue(fileRecord.getFileImage()));
        loadFileSP.execute(inParams);
    }

    @Override
    public void setStatus(FileRecord fileRecord, String status) throws SQLException {
        setStatus(fileRecord.getId(), status);
    }

    @Override
    public void setStatus(int fileId, String status) throws SQLException {
        LOGGER.debug("Calling set status to {} for file with id {}", status, fileId);
        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_file_id", fileId)
                .addValue("ip_file_status", status);
        logFileStatusSP.execute(inParams);
        LOGGER.debug("Setting status is done");
    }

    @Override
    public void loadFile(FileRecord fileRecord, Workbook workbook) throws SQLException, IOException {
        LOGGER.debug("Calling load file for {}", fileRecord);
        MapSqlParameterSource inParams = new MapSqlParameterSource();

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            workbook.write(byteArrayOutputStream);
            inParams.addValue("ip_file_image", new SqlLobValue(byteArrayOutputStream.toByteArray()));
        }
        inParams.addValue("ip_file_id", fileRecord.getId());
        loadFileSP.execute(inParams);
        LOGGER.debug("Calling load file is done.");
    }

    @Override
    public void loadFile(FileRecord fileRecord, InputStream is) throws SQLException, IOException {
        LOGGER.debug("Calling load file for {}", fileRecord);
        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_file_id", fileRecord.getId())
                .addValue("ip_file_image", new SqlLobValue(IOUtils.toByteArray(is)));
        loadFileSP.execute(inParams);
        LOGGER.debug("Calling load file is done.");
    }
}
