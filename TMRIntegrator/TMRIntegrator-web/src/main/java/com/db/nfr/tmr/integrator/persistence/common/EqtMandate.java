package com.db.nfr.tmr.integrator.persistence.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * This class represents equity mandate
 * Created by tymkvit on 07/02/2017.
 */
@Getter
@Setter
@ToString
public class EqtMandate {
    private String trader;
    private String book;
    private String risk;
}
