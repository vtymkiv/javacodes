package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import java.io.InputStream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.persistence.common.DAO;
import com.db.nfr.tmr.integrator.quantstruts.common.Header;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gasiork
 * @author Vitaliy Tymkiv
 */
@Slf4j
@Component
public class EqtProdLogProcessor implements Processor {

    @Autowired
    private DAO dao;

    @Override
    public void process(Exchange exchng) throws Exception {
        LOGGER.debug("Processing EqtProdLog...");
        InputStream is = exchng.getIn().getBody(InputStream.class);
        EqtProdClient client = new EqtProdClient(dao);
        client.makeStartCall(client);
        boolean hasData = client.makeLogCall(is);
        if (!hasData) {
            exchng.setProperty(Exchange.ROUTE_STOP, Boolean.TRUE);
        }
        TypeRecAllFiles rec = exchng.getIn().getHeader(Header.REC_ALL_FILES, TypeRecAllFiles.class);
        exchng.getOut().setHeader(Header.REC_ALL_FILES, rec);
        exchng.getOut().setBody(exchng.getIn().getBody());
        LOGGER.debug("Processing EqtProdLog is done.");
    }

}
