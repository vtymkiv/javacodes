package com.db.nfr.tmr.integrator.persistence.common;

import javax.sql.DataSource;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.quantstruts.quant.common.Client;


import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.FEED_APP_NAME;

//TODO: Requires refactoring

/**
 * @author gasiork
 */
@Component
public class BulkDAO implements DAO {
    private final static String PREFIX = "adm_owner.";

    @Autowired
    @Qualifier("stgFeedDS")
    private DataSource stgFeedDataSource;

    @Autowired
    private CustomOracleTypes customOracleTypes;

    @Override
    public void cleanUp(String sql) throws SQLException {
        try (Connection c = stgFeedDataSource.getConnection(); CallableStatement cs = c.prepareCall(sql)) {
            cs.execute();
            cs.close();
        }
    }

    @Override
    public void persist(final List<List<Object>> data, String bulkType, String type, String sql) {
        try {
            call(data, bulkType.toUpperCase(), type.toUpperCase(), sql);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void markFinish(String validateSql, String updateSql) throws SQLException {
        if (updateSql != null && !updateSql.isEmpty()) {
            try (Connection c = stgFeedDataSource.getConnection(); CallableStatement cs = c.prepareCall(updateSql)) {
                cs.execute();
                cs.close();
            }
        }
    }


    @Override
    public boolean logCall(Client client, int lineNumber) throws SQLException {
        if (lineNumber == -1) {
            makeFileErrorlog(client, lineNumber);
            return false;
        }
        try (Connection c = stgFeedDataSource.getConnection();
             CallableStatement cs = c.prepareCall(String.format("{call %spkg_logger.set_context(?)}", PREFIX));
             CallableStatement cs1 = c.prepareCall(String.format("{call %spkg_logger.log(?,?,?)}", PREFIX))) {
            cs.setString(1, FEED_APP_NAME);
            cs.execute();
            cs.close();

            cs1.setString(1, lineNumber > 0 ? "INFO" : "WARNING");
            String textInfo = getTextInfo(client, lineNumber);
            cs1.setString(2, textInfo);
            cs1.setInt(3, lineNumber);
            cs1.execute();
            cs1.close();
            return lineNumber > 0;
        }
    }

    @Override
    public void logCall(Client client, Exception ex) throws SQLException {
        try (Connection c = stgFeedDataSource.getConnection();
             CallableStatement cs = c.prepareCall(String.format("{call %spkg_logger.set_context(?)}", PREFIX));
             CallableStatement cs1 = c.prepareCall(String.format("{call %spkg_logger.log(?,?,?)}", PREFIX))) {
            cs.setString(1, FEED_APP_NAME);
            cs.execute();
            cs.close();

            cs1.setString(1, "EXCEPTION");
            String textInfo = ex.getMessage();
            cs1.setString(2, textInfo);
            cs1.setObject(3, null);
            cs1.execute();
            cs1.close();
        }
    }

    @Override
    public void markStartCall(Client client) {
        try (Connection c = stgFeedDataSource.getConnection()) {
            CallableStatement csSetCtx = c.prepareCall(String.format("{call %spkg_logger.set_context(?)}", PREFIX));
            csSetCtx.setString(1, FEED_APP_NAME);
            csSetCtx.execute();
            csSetCtx.close();

            CallableStatement csLog = c.prepareCall(String.format("{call %spkg_logger.log(?,?)}", PREFIX));
            csLog.setString(1, "INFO");
            csLog.setString(2, String.format("%s - START", client.getName().toLowerCase()));
            csLog.execute();
            csLog.close();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void gatherStatistics(String tableName) {
        try (Connection c = stgFeedDataSource.getConnection()) {
            CallableStatement cs = c.prepareCall("{call stg_owner.PKG_STG_MANDATES.p_gather_statistics(?,?)}");
            cs.setString(1, "STG_OWNER");
            cs.setString(2, tableName);
            cs.execute();
            cs.close();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private void makeFileErrorlog(Client client, int lineNumber) throws SQLException {
        try (Connection c = stgFeedDataSource.getConnection();
             CallableStatement cs = c.prepareCall(String.format("{call %spkg_logger.set_context(?)}", PREFIX));
             CallableStatement cs1 = c.prepareCall(String.format("{call %spkg_logger.log(?,?,?)}", PREFIX))) {
            cs.setString(1, FEED_APP_NAME);
            cs.execute();
            cs.close();

            cs1.setString(1, "EXCEPTION");
            String textInfo = getTextInfo(client, lineNumber);
            cs1.setString(2, textInfo);
            cs1.setObject(3, null);
            cs1.execute();
            cs1.close();
        }
    }

    private void call(List<List<Object>> list, String oracleBulkTypeName, String oracleTypeName, String sql) throws SQLException {
        try (Connection c = stgFeedDataSource.getConnection();
             CallableStatement cs = c.prepareCall(sql)) {
            Array array = customOracleTypes.convert(list, oracleBulkTypeName, c, oracleTypeName);
            cs.setObject(1, array);
            cs.execute();
            cs.close();
        }
    }


    private String getTextInfo(Client client, int lineNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(client.getName().toLowerCase());

        if (lineNumber != Client.NO_FILE_DATA_AMOUNT) {
            sb.append(" - File: ");
            sb.append(String.format("size=%s;", client.getFileSize()));
            sb.append(String.format("last.modified=%s;", client.getFileDateText()));
        } else {
            sb.append(" - FILE ERROR: File not found");
        }

        return sb.toString();
    }
}
