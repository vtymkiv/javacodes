package com.db.nfr.tmr.integrator.persistence;

import java.sql.SQLException;

import com.db.nfr.tmr.integrator.persistence.common.DebtMandate;
import com.db.nfr.tmr.integrator.persistence.common.EqtMandate;

/**
 * This class represents the data access object that would be
 * used for accessing to the database's pkg_stg_mandates package functions
 * <p>
 * Created by tymkvit on 01/02/2017.
 */
public interface PkgStgMandatesRepoDAO {
    /**
     * Clean up debts
     * It will call pkg_stg_mandates.p_clean_stg_mandates_debt
     *
     * @throws SQLException
     */
    void cleanDebts() throws SQLException;

    /**
     * Clean up Equities
     * It will call pkg_stg_mandates.p_clean_stg_mandates_debt
     *
     * @throws SQLException
     */
    void cleanEqts() throws SQLException;

    /**
     * Uploading debts into STG_ table.
     * It will call pkg_stg_mandates.p_fill_stg_mandates_debt
     *
     * @throws SQLException
     */
    void populateDebts(DebtMandate debtMandate) throws SQLException;

    /**
     * Uploading equities into STG_ table
     * It will call pkg_stg_mandates.p_fill_stg_mandates_eqt
     *
     * @throws SQLException
     */
    void populateEqts(EqtMandate eqtMandate) throws SQLException;

}
