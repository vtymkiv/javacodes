/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.nfr.tmr.integrator.quantstruts.common;

/**
 * @author prokden
 */
public enum MandateType {

    DEBT("DEBT"), EQUITY("EQT");

    private String code;

    MandateType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static MandateType findByCode(String name) {
        MandateType foundMandateType = null;
        for (MandateType mandateType : values()) {
            if (mandateType.getCode().equalsIgnoreCase(name)) {
                foundMandateType = mandateType;
                break;
            }
        }
        return foundMandateType;
    }
}
