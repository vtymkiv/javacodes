package com.db.nfr.tmr.integrator.quantstruts.common;

/**
 * @author gasiork
 */
public final class Header {
    private Header() {
    }

    public static final String FEED_CONFIG_ID = "HEADER_FEED_CONFIG_ID";
    public static final String REC_ALL_FILES = "REC_ALL_FILES";
    public static final String EQT_PROCESSING_STATUS = "EQT_PROCESSING_STATUS";
    public static final String DEBT_PROCESSING_STATUS = "DEBT_PROCESSING_STATUS";
    public static final String EQT_MASTER_FILE_PROPERTY = "EQT_MASTER_FILE";
    public static final String DEBT_MASTER_FILE_PROPERTY = "DEBT_MASTER_FILE";
    public static final String SFTP_FILE_LIST = "SFTP_FILE_LIST";
}
