package com.db.nfr.tmr.integrator.quantstruts.quant.debt;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.persistence.common.DAO;
import com.db.nfr.tmr.integrator.quantstruts.common.Header;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gasiork
 */
@Slf4j
@Component
public class DebtMandateProcessor implements Processor {
    @Autowired
    private DAO dao;

    @Override
    public void process(Exchange exchng) throws Exception {
        LOGGER.info("Processing Debt Mandates started");

        TypeRecAllFiles rec = exchng.getIn().getHeader(Header.REC_ALL_FILES, TypeRecAllFiles.class);
        Workbook workbook = exchng.getIn().getBody(Workbook.class);
        DebtMandateClient client = new DebtMandateClient(dao);
        client.execute(rec, workbook);
        exchng.getOut().setBody(workbook);
        exchng.getOut().setHeader(Header.REC_ALL_FILES, rec);
        exchng.getOut().setBody(workbook);

        LOGGER.info("Processing Debt Mandates finished");
    }

}
