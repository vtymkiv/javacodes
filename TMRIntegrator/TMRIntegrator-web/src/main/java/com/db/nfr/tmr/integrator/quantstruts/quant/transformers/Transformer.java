package com.db.nfr.tmr.integrator.quantstruts.quant.transformers;

import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Row;

/**
 * @author gasiork
 */
public interface Transformer {
    List<String> transformLine(CSVRecord line);

    List<String> transformRow(Row row);

    List<String> transformRow(Row row, int columnCount);

    Map<String, String> transformLineToMap(String line);
}
