package com.db.nfr.tmr.integrator.quantstruts.quant.debt;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.PkgFileRepoDAO;
import com.db.nfr.tmr.integrator.persistence.PkgLoadMandatesRepoDAO;
import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.quantstruts.common.Header;
import com.db.nfr.tmr.integrator.quantstruts.common.MandateType;
import com.db.nfr.tmr.integrator.quantstruts.common.Status;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gasiork
 */
@Slf4j
@Component
public class DebtLoadProcessor implements Processor {
    @Autowired
    private PkgFileRepoDAO pkgFileRepoDAO;

    @Autowired
    private PkgLoadMandatesRepoDAO pkgLoadMandatesRepoDAO;

    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.info("Debt Load started");
        TypeRecAllFiles rec = exchange.getIn().getHeader(Header.REC_ALL_FILES, TypeRecAllFiles.class);
        pkgLoadMandatesRepoDAO.loadMandates(rec.getFileDate(), MandateType.DEBT);
        pkgLoadMandatesRepoDAO.gatherStatistics();

        // Loading finished successfully
        LOGGER.info("Setting status to completed...");
        pkgFileRepoDAO.setStatus(rec.getId(), Status.COMPLETED.getStatusCode());
        LOGGER.info("Setting status to completed is done.");
        exchange.getProperties().put(Header.DEBT_PROCESSING_STATUS, Status.DEBT_LOAD_COMPLETED.getStatusCode());
        exchange.getOut().copyFrom(exchange.getIn());
        LOGGER.info("Debt load finished");
    }
}
