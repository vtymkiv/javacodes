package com.db.nfr.tmr.integrator.quantstruts.quant.debt;

import java.util.List;

import com.db.nfr.tmr.integrator.persistence.common.DAO;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.AbstractClient;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.OracleMapping;

/**
 * @author gasiork
 */
public class DebtProdClient extends AbstractClient {

    private final OracleMapping mapping = new DebtProdMapping();

    public DebtProdClient(DAO dao) {
        super(dao);
    }

    @Override
    public List transformData(List<String> bulkData) {
        bulkData.remove(0);
        return bulkData;
    }

    @Override
    public OracleMapping getOracleMapping() {
        return mapping;
    }

    @Override
    public String getName() {
        return "mandates_prod_debt";
    }

    @Override
    public int getSheetNumber() {
        return 1;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

}
