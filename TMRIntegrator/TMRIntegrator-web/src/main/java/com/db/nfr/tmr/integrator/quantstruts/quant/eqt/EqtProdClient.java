package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import java.util.List;

import com.db.nfr.tmr.integrator.persistence.common.DAO;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.AbstractClient;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.OracleMapping;

/**
 * @author gasiork
 */
public class EqtProdClient extends AbstractClient {

    private final OracleMapping mapping = new EqtProdMapping();

    public EqtProdClient(DAO dao) {
        super(dao);
    }

    @Override
    public List transformData(List<String> bulkData) {
        return bulkData;
    }

    @Override
    public OracleMapping getOracleMapping() {
        return mapping;
    }

    @Override
    public String getName() {
        return "risk2pm";
    }

    @Override
    public int getSheetNumber() {
        return 0;
    }
}
