package com.db.nfr.tmr.integrator.quantstruts.common;

import java.util.Arrays;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.PkgFileRepoDAO;
import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gasiork
 * @author Vitaliy Tymkiv
 */
@Slf4j
@Component
public class FileRepoWorkbookProcessor implements Processor {
    @Autowired
    private PkgFileRepoDAO pkgFileRepoDAO;

    @Override
    public void process(Exchange exchng) throws Exception {
        LOGGER.info("Processing workbook using FileRepoWorkbookProcessor...");
        Workbook workbook = exchng.getIn().getBody(Workbook.class);

        TypeRecAllFiles rec = exchng.getIn().getHeader(Header.REC_ALL_FILES, TypeRecAllFiles.class);

        List<FileRecord> fileRecordList = pkgFileRepoDAO.checkFiles(Arrays.asList(rec), FileRecord.class);
        if (!fileRecordList.isEmpty()) {
            FileRecord masterFileRec = fileRecordList.get(0);
            rec.setId(masterFileRec.getId());

            pkgFileRepoDAO.loadFile(masterFileRec, workbook);

            exchng.getOut().setHeader(Header.REC_ALL_FILES, rec);
            exchng.getOut().setBody(workbook);
        } else {
            exchng.setProperty(Exchange.ROUTE_STOP, Boolean.TRUE);
        }

        LOGGER.info("Processing workbook using FileRepoWorkbookProcessor is done.");
    }

}
