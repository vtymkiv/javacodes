package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import java.io.InputStream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.persistence.common.DAO;
import com.db.nfr.tmr.integrator.quantstruts.common.Constant;
import com.db.nfr.tmr.integrator.quantstruts.common.Header;
import com.monitorjbl.xlsx.StreamingReader;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gasiork
 * @author Vitaliy Tymkiv
 */
@Slf4j
@Component
public class EqtMandateLogProcessor implements Processor {

    @Autowired
    private DAO dao;

    @Override
    public void process(Exchange exchng) throws Exception {
        LOGGER.info("Processing EqtMandateLog...");
        InputStream is = exchng.getIn().getBody(InputStream.class);
        EqtMandateClient client = new EqtMandateClient(dao);
        try (Workbook workbook = StreamingReader.builder().rowCacheSize(Constant.ROWCACHESIZE)
                .bufferSize(Constant.BUFFERSIZE).open(is)) {
            client.makeStartCall(client);
            boolean hasData = client.makeLogCall(workbook, client.getSheetNumber());
            if (!hasData) {
                exchng.setProperty(Exchange.ROUTE_STOP, Boolean.TRUE);
            }
            TypeRecAllFiles rec = exchng.getIn().getHeader(Header.REC_ALL_FILES, TypeRecAllFiles.class);
            exchng.getOut().setHeader(Header.REC_ALL_FILES, rec);
            exchng.getOut().setBody(exchng.getIn().getBody());
        }
        LOGGER.info("Processing EqtMandateLog is done.");
    }

}
