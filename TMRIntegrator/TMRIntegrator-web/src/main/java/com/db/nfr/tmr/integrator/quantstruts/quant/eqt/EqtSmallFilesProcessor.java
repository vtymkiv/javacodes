package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.quantstruts.common.MandateType;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.CommonSmallFilesProcessor;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by tymkvit on 09/02/2017.
 */
@Slf4j
@Component
public class EqtSmallFilesProcessor implements Processor {
    @Autowired
    private CommonSmallFilesProcessor commonSmallFilesProcessor;

    /**
     * Processes the message exchange
     *
     * @param exchange the message exchange
     * @throws Exception if an internal processing error has occurred.
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.debug("Starting to process small files for equities...");
        commonSmallFilesProcessor.process("{{ftp-eqt-slave-files}}", exchange, MandateType.EQUITY);
        LOGGER.debug("Processing small files for equities is done.");
    }
}
