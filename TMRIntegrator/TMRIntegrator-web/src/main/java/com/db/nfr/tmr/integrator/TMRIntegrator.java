package com.db.nfr.tmr.integrator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.web.WebApplicationInitializer;

/**
 * @author gasiork
 */
@SpringBootApplication
public class TMRIntegrator extends SpringBootServletInitializer implements WebApplicationInitializer {
    static {
        System.setProperty("spring.config.location", "classpath:TMRIntegrator/");
    }

    public static void main(String[] args) {
        SpringApplication.run(TMRIntegrator.class, args);
    }

}
