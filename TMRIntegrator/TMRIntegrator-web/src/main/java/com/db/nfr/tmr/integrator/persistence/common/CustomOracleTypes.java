package com.db.nfr.tmr.integrator.persistence.common;

import javax.sql.DataSource;

import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Struct;
import java.util.List;

import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;

import oracle.jdbc.OracleConnection;

/**
 * Created by tymkvit on 02/02/2017.
 */
@Component
public class CustomOracleTypes {

    public Array getRecAllFilesStructArray(DataSource dataSource, List<TypeRecAllFiles> typeRecAllFiles) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            return getRecAllFilesStructArray(conn, typeRecAllFiles);
        }
    }

    private Array getRecAllFilesStructArray(Connection conn, List<TypeRecAllFiles> typeRecAllFiles) throws SQLException {
        OracleConnection oracleConnection = conn.unwrap(OracleConnection.class);
        Struct[] structureList = new Struct[typeRecAllFiles.size()];
        for (int i = 0; i < structureList.length; i++) {
            structureList[i] = getRecAllFilesStruct(oracleConnection, typeRecAllFiles.get(i));
        }

        return oracleConnection.createOracleArray("STG_OWNER.T_ALL_FILES", structureList);
    }

    private Struct getRecAllFilesStruct(Connection conn, TypeRecAllFiles item) throws SQLException {
        return conn.createStruct("STG_OWNER.REC_ALL_FILES", new Object[]{item.getFeedConfigId(), item.getFileName(), new java.sql.Date(item.getFileDate().getTime()), item
                .getFileSize()});
    }

    public Array getDebtMandatesStructArray(DataSource dataSource, List<DebtMandate> debtMandateList) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            return getDebtMandatesStructArray(conn, debtMandateList);
        }
    }

    private Array getDebtMandatesStructArray(Connection conn, List<DebtMandate> debtMandateList) throws SQLException {
        Struct[] structureList = new Struct[debtMandateList.size()];
        for (int i = 0; i < structureList.length; i++) {
            structureList[i] = getDebtMandatesStruct(conn, debtMandateList.get(i));
        }

        OracleConnection oracleConnection = conn.unwrap(OracleConnection.class);
        return oracleConnection.createOracleArray("STG_OWNER.TT_STG_DEBT_MANDATES", structureList);
    }

    private Struct getDebtMandatesStruct(Connection conn, DebtMandate debtMandate) throws SQLException {
        return conn.createStruct("STG_OWNER.TO_STG_DEBT_MANDATES", new Object[]{debtMandate.getTrader(), debtMandate.getBookFamily(), debtMandate.getProductFamily()});
    }


    public Array getEqtMandatesStructArray(DataSource dataSource, List<EqtMandate> eqtMandates) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            return getEqtMandatesStructArray(conn, eqtMandates);
        }
    }

    private Array getEqtMandatesStructArray(Connection conn, List<EqtMandate> eqtMandates) throws SQLException {
        Struct[] structureList = new Struct[eqtMandates.size()];
        for (int i = 0; i < structureList.length; i++) {
            structureList[i] = getEqtMandatesStruct(conn, eqtMandates.get(i));
        }

        OracleConnection oracleConnection = conn.unwrap(OracleConnection.class);
        return oracleConnection.createOracleArray("STG_OWNER.TT_STG_EQT_MANDATES", structureList);

    }

    private Struct getEqtMandatesStruct(Connection conn, EqtMandate eqtMandate) throws SQLException {
        return conn.createStruct("STG_OWNER.TO_STG_EQT_MANDATES", new Object[]{eqtMandate.getTrader(), eqtMandate.getBook(), eqtMandate.getRisk()});
    }

    private Struct convert(List<Object> data, String oracleTypeName, Connection conn) throws SQLException {
        for (int i = 0; i < data.size(); i++) {
            Object item = data.get(i);
            if (item != null && item.getClass() == String.class) {
                data.set(i, item.toString().replaceAll("\"", ""));
            }
        }
        if (conn.isWrapperFor(OracleConnection.class)) {
            return conn.unwrap(OracleConnection.class).createStruct(oracleTypeName, data.toArray());
        }
        return conn.createStruct(oracleTypeName, data.toArray());

    }

    public Array convert(List<List<Object>> list, String oracleBulkTypeName, Connection conn, String oracleTypeName) throws SQLException {
        Object[] data = new Object[list.size()];
        for (int i = 0; i < list.size(); i++) {
            data[i] = convert(list.get(i), oracleTypeName, conn);
        }

        if (conn.isWrapperFor(OracleConnection.class)) {
            return conn.unwrap(OracleConnection.class).createOracleArray(oracleBulkTypeName, data);
        } else {
            return conn.createArrayOf(oracleTypeName, data);
        }
    }

}
