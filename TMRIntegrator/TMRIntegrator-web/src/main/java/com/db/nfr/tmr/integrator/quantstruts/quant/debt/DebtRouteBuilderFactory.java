package com.db.nfr.tmr.integrator.quantstruts.quant.debt;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.PkgLoadMandatesRepoDAO;
import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.quantstruts.common.FileRepoWorkbookProcessor;
import com.db.nfr.tmr.integrator.quantstruts.common.Header;
import com.db.nfr.tmr.integrator.quantstruts.common.MandateType;
import com.db.nfr.tmr.integrator.quantstruts.common.RecAllFilesProcessor;
import com.db.nfr.tmr.integrator.quantstruts.common.Status;
import com.db.nfr.tmr.integrator.quantstruts.common.WorkbookConverter;


import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.DEBT_FILE_ID;
import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.DEBT_FILE_NAME;

/**
 * @author gasiork
 */
@Component
public class DebtRouteBuilderFactory {
    private String routeId;
    private String cronExpr;

    @Value("${camel.route.scheduler}")
    private String routeScheduler;

    @Autowired
    private RecAllFilesProcessor recAllFilesProcessor;
    @Autowired
    private FileRepoWorkbookProcessor fileRepoWorkbookProcessor;
    @Autowired
    private WorkbookConverter workbookConverter;
    @Autowired
    private DebtProdProcessor debtProdProcessor;
    @Autowired
    private DebtMandateProcessor debtMandateProcessor;
    @Autowired
    private DebtLoadProcessor debtLoadProcessor;
    @Autowired
    private DebtSmallFilesProcessor debtSmallFilesProcessor;
    @Autowired
    private DebtDeleteSmallFilesProcessor debtDeleteSmallFilesProcessor;
    @Autowired
    private PkgLoadMandatesRepoDAO pkgLoadMandatesRepoDAO;

    public RouteBuilder getRouteBuilder() {
        return new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                String scheduler = routeScheduler + cronExpr.replaceAll(" ", "+");

                from("{{ftp-debt-master-file}}" + DEBT_FILE_NAME + "&" + scheduler)
                        .process(exchange -> {
                            exchange.getProperties().put(Header.DEBT_PROCESSING_STATUS, Status.STARTED.getStatusCode());
                        })
                        .setHeader(Header.FEED_CONFIG_ID, constant(DEBT_FILE_ID))
                        .process(recAllFilesProcessor)
                        .bean(workbookConverter)
                        .process(fileRepoWorkbookProcessor)
                        // Mark that loading process has been started
                        .process(exchange -> {
                            TypeRecAllFiles masterFileRec = exchange.getIn().getHeader(Header.REC_ALL_FILES, TypeRecAllFiles.class);
                            pkgLoadMandatesRepoDAO.startLoading(MandateType.DEBT, masterFileRec.getFileDate());
                            exchange.getIn().setHeader(Header.REC_ALL_FILES, masterFileRec);
                            exchange.getProperties().put(Header.DEBT_MASTER_FILE_PROPERTY, masterFileRec);
                        })
                        .process(debtSmallFilesProcessor)
                        .process(debtProdProcessor)
                        .process(debtMandateProcessor)
                        .process(debtLoadProcessor)
                        .process(debtDeleteSmallFilesProcessor)
                        .log("DebtRouteBuilderFactory finished.Sending email...")
                        .setBody(constant("Import of debts is done."))
                        .to("{{emailTo}}" + "{{subjectForDebt}}")
                        /*.onCompletion().process(new Processor() {
                        @Override
                        public void process(Exchange exchange) throws Exception {
                               camelCo
                        }
                })*/
                        .routeId(getRouteId());
            }
        };
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getCronExpr() {
        return cronExpr;
    }

    public void setCronExpr(String cron) {
        this.cronExpr = cron;
    }
}
