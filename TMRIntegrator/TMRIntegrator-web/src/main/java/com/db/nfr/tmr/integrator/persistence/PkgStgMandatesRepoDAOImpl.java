package com.db.nfr.tmr.integrator.persistence;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.db.nfr.tmr.integrator.persistence.common.CustomOracleTypes;
import com.db.nfr.tmr.integrator.persistence.common.DebtMandate;
import com.db.nfr.tmr.integrator.persistence.common.EqtMandate;

import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleTypes;

/**
 * Created by tymkvit on 07/02/2017.
 */
@Slf4j
@Repository
public class PkgStgMandatesRepoDAOImpl implements PkgStgMandatesRepoDAO {
    @Autowired
    private CustomOracleTypes customOracleTypes;

    private final JdbcTemplate stgJdbcTemplate;
    private SimpleJdbcCall cleanStgMandatesDebtSP;
    private SimpleJdbcCall cleanStgMandatesEqtSP;
    private SimpleJdbcCall fillStgMandatesDebtSP;
    private SimpleJdbcCall fillStgMandatesEqtSP;


    @Autowired
    public PkgStgMandatesRepoDAOImpl(@Qualifier("stgJdbcTemplate") JdbcTemplate stgJdbcTemplate) {
        this.stgJdbcTemplate = stgJdbcTemplate;

        cleanStgMandatesDebtSP = new SimpleJdbcCall(this.stgJdbcTemplate)
                .withSchemaName("stg_owner")
                .withCatalogName("pkg_stg_mandates")
                .withProcedureName("p_clean_stg_mandates_debt");

        cleanStgMandatesEqtSP = new SimpleJdbcCall(this.stgJdbcTemplate)
                .withSchemaName("stg_owner")
                .withCatalogName("pkg_stg_mandates")
                .withProcedureName("p_clean_stg_mandates_eqt");

        fillStgMandatesDebtSP = new SimpleJdbcCall(this.stgJdbcTemplate)
                .withSchemaName("stg_owner")
                .withCatalogName("pkg_stg_mandates")
                .withProcedureName("p_fill_stg_mandates_debt")
                .declareParameters(
                        new SqlParameter("ip_record", OracleTypes.ARRAY)
                );

        fillStgMandatesEqtSP = new SimpleJdbcCall(this.stgJdbcTemplate)
                .withSchemaName("stg_owner")
                .withCatalogName("pkg_stg_mandates")
                .withProcedureName("p_fill_stg_mandates_eqt")
                .declareParameters(
                        new SqlParameter("ip_record", OracleTypes.ARRAY)
                );
    }

    /**
     * Clean up debts
     * It will call pkg_stg_mandates.p_clean_stg_mandates_debt
     *
     * @throws SQLException
     */
    @Override
    public void cleanDebts() throws SQLException {
        cleanStgMandatesDebtSP.execute();
    }

    /**
     * Clean up Equities
     * It will call pkg_stg_mandates.p_clean_stg_mandates_debt
     *
     * @throws SQLException
     */
    @Override
    public void cleanEqts() throws SQLException {
        cleanStgMandatesEqtSP.execute();
    }

    /**
     * Uploading debts into STG_ table.
     * It will call pkg_stg_mandates.p_fill_stg_mandates_debt
     *
     * @param debtMandate
     * @throws SQLException
     */
    @Override
    public void populateDebts(DebtMandate debtMandate) throws SQLException {
        Objects.requireNonNull(debtMandate, "Mandate for debt cannot be null.");
        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_record", customOracleTypes.getDebtMandatesStructArray(fillStgMandatesDebtSP.getJdbcTemplate().getDataSource(), Arrays.asList(debtMandate)));
        fillStgMandatesDebtSP.execute(inParams);
    }

    /**
     * Uploading equities into STG_ table
     * It will call pkg_stg_mandates.p_fill_stg_mandates_eqt
     *
     * @param eqtMandate
     * @throws SQLException
     */
    @Override
    public void populateEqts(EqtMandate eqtMandate) throws SQLException {
        Objects.requireNonNull(eqtMandate, "Mandate for equities cannot be null.");
        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_record", customOracleTypes.getEqtMandatesStructArray(fillStgMandatesEqtSP.getJdbcTemplate().getDataSource(), Arrays.asList(eqtMandate)));
        fillStgMandatesEqtSP.execute(inParams);
    }
}
