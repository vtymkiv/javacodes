package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import org.apache.camel.ConsumerTemplate;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.quantstruts.common.Header;
import com.db.nfr.tmr.integrator.quantstruts.common.Status;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.CommonDeleteSmallFilesProcessor;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by tymkvit on 20/02/2017.
 */
@Slf4j
@Component
public class EqtDeleteSmallFilesProcessor implements Processor {
    @Value("${common-folder}/${eqt-folder}")
    private String eqtFolder;

    @Autowired
    private ConsumerTemplate eqtConsumer;

    @Autowired
    private CommonDeleteSmallFilesProcessor commonDeleteSmallFilesProcessor;


    /**
     * Processes the message exchange
     *
     * @param exchange the message exchange
     * @throws Exception if an internal processing error has occurred.
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.info("Delete small files processor started.");
        String status = exchange.getProperty(Header.EQT_PROCESSING_STATUS, String.class);
        TypeRecAllFiles masterFileRec = exchange.getProperty(Header.EQT_MASTER_FILE_PROPERTY, TypeRecAllFiles.class);
        LOGGER.info("EQT processing status: {}", status);
        Status eqtProcessingStatus = Status.findByCode(status);

        if (Status.EQT_LOAD_COMPLETED == eqtProcessingStatus) {
            LOGGER.info("Eqt Deleting small files processor started....");
            commonDeleteSmallFilesProcessor.process(eqtFolder, "{{ftp-eqt-slave-files-delete}}", masterFileRec);
            LOGGER.info("Eqt Deleting small files processor finished.....");
        }
        LOGGER.info("Delete small files processor finished.");
    }
}
