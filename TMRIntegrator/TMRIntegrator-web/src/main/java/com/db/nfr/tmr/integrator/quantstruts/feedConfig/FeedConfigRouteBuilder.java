package com.db.nfr.tmr.integrator.quantstruts.feedConfig;

import javax.annotation.PostConstruct;

import org.apache.camel.CamelContext;
import org.apache.camel.ServiceStatus;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.RouteDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;


import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.DEBT_APP_SUB_NAME;
import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.EQT_APP_SUB_NAME;
import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.FEED_APP_NAME;

/**
 * Route builder based on feed config from database
 */
@Slf4j
@Component
public class FeedConfigRouteBuilder extends RouteBuilder {
    private static final String FEED_CONFIG_SQL = "SELECT * FROM adm_owner.feed_config WHERE feed_app_name=:?feed_app_name AND feed_sub_name IN ('" + DEBT_APP_SUB_NAME
            + "', '" + EQT_APP_SUB_NAME + "')";

    @Value("${feed.config.route.id}:FeedConfig")
    private String feedConfigRouteId;

    @Autowired
    private CamelContext camelContext;

    @Autowired
    private FeedSubNameProcessor feedSubNameProcessor;

    @Override
    public void configure() throws Exception {
        routeFeedConfig(from("{{feed.config.scheduler}}"), feedConfigRouteId);
        routeFeedConfig(from("{{feed.config.first.run.scheduler}}"), feedConfigRouteId + "_first_run");
    }

    private void routeFeedConfig(RouteDefinition routeDefinition, String routeId) {
        LOGGER.debug("routeFeedConfig SQL: {}", FEED_CONFIG_SQL);
        routeDefinition
                .routeId(routeId)
                .setHeader("feed_app_name", constant(FEED_APP_NAME))
                .setBody(constant(FEED_CONFIG_SQL))
                .to("{{feed-config-route}}")
                .split(body())
                .process(feedSubNameProcessor)
                .log("routeFeedConfig finished");
    }

    @PostConstruct
    public void init() {
        try {
            camelContext.setTracing(false);
            camelContext.removeRoute(feedConfigRouteId);
            camelContext.setStreamCaching(true);
            camelContext.disableJMX();
            camelContext.getStreamCachingStrategy().setSpoolDirectory("${java.io.tmpdir}/quantstruts/tmp");
            camelContext.getStreamCachingStrategy().setSpoolThreshold(-1);
            camelContext.getStreamCachingStrategy().setBufferSize(128 * 1024);
            camelContext.getStreamCachingStrategy().setRemoveSpoolDirectoryWhenStopping(true);
            camelContext.getStreamCachingStrategy().setAnySpoolRules(true);
            camelContext.getStreamCachingStrategy().setSpoolUsedHeapMemoryThreshold(20);
            camelContext.addRoutes(this);
            if (!ServiceStatus.Started.equals(camelContext.getStatus())) {
                camelContext.start();
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
