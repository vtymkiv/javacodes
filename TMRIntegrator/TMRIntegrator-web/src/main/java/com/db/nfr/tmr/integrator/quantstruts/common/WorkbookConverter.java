package com.db.nfr.tmr.integrator.quantstruts.common;

import java.io.IOException;
import java.io.InputStream;

import org.apache.camel.Body;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 * @author Vitaliy Tymkiv
 */
@Component
public class WorkbookConverter {

    /**
     * Convert InputStream body to XSSFWorkbook
     *
     * @param body
     * @return
     * @throws IOException
     */
    public Workbook toWorkbook(@Body InputStream body) throws IOException {
        return new XSSFWorkbook(body);
    }
}
