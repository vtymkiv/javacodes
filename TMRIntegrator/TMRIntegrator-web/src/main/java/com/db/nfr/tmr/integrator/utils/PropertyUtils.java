package com.db.nfr.tmr.integrator.utils;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by danyvik on 06/12/2016.
 */
public final class PropertyUtils {

    private PropertyUtils() {
    }

    public static Properties getConfigProperties() {
        try {
            Properties p = new Properties();
            p.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("TMRIntegrator/config.properties"));
            return p;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
