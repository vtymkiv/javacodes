package com.db.nfr.tmr.integrator.quantstruts.quant.common;

/**
 * Indicating the status of file
 * <p>
 * Created by vtymkiv on 21.04.2017.
 */
public enum FileStatus {
    PROCESSED,
    NOT_PROCESSED,
    FAILED_TO_PROCESS
}
