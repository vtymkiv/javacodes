package com.db.nfr.tmr.integrator.quantstruts.quant.common;

import com.db.nfr.tmr.integrator.persistence.common.DAO;

/**
 * @author gasiork
 */
public interface Client {
    int NO_FILE_DATA_AMOUNT = -1;

    OracleMapping getOracleMapping();

    String getName();

    long getFileSize();

    String getFileDateText();

    void setDao(DAO dao);
}
