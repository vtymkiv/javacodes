package com.db.nfr.tmr.integrator.quantstruts.quant.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author gasiork
 */
@Setter
@Getter
@ToString
public class OracleMapping {
    private String bulkType;
    private String type;
    private String sql;
    private String validateSql;
    private String updateSql;
    private String cleanUpSql;
    private String tableName;
}
