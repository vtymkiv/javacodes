package com.db.nfr.tmr.integrator.quantstruts.feedConfig;

import java.util.Map;
import java.util.Properties;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.integrator.quantstruts.quant.debt.DebtRouteBuilderFactory;
import com.db.nfr.tmr.integrator.quantstruts.quant.eqt.EqtRouteBuilderFactory;

import lombok.extern.slf4j.Slf4j;


import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.DEBT_APP_SUB_NAME;
import static com.db.nfr.tmr.integrator.quantstruts.common.Constant.EQT_APP_SUB_NAME;
import static com.db.nfr.tmr.integrator.utils.PropertyUtils.getConfigProperties;

/**
 * @author gasiork
 */
@Slf4j
@Component
public class FeedSubNameProcessor implements Processor {
    @Autowired
    private CamelContext camelContext;
    @Autowired
    private DebtRouteBuilderFactory debtRouteBuilderFactory;
    @Autowired
    private EqtRouteBuilderFactory eqtRouteBuilderFactory;

    @Override
    public void process(Exchange exchng) throws Exception {
        @SuppressWarnings("unchecked")
        Map<String, Object> map = (Map<String, Object>) exchng.getIn().getBody();
        String feedSubName = map.get("FEED_SUB_NAME").toString();

        //check if file processing is in progress
        while (camelContext.getInflightRepository().size(feedSubName) > 0) {
            // Wait for in-flight exchanges to be finished
            Thread.sleep(10 * 1000); //10 secs
        }
        //remove old route
        LOGGER.info("Removing route with id {} ...", feedSubName);
        camelContext.stopRoute(feedSubName);
        camelContext.removeRoute(feedSubName);
        LOGGER.info("Removing route with id {} is done.", feedSubName);

        if (!isValidForProcessing(map)) {
            LOGGER.debug("Is not valid for processing.");
            return;
        }

        LOGGER.debug("Valid for processing. We will process it.");
        //Get cron expression
        String cronExpr = getCronExpression(map);

        //set new route
        switch (feedSubName) {
            case DEBT_APP_SUB_NAME: {
                debtRouteBuilderFactory.setRouteId(feedSubName);
                debtRouteBuilderFactory.setCronExpr(cronExpr);
                camelContext.addRoutes(debtRouteBuilderFactory.getRouteBuilder());
                break;
            }
            case EQT_APP_SUB_NAME: {
                eqtRouteBuilderFactory.setRouteId(feedSubName);
                eqtRouteBuilderFactory.setCronExpr(cronExpr);
                camelContext.addRoutes(eqtRouteBuilderFactory.getRouteBuilder());
                break;
            }
            default:
                throw new RuntimeException("Can note identify mandate type by feed subname");
        }
    }

    private String getCronExpression(Map<String, Object> map) {
        return map.get("SECOND").toString() + ' ' +
                map.get("MINUTE").toString() + ' ' +
                map.get("HOUR").toString() + ' ' +
                map.get("DAY_OF_MONTH").toString() + ' ' +
                map.get("DAY_OF_WEEK").toString() + " *";
    }

    private boolean isValidForProcessing(Map<String, Object> map) {
        LOGGER.debug("Map of properties [{}], Server name: {}", map, System.getenv("SERVER_NAME"));
        Properties p = getConfigProperties();

        // Check if routes allowed on this server
        final String camelRouteActive = p.getProperty("camel.route.active");

        if (!"true".equalsIgnoreCase(camelRouteActive)) {
            return false;
        }

        // Check new settings
        boolean isEnabled = "1".equals(map.get("ENABLED").toString());
        boolean isTargetedToNode = map.get("TARGET_NODE_NAME").equals(System.getenv("SERVER_NAME"));
        return !(!isEnabled || !isTargetedToNode);
    }
}
