package com.db.nfr.tmr.integrator.persistence;

import java.nio.charset.Charset;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.nfr.tmr.integrator.persistence.common.CustomOracleTypes;
import com.db.nfr.tmr.integrator.quantstruts.common.MandateFile;
import com.db.nfr.tmr.integrator.quantstruts.common.MandateType;

/**
 * Created by tymkvit on 06/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DataSourceConfigTest.class, PkgLoadMandatesRepoDAOImpl.class, CustomOracleTypes.class/*,
TransactionManagersTestConfig.class*/})
@ActiveProfiles("integration-test")
public class PkgLoadMandatesRepoDAOImplTest {

    @Autowired
    private PkgLoadMandatesRepoDAOImpl pkgLoadMandatesRepoDAO;

    @Test
    public void loadMandateFile() throws Exception {
        // Given
        long fileSize = 10;
        String fileType = "pdf";

        // When
        MandateFile mandateFile = new MandateFile();
        // TODO: Implement mandate file builder
        mandateFile.setFileName("testMandateFileName");
        mandateFile.setFileDate(new Date());
        mandateFile.setFileSize(fileSize);
        mandateFile.setFileType(fileType);
        mandateFile.setFileImage("Test".getBytes(Charset.defaultCharset()));

        MandateFile.Mandate mandate = new MandateFile.Mandate();
        mandate.setMandateDate(new Date());
        mandate.setMandateType(MandateType.EQUITY.getCode());
        mandate.setTraderEmail("test@yahoo.com");

        mandateFile.setMandate(mandate);
        pkgLoadMandatesRepoDAO.loadMandateFile(mandateFile);

        // Then
        // assert that no exception
    }

}