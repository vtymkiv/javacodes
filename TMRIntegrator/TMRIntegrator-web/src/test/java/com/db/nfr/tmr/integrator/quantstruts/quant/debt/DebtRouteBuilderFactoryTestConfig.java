package com.db.nfr.tmr.integrator.quantstruts.quant.debt;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.db.nfr.tmr.integrator.quantstruts.common.FileRepoWorkbookProcessor;
import com.db.nfr.tmr.integrator.quantstruts.common.RecAllFilesProcessor;
import com.db.nfr.tmr.integrator.quantstruts.common.WorkbookConverter;

/**
 * Created by tymkvit on 09/02/2017.
 */
@Configuration
@Profile("integration-test")
public class DebtRouteBuilderFactoryTestConfig {

    @Bean(name = "testDebtRouteBuilderFactory")
    public DebtRouteBuilderFactory debtRouteBuilderFactory() {
        return new DebtRouteBuilderFactory();
    }

    @Bean(name = "recAllFilesProcessor")
    public RecAllFilesProcessor recAllFilesProcessor() {
        return new RecAllFilesProcessor();
    }

    @Bean(name = "fileRepoWorkbookProcessor")
    public FileRepoWorkbookProcessor fileRepoWorkbookProcessor() {
        return new FileRepoWorkbookProcessor();
    }

    @Bean(name = "workbookConverter")
    public WorkbookConverter workbookConverter() {
        return new WorkbookConverter();
    }


    @Bean(name = "debtProdProcessor")
    public DebtProdProcessor debtProdProcessor() {
        return new DebtProdProcessor();
    }

    @Bean(name = "debtMandateProcessor")
    public DebtMandateProcessor debtMandateProcessor() {
        return new DebtMandateProcessor();
    }


    @Bean(name = "debtSmallFilesProcessor")
    public DebtSmallFilesProcessor debtSmallFilesProcessor() {
        return new DebtSmallFilesProcessor();
    }
}
