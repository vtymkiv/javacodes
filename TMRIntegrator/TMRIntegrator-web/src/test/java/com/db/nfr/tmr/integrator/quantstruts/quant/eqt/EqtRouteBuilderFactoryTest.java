package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;

import com.db.nfr.tmr.integrator.persistence.DataSourceConfigTest;
import com.db.nfr.tmr.integrator.persistence.PkgFileRepoDAOImpl;
import com.db.nfr.tmr.integrator.persistence.PkgLoadMandatesRepoDAOImpl;
import com.db.nfr.tmr.integrator.persistence.common.BulkDAO;
import com.db.nfr.tmr.integrator.persistence.common.CustomOracleTypes;
import com.db.nfr.tmr.integrator.quantstruts.common.FileRepoStreamProcessor;
import com.db.nfr.tmr.integrator.quantstruts.common.RecAllFilesProcessor;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.CommonDeleteSmallFilesProcessor;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.CommonSmallFilesProcessor;

@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes =
        {
                DataSourceConfigTest.class,
                EqtRouteBuilderFactoryTestConfig.class,
                RecAllFilesProcessor.class,
                BulkDAO.class,
                FileRepoStreamProcessor.class,
                PkgFileRepoDAOImpl.class,
                CustomOracleTypes.class,
                EqtLoadProcessor.class,
                PkgLoadMandatesRepoDAOImpl.class,
                EqtAggregationStrategy.class,
                EqtProdLogProcessor.class,
                CommonSmallFilesProcessor.class,
                EqtDeleteSmallFilesProcessor.class,
                CommonDeleteSmallFilesProcessor.class
        },
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
@ActiveProfiles("integration-test")
public class EqtRouteBuilderFactoryTest {
    private final static String TRADER_2_RISK = "trader2risk_old.xlsx.old";
    private final static String FILE_DIR = System.getProperty("user.home");

    @Autowired
    private CamelContext camelContext;

    @Autowired
    @Qualifier("testDebtRouteBuilderFactory")
    private EqtRouteBuilderFactory eqtRouteBuilderFactory;

    @Test
    public void getCustomRouteBuilder() throws Exception {
        CamelContext camelContext = new DefaultCamelContext();
        camelContext.setStreamCaching(true);
        camelContext.disableJMX();
        camelContext.getStreamCachingStrategy().setSpoolDirectory(FILE_DIR);
        camelContext.getStreamCachingStrategy().setSpoolThreshold(64 * 1024);
        camelContext.getStreamCachingStrategy().setBufferSize(4 * 1024);
        camelContext.getStreamCachingStrategy().setRemoveSpoolDirectoryWhenStopping(false);
        camelContext.getStreamCachingStrategy().setAnySpoolRules(true);
        camelContext.getStreamCachingStrategy().setSpoolUsedHeapMemoryThreshold(30);


        Resource propertyResource = new ClassPathResource("TMRIntegrator/config.properties");
        Resource keyResource = new ClassPathResource("TMRIntegrator/QUANT_TM_prv.ppk");

        //TODO: Add assertions
        PropertiesComponent pc1 = new PropertiesComponent(String.valueOf(propertyResource));
        PropertiesComponent pc2 = new PropertiesComponent(String.valueOf(keyResource));
        pc1.setLocation("classpath:TMRIntegrator/config.properties");
        pc2.setLocation("classpath:TMRIntegrator/QUANT_TM_prv.ppk");
        camelContext.addComponent("properties", pc1);
        camelContext.addComponent("QUANT_TM_prv.ppk", pc2);


        camelContext.addRoutes(new RouteBuilder() {
                                   public void configure() {
                                       from("{{quantstruts.route.uri}}" + TRADER_2_RISK)
                                               .to("file://" + FILE_DIR);
                                   }
                               }
        );

        camelContext.start();
        Thread.sleep(100000);
        camelContext.stop();
    }

    @Test
    public void startEqtRouteBuilder() throws Exception {
        eqtRouteBuilderFactory.setCronExpr("52+*+*+*+*+*");
        eqtRouteBuilderFactory.setRouteId("Test EQT");

        camelContext.addRoutes(eqtRouteBuilderFactory.getRouteBuilder());
        camelContext.start();
        // Sleep for 1 hour
        Thread.sleep(4 * 60 * 60 * 1000);

        camelContext.stop();
        // Then
    }
}