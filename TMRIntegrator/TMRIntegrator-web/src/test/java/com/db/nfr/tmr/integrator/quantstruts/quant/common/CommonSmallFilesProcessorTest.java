package com.db.nfr.tmr.integrator.quantstruts.quant.common;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;

import com.db.nfr.tmr.integrator.persistence.PkgLoadMandatesRepoDAO;
import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;
import com.db.nfr.tmr.integrator.quantstruts.common.Header;
import com.db.nfr.tmr.integrator.quantstruts.common.MandateType;


import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by tymkvit on 14/03/2017.
 */
@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes = {CommonSmallFilesProcessor.class, CommonDeleteSmallFilesProcessor.class, DefaultCamelContext.class})
@ActiveProfiles("integration-test")
@IfProfileValue(name = "integration-test", value = "true")
public class CommonSmallFilesProcessorTest {
    @Autowired
    private CommonSmallFilesProcessor commonSmallFilesProcessor;
    @Autowired
    private CommonDeleteSmallFilesProcessor commonDeleteSmallFilesProcessor;

    @Value("${protocol}/${common-folder}/${debt-folder}?autoCreate=false&startingDirectoryMustExist=true" +
            "&maximumReconnectAttempts=1&noop=true&password=tm&disconnect=false&idempotentKey=$simple{file:absolute" +
            ".path}-$simple{file:size}-$simple{file:modified}&maxMessagesPerPoll=1000")
    private String ftpDebtSlaveFiles;

    @Value("${common-folder}/DEBT")
    private String debtFolder;

    @MockBean
    private ConsumerTemplate consumerTemplate;

    @MockBean
    private PkgLoadMandatesRepoDAO pkgLoadMandatesRepoDAO;

    @Autowired
    private CamelContext camelContext;

    @MockBean(answer = Answers.RETURNS_DEEP_STUBS)
    private TypeRecAllFiles typeRecAllFiles;

    @Test
    public void testProcessDebts() throws Exception {
        // Given
        Exchange exchange = mock(Exchange.class, RETURNS_DEEP_STUBS);
        when(exchange.getIn().getHeader(Header.REC_ALL_FILES, TypeRecAllFiles.class)).thenReturn(typeRecAllFiles);
        when(typeRecAllFiles.getFileDate().getTime()).thenReturn(LocalDateTime.now().toInstant(ZoneOffset.UTC)
                .toEpochMilli());

        // When
        commonSmallFilesProcessor.process(ftpDebtSlaveFiles, exchange, MandateType.DEBT);

        // Then

    }

}