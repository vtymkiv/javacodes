package com.db.nfr.tmr.integrator.persistence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.nfr.tmr.integrator.persistence.common.CustomOracleTypes;
import com.db.nfr.tmr.integrator.persistence.common.DebtMandate;
import com.db.nfr.tmr.integrator.persistence.common.EqtMandate;

/**
 * Created by tymkvit on 07/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DataSourceConfigTest.class, PkgStgMandatesRepoDAOImpl.class, CustomOracleTypes.class})
@ActiveProfiles("integration-test")
public class PkgStgMandatesRepoDAOImplTest {
    @Autowired
    private PkgStgMandatesRepoDAOImpl pkgStgMandatesRepoDAO;


    @Test
    public void cleanDebts() throws Exception {
        // Given

        // When
        pkgStgMandatesRepoDAO.cleanDebts();

        // Then
    }

    @Test
    public void cleanEqts() throws Exception {
        // Given

        // When
        pkgStgMandatesRepoDAO.cleanEqts();

        // Then
    }

    @Test
    public void populateDebts() throws Exception {
        // Given
        DebtMandate debtMandate = new DebtMandate();
        debtMandate.setBookFamily("TestDebtMandateBookFamily");
        debtMandate.setProductFamily("TestDebtMandateProductFamily");
        debtMandate.setTrader("TestDebtTrader");

        // When

        pkgStgMandatesRepoDAO.populateDebts(debtMandate);

        // Then
    }

    @Test
    public void populateEqts() throws Exception {
        // Given
        EqtMandate eqtMandate = new EqtMandate();
        eqtMandate.setBook("TestEqtMandateBook");
        eqtMandate.setRisk("TestEqtMandateRisk");
        eqtMandate.setTrader("TestEqtTrader");

        // When

        pkgStgMandatesRepoDAO.populateEqts(eqtMandate);

        // Then
    }

}