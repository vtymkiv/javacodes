package com.db.nfr.tmr.integrator.quantstruts.common;

import org.junit.Ignore;
import org.junit.Test;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by tymkvit on 10/02/2017.
 */
public class UtilsTest {

    @Test
    public void getEmailFromString() throws Exception {
        // Given
        String fileName = "DEBT_20170207_aaron.doyle_TMR.xlsx";
        // When
        String email = Utils.getEmailFromString(fileName);

        // Then
        assertThat(email, is("aaron.doyle@db.com"));
    }

    @Test
    @Ignore
    public void getEmailFromStringShouldThrowException() throws Exception {
        // Given
        String fileName = "DEBT_20170207aaron.doyle_TMR.xlsx";
        // When
        String email = Utils.getEmailFromString(fileName);

        // Then
        assertThat(email, is("aaron.doyle@db.com"));
    }

    @Test
    public void getExtFromFileName() throws Exception {
        // Given
        String fileName = "DEBT_20170207aaron.doyle_TMR.xlsx";
        // When
        String ext = Utils.getExtFromFileName(fileName);

        // Then
        assertThat(ext, is("xlsx"));
    }
}