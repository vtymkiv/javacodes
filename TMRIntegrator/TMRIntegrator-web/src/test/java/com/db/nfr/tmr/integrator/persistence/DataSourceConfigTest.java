package com.db.nfr.tmr.integrator.persistence;

import javax.naming.NamingException;
import javax.sql.DataSource;

import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import lombok.extern.slf4j.Slf4j;
import sun.misc.BASE64Encoder;


/**
 * Created by tymkvit on 02/02/2017.
 */
@Slf4j
@EnableConfigurationProperties
@Configuration
@Profile("integration-test")
@Qualifier("dataSourceConfigTest")
public class DataSourceConfigTest {

    @ConfigurationProperties(prefix = "data-sources.stg")
    @Primary
    @Bean(name = "stgFeedDS", destroyMethod = "")
    public DataSource stgFeedDataSource() throws NamingException {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        setPassword(dataSourceBuilder, "bmV3XzM0NTY=");
        return dataSourceBuilder.build();
    }


    @ConfigurationProperties(prefix = "data-sources.tms")
    @Bean(name = "tmsFeedDS", destroyMethod = "")
    public DataSource tmsDataSource() throws NamingException {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        setPassword(dataSourceBuilder, "UXdlcnR5MTIzNCM=");
        return dataSourceBuilder.build();
    }

    @ConfigurationProperties(prefix = "data-sources.adm")
    @Bean(name = "admAppDS", destroyMethod = "")
    public DataSource admDataSource() throws NamingException {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        setPassword(dataSourceBuilder, "UXdlcnR5MTIzNCM=");
        return dataSourceBuilder.build();
    }

    @Bean
    public JdbcTemplate admJdbcTemplate() throws NamingException {
        return new JdbcTemplate(admDataSource());
    }

    @Bean
    public SimpleJdbcCall admSimpleJdbcCall() throws NamingException {
        return new SimpleJdbcCall(admJdbcTemplate());
    }

    @Bean
    public JdbcTemplate tmsJdbcTemplate() throws NamingException {
        return new JdbcTemplate(tmsDataSource());
    }

    @Bean
    public SimpleJdbcCall tmsSimpleJdbcCall() throws NamingException {
        return new SimpleJdbcCall(tmsJdbcTemplate());
    }

    @Bean(name = "stgJdbcTemplate")
    public JdbcTemplate stgJdbcTemplate() throws NamingException, SQLException {
        return new JdbcTemplate(stgFeedDataSource());
    }

    @Bean(name = "stgSimpleJdbcCall")
    public SimpleJdbcCall stgSimpleJdbcCall() throws NamingException, SQLException {
        return new SimpleJdbcCall(stgJdbcTemplate());
    }


    private void setPassword(DataSourceBuilder dataSourceBuilder, String pwd) {
        Base64.Decoder mimeDecoder = java.util.Base64.getMimeDecoder();
        byte[] mimeDecoded = mimeDecoder.decode(pwd);
        String mimeDecodedString = new String(mimeDecoded, Charset.forName("UTF-8"));
        dataSourceBuilder.password(mimeDecodedString);
    }

    /**
     * Encode given password
     *
     * @param pwd
     * @return
     */
    private String encodePwd(String pwd) {
        BASE64Encoder encoder = new BASE64Encoder();
        return String.valueOf(encoder.encodeBuffer(pwd.getBytes(Charset.defaultCharset())));
    }


}



