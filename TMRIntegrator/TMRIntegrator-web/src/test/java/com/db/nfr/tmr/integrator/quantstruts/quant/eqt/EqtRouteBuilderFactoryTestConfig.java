package com.db.nfr.tmr.integrator.quantstruts.quant.eqt;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Created by tymkvit on 09/02/2017.
 */
@Configuration
@Profile("integration-test")
public class EqtRouteBuilderFactoryTestConfig {

    @Bean(name = "testDebtRouteBuilderFactory")
    public EqtRouteBuilderFactory eqtRouteBuilderFactory() {
        return new EqtRouteBuilderFactory();
    }

    @Bean(name = "eqtProdProcessor")
    public EqtProdProcessor eqtProdProcessor() {
        return new EqtProdProcessor();
    }

    @Bean(name = "eqtMandateProcessor")
    public EqtMandateProcessor eqtMandateProcessor() {
        return new EqtMandateProcessor();
    }


    @Bean(name = "eqtSmallFilesProcessor")
    public EqtSmallFilesProcessor eqtSmallFilesProcessor() {
        return new EqtSmallFilesProcessor();
    }

    @Bean(name = "eqtMandateLogProcessor")
    public EqtMandateLogProcessor eqtMandateLogProcessor() {
        return new EqtMandateLogProcessor();
    }

}
