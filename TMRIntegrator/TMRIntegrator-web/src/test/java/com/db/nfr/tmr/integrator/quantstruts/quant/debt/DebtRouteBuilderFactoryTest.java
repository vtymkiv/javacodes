package com.db.nfr.tmr.integrator.quantstruts.quant.debt;

import org.apache.camel.CamelContext;
import org.apache.camel.component.file.remote.SftpEndpoint;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.db.nfr.tmr.integrator.persistence.DataSourceConfigTest;
import com.db.nfr.tmr.integrator.persistence.PkgFileRepoDAOImpl;
import com.db.nfr.tmr.integrator.persistence.PkgLoadMandatesRepoDAOImpl;
import com.db.nfr.tmr.integrator.persistence.common.BulkDAO;
import com.db.nfr.tmr.integrator.persistence.common.CustomOracleTypes;
import com.db.nfr.tmr.integrator.quantstruts.common.FileRepoWorkbookProcessor;
import com.db.nfr.tmr.integrator.quantstruts.common.RecAllFilesProcessor;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.CommonDeleteSmallFilesProcessor;
import com.db.nfr.tmr.integrator.quantstruts.quant.common.CommonSmallFilesProcessor;

/**
 * Created by tymkvit on 08/02/2017.
 */
@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes =
        {
                DataSourceConfigTest.class,
                DebtRouteBuilderFactoryTestConfig.class,
                RecAllFilesProcessor.class,
                BulkDAO.class,
                FileRepoWorkbookProcessor.class,
                PkgFileRepoDAOImpl.class,
                CustomOracleTypes.class,
                DebtLoadProcessor.class,
                PkgLoadMandatesRepoDAOImpl.class,
                DebtProdProcessor.class,
                CommonSmallFilesProcessor.class,
                DebtDeleteSmallFilesProcessor.class,
                CommonDeleteSmallFilesProcessor.class,
                SftpEndpoint.class
        },
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
@ActiveProfiles("integration-test")
public class DebtRouteBuilderFactoryTest {

    @Autowired
    CamelContext camelContext;

    @Autowired
    private DebtRouteBuilderFactory debtRouteBuilderFactory;

    //TODO: Implement integration test for DebtRouter only
    @Test
    @Ignore
    public void getRouteBuilder() throws Exception {
        // Sleep for 8 minutes
        Thread.sleep(60 * 60 * 1000);
    }

    @Test
    public void startDebtRouteBuilder() throws Exception {
        debtRouteBuilderFactory.setCronExpr("33+*+*+*+*+*");
        debtRouteBuilderFactory.setRouteId("Test DEBT");

        camelContext.addRoutes(debtRouteBuilderFactory.getRouteBuilder());
        camelContext.start();
        // Sleep for 2 hour
        Thread.sleep(5 * 60 * 60 * 1000);

        camelContext.stop();
        // Then
    }
}