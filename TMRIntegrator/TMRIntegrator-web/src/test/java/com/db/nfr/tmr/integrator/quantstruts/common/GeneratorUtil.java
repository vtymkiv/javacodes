package com.db.nfr.tmr.integrator.quantstruts.common;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;

import org.junit.Test;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;

import lombok.extern.slf4j.Slf4j;
import sun.misc.BASE64Encoder;

/**
 * Created by tymkvit on 15/03/2017.
 */
@Slf4j
public final class GeneratorUtil {
    private String encodePwd(String pwd) {
        BASE64Encoder encoder = new BASE64Encoder();
        return String.valueOf(encoder.encodeBuffer(pwd.getBytes(Charset.defaultCharset())));
    }

    @Test
    public void generatePwd() throws IOException {
        encodeDecode("Test User", "password");
    }

    /**
     * Encode given password
     *
     * @param
     * @return
     */
    static void encodeDecode(final String msg, final String strToEncode) throws IOException {
        Base64.Encoder mimeEncoder = java.util.Base64.getMimeEncoder();
        Base64.Decoder mimeDecoder = java.util.Base64.getMimeDecoder();


        String mimeEncoded = mimeEncoder.encodeToString(strToEncode.getBytes(Charset.forName("UTF-8")));
        LOGGER.info("Encoded {} for {}: {}", strToEncode, msg, mimeEncoded);


        byte[] mimeDecoded = mimeDecoder.decode(mimeEncoded);
        String mimeDecodedString = new String(mimeDecoded, Charset.forName("UTF-8"));

        LOGGER.info("Decoded {} for {}: {}", strToEncode, msg, mimeDecodedString);
    }

    private void setPassword(DataSourceBuilder dataSourceBuilder, String pwd) {
        Base64.Decoder mimeDecoder = java.util.Base64.getMimeDecoder();
        byte[] mimeDecoded = mimeDecoder.decode(pwd);
        String mimeDecodedString = new String(mimeDecoded, Charset.forName("UTF-8"));
        dataSourceBuilder.password(mimeDecodedString);
    }
}


