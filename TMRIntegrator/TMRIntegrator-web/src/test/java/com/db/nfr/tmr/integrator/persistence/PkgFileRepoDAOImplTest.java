package com.db.nfr.tmr.integrator.persistence;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.nfr.tmr.integrator.persistence.common.CustomOracleTypes;
import com.db.nfr.tmr.integrator.quantstruts.common.FileInfo;
import com.db.nfr.tmr.integrator.quantstruts.common.FileRecord;


import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;

/**
 * Created by tymkvit on 02/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DataSourceConfigTest.class, PkgFileRepoDAOImpl.class, CustomOracleTypes.class})
@ActiveProfiles("integration-test")
public class PkgFileRepoDAOImplTest {

    @Autowired
    private PkgFileRepoDAO pkgFileRepoDAO;

    @Test
    @Rollback
    public void getNewFiles() throws Exception {
        // Given
        List<TypeRecAllFiles> existingFiles = new ArrayList<>();

        String fileName1 = "DEBT_QS_Data.xlsx";
        Date fileDate1 = new Date();
        int fileSize1 = 100;

        int feedConfigId = 58;

        TypeRecAllFiles file1 = new TypeRecAllFiles(feedConfigId, fileName1, fileDate1, fileSize1);
        existingFiles.add(file1);

        // When
        List<FileRecord> newFileList = pkgFileRepoDAO.checkFiles(existingFiles, FileRecord.class);

        // Then
        assertThat("List of files is null", newFileList, notNullValue());
        assertThat("List of files is empty", newFileList, hasSize(greaterThan(0)));
        assertThat("List of files is null", newFileList.get(0).getId(), greaterThan(0));
    }

    @Test
    public void getFileInfo() throws Exception {
        // Given
        int feedConfigId = 58;

        // When
        FileInfo fileInfo = pkgFileRepoDAO.getFileInfo(feedConfigId);

        // Then
        assertThat("File info is null", fileInfo, notNullValue());
    }

    @Test
    @Rollback
    public void loadFile() throws Exception {
        // Given
        FileRecord fileRecord = new FileRecord();
        fileRecord.setImageType("BLOB");
        fileRecord.setId(2727);
        fileRecord.setFileName("DEBT_QS_Data.xlsx");
        fileRecord.setFileImage("This is a another test and could be deleted".getBytes(Charset.defaultCharset()));

        // When
        pkgFileRepoDAO.loadFile(fileRecord);

        // Then
        // assert that exception has not been thrown
    }
}