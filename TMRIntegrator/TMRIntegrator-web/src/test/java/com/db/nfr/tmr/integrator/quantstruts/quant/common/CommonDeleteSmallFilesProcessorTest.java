package com.db.nfr.tmr.integrator.quantstruts.quant.common;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import com.db.nfr.tmr.integrator.persistence.TypeRecAllFiles;


import static org.mockito.Mockito.*;

/**
 * Created by tymkvit on 22/03/2017.
 */
@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes = {CommonDeleteSmallFilesProcessor.class, DefaultCamelContext.class})
public class CommonDeleteSmallFilesProcessorTest {
    @Value("${protocol}/${common-folder}/DEBT?autoCreate=false&startingDirectoryMustExist=true&disconnect=false" +
            "&maximumReconnectAttempts=1&${privateKeyUri}")
    private String debtSlaveFilesToDelete;

    @Value("${common-folder}/DEBT")
    private String debtFolder;

    @Autowired
    private CommonDeleteSmallFilesProcessor commonDeleteSmallFilesProcessor;

    @Autowired
    private CamelContext camelContext;

    @Test
    public void process() throws Exception {
        // Given
        TypeRecAllFiles typeRecAllFiles = mock(TypeRecAllFiles.class, RETURNS_DEEP_STUBS);
        long fileDate = 9490178582000L;
        when(typeRecAllFiles.getFileDate().getTime()).thenReturn(fileDate);
        // When
        commonDeleteSmallFilesProcessor.process(debtFolder, debtSlaveFilesToDelete, typeRecAllFiles);

        // Then
    }

}