{wikipedia-hateoas}[Hypermedia] is an important aspect of link:/understanding/REST[REST].
It allows you to build services that decouple client and server to a large extent and allow them to evolve independently.
The representations returned for REST resources contain not only data, but links to related resources.
Thus the design of the representations is crucial to the design of the overall service.

== What you'll build

You'll build a hypermedia-driven REST service with Spring HATEOAS, a library of APIs that you can use
to easily create links pointing to Spring MVC controllers,
build up resource representations, and control how they're rendered into supported hypermedia formats such as HAL.

The service will accept HTTP GET requests at:

    http://localhost:8080/tasks

and respond with a link:/understanding/JSON[JSON] representation of a greeting enriched with the simplest possible hypermedia element,
a link pointing to the resource itself:

[source,javascript]
----
[
  {
    "name": null,
    "state": null,
    "status": null,
    "type": null,
    "owner": null,
    "expireDate": null,
    "deskName": null,
    "alertType": null,
    "tradeDate": null,
    "subtasks": null,
    "links": [
      {
        "rel": "self",
        "href": "http://localhost:8080/tasks?page=1&limit=5&filterType=open&numOfRows=5"
      }
    ]
  }
]
----

The response already indicates you can customize the tasks with an optional 'page' and 'limit'
parameter
 in the
query string:

   http://localhost:8080/tasks?page=1&limit=5&filterType=open&numOfRows=5