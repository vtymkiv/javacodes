package com.db.rest;

import java.net.URI;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by vtymkiv on 06.09.2016.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskControllerImplTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private TaskControllerImpl taskController;

    @Test
    public void testCallGSSTokenService() throws Exception {
        // Given
        ResponseEntity<String> response = mock(ResponseEntity.class);

        String expectedToken = "Token";
        when(response.getBody()).thenReturn(expectedToken);

        when(restTemplate
                .exchange(any(URI.class), eq(HttpMethod.GET), Matchers.<HttpEntity<?>>any(), eq(String
                        .class))).thenReturn(response);

        // When
        HttpEntity<String> stringHttpEntity = taskController.callGSSTokenService();

        // Then
        assertThat(stringHttpEntity, is(notNullValue()));
        assertThat(stringHttpEntity.getBody(), is(expectedToken));
    }

}