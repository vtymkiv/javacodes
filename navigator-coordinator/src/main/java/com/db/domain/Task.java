package com.db.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Defines a task/control that is subject for fraudulent activity investigation.
 * <p>
 * Created by vtymkiv on 02.09.2016.
 */
@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class Task {
    private String name;
    private String state;
    private String status;
    private String type;
    private String owner;
    private String expireDate;
    private String deskName;
    private String alertType;
    private String tradeDate;

    private List<Task> subtasks;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getDeskName() {
        return deskName;
    }

    public void setDeskName(String deskName) {
        this.deskName = deskName;
    }

    public String getAlertType() {
        return alertType;
    }

    public void setAlertType(String alertType) {
        this.alertType = alertType;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public List<Task> getSubtasks() {
        return subtasks;
    }

    public void setSubtasks(List<Task> subtasks) {
        this.subtasks = subtasks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Task))
            return false;
        if (!super.equals(o))
            return false;

        Task task = (Task) o;

        if (!getName().equals(task.getName()))
            return false;
        if (!getState().equals(task.getState()))
            return false;
        if (!getStatus().equals(task.getStatus()))
            return false;
        if (!getType().equals(task.getType()))
            return false;
        if (!getOwner().equals(task.getOwner()))
            return false;
        if (getExpireDate() != null ? !getExpireDate().equals(task.getExpireDate()) : task
                .getExpireDate() != null)
            return false;
        if (!getDeskName().equals(task.getDeskName()))
            return false;
        if (!getAlertType().equals(task.getAlertType()))
            return false;
        if (!getTradeDate().equals(task.getTradeDate()))
            return false;
        return getSubtasks() != null ? getSubtasks().equals(task.getSubtasks()) : task.getSubtasks()
                == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getState().hashCode();
        result = 31 * result + getStatus().hashCode();
        result = 31 * result + getType().hashCode();
        result = 31 * result + getOwner().hashCode();
        result = 31 * result + (getExpireDate() != null ? getExpireDate().hashCode() : 0);
        result = 31 * result + getDeskName().hashCode();
        result = 31 * result + getAlertType().hashCode();
        result = 31 * result + getTradeDate().hashCode();
        result = 31 * result + (getSubtasks() != null ? getSubtasks().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", state='" + state + '\'' +
                ", status='" + status + '\'' +
                ", type='" + type + '\'' +
                ", owner='" + owner + '\'' +
                ", expireDate='" + expireDate + '\'' +
                ", deskName='" + deskName + '\'' +
                ", alertType='" + alertType + '\'' +
                ", tradeDate='" + tradeDate + '\'' +
                ", subtasks=" + subtasks +
                '}';
    }
}
