package com.db.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Created by vtymkiv on 02.09.2016.
 */
@Configuration
public class NavCoordinatorConfig {
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
