package com.db.rest.exceptions;

/**
 * This class presents information about what error was
 * happened during call of the rest web service.
 * <p>
 * Created by vtymkiv on 05.09.2016.
 */
public class ErrorInfo {
    public final String url;
    public final String errorDescription;

    public ErrorInfo(String url, Exception ex) {
        this.url = url;
        this.errorDescription = ex.getLocalizedMessage();
    }
}
