package com.db.rest.exceptions;

/**
 * Created by vtymkiv on 05.09.2016.
 */
public class WrongTaskIdSpecifiedException extends RuntimeException {

    /**
     * Constructor.
     *
     * @param errorMessage
     */
    public WrongTaskIdSpecifiedException(String errorMessage) {
        super(errorMessage);
    }


    /**
     * Constructor.
     *
     * @param cause
     * @param errorMessage
     */
    public WrongTaskIdSpecifiedException(Throwable cause, String errorMessage) {
        super(errorMessage, cause);
    }

}
