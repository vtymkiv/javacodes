package com.db.rest;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.db.rest.resources.TaskListResources;
import com.db.rest.resources.TaskResource;

/**
 * Created by vtymkiv on 02.09.2016.
 */
@Component
public interface TaskController {

    String TASKS = "/tasks";
    String DATE_FORMAT = "MM/dd/yyyy";

    /**
     * Get list of tasks
     *
     * @param page       The number of page to start from. By default it is 1.
     * @param limit      The number of tasks to be shown on the page. By default it is 5.
     * @param filterType The filter that will be applied to the list of tasks.
     *                   Possible values are:
     *                   open
     *                   delegated
     *                   closed
     *                   overdue
     *                   upcoming
     *                   calendar
     * @return List of tasks, List<Task> taskList
     */
    @RequestMapping(TASKS)
    HttpEntity<TaskListResources> tasks(
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "limit", required = false, defaultValue = "5") Integer limit,
            @RequestParam(value = "filterType", required = false, defaultValue = "open") String
                    filterType,
            @RequestParam(value = "numOfRows", required = false, defaultValue = "5") Integer numOfRows,
            @RequestParam(value = "startDate", required = false, defaultValue = "") @DateTimeFormat
                    (pattern = TaskControllerImpl.DATE_FORMAT) Date
                    startDate,
            @RequestParam(value = "endDate", required = false, defaultValue = "") @DateTimeFormat
                    (pattern = TaskControllerImpl.DATE_FORMAT) Date
                    endDate);

    /**
     * Get the details of the task.
     *
     * @param taskId
     * @return
     */
    @RequestMapping(TASKS + "/{taskId}")
    HttpEntity<TaskResource> task(@PathVariable String taskId);

    @RequestMapping(TASKS + "/{taskId}/disclosures")
    HttpEntity<TaskResource> taskDisclosures(@PathVariable String taskId);

    @RequestMapping(TASKS + "/{taskId}/disclosures/meta")
    HttpEntity<TaskResource> taskDisclosuresMeta(@PathVariable String taskId);

}
