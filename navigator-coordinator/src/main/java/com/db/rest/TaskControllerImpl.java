package com.db.rest;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.db.domain.Task;
import com.db.rest.exceptions.WrongTaskIdSpecifiedException;
import com.db.rest.resources.TaskListResources;
import com.db.rest.resources.TaskResource;


import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
@RestController
@ManagedResource(objectName = "com.db.rest.config:type=core,subType=restClient," +
        "name=taskControllerImpl")
public class TaskControllerImpl implements TaskController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskControllerImpl.class);

    @Resource(name = "restTemplate")
    private RestTemplate restTemplate;


    @Value("${rest-endpoints.CCA.endpoint}:${rest-endpoints.CCA.port}")
    String ccaEndpoint;

    @Value("${rest-endpoints.BPM.endpoint}:${rest-endpoints.BPM.port}")
    String bmpEndpoint;

    @Value("${rest-endpoints.GSS.endpoint}:${rest-endpoints.GSS.port}/${rest-endpoints.GSS.version}")
    String gssEndpoint;

    @Value("${rest-endpoints.GSS.token}?email=${rest-endpoints.GSS" +
            ".email}&application=${rest-endpoints.GSS.application}")
    String gssToken;

    @Value("${rest-endpoints.GSS.USER.login}")
    private String userName;
    @Value("${rest-endpoints.GSS.USER.pwd}")
    private String userPwd;


    @Value("${rest-endpoints.GSS.task-list-endpoints.task-list}")
    private String gssTasksLists;


    @Override
    public HttpEntity<TaskListResources> tasks(
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "limit", required = false, defaultValue = "5") Integer limit,
            @RequestParam(value = "filterType", required = false, defaultValue = "open") String
                    filterType,
            @RequestParam(value = "numOfRows", required = false, defaultValue = "5") Integer numOfRows,
            @RequestParam(value = "startDate", required = false, defaultValue = "") @DateTimeFormat
                    (pattern = DATE_FORMAT) Date
                    startDate,
            @RequestParam(value = "endDate", required = false, defaultValue = "") @DateTimeFormat
                    (pattern = DATE_FORMAT) Date
                    endDate) {

        LOGGER.info("Getting task list...");

        // Create resources
        TaskListResources resources = new TaskListResources();
        resources.add(linkTo(methodOn(TaskControllerImpl.class).tasks(page, limit, filterType, numOfRows,
                startDate, endDate))
                .withSelfRel());

        resources.add(linkTo(methodOn(TaskControllerImpl.class).callGSSTokenService()).withSelfRel());

        // TODO: Here should be call to rest end point from GSS and/or BMP.
        String taskListURL = gssEndpoint + gssTasksLists;
        LOGGER.info("URL to call to get token: {}", taskListURL);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(taskListURL);

        LOGGER.info("Call service to get token...", taskListURL);
        String token = callGSSTokenService().getBody();

        LOGGER.info("Received token: {}", token);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Authorization", String.valueOf(token));


        HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity<?> response = restTemplate.exchange(
                builder.build().encode().toUri(),
                HttpMethod.GET,
                entity,
                String.class);

        LOGGER.info("Response from {} :  {}", taskListURL, response);


        List<Task> taskList = new ArrayList<>();
        Task task = new Task();
        task.setAlertType("CASE");
        task.setState("Closed");

        taskList.add(task);
        resources.setTaskList(taskList);

        LOGGER.info("Task list was received {}", taskList);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @Override
    public HttpEntity<TaskResource> task(@PathVariable String taskId) {
        LOGGER.info("Getting task {} details..", taskId);

        if (!StringUtils.isNumeric(taskId)) {
            String errorMsg = String.format("Wrong task id %s was " +
                    "specified", taskId);
            // Log error then throw it
            LOGGER.error(errorMsg);
            // Throw an error
            throw new WrongTaskIdSpecifiedException(errorMsg);
        }
        TaskResource taskResource = new TaskResource();
        taskResource.add(linkTo((methodOn(TaskControllerImpl.class).task(taskId))).withSelfRel());

        return new ResponseEntity<>(taskResource, HttpStatus.OK);
    }

    @Override
    public HttpEntity<TaskResource> taskDisclosures(@PathVariable String taskId) {
        TaskResource taskResource = new TaskResource();
        Task task = new Task();
        task.setAlertType("ALERT");
        task.setDeskName("Desk");

        taskResource.setTask(task);
        taskResource.add(linkTo((methodOn(TaskControllerImpl.class).task(taskId))).withSelfRel());
        return new ResponseEntity<>(taskResource, HttpStatus.OK);
    }

    @Override
    public HttpEntity<TaskResource> taskDisclosuresMeta(@PathVariable String taskId) {
        return null;
    }

    @RequestMapping("/callGSSTokenService")
    public HttpEntity<String> callGSSTokenService() {
        LOGGER.info("Calling GSSTokenService...");

        String tokenURL = gssEndpoint + gssToken;
        LOGGER.info("URL to call to get token: {}", tokenURL);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(tokenURL);

       /* HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);*/

        /*String credentials = userName + ":" + userPwd;
        byte[] token =  getEncoder().encode(credentials.getBytes());
        headers.add("Authorization", "Basic " + String.valueOf(token));*/

        //HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.exchange(
                builder.build().encode().toUri(),
                HttpMethod.GET,
                null,
                String.class);

        LOGGER.info("Got the response {}", response.getBody());
        return response;
    }

    @ManagedAttribute
    public String getGssEndpoint() {
        return gssEndpoint;
    }

    @ManagedAttribute
    public void setGssEndpoint(String gssEndpoint) {
        this.gssEndpoint = gssEndpoint;
    }

    public String getGssToken() {
        return gssToken;
    }

    public void setGssToken(String gssToken) {
        this.gssToken = gssToken;
    }

    public String getGssTasksLists() {
        return gssTasksLists;
    }

    public void setGssTasksLists(String gssTasksLists) {
        this.gssTasksLists = gssTasksLists;
    }
}
