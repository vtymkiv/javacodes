package com.db.rest.resources;

import org.springframework.hateoas.ResourceSupport;

import com.db.domain.Task;

/**
 * Created by vtymkiv on 02.09.2016.
 */
public class TaskResource extends ResourceSupport {
    private Task task;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
