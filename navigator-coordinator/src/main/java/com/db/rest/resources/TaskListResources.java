package com.db.rest.resources;

import java.util.List;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;

import com.db.domain.Task;

/**
 * Created by vtymkiv on 02.09.2016.
 */
public class TaskListResources extends Resources<Resource<Task>> {
    private List<Task> taskList;

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }
}
