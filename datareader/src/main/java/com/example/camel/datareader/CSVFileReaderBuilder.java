package com.example.camel.datareader;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class CSVFileReaderBuilder extends RouteBuilder {
    public void configure() throws Exception {
        from("{{read.from}}").to("read.to");
    }
}
