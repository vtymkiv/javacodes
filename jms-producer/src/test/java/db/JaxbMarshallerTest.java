package db;

import db.utils.JaxbMarshaller;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.annotation.XmlRootElement;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by vtymkiv on 12.08.2016.
 */
public class JaxbMarshallerTest {
    private CITemplate ciTemplate;
    private Context context;
    private String ciTempXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
            "\t<CITemplate>\n" +
            "\t\t<context>\n" +
            "\t\t\t<system>CCA</system>\n" +
            "\t\t</context>\n" +
            "\t</CITemplate>";

    @Before
    public void setUp() throws Exception {
        ciTemplate = new CITemplate();
        context = new Context();
        ciTemplate.setContext(context);
        ciTemplate.getContext().setSystem("CCA");
    }

    @Test
    public void unmarshal() throws Exception {
        CITemplate ciTemplate = JaxbMarshaller.unmarshal(ciTempXml, CITemplate.class);
        assertThat(ciTemplate, is(notNullValue()));
    }

    @Ignore
    @Test
    public void marshal() throws Exception {
        String ciTemplateXml = JaxbMarshaller.marshal(ciTemplate, CITemplate.class);
        assertThat(ciTemplateXml, is(ciTempXml));
    }

    @XmlRootElement(name = "CITemplate")
    static private class CITemplate {
        private Context context;

        public void setContext(Context context) {
            this.context = context;
        }

        public Context getContext() {
            return context;
        }
    }


    static private class Context {
        private String system;

        public void setSystem(String system) {
            this.system = system;
        }

        public String getSystem() {
            return system;
        }
    }
}