package db.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Utils class for working with input and output for files
 * Created by vtymkiv on 12.08.2016.
 */
public final class IOUtils {
    /**
     * Private default constructor
     */
    private IOUtils() {
    }

    /**
     * Return text or xml file as one string
     *
     * @param path
     * @return
     * @throws IOException
     */
    public static String readFileAsString(Path path) throws IOException {
        StringBuilder sb = new StringBuilder(1024);
        Files.lines(path, StandardCharsets.UTF_8).forEach(s -> sb.append(s));
        return sb.toString();
    }
}
