package db.utils;

import org.apache.commons.io.IOUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Vitaliy Tymkiv on 18/08/2014.
 */
public class JaxbMarshaller {
    public static <T> T unmarshal(String xml, Class<T> unmarshalClass)
            throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(unmarshalClass);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        InputStream file = IOUtils.toInputStream(xml, "UTF-8");
        T object = (T) jaxbUnmarshaller.unmarshal(file);
        return object;
    }

    public static <T> String marshal(T object, Class<T> marshalClass) throws
            JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(marshalClass);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(object, stream);
        return stream.toString();
    }
}
