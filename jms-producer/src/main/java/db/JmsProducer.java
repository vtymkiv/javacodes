package db;

import com.sun.javafx.binding.StringFormatter;
import db.jms.producer.ProducerImpl;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.springframework.util.ResourceUtils.getFile;

/**
 * Produces JMS message that could be read any consumer
 */
@SpringBootApplication

public class JmsProducer {
    private static final Logger LOG = Logger.getLogger(JmsProducer.class);


    public static void main(String[] args) throws IOException {
        // Launch the application
        ConfigurableApplicationContext context = SpringApplication.run(JmsProducer.class, args);

        ProducerImpl producer = context.getBean(ProducerImpl.class);
        producer.sendDefaultMessage();

        producer.sendStockMessage("Stock Queue", "Bla bla bla", "NYSTC", 200);

        // Send xml file
        File file = getFile("classpath:example.xml");
        Path path = Paths.get(file.getAbsolutePath());

        if (!Files.exists(path)) {
            LOG.info(StringFormatter.format("No file %s found in %s", file.getName(), Paths.get(".").toAbsolutePath().toString()));
            return;
        }
        String xmlFileAsString = db.utils.IOUtils.readFileAsString(path);
        producer.sendMessage(xmlFileAsString);
    }


}
