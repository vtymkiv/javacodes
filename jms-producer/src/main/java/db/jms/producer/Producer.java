package db.jms.producer;

/**
 * Created by vtymkiv on 18.08.2016.
 */
public interface Producer {
    void sendMessage(final String message);

    void sendDefaultMessage();

    void sendStockMessage(String destination, String payload, String symbol, double price);
}
