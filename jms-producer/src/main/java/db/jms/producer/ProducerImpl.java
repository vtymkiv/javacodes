package db.jms.producer;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.TextMessage;

/**
 * Created by vtymkiv on 10.08.2016.
 */
@Component
public class ProducerImpl implements Producer {
    private static final Logger LOGGER = Logger.getLogger(ProducerImpl.class);

    private final JmsTemplate jmsTemplate;

    @Value("${destination}")
    private String destinationName;

    @Value("${custom.msg}")
    private String customMsg;

    @Autowired
    public ProducerImpl(final JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void sendMessage(final String message) {
        LOGGER.info("Sending a message: " + message);
        jmsTemplate.convertAndSend(destinationName, message);
    }

    @Override
    public void sendDefaultMessage() {
        sendMessage(customMsg);
    }

    @Override
    public void sendStockMessage(String destination, String payload, String symbol, double price) {
        MessageCreator messageCreator = session -> {
            TextMessage textMessage = session.createTextMessage("Bla bla");
            textMessage.setStringProperty("SYMBOL", symbol);
            textMessage.setDoubleProperty("PRICE", price);
            return textMessage;
        };
        jmsTemplate.send(destination, messageCreator);
    }
}
