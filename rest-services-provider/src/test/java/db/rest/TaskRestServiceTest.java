package db.rest;

import java.io.File;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import db.dto.ws.response.TaskListResponse;


import static db.rest.TaskRestService.BASE_URI;
import static db.rest.TaskRestService.TASK_LIST_URI;
import static db.rest.TaskRestService.UPLOAD_FILE_URI;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


/**
 * Testing the rest end points
 * <p>
 * Created by vtymkiv on 26.08.2016.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TaskRestServiceTest {


    //private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private String localPort;

    @Test
    public void testGetTaskList() throws Exception {
        String taskListRequest = new StringBuilder()
                .append(BASE_URI)

                .append(TASK_LIST_URI).toString();

        ResponseEntity<TaskListResponse> entity = this.restTemplate.getForEntity(taskListRequest,
                TaskListResponse.class);

        // Assert that response is Ok.
        assertThat(entity.getStatusCode(), is(HttpStatus.OK));

    }

    @Test
    public void testUploadFile() throws Exception {
        String uploadFileEndpoint = new StringBuilder()
                .append(BASE_URI)
                .append(UPLOAD_FILE_URI).toString();

        // Set the headers...
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(org.springframework.http.MediaType.MULTIPART_FORM_DATA);
        headers.setAccept(Arrays.asList(org.springframework.http.MediaType.TEXT_PLAIN));

        File file = new File(".", "temp-file-name.txt");
        //file.createNewFile();
    /*final FileDataBodyPart filePart = new FileDataBodyPart("file", file);
    FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
    final FormDataMultiPart multipart = (FormDataMultiPart) formDataMultiPart.bodyPart(filePart);*/


        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();

        parameters.add("file", new FileSystemResource(file)); // load file into parameter*/

        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);


        RestTemplate restTemplate = this.restTemplate.getRestTemplate();
        restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());


        ResponseEntity<String> responseEntity = restTemplate.exchange(uploadFileEndpoint, HttpMethod
                        .POST,
                entity, String.class);


        // Assert that response is Ok.
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));

    }

}