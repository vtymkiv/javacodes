package db.rest;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by vtymkiv on 29.08.2016.
 */
@Configuration
@ComponentScan(basePackageClasses = TaskRestService.class)
public class TestConfig {

}
