package db.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ConversionServiceFactoryBean;

/**
 * Created by vtymkiv on 19.08.2016.
 */
@Configuration
public class ConvertersConfig {
    /**
     * create conversionService bean
     */
    @Bean(name = "conversionService")
    public ConversionServiceFactoryBean createConverter() {
        return new ConversionServiceFactoryBean();
    }
}
