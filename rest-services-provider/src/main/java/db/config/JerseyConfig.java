package db.config;


import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import db.rest.TaskRestService;


/**
 * Created by vtymkiv on 16.08.2016.
 */
@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        // Register end points
        registerEndpoints();
    }

    private void registerEndpoints() {
        register(TaskRestService.class);
    }
}
