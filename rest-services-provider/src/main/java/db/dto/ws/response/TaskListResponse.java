package db.dto.ws.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import java.io.Serializable;
import java.util.List;

import db.domain.Task;

/**
 * * <b>Description:</b> This class represents the response of task list
 * <p>
 * Created by vtymkiv on 19.08.2016.
 */
@XmlRootElement(name = "taskList")
@XmlAccessorType(XmlAccessType.FIELD)
public class TaskListResponse implements Serializable {

    private List<Task> tasks;

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    @Override
    public String toString() {
        return "TaskListResponse{" +
                "tasks=" + tasks +
                '}';
    }
}
