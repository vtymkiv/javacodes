package db.services;

import java.util.List;

import org.springframework.stereotype.Component;

import db.domain.Task;

/**
 * Defines interface for working with tasks
 * <p>
 * Created by vtymkiv on 16.08.2016.
 */
@Component("taskService")
public interface TaskService {
    List<String> getTaskListNames();

    List<Task> getTaskList();

    void addTask(String taskName);
}
