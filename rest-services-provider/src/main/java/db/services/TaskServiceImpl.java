package db.services;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import db.domain.Task;


import static java.util.stream.Collectors.toList;

/**
 * Responsible for work with tasks like:
 * <pre>
 * <ul>
 *  <li>Adding task</li>
 *  <li>Closing task</li>
 *  <li>Completing task</li>
 *  <li>Getting list of tasks and so on..</li>
 * </ul>
 * </pre>
 * <p>
 * Created by vtymkiv on 16.08.2016.
 */
@Component
@Qualifier("taskService")
public class TaskServiceImpl implements TaskService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskServiceImpl.class);


    @Override
    public List<String> getTaskListNames() {
        LOGGER.info("Getting list of the tasks...");
        // Right now just a dummy staff
        return Arrays.asList("Task1", "Task2", "Task3");
    }

    @Override
    public List<Task> getTaskList() {
        return IntStream.range(1, 4).mapToObj(id -> new Task("Task_" + id, id, Task.Status.CLOSED, Task.State.READY_TO_FINISH)).collect(toList());
    }

    @Override
    public void addTask(String taskName) {
        LOGGER.info("Mimic of adding task {} to the list of tasks...", taskName);
    }
}
