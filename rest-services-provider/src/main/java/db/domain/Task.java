package db.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import java.io.Serializable;

/**
 * This is just for testing purpose.
 * <p>
 * This class should have corresponding class without any xml annotations and
 * for passing objects from and to rest endpoints the corresponding convertors should
 * exist.
 * <p>
 * Created by vtymkiv on 19.08.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Task", propOrder = {"name", "id", "status", "state"})
public class Task implements Serializable {
    @XmlElement
    private String name;
    @XmlElement
    private Integer id;
    @XmlElement
    private Status status;
    @XmlElement
    private State state;

    public Task() {
    }

    public Task(String name, Integer id, Status status, State state) {
        this.name = name;
        this.id = id;
        this.status = status;
        this.state = state;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "Status", propOrder = {"id", "name", "descr"})
    public enum Status {
        CLOSED("CLOSED", "Closed", "Task is closed");

        @XmlElement
        private String id;
        @XmlElement
        private String name;
        @XmlElement
        private String descr;

        Status(String id, String name, String description) {
            this.id = id;
            this.name = name;
            this.descr = description;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "State", propOrder = {"id", "name", "descr"})
    public enum State {
        READY_TO_FINISH("READY_TO_FINISH", "Ready to finish", "Task is ready to finish");

        @XmlElement
        private String id;
        @XmlElement
        private String name;
        @XmlElement
        private String descr;

        State(String id, String name, String description) {
            this.id = id;
            this.name = name;
            this.descr = description;
        }
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", status=" + status +
                ", state=" + state +
                '}';
    }
}
