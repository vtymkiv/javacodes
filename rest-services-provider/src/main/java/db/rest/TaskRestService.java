package db.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import db.dto.ws.response.TaskListResponse;
import db.services.TaskService;


import static db.rest.TaskRestService.BASE_URI;

//import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 * Created by vtymkiv on 16.08.2016.
 */

@Component
@RestController
@Path(BASE_URI)
public class TaskRestService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskRestService.class);
    public static final String BASE_URI = "/tasks";
    public static final String TASK_LIST_URI = "/getTaskList";
    public static final String UPLOAD_FILE_URI = "/upload";

/*
    @Autowired
    private ConversionService conversionService;
*/

    @Autowired
    @Qualifier("taskService")
    private TaskService taskService;

    @Path("/getTaskListNames")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getTaskListNames() {
        LOGGER.debug("Get task list data");
        return Response.ok(taskService.getTaskListNames()).build();
    }

    @Path(TASK_LIST_URI)
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getTaskList() {
        LOGGER.info("Get task list data...");
        // TODO: add conversion
        //TaskListResponse taskListResponse = conversionService.convert(taskService.getTaskList(), TaskListResponse.class);
        TaskListResponse taskListResponse = new TaskListResponse();
        taskListResponse.setTasks(taskService.getTaskList());
        //return Response.ok(taskService.getTaskList()).build();
        LOGGER.info("Response: {}", taskListResponse);
        return Response.ok(taskListResponse).build();
    }

    @Path("/addTask")
    @PUT
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response addTask(String taskName) {
        taskService.addTask(taskName);
        return Response.ok(taskName).build();
    }


    @Path(value = UPLOAD_FILE_URI)
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public Response uploadFiles(final @RequestParam("file") InputStream fileInputStream) {
        saveFile(fileInputStream, "uploaded1.txt");
        return Response.ok("Data uploaded successfully !!").build();
    }

    private void saveFile(InputStream file, String name) {
        try {
            /* Change directory path */
            java.nio.file.Path path = FileSystems.getDefault().getPath
                    ("/Volumes/Drive2/temp/file" + name);
            if (!Files.exists(path)) {
                Files.createDirectory(path);
            }

            /* Save InputStream as file */
            Files.copy(file, path, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

}
