package com.db.ftp.consumer;

import java.util.Collection;
import java.util.function.Consumer;

/**
 * This service is responsible for communication with ftp
 * Created by vtymkiv on 10.05.2017.
 */
public interface SftpClientService {
    boolean connect();

    void disconnect();

    Collection<FileDescription> listFiles(String pathToDir);

    <T> void downloadFile(String pathToFile, Consumer<T> fileConsumer);
}
