package com.db.ftp.consumer;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

/**
 * Created by vtymkiv on 10.05.2017.
 */
@Log4j2
@Data
public class FileDescription {
    private String fileName;
    private long fileSize;
    private long fileModificationTime;
}
