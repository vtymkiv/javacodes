package home.org.zipper;

import home.org.zipper.exceptions.FileProcessorException;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileProcessorImplTest {
    private FileProcessor fileProcessor;
    @org.junit.jupiter.api.BeforeEach

    void setUp() {
        this.fileProcessor = new FileProcessorImpl();
    }

    @Test
    public void compressFileListTest() throws IOException, FileProcessorException {
            this.fileProcessor.compressFileList(Files.list(Paths.get("data")));
            // Assert that directory contains only zipped filesØØ
    }

    @Test
    public void predicateTest() {
        List<String> rowList = Arrays.asList(
                                                "val,1,col3_val1,7",
                                                "val,0,col3_val1,1",
                                                "val,2,col3_val1,2",
                                                "val,3,col3_val1,3",
                                                "val,1,col3_val1,4",
                                                "val,7,col3_val1,7"
                                            );
        for(String row : rowList) {
            String[] dataArray = row.split(",");
            if(Integer.parseInt(dataArray[1]) == 1 || Integer.parseInt(dataArray[1]) == 7 ){
                continue;
            }

            System.out.println(row);
        }

        System.out.println("\nAnother approach\n");
        List<String> rowLst = rowList.stream()
                .map(row -> row.split(",", -1))
                .filter(dataArray -> Integer.parseInt(dataArray[1]) != 1 && Integer.parseInt(dataArray[1]) != 7)
                .map(x -> String.join(",", x))
                .collect(Collectors.toList());
        rowLst.forEach(System.out::println);
    }
}