package home.org.zipper;

import home.org.zipper.exceptions.FileProcessorException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

@Slf4j
public class FileProcessorImpl implements FileProcessor {
    @Override
    public void compressFileList(List<String> fileList) throws FileProcessorException {
        if(CollectionUtils.isEmpty(fileList)) {
            throw new FileProcessorException("List of files to compress is empty.");
        }

        log.info("Compressing files: {}", fileList);
    }

    @Override
    public void compressFileList(Stream<Path> fileList) throws FileProcessorException {

    }
}
