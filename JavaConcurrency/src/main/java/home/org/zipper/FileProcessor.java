package home.org.zipper;

import home.org.zipper.exceptions.FileProcessorException;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

public interface FileProcessor {
    void compressFileList(List<String> fileList) throws FileProcessorException;
    void compressFileList(Stream<Path> fileList) throws FileProcessorException;
}
