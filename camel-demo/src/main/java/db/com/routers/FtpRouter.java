package db.com.routers;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.file.GenericFile;
import org.apache.camel.component.file.GenericFileMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Route builder for getting files from ftp.
 * We need to get files from ftp based on some criteria.
 * List files which timestamp is less or equal to master file.
 * Created by admin on 2/4/2017.
 */
@Component
public class FtpRouter extends RouteBuilder {
    private static final Logger LOGGER = LoggerFactory.getLogger(FtpRouter.class);

    @Override
    public void configure() throws Exception {
        // 02/05/17 13:49
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy MM dd HH:mm");
        String text = "2017 02 05 15:33";
        LocalDateTime masterFileDate = LocalDateTime.parse(text, formatter);

        // from("{{ftp.route.from.master}}")
        from("{{ftp.route.from}}")
                // Set the timestamp of master file into the header
                .process(exchange -> {
                            GenericFile genericFile = exchange.getIn().getBody(GenericFileMessage.class).getGenericFile();
                            LOGGER.info("File name: {}, last modified date {}, master modified time: {}", genericFile.getFileName(), genericFile.getLastModified(), masterFileDate.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());

//                            Map<String, Object> outHeaders = exchange.getOut().getHeaders();
//                            outHeaders.put("fileLastModifiedDate", genericFile.getLastModified());
//                            if (genericFile.getFileName().equalsIgnoreCase("masterFile.txt")) {
//                                outHeaders.put("masterFileLastModifiedDate", genericFile.getLastModified());
//                            }

                        }
                )
                // Add filtering files
                //.process(filterFilesProcess)
                // .filter(header("fileLastModifiedDate").isLessThanOrEqualTo(masterFileDate.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()))
                .filter(header("CamelFileLastModified").isLessThanOrEqualTo(masterFileDate.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()))
                .to("{{file.route.to}}")
                .log("Downloaded file ${file:name} complete.");
    }
}
