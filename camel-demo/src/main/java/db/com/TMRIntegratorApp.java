package db.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TMRIntegratorApp {

    public static void main(String[] args) {
        SpringApplication.run(TMRIntegratorApp.class, args);
    }
}
