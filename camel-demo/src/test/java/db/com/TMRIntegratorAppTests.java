package db.com;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TMRIntegratorApp.class})
public class TMRIntegratorAppTests {

    @Test
    public void contextLoads() throws InterruptedException {
        Thread.sleep(15 * 1000);
    }

}
