package db.jms.consumer;

import org.apache.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

/**
 * Created by vtymkiv on 12.08.2016.
 */
@Component
public class Consumer {
    private static final Logger LOGGER = Logger.getLogger(Consumer.class);

    @JmsListener(destination = "${destination}")
    @SendTo("status")
    public String processMessage(String data) {
        LOGGER.info(data);
        return "Xml was processed";
    }

}
