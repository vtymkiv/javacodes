package db.com.example.bigfilereader;

public enum ReaderFileType {
    XML,
    CSV
}
