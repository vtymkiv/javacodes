package db.com.example.bigfilereader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BigfilereaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(BigfilereaderApplication.class, args);
    }
}
