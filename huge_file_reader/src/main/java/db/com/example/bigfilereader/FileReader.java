package db.com.example.bigfilereader;

public interface FileReader {
    void readFile(String pathToFile, ReaderFileType fileType);
}
