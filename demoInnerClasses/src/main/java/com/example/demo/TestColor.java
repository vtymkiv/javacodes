package com.example.demo;

import lombok.ToString;


abstract class Shape {
    @ToString(of = {"red", "green", "blue"}, doNotUseGetters = true)
    public static class Color {
        private int red;
        private int green;
        private int blue;

        public Color() {
            // call the other overloaded Color constructor by passing default values
            this(0, 0, 0);
        }

        public Color(int red, int green, int blue) {
            this.red = red;
            this.green = green;
            this.blue = blue;
        }

        // other color members elided
    }

    @ToString(of = {"red", "green", "blue"}, doNotUseGetters = true)
    protected static class Color1 {
        private int red;
        private int green;
        private int blue;

        public Color1(int red, int green, int blue) {
            this.red = red;
            this.green = green;
            this.blue = blue;
        }
    }


    // other Shape members elided
}

public class TestColor {
    public static void main(String[] args) {
        // since Color is a static nested class,
        // we access it using the name of the outer class, as in Shape.Color
        // note that we do not (and cannot) instantiate Shape class for using Color class
        Shape.Color white = new Shape.Color(255, 255, 255);
        System.out.println("White color has values: " + white);

        Shape.Color1 black = new Shape.Color1(0, 0, 0);
        System.out.println("Black color has values: " + black);
    }
}
