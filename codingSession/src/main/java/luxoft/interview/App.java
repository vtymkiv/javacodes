package luxoft.interview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public class App {
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();
        Student student1 = new Student("Petya", Arrays.asList("math", "biology"));
        Student student2 = new Student("Vasya", Arrays.asList("literature", "history"));
        Student student3 = new Student("Pavlo", Arrays.asList("history", "biology"));
        Student student4 = new Student("Igor", Arrays.asList("math", "history"));
        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);
        studentList.add(student4);

        List<String> collectedUniqueSubjects = studentList.stream().map(Student::getSubjectList).flatMap(Collection::stream).distinct().sorted().collect(Collectors.toList());

        System.out.println(studentList.stream().map(Student::getSubjectList).distinct().collect(Collectors.toList()));
        System.out.println(collectedUniqueSubjects);

    }
}
