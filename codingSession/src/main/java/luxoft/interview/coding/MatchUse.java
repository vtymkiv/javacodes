package luxoft.interview.coding;

import java.util.Arrays;

public class MatchUse {
    public static void main(String[] args) {
        // Average temperatures in Concordia, Antarctica in a week in October 2015
        int numbers[] = {-56, -57, -55, -52, -48, -51, -49};
        boolean anyMatch = Arrays.stream(numbers).anyMatch(num -> num > 0);
        System.out.println("anyMatch(num -> num > 0): " + anyMatch);

        boolean allMatch = Arrays.stream(numbers).allMatch(num -> num > 0);
        System.out.println("allMatch(num -> num > 0): " + allMatch);

        boolean noneMatch = Arrays.stream(numbers).noneMatch(num -> num > 0);
        System.out.println("noneMatch(num -> num > 0): " + noneMatch);
    }
}
