package com.db.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by vtymkiv on 04.10.2016.
 */
@ConfigurationProperties(locations = "classpath:/scheduler/quartz-scheduler.yml")
public class SchedulerConfigurationProperties {
    private DbSchedule dbSchedule;
    private String sqlForConfig;


    public static class DbSchedule {
        private String schema;
        private String schedulerTableName;
        private String appName;
        private String appSubName;


        public String getSchema() {
            return schema;
        }

        public void setSchema(String schema) {
            this.schema = schema;
        }

        public String getSchedulerTableName() {
            return schedulerTableName;
        }

        public void setSchedulerTableName(String schedulerTableName) {
            this.schedulerTableName = schedulerTableName;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public String getAppSubName() {
            return appSubName;
        }

        public void setAppSubName(String appSubName) {
            this.appSubName = appSubName;
        }
    }

    public DbSchedule getDbSchedule() {
        return dbSchedule;
    }

    public void setDbSchedule(DbSchedule dbSchedule) {
        this.dbSchedule = dbSchedule;
    }

    public String getSqlForConfig() {
        return sqlForConfig;
    }

    public void setSqlForConfig(String sqlForConfig) {
        this.sqlForConfig = sqlForConfig;
    }

}
