package com.db.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by vtymkiv on 04.10.2016.
 */
@ConfigurationProperties(locations = "classpath:/scheduler/quartz-scheduler.yml", prefix = "quartz")
public class QuartzConfigurationProperties {
    private GssReport gssReport;

    public static class GssReport {
        private String jobGroup;
        private String jobName;
        private String jobCronTriggerPrefix;
        private String jobCronTriggerName;
        private String defaultCronExpr;

        public String getJobGroup() {
            return jobGroup;
        }

        public void setJobGroup(String jobGroup) {
            this.jobGroup = jobGroup;
        }

        public String getJobName() {
            return jobName;
        }

        public void setJobName(String jobName) {
            this.jobName = jobName;
        }

        public String getJobCronTriggerPrefix() {
            return jobCronTriggerPrefix;
        }

        public void setJobCronTriggerPrefix(String jobCronTriggerPrefix) {
            this.jobCronTriggerPrefix = jobCronTriggerPrefix;
        }

        public String getJobCronTriggerName() {
            return jobCronTriggerName;
        }

        public void setJobCronTriggerName(String jobCronTriggerName) {
            this.jobCronTriggerName = jobCronTriggerName;
        }

        public String getDefaultCronExpr() {
            return defaultCronExpr;
        }

        public void setDefaultCronExpr(String defaultCronExpr) {
            this.defaultCronExpr = defaultCronExpr;
        }
    }

    public GssReport getGssReport() {
        return gssReport;
    }

    public void setGssReport(GssReport gssReport) {
        this.gssReport = gssReport;
    }
}
