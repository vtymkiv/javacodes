package com.db.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by vtymkiv on 04.10.2016.
 */

@Configuration
@EnableConfigurationProperties(value = {SchedulerConfigurationProperties.class, QuartzConfigurationProperties.class})
public class SchedulerConfiguration {
}
