package com.db.config;

import com.db.scheduler.jobs.GenerateGSSReportJob;
import com.db.scheduler.jobs.ReportService;
import com.db.scheduler.jobs.ReportServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vtymkiv on 04.10.2016.
 */
@Configuration
@ComponentScan("com.db")
@EnableConfigurationProperties(SchedulerConfigurationProperties.class)
public class QuartzConfiguration {
    @Autowired
    private QuartzConfigurationProperties quartzConfigurationProperties;

    @Bean
    public JobDetailFactoryBean jobDetailFactoryBean() {
        JobDetailFactoryBean factory = new JobDetailFactoryBean();
        factory.setJobClass(GenerateGSSReportJob.class);

        Map<String, Object> map = new HashMap<>();
        map.put("reportService", reportService());
        factory.setJobDataAsMap(map);
        factory.setGroup(quartzConfigurationProperties.getGssReport().getJobGroup());
        factory.setName(quartzConfigurationProperties.getGssReport().getJobName());
        return factory;
    }

    //Job is scheduled after every 1 minute
    @Bean
    public CronTriggerFactoryBean cronTriggerFactoryBean() {
        CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
        stFactory.setJobDetail(jobDetailFactoryBean().getObject());
        stFactory.setStartDelay(0);
        stFactory.setGroup(quartzConfigurationProperties.getGssReport().getJobGroup());
        stFactory.setCronExpression(quartzConfigurationProperties.getGssReport().getDefaultCronExpr());
        return stFactory;
    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean() {
        return new SchedulerFactoryBean();
    }

    @Bean
    public ReportService reportService() {
        return new ReportServiceImpl();
    }
}
