package com.db.scheduler.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by vtymkiv on 04.10.2016.
 */
@Component
public class ReportServiceImpl implements ReportService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceImpl.class);

    @Override
    public String generateGssReport() {
        LOGGER.info("Generating GSS Report");
        return "Ok.";
    }
}
