package com.db.scheduler.jobs;

import org.springframework.stereotype.Component;

/**
 * Created by vtymkiv on 05.10.2016.
 */
@Component
public interface ReportService {
    String generateGssReport();
}
