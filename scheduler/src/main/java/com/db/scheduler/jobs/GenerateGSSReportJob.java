package com.db.scheduler.jobs;

import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by vtymkiv on 04.10.2016.
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Component
public class GenerateGSSReportJob extends QuartzJobBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(GenerateGSSReportJob.class);
    private ReportService reportService;
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS MM/dd/yyyy");

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        long startJobTime = System.currentTimeMillis();
        LOGGER.info("Start to run job at {}.", sdf.format(new Date(startJobTime)));


        reportService.generateGssReport();

        long endJobTime = System.currentTimeMillis();
        LOGGER.info("Finished running job at {}.  Running the job took {} ms.\n", sdf.format(new Date
                (endJobTime)), endJobTime - startJobTime);

    }
}
