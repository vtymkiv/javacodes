package com.db.scheduler.config;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.db.config.SchedulerConfigurationProperties;

/**
 * Created by vtymkiv on 04.10.2016.
 */
@Component
public class SchedulerConfigRepoImpl implements SchedulerConfigRepo {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerConfigRepoImpl.class);

    @Autowired
    private SchedulerConfigurationProperties schedulerConfigurationProperties;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public SchedulerConfig getConfiguration() {
        LOGGER.debug("Getting configuration for running job.");

        //MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        //sqlParameterSource.addValue("tableName", new (schedulerConfigTableName));
        //sqlParameterSource.addValue("appName", appName);
        //sqlParameterSource.addValue("subName", appSubName);
        //sqlParameterSource.addValue("targetNodeName", System.getenv().get("SERVER_NAME"));
        String sql = schedulerConfigurationProperties.getSqlForConfig();

        LOGGER.debug("Sql: {}", sql);

        List<SchedulerConfig> schedulerConfigList = namedParameterJdbcTemplate.query(sql, new RowMapper<SchedulerConfig>() {
            private SchedulerConfigImpl schedulerConfig = new SchedulerConfigImpl();

            @Override
            public SchedulerConfig mapRow(ResultSet rs, int rowNum) throws SQLException {
                schedulerConfig.setId(rs.getInt("id"));
                schedulerConfig.setTargetNode(rs.getString("target_node_name"));
                schedulerConfig.setCronExpr(rs.getString("feed_config_cron"));
                return schedulerConfig;
            }
        });
        return schedulerConfigList.get(0);
    }
}
