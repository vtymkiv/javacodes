package com.db.scheduler.config;

/**
 * This is configuration for scheduler.
 * It includes scheduler id and time when scheduler
 * should start the task. In this case gss report.
 * <p>
 * Created by vtymkiv on 04.10.2016.
 */
public class SchedulerConfigImpl implements SchedulerConfig {
    private String cronExpr;
    private String targetNode;
    private int id;

    @Override
    public int getTaskId() {
        return id;
    }

    @Override
    public String getCronExpression() {
        return cronExpr;
    }

    public String getCronExpr() {
        return cronExpr;
    }

    public void setCronExpr(String cronExpr) {
        this.cronExpr = cronExpr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTargetNode() {
        return targetNode;
    }

    public void setTargetNode(String targetNode) {
        this.targetNode = targetNode;
    }
}
