package com.db.scheduler.config;

import org.springframework.stereotype.Component;

/**
 * Created by vtymkiv on 04.10.2016.
 */
@Component
public interface SchedulerConfigRepo {
    SchedulerConfig getConfiguration();
}
