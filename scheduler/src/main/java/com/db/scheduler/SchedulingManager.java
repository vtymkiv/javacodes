package com.db.scheduler;

import com.db.config.QuartzConfigurationProperties;
import com.db.scheduler.config.SchedulerConfig;
import com.db.scheduler.config.SchedulerConfigRepo;
import com.db.scheduler.jobs.ReportServiceImpl;
import org.quartz.*;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.util.List;

import static org.quartz.TriggerKey.triggerKey;

/**
 * <pre>
 * Description:</b>
 * Responsible for starting task on schedule.
 * <p/>
 * <b>The jobs that will be started:</b>
 * {@link com.db.scheduler.jobs.GenerateGSSReportJob}
 *
 * Created by Vitaliy Tymkiv on 04.10.2016.
 * </pre>
 */
@Component
public class SchedulingManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchedulingManager.class);

    @Autowired
    private QuartzConfigurationProperties quartzConfigurationProperties;

    // Repository where the information about scheduling is kept.
    @Autowired
    private SchedulerConfigRepo schedulerConfigRepo;

    @Autowired
    private ReportServiceImpl reportService;

    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;

    @Autowired
    private CronTriggerFactoryBean cronTriggerFactoryBean;

    @Autowired
    private JobDetailFactoryBean jobDetailFactoryBean;

    /**
     * Start task based on configuration.
     *
     * @param task            Task to start
     * @param schedulerConfig Configuration used to start task
     */
    public void schedule(Runnable task, SchedulerConfig schedulerConfig) {
        Assert.notNull(task, "task cannot be null");
        Assert.notNull(schedulerConfig, "schedulerConfig cannot be null");
    }

    /**
     * Reschedule job to run in different time.
     */
    public void rescheduleJob() {
        // Get information when task to run from database repository
        SchedulerConfig configuration = schedulerConfigRepo.getConfiguration();
        Runnable task = new Runnable() {

            @Override
            public void run() {
                reportService.generateGssReport();
            }
        };

        schedule(task, configuration);
    }


    /**
     * Reschedule job to run based on schedule specified in repository.
     */
    public void rescheduleQuartzJob() throws SchedulerException, ParseException {
        // Get information when task to run from database repository
        rescheduleQuartzJob(schedulerConfigRepo.getConfiguration());
    }

    /**
     * Start task based on configuration.
     *
     * @param configuration Configuration used to start task
     */
    public void rescheduleQuartzJob(final SchedulerConfig configuration) throws SchedulerException, ParseException {
        // We cannot use here the assertion because it will fire exception and stop process
        if (configuration == null || !StringUtils.hasText(configuration.getCronExpression())) {
            LOGGER.warn("Report generation will not be rescheduled because there is no data in db for rescheduling. The report generating would be run as scheduled previously.");
            return;
        }
        rescheduleQuartzJob(configuration.getTaskId(), configuration.getCronExpression());
    }

    /**
     * Reschedule job to run in different time.
     */
    public void rescheduleQuartzJob(final int cronId, final String cronExpr) throws SchedulerException, ParseException {
        Assert.hasText(cronExpr, "cronExp cannot be empty.");
        LOGGER.info("Rescheduling gss reporting job. The job will run based on the following schedule: {}", cronExpr);
        String jobGroup = quartzConfigurationProperties.getGssReport().getJobGroup();
        String jobName = quartzConfigurationProperties.getGssReport().getJobName();
        String jobTriggerName = quartzConfigurationProperties.getGssReport().getJobCronTriggerName() + String.valueOf(cronId);
        Scheduler scheduler = schedulerFactoryBean.getScheduler();

        CronTrigger newTrigger = getNewCronTrigger(cronExpr, jobTriggerName);

        List<? extends Trigger> triggersOfJob = scheduler.getTriggersOfJob(JobKey.jobKey(jobName, jobGroup));
        if (triggersOfJob.isEmpty() || !triggersOfJob.contains(newTrigger)) {
            scheduleQuartzJob(cronId, cronExpr);
        } else {
            Trigger oldTrigger = scheduler.getTrigger(triggerKey(jobTriggerName, jobGroup));
            scheduler.rescheduleJob(oldTrigger.getKey(), newTrigger);
        }
    }

    public void scheduleQuartzJob(final int cronId, final String cronExpr) throws SchedulerException, ParseException {
        Assert.hasText(cronExpr, "cronExp cannot be empty.");
        LOGGER.info("Rescheduling gss reporting job. The job will run based on the following schedule: {}", cronExpr);
        String jobTriggerName = quartzConfigurationProperties.getGssReport().getJobCronTriggerName() + String.valueOf(cronId);
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        CronTrigger newTrigger = getNewCronTrigger(cronExpr, jobTriggerName);
        scheduler.scheduleJob(jobDetailFactoryBean.getObject(), newTrigger);

    }

    private CronTrigger getNewCronTrigger(String cronExpr, String jobTriggerName) throws ParseException {
        CronTrigger newTrigger = cronTriggerFactoryBean.getObject();
        ((CronTriggerImpl) newTrigger).setCronExpression(cronExpr);
        ((CronTriggerImpl) newTrigger).setName(jobTriggerName);
        return newTrigger;
    }
}
