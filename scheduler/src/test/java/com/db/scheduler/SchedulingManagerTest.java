package com.db.scheduler;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by vtymkiv on 05.10.2016.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SchedulingManagerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchedulingManagerTest.class);
    @Autowired
    private SchedulingManager schedulingManager;

    @Test
    public void schedule() throws Exception {

    }

    @Test
    public void rescheduleQuartzJob() throws Exception {
        schedulingManager.rescheduleQuartzJob();
    }

    @Test
    public void rescheduleQuartzJobWithCronExpr() throws Exception {
        schedulingManager.rescheduleQuartzJob(1, "0/2 * * * * ?");
        LOGGER.info("Start running jobs with default scheduler.");
        LOGGER.info("######################\n");
        Thread.currentThread().sleep(5 * 1000);

        schedulingManager.rescheduleQuartzJob(1, "0/10 * * * * ?");

        LOGGER.info("######################\n");

        Thread.currentThread().sleep(20 * 1000);
        LOGGER.info("Rescheduled running jobs.");
        LOGGER.info("######################\n");

        Thread.currentThread().sleep(12 * 60 * 1000);
        LOGGER.info("######################\n");
    }

}