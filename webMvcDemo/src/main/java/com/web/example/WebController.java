package com.web.example;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by vtymkiv on 14.12.2016.
 */
@Controller
public class WebController {

    @RequestMapping(value = "/")
    public String index(Model model) {
        model.addAttribute("name", "Test 123");
        model.addAttribute("blogTitle", "Test blog title.");
        model.addAttribute("message", "Test message.");
        model.addAttribute("message", "Test message.");
        //model.addAttribute("statics", new BeansWrapperBuilder(Configuration.getVersion()).build().getStaticModels());
        return "index";
    }

    @RequestMapping(value = "/ftl")
    public String testFtl(Model model) {
        model.addAttribute("blogTitle", "Test blog title.");
        model.addAttribute("message", "Test message.");
        model.addAttribute("message", "Test message.");
        //model.addAttribute("statics", new BeansWrapperBuilder(Configuration.getVersion()).build().getStaticModels());
        return "blog-template";
    }
}
