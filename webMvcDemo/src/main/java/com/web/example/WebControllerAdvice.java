package com.web.example;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.template.Configuration;

/**
 * Created by vtymkiv on 16.12.2016.
 */
@ControllerAdvice
public class WebControllerAdvice {

    public static final String STATICS = "statics";
    public static final String ENUMS = "enums";

    @ModelAttribute
    public Model getStaticsAttr(Model model) {
        model.addAttribute(STATICS, new BeansWrapperBuilder(Configuration.getVersion()).build().getStaticModels());
        return model;
    }

    @ModelAttribute
    public Model getEnums(Model model) {
        model.addAttribute(ENUMS, new BeansWrapperBuilder(Configuration.getVersion()).build().getEnumModels());
        return model;
    }
}
