package com.web.example;

/**
 * Created by vtymkiv on 15.12.2016.
 */
public final class Utils {
    private Utils() {
    }

    public static String getGreeting() {
        return "Greeting";
    }

    public static boolean isRunning() {
        return true;
    }
}

