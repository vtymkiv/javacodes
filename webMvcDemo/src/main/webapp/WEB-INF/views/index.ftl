<html>
<head><title> ${blogTitle} </title>
<body>
<h1> ${blogTitle} </h1>
<p>
    ${message}
    <#if statics['com.web.example.Utils'].isRunning() >
        Hello statics
    </#if>
</p>
<#if references??>
    <h3>References</h3>
    <#list references>
        <#items as reference>
            ${reference_index + 1}. <a href="${reference.url}"> ${reference.title} </a> <br/>
        </#items>
    </#list>
</#if>
<#--<#list references as reference>

    ${reference_index + 1}. <a href="${reference.url}"> ${reference.title} </a> <br/>


</#list>-->
</body>
</html>