package kyiv.home.java8;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Grouping {
    public static void main(String[] args) {
        excludeOneListFromAnother();
        //groupBy();
    }

    private static void excludeOneListFromAnother() {
        List<String> sourceList = Arrays.asList("Item8", "Item2");
        List<String> excludedList = Arrays.asList("Item1", "Item2", "Item3");

        System.out.println(retainList(sourceList, excludedList, true));
        System.out.println(retainList(sourceList, excludedList, false));
    }

    /**
     * Retain only items from the source list that matched items in retainList if retain is true
     * If retain is false then only items from source list that do not match items in retainList would be left
     * @param sourceList
     * @param retainList
     * @param retain
     * @return
     */
    private static List<String> retainList(List<String> sourceList, List<String> retainList, boolean retain) {
        Predicate<String> retainPredicate = item -> retain == retainList.contains(item);
        return sourceList.stream().filter(retainPredicate).collect(Collectors.toList());
    }

    private static void groupBy() {
        Map<String, List<String>> regionToModelMap = new HashMap<String, List<String>>() {{
            put("ASIA1", Arrays.asList("Model1","Model2","Model4","Model5"));
            put("ASIA2", Arrays.asList("Model2","Model4","Model6","Model7"));
            put("ASIA3", Arrays.asList("Model1","Model2","Model3"));
            put("ASIA4", Arrays.asList("Model4","Model5","Model6","Model7", "Model8"));
        }};

        final List<String> modelList = Arrays.asList("Model2","Model3", "Model8");

        // Filter original map by model list
        Map<String, List<String>> regionToModelMapFiltered = regionToModelMap
                .entrySet()
                .stream()
                .filter(map -> map.getValue().stream().anyMatch(modelList::contains))
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldVal, newVal) -> oldVal, LinkedHashMap::new));

        Map<String, List<String>> regionsWithFilteredModels = regionToModelMap
                .entrySet()
                .stream()
                .filter(map -> map.getValue().stream().anyMatch(modelList::contains))
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        val-> val.getValue().stream().filter(modelList::contains).collect(Collectors.toList()),
                        (oldVal, newVal) -> oldVal, LinkedHashMap::new));

        System.out.println(regionToModelMapFiltered);
        System.out.println("Regions with removed models: " + regionsWithFilteredModels);
    }

}
