package com.db.service;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.db.domain.Task;

@Service
public interface TaskService {
    Collection<Task> list();
}
