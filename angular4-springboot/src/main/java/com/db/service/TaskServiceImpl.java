package com.db.service;

import java.util.ArrayList;
import java.util.Collection;

import com.db.domain.Task;

public class TaskServiceImpl implements TaskService {
    @Override
    public Collection<Task> list() {
        Collection<Task> listOfTasks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            listOfTasks.add(new Task().withName("Task " + i).withId(String.valueOf(i)).withDescription("Description " +
                    "of" +
                    " task" + i));
        }
        return listOfTasks;
    }
}
