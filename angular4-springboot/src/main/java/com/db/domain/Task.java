package com.db.domain;

import lombok.Data;

@Data
public class Task {
    private String id;
    private String name;
    private String description;

    public Task withName(String name) {
        this.name = name;
        return this;
    }

    public Task withId(String id) {
        this.id = id;
        return this;
    }

    public Task withDescription(String description) {
        this.description = description;
        return this;
    }
}
