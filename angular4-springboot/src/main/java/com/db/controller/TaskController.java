package com.db.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.db.domain.Task;
import com.db.service.TaskService;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    private TaskService taskService;

    @GetMapping(value = {"", "/"})
    public Collection<Task> listTasks() {
        return taskService.list();
    }

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }
}
