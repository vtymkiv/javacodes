package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.processors;

import org.apache.camel.Exchange;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.internal.InternalSearchHit;
import org.elasticsearch.search.internal.InternalSearchHits;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.nfr.tmr.services.disclosuredata.DisclosureDataService;
import com.db.nfr.tmr.services.disclosuredata.DisclosureStatus;
import com.db.nfr.tmr.services.disclosuredata.converters.Converter;
import com.db.nfr.tmr.services.disclosuredata.converters.ConverterImpl;
import com.db.nfr.tmr.services.disclosuredata.converters.DisclosureDataTransformationService;
import com.db.nfr.tmr.services.utils.JsonUtil;


import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by tymkvit on 29/03/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        ConvertSearchResultsProcessor.class,
        ConverterImpl.class,
        JsonUtil.class})
public class ConvertSearchResultsProcessorTest {
    @Autowired
    private ConvertSearchResultsProcessor convertSearchResultsProcessor;

    @Autowired
    @Qualifier("jsonConverter")
    private Converter converter;

    @MockBean
    private DisclosureDataService disclosureDataService;

    @MockBean
    private DisclosureDataTransformationService disclosureDataTransformService;

    /**
     * It's used only for testing purposes
     */
    @Autowired
    private JsonUtil jsonUtil;

    @Test
    public void process() throws Exception {
        // Given
        Exchange exchange = mock(Exchange.class, RETURNS_DEEP_STUBS);
        SearchResponse searchResponse = mock(SearchResponse.class, RETURNS_DEEP_STUBS);
        InternalSearchHit hit1 = mock(InternalSearchHit.class, RETURNS_DEEP_STUBS);
        InternalSearchHit hit2 = mock(InternalSearchHit.class, RETURNS_DEEP_STUBS);
        SearchHits searchHitsValues = getSearchHits(hit1, hit2);

        when(exchange.getIn().getBody(SearchResponse.class)).thenReturn(searchResponse);
        when(searchResponse.getHits()).thenReturn(searchHitsValues);
        when(hit1.getSourceAsString()).thenReturn(jsonUtil.getJsonAsString("classpath:TMRServices/ds-date-es-response-hit-source.json"));
        when(hit2.getSourceAsString()).thenReturn(jsonUtil.getJsonAsString("classpath:TMRServices/ds-date-es-response-hit1-source.json"));
        when(disclosureDataTransformService.getStatus(anyString())).thenReturn(DisclosureStatus.COMPLETE);

        // When
        convertSearchResultsProcessor.process(exchange);

        // Then
        verify(disclosureDataService, times(1)).recertifyMandates(anyList());
    }

    @Test
    public void processHitWithArrayOfDisclosureRows() throws Exception {
        // Given
        Exchange exchange = mock(Exchange.class, RETURNS_DEEP_STUBS);
        SearchResponse searchResponse = mock(SearchResponse.class, RETURNS_DEEP_STUBS);
        InternalSearchHit hit1 = mock(InternalSearchHit.class, RETURNS_DEEP_STUBS);
        InternalSearchHit hit2 = mock(InternalSearchHit.class, RETURNS_DEEP_STUBS);
        SearchHits searchHitsValues = getSearchHits(hit1, hit2);

        when(exchange.getIn().getBody(SearchResponse.class)).thenReturn(searchResponse);
        when(searchResponse.getHits()).thenReturn(searchHitsValues);
        when(hit1.getSourceAsString()).thenReturn(jsonUtil.getJsonAsString("classpath:TMRServices/ds-date-es-response-hit-source-with-array_discRows.json"));
        when(hit2.getSourceAsString()).thenReturn(jsonUtil.getJsonAsString("classpath:TMRServices/ds-date-es-response-hit-source-with-1-discRows.json"));
        when(disclosureDataTransformService.getStatus(anyString())).thenReturn(DisclosureStatus.COMPLETE);

        // When
        convertSearchResultsProcessor.process(exchange);

        // Then
        verify(disclosureDataService, times(1)).recertifyMandates(anyList());
    }

    private InternalSearchHits getSearchHits(InternalSearchHit... hits) {
        float score = 0.2345f;
        return new InternalSearchHits(hits, 28, score);
    }
}