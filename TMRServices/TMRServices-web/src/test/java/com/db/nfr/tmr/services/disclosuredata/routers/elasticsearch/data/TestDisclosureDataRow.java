package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * Created by tymkvit on 07/04/2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestDisclosureDataRow {
    public static final int EMAIL_MAX_SIZE = 2;
    public static final int REASON_MAX_SIZE = 5;
    public static final int COMMENT_MAX_SIZE = 6;

    @JsonProperty("mandateDate")
    private String mandateDate;
    @JsonProperty("versionId")
    private String versionId;

    @JsonPropertyLength(max = EMAIL_MAX_SIZE)
    @JsonDeserialize(using = JsonTextFieldDeserializer.class)
    @JsonProperty("trader")
    private String traderEmail;

    @JsonProperty("declaration")
    private String acknowledge;

    @JsonPropertyLength(max = REASON_MAX_SIZE)
    @JsonDeserialize(using = JsonTextFieldDeserializer.class)
    @JsonProperty("rating")
    private String reason;

    @JsonPropertyLength(max = COMMENT_MAX_SIZE)
    @JsonProperty("comment")
    @JsonDeserialize(using = JsonTextFieldDeserializer.class)
    private String comment;
}
