package com.db.nfr.tmr.services.dbapi.mandategss.config;

import org.springframework.context.annotation.Bean;

import com.db.nfr.tmr.services.dataimport.ImportDAO;

/**
 * Created by tymkvit on 15/11/2016.
 */
public class ImportDAOTestConfig {

    @Bean(name = "importDAO")
    public ImportDAO importDAO() {
        return new ImportDAO();
    }
}
