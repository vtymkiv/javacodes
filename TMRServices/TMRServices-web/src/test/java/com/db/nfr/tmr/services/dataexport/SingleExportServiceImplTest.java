package com.db.nfr.tmr.services.dataexport;

import javax.sql.DataSource;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.nfr.tmr.services.common.ExportSingleData;
import com.db.nfr.tmr.services.common.NoDataFoundException;
import com.db.nfr.tmr.services.common.SingleTrader;
import com.db.nfr.tmr.services.dbapi.CustomOracleTypes;
import com.db.nfr.tmr.services.dbapi.PkgMandateGssRecertif;
import com.db.nfr.tmr.services.dbapi.mandategss.config.datasources.DatasourceConfigTest;
import com.fasterxml.jackson.databind.ObjectMapper;


import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by danyvik on 11/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DatasourceConfigTest.class, PkgMandateGssRecertif.class, CustomOracleTypes.class})
@ActiveProfiles("integration-test")
@IfProfileValue(name = "integration-test", value = "true")
public class SingleExportServiceImplTest {

    @Autowired
    @Qualifier("tmsAppDS")
    private DataSource tmsAppDataSource;

    @Autowired
    private PkgMandateGssRecertif pkgMandateGssRecertif;

    @Autowired
    private CustomOracleTypes customOracleTypes;

    ExportSingleData exportSingleData = null;
    SingleTrader singleTraderJson = null;

    @Before
    public void beforeTest() throws IOException {
        singleTraderJson = getJsonObj();
    }

    private SingleTrader getJsonObj() throws IOException {
        InputStream is = getClass().getClassLoader().getResourceAsStream("singleexport_debt.json");
        ObjectMapper mapper = new ObjectMapper();
        SingleTrader singleTraderJson = mapper.readValue(is, SingleTrader.class);
        assertThat(singleTraderJson.getMandateType(), notNullValue());
        return singleTraderJson;
    }

    @Test
    public void process() throws SQLException, IOException, NoDataFoundException {
        try (Connection tmsAppDataSourceConnection = tmsAppDataSource.getConnection()) {
            exportSingleData = pkgMandateGssRecertif.pExportMandateFile(singleTraderJson, tmsAppDataSourceConnection);
            //Then
            assertThat(exportSingleData.getFileImage().length, notNullValue());
        }
    }
}