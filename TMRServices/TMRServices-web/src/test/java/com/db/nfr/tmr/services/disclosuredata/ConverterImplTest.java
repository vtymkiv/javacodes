package com.db.nfr.tmr.services.disclosuredata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.nfr.tmr.services.disclosuredata.converters.ConverterImpl;
import com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data.SearchHitDataSource;
import com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data.SearchHitDisclosureDataRow;
import com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data.SearchHitDto;
import com.db.nfr.tmr.services.utils.JsonUtil;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by tymkvit on 10/03/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ConverterImpl.class, JsonUtil.class})
public class ConverterImplTest {
    @Autowired
    private ConverterImpl converter;

    @Autowired
    private JsonUtil jsonUtil;

    @Test
    public void convertFromGivenSearchHitSource() throws Exception {
        // Given
        String json = jsonUtil.getJsonAsString("classpath:TMRServices/ds-date-es-response-hit-source.json");
        // When
        SearchHitDataSource searchHitDataSource = converter.convertFrom(json, SearchHitDataSource.class);

        // Then
        assertThat(searchHitDataSource.getDto(), notNullValue());
        assertThat(searchHitDataSource.getDisclosureDataRows(), notNullValue());
        assertThat(searchHitDataSource.getDisclosureDataRows().get(0), notNullValue());

        assertThat(searchHitDataSource.getDto().getStatus(), is("REMINDER"));
        assertThat(searchHitDataSource.getDto().getGuiKey(), is("tmRec_gme_trader"));
        assertThat(searchHitDataSource.getDto().getEvidenceProvisionDate(), is("2017/04/04 16:15:11"));

        SearchHitDisclosureDataRow searchHitDisclosureDataRow = searchHitDataSource.getDisclosureDataRows().get(0);
        assertThat(searchHitDisclosureDataRow.getMandateDate(), is("21-FEB-2017"));

        assertThat(searchHitDisclosureDataRow.getComment(), is("This a comment for test"));
        assertThat(searchHitDisclosureDataRow.getReason(), is("This is a rating for test"));
        assertThat(searchHitDisclosureDataRow.getTraderEmail(), is("yashar.asl@db.com"));
        assertThat(searchHitDisclosureDataRow.getVersionId(), is("17251"));
        assertThat(searchHitDisclosureDataRow.getAcknowledge(), is("Yes"));
    }

    @Test
    public void convertFromJsonStringAsMap() throws Exception {
        // Given
        String json = jsonUtil.getJsonAsString("classpath:TMRServices/ds-date-es-response-hit-source.json");

        // When
        Map<String, ?> searchHitDataSourceMap = converter.convertFrom(json);

        // Then
        assertThat(searchHitDataSourceMap.get("dto"), notNullValue());
        assertThat(searchHitDataSourceMap.get("disclosureDataRows"), notNullValue());
        assertThat(((ArrayList<HashMap<String, ?>>) searchHitDataSourceMap.get("disclosureDataRows")).get(0), notNullValue());

        HashMap<String, ?> dtoMap = (HashMap<String, ?>) searchHitDataSourceMap.get("dto");
        assertThat(dtoMap.get("status"), is("REMINDER"));
        assertThat(dtoMap.get("guiKey"), is("tmRec_gme_trader"));

        HashMap<String, ?> disclosureDataRowsFirstRowMap = ((ArrayList<HashMap<String, ?>>) searchHitDataSourceMap.get("disclosureDataRows")).get(0);
        assertThat(disclosureDataRowsFirstRowMap.get("mandateDate"), is("21-FEB-2017"));
        assertThat(disclosureDataRowsFirstRowMap.get("evidenceProvisionDate"), is("21-APR-2017"));
        assertThat(disclosureDataRowsFirstRowMap.get("comment"), is("This a comment for test"));
        assertThat(disclosureDataRowsFirstRowMap.get("rating"), is("This is a rating for test"));
        assertThat(disclosureDataRowsFirstRowMap.get("trader"), is("yashar.asl@db.com"));
        assertThat(disclosureDataRowsFirstRowMap.get("versionId"), is("17251"));
        assertThat(disclosureDataRowsFirstRowMap.get("declaration"), is("Yes"));
    }

    @Test
    public void convertFromJsonStringByPathToDto() throws Exception {
        // Given
        String json = jsonUtil.getJsonAsString("classpath:TMRServices/ds-date-es-response-hit-source.json");

        // When
        SearchHitDto searchHitDto = converter.convertFrom(json, SearchHitDto.class, "/dto");

        // Then
        assertThat(searchHitDto, notNullValue());
        assertThat(searchHitDto.getStatus(), is("REMINDER"));
        assertThat(searchHitDto.getGuiKey(), is("tmRec_gme_trader"));
    }

    @Test
    public void convertFromJsonStringByPathToDisclosureDataRow() throws Exception {
        // Given
        String json = jsonUtil.getJsonAsString("classpath:TMRServices/ds-date-es-response-hit-source.json");

        // When
        SearchHitDisclosureDataRow searchHitDisclosureDataRow = converter.convertFrom(json, SearchHitDisclosureDataRow.class, "/disclosureDataRows/0");

        // Then
        assertThat(searchHitDisclosureDataRow, notNullValue());
        assertThat(searchHitDisclosureDataRow.getMandateDate(), is("21-FEB-2017"));
        assertThat(searchHitDisclosureDataRow.getComment(), is("This a comment for test"));
        assertThat(searchHitDisclosureDataRow.getReason(), is("This is a rating for test"));
        assertThat(searchHitDisclosureDataRow.getTraderEmail(), is("yashar.asl@db.com"));
        assertThat(searchHitDisclosureDataRow.getVersionId(), is("17251"));
        assertThat(searchHitDisclosureDataRow.getAcknowledge(), is("Yes"));
    }

    @Test
    public void convertFromJsonStringByPathToDisclosureDataRows() throws Exception {
        // Given
        String json = jsonUtil.getJsonAsString("classpath:TMRServices/ds-date-es-response-hit-source.json");

        // When
        List<SearchHitDisclosureDataRow> searchHitDisclosureDataRows = converter.convertFromArray(json, SearchHitDisclosureDataRow.class, "/disclosureDataRows");
        assertThat(searchHitDisclosureDataRows, notNullValue());
        SearchHitDisclosureDataRow searchHitDisclosureDataRow = searchHitDisclosureDataRows.get(0);

        // Then
        assertThat(searchHitDisclosureDataRow, notNullValue());
        assertThat(searchHitDisclosureDataRow.getMandateDate(), is("21-FEB-2017"));
        assertThat(searchHitDisclosureDataRow.getComment(), is("This a comment for test"));
        assertThat(searchHitDisclosureDataRow.getReason(), is("This is a rating for test"));
        assertThat(searchHitDisclosureDataRow.getTraderEmail(), is("yashar.asl@db.com"));
        assertThat(searchHitDisclosureDataRow.getVersionId(), is("17251"));
        assertThat(searchHitDisclosureDataRow.getAcknowledge(), is("Yes"));
    }

}