package com.db.nfr.tmr.services.utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;

import org.junit.Test;

import lombok.extern.slf4j.Slf4j;
import sun.misc.BASE64Encoder;

/**
 * Created by tymkvit on 15/03/2017.
 */
@Slf4j
public final class GeneratorUtil {
    /**
     * Encode given password
     *
     * @param pwd
     * @return
     */
    private String encodePwd(String pwd) {
        BASE64Encoder encoder = new BASE64Encoder();
        return String.valueOf(encoder.encodeBuffer(pwd.getBytes(Charset.defaultCharset())));
    }

    @Test
    public void generatePwd() throws IOException {
        encodeDecode("testPwd");
        encodeDecode("test1Pwd1");
    }

    static void encodeDecode(final String testInputString) throws IOException {
        Base64.Encoder mimeEncoder = java.util.Base64.getMimeEncoder();
        Base64.Decoder mimeDecoder = java.util.Base64.getMimeDecoder();


        String mimeEncoded = mimeEncoder.encodeToString(testInputString.getBytes(Charset.defaultCharset()));
        LOGGER.info("Java 8 Base64 MIME encoded {}: {}", testInputString, mimeEncoded);


        byte[] mimeDecoded = mimeDecoder.decode(mimeEncoded);
        String mimeDecodedString = new String(mimeDecoded, Charset.forName("UTF-8"));

        LOGGER.info("Java 8 Base64 MIME decoded {}: {}", testInputString, mimeDecodedString);
    }
}
