package com.db.nfr.tmr.services.dbapi.mandategss.config.datasources;

import javax.sql.DataSource;

import java.nio.charset.Charset;
import java.util.Base64;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;

import com.db.nfr.tmr.services.dbapi.PkgMandateRecertifDAO;
import com.db.nfr.tmr.services.dbapi.PkgMandateRecertifDAOImpl;
import com.db.nfr.tmr.services.dbapi.PkgStgMandatesRepoDAO;
import com.db.nfr.tmr.services.dbapi.PkgStgMandatesRepoDAOImpl;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by tymkvit on 22/09/2016.
 */
@Slf4j
@Configuration
@EnableConfigurationProperties
@Profile("integration-test")
public class DatasourceConfigTest {

    @ConfigurationProperties(prefix = "data-sources.tms")
    @Bean(destroyMethod = "", name = "tmsAppDS")
    public DataSource tmsAppDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        setPassword(dataSourceBuilder, "UXdlcnR5MTIzNCM=");
        return dataSourceBuilder.build();
    }

    @ConfigurationProperties(prefix = "data-sources.stage")
    @Bean(destroyMethod = "", name = "stgFeedDS")
    public DataSource stgFeedDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        setPassword(dataSourceBuilder, "bmV3XzM0NTY=");
        return dataSourceBuilder.build();
    }

    @Bean
    public JdbcTemplate tmsJdbcTemplate() {
        return new JdbcTemplate(tmsAppDataSource());
    }

    @Bean
    public JdbcTemplate stageJdbcTemplate() {
        return new JdbcTemplate(stgFeedDataSource());
    }

    @Bean
    public PkgStgMandatesRepoDAO pkgStgMandatesRepoDAO() {
        return new PkgStgMandatesRepoDAOImpl(stgFeedDataSource());
    }

    @Bean
    public PkgMandateRecertifDAO pkgMandateRecertifDAO() {
        return new PkgMandateRecertifDAOImpl(tmsAppDataSource());
    }

    private void setPassword(DataSourceBuilder dataSourceBuilder, String pwd) {
        Base64.Decoder mimeDecoder = java.util.Base64.getMimeDecoder();
        byte[] mimeDecoded = mimeDecoder.decode(pwd);
        String mimeDecodedString = new String(mimeDecoded, Charset.forName("UTF-8"));
        dataSourceBuilder.password(mimeDecodedString);
    }

}
