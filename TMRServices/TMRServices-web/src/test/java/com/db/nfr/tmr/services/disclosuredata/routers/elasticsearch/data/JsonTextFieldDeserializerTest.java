package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.nfr.tmr.services.disclosuredata.converters.ConverterImpl;
import com.db.nfr.tmr.services.utils.JsonUtil;


import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

/**
 * Created by tymkvit on 07/04/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ConverterImpl.class, JsonUtil.class})
public class JsonTextFieldDeserializerTest {
    @Autowired
    private ConverterImpl converter;

    @Autowired
    private JsonUtil jsonUtil;

    @Test
    public void testDeserialize() throws Exception {
        // Given
        String json = jsonUtil.getJsonAsString("classpath:TMRServices/ds-date-es-response-hit-with-big-comment.json");

        // When
        TestSearchHit testSearchHit = converter.convertFrom(json, TestSearchHit.class);

        // Then
        TestDisclosureDataRow searchHitDisclosureDataFirstRow = testSearchHit.getDisclosureDataRows().get(0);
        assertThat(searchHitDisclosureDataFirstRow.getComment().length(), lessThanOrEqualTo(TestDisclosureDataRow.COMMENT_MAX_SIZE));
        assertThat(searchHitDisclosureDataFirstRow.getTraderEmail().length(), lessThanOrEqualTo(TestDisclosureDataRow.EMAIL_MAX_SIZE));
        assertThat(searchHitDisclosureDataFirstRow.getReason().length(), lessThanOrEqualTo(TestDisclosureDataRow.REASON_MAX_SIZE));

        TestDisclosureDataRow searchHitDisclosureDataSecondRow = testSearchHit.getDisclosureDataRows().get(1);
        assertThat(searchHitDisclosureDataSecondRow.getComment(), nullValue());
    }
}