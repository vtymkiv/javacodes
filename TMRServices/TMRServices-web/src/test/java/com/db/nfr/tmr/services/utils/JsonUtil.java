package com.db.nfr.tmr.services.utils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

/**
 * Created by tymkvit on 30/03/2017.
 */
@Component
public class JsonUtil {

    @Autowired
    private ResourceLoader resourceLoader;

    public String getJsonAsString(String pathToJsonFile) throws IOException {
        StringBuilder json = new StringBuilder();
        try (InputStream inputStream = resourceLoader.getResource(pathToJsonFile).getInputStream()) {
            json.append(IOUtils.toString(inputStream, Charset.defaultCharset()));
        }
        return json.toString();
    }
}
