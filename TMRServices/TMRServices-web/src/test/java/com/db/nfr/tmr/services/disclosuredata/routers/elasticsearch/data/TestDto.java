package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * Created by tymkvit on 07/04/2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestDto {
    @JsonProperty("status")
    private String status;
    @JsonProperty("guiKey")
    private String guiKey;
    @JsonProperty("evidenceProvisionDate")
    private String evidenceProvisionDate;
}
