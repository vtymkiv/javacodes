package com.db.nfr.tmr.services.disclosuredata;

import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.nfr.tmr.services.dbapi.CustomOracleTypes;
import com.db.nfr.tmr.services.dbapi.PkgFileRepoDAOImpl;
import com.db.nfr.tmr.services.dbapi.PkgStgMandatesRepoDAOImpl;
import com.db.nfr.tmr.services.dbapi.mandategss.config.datasources.DatasourceConfigTest;
import com.db.nfr.tmr.services.disclosuredata.converters.ConverterImpl;

/**
 * Created by tymkvit on 12/03/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DatasourceConfigTest.class,
        DisclosureDataServiceImpl.class, PkgStgMandatesRepoDAOImpl.class, PkgFileRepoDAOImpl.class,
        ConverterImpl.class,
        CustomOracleTypes.class})
@ActiveProfiles("integration-test")
@IfProfileValue(name = "integration-test", value = "true")
public class DisclosureDataServiceImplTest {
    @Autowired
    private DisclosureDataServiceImpl disclosureDataService;

    @Test
    @Rollback
    public void process() throws Exception {
        // Given
        DisclosureData disclosureData = new DisclosureData();
        disclosureData.setTraderEmail("test.email@yahoo.com");
        disclosureData.setVersionId("16673");
        disclosureData.setMandateDate("2017-02-20");
        // When

        disclosureDataService.recertifyMandates(disclosureData);

        // Then
    }

    @Test
    public void saveFile() throws Exception {
        // Given
        String jsonStr;
        try (InputStream is = getClass().getClassLoader().getResourceAsStream("disclouser_data.json")) {
            jsonStr = IOUtils.toString(is, Charset.defaultCharset());
        }

        // When
        disclosureDataService.saveFile("guikeyTest", jsonStr);

        // Then
    }

    @Test
    public void recertifMandateGivenGuiKey() throws Exception {
        // Given
        String jsonStr;
        try (InputStream is = getClass().getClassLoader().getResourceAsStream("disclouser_data.json")) {
            jsonStr = IOUtils.toString(is, Charset.defaultCharset());
        }
        // When
        disclosureDataService.recertifyMandates("guikeyTest", jsonStr);

        // Then
    }

}