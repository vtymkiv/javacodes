package com.db.nfr.tmr.services.disclosuredata.converters;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;

/**
 * Created by tymkvit on 31/03/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DateConverterServiceImpl.class})
public class DateConverterServiceImplTest {
    @Autowired
    private DateConverterServiceImpl dateConverterService;

    @Test
    public void convertDateTo() throws Exception {
        // Given
        String originalDate = "12-FEB-2017";

        // When
        String convertedDate = dateConverterService.convertDateTo(originalDate, DateConverterServiceImpl.ORIGINAL_MANDATE_DATE_PATTERN, DateConverterServiceImpl
                .NEW_MANDATE_DATE_PATTERN);

        // Then
        assertThat(convertedDate, not(isEmptyOrNullString()));
        assertThat(convertedDate, is("2017-02-12"));
    }

    @Test
    public void convertDateToWithTime() throws Exception {
        // Given
        String originalDate = "2017/04/04 16:15:11";

        // When
        String convertedDate = dateConverterService.convertDateTo(originalDate, DateConverterServiceImpl.ORIGINAL_EVIDENCE_PROVISION_DATE_PATTERN, DateConverterServiceImpl
                .NEW_EVIDENCE_PROVISION_DATE_PATTERN);

        // Then
        assertThat(convertedDate, not(isEmptyOrNullString()));
        assertThat(convertedDate, is("2017-04-04 16:15:11"));
    }
}
