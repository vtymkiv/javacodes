package com.db.nfr.tmr.services.dataimport;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.nfr.tmr.services.dbapi.CustomOracleTypes;
import com.db.nfr.tmr.services.dbapi.mandategss.config.ImportDAOTestConfig;
import com.db.nfr.tmr.services.dbapi.mandategss.config.datasources.DatasourceConfigTest;

/**
 * Created by danyvik on 10/11/2016.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DatasourceConfigTest.class, ImportDAOTestConfig.class, CustomOracleTypes.class})
@ActiveProfiles("integration-test")
@IfProfileValue(name = "integration-test", value = "true")
public class ImportDAOTest {

    @Autowired
    @Qualifier("importDAO")
    private ImportDAO importDAO;

    @Test
    public void testRecertifMandate() throws Exception {
        // Given
        // When
        importDAO.recertifMandate();
        // Then
    }
}