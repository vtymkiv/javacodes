package com.db.nfr.tmr.services.disclosuredata.converters;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;

import com.db.nfr.tmr.services.common.CertificationPersonType;
import com.db.nfr.tmr.services.common.MandateType;
import com.db.nfr.tmr.services.disclosuredata.DisclosureData;
import com.db.nfr.tmr.services.disclosuredata.DisclosureStatus;
import com.db.nfr.tmr.services.disclosuredata.DtoStatus;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;


import static com.db.nfr.tmr.services.disclosuredata.DtoStatus.ESCALATION;
import static com.db.nfr.tmr.services.disclosuredata.DtoStatus.REMINDER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by tymkvit on 31/03/2017.
 */
@RunWith(JUnitParamsRunner.class)
@SpringBootTest(classes = {DisclosureDataTransformationServiceImpl.class, DateConverterServiceImpl.class})
public class DisclosureDataTransformationServiceImplTest {
    @ClassRule
    public static final SpringClassRule SCR = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private DisclosureDataTransformationServiceImpl disclosureDataTransformationService;

    @Autowired
    private DateConverterServiceImpl dateConverterService;

    @Test
    @Parameters(method = "parametersForSetRecertificationAndMandateTypeMethod")
    public void setRecertificationAndMandateType(String guiKey, MandateType expectedMandateType, CertificationPersonType expectedCertificationPersonType) throws Exception {
        // Given
        DisclosureData disclosureData = new DisclosureData();

        // When
        disclosureDataTransformationService.setRecertificationAndMandateType(guiKey, disclosureData);

        // Then
        assertThat(disclosureData.getMandateType(), is(expectedMandateType.getCode()));
        assertThat(disclosureData.getCertificationType(), is(expectedCertificationPersonType.getCode()));
    }

    @Test
    @Parameters({"2017/04/04 16:15:11, 12-FEB-2017, 2017-04-04 16:15:11, 2017-02-12",
            //Format for mandate original date is dd-MMM-yyyy, the date is expected in format yyyy-MM-dd
            //Format for evidence original date is yyyy/MM/dd HH:mm:ss, the date is expected in format yyyy-MM-dd
            "2017/01/02 18:19:11, 01-FEB-2017, 2017-01-02 18:19:11, 2017-02-01"})
    public void setDates(String originalEvidenceProvisionDate, String originalMandateDate, String expectedProvisionDate, String expectedMandateDate) throws Exception {
        // Given
        DisclosureData disclosureData = new DisclosureData();

        // When
        disclosureDataTransformationService.setDates(originalEvidenceProvisionDate, originalMandateDate, disclosureData);

        // Then
        assertThat(disclosureData.getProvisionDate(), is(expectedProvisionDate));
        assertThat(disclosureData.getMandateDate(), is(expectedMandateDate));
    }

    @Test
    @Parameters(method = "parametersForGetDisclosureStatusByDtoStatusTestMethod")
    public void getDisclosureStatusByDtoStatus(DtoStatus dtoStatus, DisclosureStatus expectedDisclosureStatus) throws Exception {
        // Given
        // When
        DisclosureStatus actualDisclosureStatus = disclosureDataTransformationService.getStatus(dtoStatus);

        // Then
        assertThat(actualDisclosureStatus, is(expectedDisclosureStatus));
    }

    @Test
    @Parameters(method = "parametersForGetDisclosureStatusGivenStatusAsStringTestMethod")
    public void getDisclosureStatusGivenStatusAsString(String dtoStatusAsString, DisclosureStatus expectedDisclosureStatus) throws Exception {
        // Given
        // When
        DisclosureStatus actualDisclosureStatus = disclosureDataTransformationService.getStatus(dtoStatusAsString);

        // Then
        assertThat(actualDisclosureStatus, is(expectedDisclosureStatus));
    }


    // Test data
    private Object[] parametersForSetRecertificationAndMandateTypeMethod() {
        return new Object[]{
                // 0 - input parameter, 1 - expected mandate type, 2- expected certification person type
                new Object[]{"tmRec_fic_trader", MandateType.DEBT, CertificationPersonType.TRADER},
                new Object[]{"tmRec_fic_supervisor", MandateType.DEBT, CertificationPersonType.SUPERVISOR},
                new Object[]{"tmRec_gme_trader", MandateType.EQUITY, CertificationPersonType.TRADER},
                new Object[]{"tmRec_gme_supervisor", MandateType.EQUITY, CertificationPersonType.SUPERVISOR}
        };
    }

    private Object[] parametersForGetDisclosureStatusByDtoStatusTestMethod() {
        return new Object[]{
                // 0 - input parameter, 1 - expected mandate type, 2- expected certification person type
                new Object[]{DtoStatus.CLOSED_NOK, DisclosureStatus.CLOSED},
                new Object[]{DtoStatus.CLOSED_OK, DisclosureStatus.COMPLETE},
                new Object[]{DtoStatus.PROVISION, DisclosureStatus.IN_PROGRESS},
                new Object[]{REMINDER, DisclosureStatus.IN_PROGRESS},
                new Object[]{ESCALATION, DisclosureStatus.IN_PROGRESS}
        };
    }

    private Object[] parametersForGetDisclosureStatusGivenStatusAsStringTestMethod() {
        return new Object[]{
                // 0 - input parameter, 1 - expected mandate type, 2- expected certification person type
                new Object[]{"CLOSED_NOK", DisclosureStatus.CLOSED},
                new Object[]{"CLOSED_OK", DisclosureStatus.COMPLETE},
                new Object[]{"PROVISION", DisclosureStatus.IN_PROGRESS},
                new Object[]{"REMINDER", DisclosureStatus.IN_PROGRESS},
                new Object[]{"ESCALATION", DisclosureStatus.IN_PROGRESS}
        };
    }
}