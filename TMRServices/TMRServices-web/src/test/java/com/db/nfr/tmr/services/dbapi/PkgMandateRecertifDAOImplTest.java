package com.db.nfr.tmr.services.dbapi;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.nfr.tmr.services.dbapi.mandategss.config.datasources.DatasourceConfigTest;


import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by tymkvit on 29/03/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DatasourceConfigTest.class, PkgMandateRecertifDAO.class, CustomOracleTypes.class})
@ActiveProfiles("integration-test")
@IfProfileValue(name = "integration-test", value = "true")
public class PkgMandateRecertifDAOImplTest {
    @Autowired
    private PkgMandateRecertifDAO pkgMandateRecertifDAO;

    @Test
    public void recertifMandate() throws Exception {
        // Given

        // When


        // Then
    }

    @Test
    public void getRecertifPeriodVersions() throws Exception {
        // Given

        // When
        List<BigDecimal> recertifPeriodVersions = pkgMandateRecertifDAO.getRecertifPeriodVersions();

        // Then
        assertThat(recertifPeriodVersions, notNullValue());
    }

}