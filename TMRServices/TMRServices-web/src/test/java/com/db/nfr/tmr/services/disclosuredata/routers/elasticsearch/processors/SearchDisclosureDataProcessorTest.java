package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.processors;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;

import org.apache.camel.Exchange;
import org.elasticsearch.action.ListenableActionFuture;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.search.SearchHits;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.autoconfigure.RefreshAutoConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.nfr.tmr.services.config.disclosuredata.ElasticSearchProperties;
import com.db.nfr.tmr.services.disclosuredata.DisclosureDataService;
import com.db.nfr.tmr.services.disclosuredata.converters.ConverterImpl;
import com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.ElasticSearchService;
import com.db.nfr.tmr.services.utils.JsonUtil;


import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by tymkvit on 05/04/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        SearchDisclosureDataProcessor.class,
        ElasticSearchProperties.class,
        ConverterImpl.class,
        JsonUtil.class})
@ImportAutoConfiguration(RefreshAutoConfiguration.class)
public class SearchDisclosureDataProcessorTest {

    @Autowired
    private SearchDisclosureDataProcessor searchDisclosureDataProcessor;

    @MockBean
    private ElasticSearchProperties elasticSearchProperties;

    @MockBean
    private DisclosureDataService disclosureDataService;

    @MockBean
    private ElasticSearchService elasticSearchService;

    @Test
    public void process() throws Exception {
        // Given
        Exchange exchange = mock(Exchange.class, RETURNS_DEEP_STUBS);
        Client client = mock(Client.class, RETURNS_DEEP_STUBS);
        SearchRequestBuilder searchRequestBuilder = mock(SearchRequestBuilder.class, RETURNS_DEEP_STUBS);
        ListenableActionFuture laf = mock(ListenableActionFuture.class, RETURNS_DEEP_STUBS);
        SearchResponse searchResponse = mock(SearchResponse.class, RETURNS_DEEP_STUBS);
        SearchHits searchHits = mock(SearchHits.class);
        List<String> versionIdList = Arrays.asList("12345", "789", "125");

        when(disclosureDataService.getVersionIdList()).thenReturn(versionIdList);
        when(elasticSearchService.getTransportClient(any(Settings.class), any(InetSocketAddress.class))).thenReturn(client);
        when(elasticSearchProperties.getHostName()).thenReturn("localhost");
        when(elasticSearchProperties.getPort()).thenReturn(8080);
        when(elasticSearchProperties.getPathToRequest()).thenReturn("classpath:TMRServices/disclouser_data-es-request.json");
        when(client.prepareSearch(anyString())).thenReturn(searchRequestBuilder);
        when(searchRequestBuilder.setTypes(anyString())).thenReturn(searchRequestBuilder);
        when(searchRequestBuilder.setSource(anyString())).thenReturn(searchRequestBuilder);
        when(searchRequestBuilder.execute()).thenReturn(laf);
        when(laf.actionGet()).thenReturn(searchResponse);
        when(searchResponse.getHits()).thenReturn(searchHits);
        when(searchHits.totalHits()).thenReturn(1l);

        // When
        searchDisclosureDataProcessor.process(exchange);

        // Then
        verify(exchange.getIn(), times(1)).setBody(searchResponse);

    }

}