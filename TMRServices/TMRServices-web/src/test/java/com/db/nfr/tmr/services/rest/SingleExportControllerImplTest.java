package com.db.nfr.tmr.services.rest;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.db.nfr.tmr.services.common.ExportSingleData;
import com.db.nfr.tmr.services.common.SingleTrader;
import com.db.nfr.tmr.services.dataexport.SingleExportService;


import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by danyvik on 11/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SingleExportControllerImpl.class)
@AutoConfigureMockMvc
@ActiveProfiles("unit-test")
public class SingleExportControllerImplTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    @Qualifier("singleExportServiceImpl")
    private SingleExportService singleExportService;

    @MockBean(answer = Answers.RETURNS_DEEP_STUBS)
    private ExportSingleData exportSingleData;

    @Test
    public void testGeSingleExportService() throws Exception {
        // Given
        String json = getJson("singleexport_debt.json");
        MultiValueMap<String, String> multiValMap = new LinkedMultiValueMap<>();
        multiValMap.add("json", json);
        when(singleExportService.process(any(SingleTrader.class))).thenReturn(exportSingleData);
        when(exportSingleData.getFileImage()).thenReturn("Test".getBytes(Charset.defaultCharset()));

        // When
        ResultActions result = mockMvc.perform(get("/exportservice/getsingle").params(multiValMap));

        // Then
        result.andDo(print());
        result.andExpect(status().isOk());
    }

    @Test
    public void testContentTypeExcel() throws Exception {
        // Given
        String json = getJson("singleexport_debt.json");
        MultiValueMap<String, String> multiValMap = new LinkedMultiValueMap<>();
        multiValMap.add("json", json);
        when(singleExportService.process(any(SingleTrader.class))).thenReturn(exportSingleData);
        when(exportSingleData.getFileImage()).thenReturn("Test".getBytes(Charset.defaultCharset()));
        when(exportSingleData.getContentType()).thenReturn("application/vnd.ms-excel");

        // When
        ResultActions result = mockMvc.perform(get("/exportservice/getsingle").params(multiValMap));

        // Then
        result.andDo(print());
        result.andExpect(status().isOk());
        result.andExpect(content().contentType("application/vnd.ms-excel"));
    }

    @Test
    public void testContentTypePDF() throws Exception {
        // Given
        String json = getJson("singleexport_debt.json");
        MultiValueMap<String, String> multiValMap = new LinkedMultiValueMap<>();
        multiValMap.add("json", json);
        when(singleExportService.process(any(SingleTrader.class))).thenReturn(exportSingleData);
        when(exportSingleData.getFileImage()).thenReturn("Test".getBytes(Charset.defaultCharset()));
        when(exportSingleData.getContentType()).thenReturn("application/pdf");

        // When
        ResultActions result = mockMvc.perform(get("/exportservice/getsingle").params(multiValMap));

        // Then
        result.andDo(print());
        result.andExpect(status().isOk());
        result.andExpect(content().contentType("application/pdf"));
    }


    private String getJson(String pathToFile) throws IOException {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(pathToFile)) {
            return IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        }
    }
}