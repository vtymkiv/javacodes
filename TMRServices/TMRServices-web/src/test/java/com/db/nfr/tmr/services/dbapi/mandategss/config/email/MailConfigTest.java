package com.db.nfr.tmr.services.dbapi.mandategss.config.email;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.subethamail.wiser.Wiser;

import com.db.nfr.tmr.services.utils.MailSender;
import com.db.nfr.tmr.services.utils.MailUtils;

/**
 * Created by tymkvit on 23/11/2016.
 */
@Configuration
@Profile("integration-test")
@EnableConfigurationProperties
public class MailConfigTest {

    @Value("${spring.mail.host:localhost}")
    private String smtpHost;

    @Value("${spring.mail.port:225}")
    private int smtpPort;

    @Bean(destroyMethod = "stop")
    @Primary
    public Wiser wiser() {
        Wiser wiser = new Wiser();
        wiser.setHostname(smtpHost);
        wiser.setPort(smtpPort);
        return wiser;
    }

    @Bean
    public Workbook workbook() {
        Workbook workbook = new SXSSFWorkbook();
        Sheet sheet = workbook.createSheet("TEST_1");
        sheet.createRow(0);
        return workbook;
    }

    @Bean(name = "mailSenderTest")
    public MailSender mailSender() {
        return new MailSender();
    }

    @Bean(name = "mailUtilsTest")
    public MailUtils mailUtils() {
        return new MailUtils();
    }
}
