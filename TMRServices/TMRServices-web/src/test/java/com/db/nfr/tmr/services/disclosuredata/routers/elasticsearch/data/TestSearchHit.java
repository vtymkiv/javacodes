package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * Created by tymkvit on 07/04/2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestSearchHit {
    @JsonProperty("guiKey")
    private String guiKey;
    @JsonProperty("dto")
    private TestDto dto;
    @JsonProperty("disclosureDataRows")
    private List<TestDisclosureDataRow> disclosureDataRows;
}
