package com.db.nfr.tmr.services.dbapi;

import javax.sql.DataSource;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.nfr.tmr.services.common.MandateType;
import com.db.nfr.tmr.services.common.Request;
import com.db.nfr.tmr.services.common.RequestItem;
import com.db.nfr.tmr.services.common.ResultSetFunction;
import com.db.nfr.tmr.services.dbapi.mandategss.config.datasources.DatasourceConfigTest;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by tymkvit on 30/01/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DatasourceConfigTest.class, PkgMandateGssRecertif.class, CustomOracleTypes.class})
@ActiveProfiles("integration-test")
@IfProfileValue(name = "integration-test", value = "true")
public class PkgMandateGssRecertifTest {
    @Autowired
    @Qualifier("tmsAppDS")
    DataSource dataSource;

    @Autowired
    private PkgMandateGssRecertif pkgMandateGssRecertif;

    @Test
    public void testGetMandatesForRecertifWithUBRColumns() throws Exception {
        // Given
        int version = 8823;
        Request request = new Request("testUser@mail.com");
        String localVersion = "26/01/2017 09:31:37";
        String nextLocalVersion = "26/01/2017 09:31:37";
        String emplId = "3801987";

        RequestItem reqItem = new RequestItem("testUser@mail.com", emplId, localVersion, MandateType.DEBT.getCode(), nextLocalVersion);
        request.getItems().add(reqItem);

        // When
        pkgMandateGssRecertif.pGetMandatesForRecertif(dataSource.getConnection(), request, version, new ResultSetFunction() {
            @Override
            public void run(ResultSet rs) throws Exception {
                // Then
                assertThat(rs, notNullValue());
                ResultSetMetaData metaData = rs.getMetaData();
                assertThat(metaData, notNullValue());
                assertThat(metaData.getColumnLabel(2), is("Business UBR 6"));
                assertThat(metaData.getColumnLabel(3), is("Business UBR 7"));
                assertThat(metaData.getColumnLabel(4), is("Business UBR 8"));
            }
        });
    }

    @Test
    @Rollback
    public void pGetVersion() throws Exception {
        // Given
        int version = 8823;
        Request request = new Request("testUser@mail.com");
        String localVersion = "26/01/2017 09:31:37";
        String nextLocalVersion = "26/01/2017 09:31:37";
        String emplId = "3801987";

        RequestItem reqItem = new RequestItem("testUser@mail.com", emplId, localVersion, MandateType.DEBT.getCode(), nextLocalVersion);
        request.getItems().add(reqItem);

        // When
        ProcessingStatus processingStatus = pkgMandateGssRecertif.pGetVersion(dataSource.getConnection(), request);

        // Then
        assertThat(processingStatus, notNullValue());
    }
}