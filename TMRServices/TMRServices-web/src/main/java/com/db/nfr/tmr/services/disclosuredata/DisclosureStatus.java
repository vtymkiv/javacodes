package com.db.nfr.tmr.services.disclosuredata;

import lombok.Getter;

/**
 * Created by tymkvit on 30/03/2017.
 */
public enum DisclosureStatus {
    CLOSED("Closed"), COMPLETE("Complete"), IN_PROGRESS("In Progress"), UNKNOWN("Unknown");
    @Getter
    private String code;

    DisclosureStatus(String code) {
        this.code = code;
    }
}
