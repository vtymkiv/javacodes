package com.db.nfr.tmr.services.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * Created by gasiork on 20/04/2016.
 *
 * @author Vitaliy Tymkiv
 */
public class JsonOptions {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonOptions.class);

    private final JsonArray jsonArr;
    private final String user;
    public final Boolean isNew;

    public JsonOptions(String json) {
        LOGGER.debug("Parsing JSON: {}", json);
        JsonObject jsonObj = new JsonParser().parse(json).getAsJsonObject();
        this.jsonArr = jsonObj.getAsJsonArray("TRADERS");
        this.user = jsonObj.getAsJsonPrimitive("USER").getAsString();
        if (jsonObj.has("IS_NEW")) {
            isNew = jsonObj.getAsJsonPrimitive("IS_NEW").getAsBoolean();
        } else {
            isNew = null;
        }
    }

    public Request getRequest() {
        Request req = new Request(this.user);
        for (int i = 0; i < jsonArr.size(); i++) {
            JsonObject elem = jsonArr.get(i).getAsJsonObject();
            RequestItem reqItem = new RequestItem(
                    elem.getAsJsonPrimitive("traderEmail").getAsString(),
                    elem.getAsJsonPrimitive("employeeId").getAsString(),
                    elem.getAsJsonPrimitive("localVersion").getAsString(),
                    elem.getAsJsonPrimitive("mandateType").getAsString(),
                    elem.getAsJsonPrimitive("nextLocalVersion").getAsString()
            );
            req.getItems().add(reqItem);
        }
        LOGGER.debug("Getting request: {}", req);
        return req;
    }
}
