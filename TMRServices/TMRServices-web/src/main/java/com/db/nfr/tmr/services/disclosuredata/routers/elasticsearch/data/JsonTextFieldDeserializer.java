package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.OptionalInt;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import lombok.extern.slf4j.Slf4j;

/**
 * Deserializer for the text field that will be truncated to the specified by <code>JsonPropertyLength</code>
 * annotation <code>max</code> length. If no annotation <code>JsonPropertyLength</code> is present the length of the field would be the same as in json field.
 * <p>
 * Created by tymkvit on 07/04/2017.
 */
@Slf4j
class JsonTextFieldDeserializer extends JsonDeserializer {
    @Override
    public String deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Field[] declaredFields = jsonParser.getCurrentValue().getClass().getDeclaredFields();
        String currentName = jsonParser.getCurrentName();
        OptionalInt max = Arrays.stream(declaredFields).filter(declaredField -> declaredField.isAnnotationPresent(JsonPropertyLength.class))
                .filter(declaredField -> {
                    if (declaredField.isAnnotationPresent(JsonProperty.class)) {
                        JsonProperty jsonPropertyAnnotation = declaredField.getDeclaredAnnotation(JsonProperty.class);
                        return currentName.equals(jsonPropertyAnnotation.value());
                    } else {
                        return currentName.equals(declaredField.getName());
                    }
                })
                .mapToInt(field -> {
                    JsonPropertyLength declaredAnnotation = field.getDeclaredAnnotation(JsonPropertyLength.class);
                    return declaredAnnotation.max();
                }).max();
        String txt = jsonParser.getValueAsString(); //textField.asText();
        if (max.isPresent()) {
            txt = StringUtils.substring(txt, 0, max.getAsInt());
        }
        return txt;
    }
}
