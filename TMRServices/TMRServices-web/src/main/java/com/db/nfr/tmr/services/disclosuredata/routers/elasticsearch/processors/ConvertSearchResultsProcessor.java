package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.processors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.collections4.CollectionUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.common.FileRecord;
import com.db.nfr.tmr.services.disclosuredata.DisclosureData;
import com.db.nfr.tmr.services.disclosuredata.DisclosureDataService;
import com.db.nfr.tmr.services.disclosuredata.DisclosureStatus;
import com.db.nfr.tmr.services.disclosuredata.converters.Converter;
import com.db.nfr.tmr.services.disclosuredata.converters.DisclosureDataTransformationService;
import com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data.SearchHitDataSource;
import com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data.SearchHitDisclosureDataRow;
import com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data.SearchHitDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ConvertSearchResultsProcessor implements Processor {

    @Autowired
    @Qualifier("jsonConverter")
    private Converter converter;

    @Autowired
    private DisclosureDataService disclosureDataService;

    @Autowired
    private DisclosureDataTransformationService disclosureDataTransformService;


    @Override
    public void process(Exchange exchange) throws Exception {
        SearchResponse searchResponse = exchange.getIn().getBody(SearchResponse.class);
        List<SearchHitDataSource> searchHitDataSourceList = new ArrayList<>();
        final FileRecord savedFileRecord = disclosureDataService.saveFile("el-search-response", searchResponse.toString());

        searchResponse.getHits().forEach(searchHit -> {
            String searchHitSource = searchHit.getSourceAsString();
            SearchHitDataSource searchHitDataSource = null;
            try {
                searchHitDataSource = converter.convertFrom(searchHitSource, SearchHitDataSource.class);
                searchHitDataSourceList.add(searchHitDataSource);
            } catch (IOException e) {
                LOGGER.error("Could not convert the hit source {} into  SearchHitDataSource object.", searchHitSource);
            }
            // If the exception happened or by some reason converter returned null continue with next element(hit source)
            if (searchHitDataSource == null) {
                return;
            }

        });

        List<DisclosureData> disclosureDataList = searchHitDataSourceList.stream().flatMap(searchHitDataSource -> {
            SearchHitDto dto = searchHitDataSource.getDto();
            final String status = dto.getStatus();
            final String guiKey = dto.getGuiKey();
            final String evidenceProvisionDate = dto.getEvidenceProvisionDate();
            return searchHitDataSource.getDisclosureDataRows().stream().map(dataRow -> getDisclosureData(savedFileRecord, status, guiKey, evidenceProvisionDate, dataRow))
                    .collect(Collectors.toList()).stream();

        }).collect(Collectors.toList());

        if (CollectionUtils.isEmpty(disclosureDataList)) {
            LOGGER.info("No disclosure data rows have been found in response: {}", searchResponse);
            return;
        }
        disclosureDataService.recertifyMandates(disclosureDataList);
    }

    private DisclosureData getDisclosureData(FileRecord savedFileRecord, String status, String guiKey, String evidenceProvisionDate, SearchHitDisclosureDataRow dataRow) {
        DisclosureData disclosureData = new DisclosureData();
        disclosureData.setVersionId(dataRow.getVersionId());
        disclosureData.setTraderEmail(dataRow.getTraderEmail());
        disclosureData.setAcknowledge(dataRow.getAcknowledge());

        disclosureData.setComment(dataRow.getComment());
        disclosureData.setReason(dataRow.getReason());
        // TODO: Consider to rewrite this using annotation as it was done here: com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data.JsonTextFieldDeserializer
        disclosureDataTransformService.setDates(evidenceProvisionDate, dataRow.getMandateDate(), disclosureData);
        disclosureDataTransformService.setRecertificationAndMandateType(guiKey, disclosureData);
        DisclosureStatus discStatus = disclosureDataTransformService.getStatus(status);
        disclosureData.setStatus(discStatus.getCode());
        if (savedFileRecord != null) {
            disclosureData.setFileId(savedFileRecord.getId());
        } else {
            LOGGER.debug("Was not able to set file id for {}.The problem with saving file happened", disclosureData);
        }
        return disclosureData;
    }
}
