package com.db.nfr.tmr.services.excel;

import javax.sql.rowset.CachedRowSet;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by gasiork on 05/07/2016.
 */
public class ResultSetTitleRowMapper implements RowMapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResultSetTitleRowMapper.class);
    private CachedRowSet cachedRowSet;

    private final ResultSetMetaData metadata;
    private boolean hasNext;

    public ResultSetTitleRowMapper(CachedRowSet cachedRowSet) throws SQLException {
        this.cachedRowSet = cachedRowSet;
        this.metadata = cachedRowSet.getMetaData();
        hasNext = true;
    }


    @Override
    public boolean hasNextRow() {
        boolean hasNextRow = hasNext;
        hasNext = false;
        return hasNextRow;
    }

    @Override
    public String getCellValue(int columnNumber) {
        LOGGER.debug("Getting column name for column with index {}", columnNumber);
        try {
            //columnNumber start from 0, but metadata column index starts from 1
            return metadata.getColumnName(columnNumber + 1);
        } catch (SQLException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
}
