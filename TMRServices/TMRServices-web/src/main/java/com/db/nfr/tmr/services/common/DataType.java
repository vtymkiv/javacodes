/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.nfr.tmr.services.common;

/**
 * @author vtymkiv
 */
public enum DataType {
    SUPERVISOR("supervisor"),
    TRADER("trader");

    private final String name;

    DataType(String dataTypeName) {
        this.name = dataTypeName;
    }

    public String getName() {
        return name;
    }

    public static DataType findByCode(String name) {
        for (DataType dataType : values()) {
            if (dataType.getName().equalsIgnoreCase(name)) {
                return dataType;
            }
        }
        return null;
    }
}
