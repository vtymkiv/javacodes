package com.db.nfr.tmr.services.config.mail;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.db.nfr.tmr.services.mail.MailSender;

/**
 * Created by tymkvit on 22/09/2016.
 */
@Configuration
public class MailConfig {
    @Bean(name = "newMailSender")
    public MailSender newMailSender() {
        return new MailSender(javaMailSender());
    }

    @Bean(name = "oldMailSender")
    public com.db.nfr.tmr.services.utils.MailSender mailSender() {
        return new com.db.nfr.tmr.services.utils.MailSender();
    }

    @Bean
    public JavaMailSender javaMailSender() {
        return new JavaMailSenderImpl();
    }

}
