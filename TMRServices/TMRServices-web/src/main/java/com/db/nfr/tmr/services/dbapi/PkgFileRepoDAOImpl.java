package com.db.nfr.tmr.services.dbapi;

import javax.sql.DataSource;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.stereotype.Repository;

import com.db.nfr.tmr.services.common.FileRecord;

import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleTypes;

/**
 * Created by tymkvit on 02/02/2017.
 */
@Slf4j
@Repository("pkgFileRepoDAOImpl")
public class PkgFileRepoDAOImpl implements PkgFileRepoDAO {

    @Autowired
    private CustomOracleTypes customOracleTypes;

    private SimpleJdbcCall checkFilesSP;
    private SimpleJdbcCall loadFileSP;

    public PkgFileRepoDAOImpl(@Qualifier("stgFeedDS") DataSource dataSource) {
        checkFilesSP = new SimpleJdbcCall(dataSource)
                .withSchemaName("stg_owner")
                .withCatalogName("pkg_file_repo")
                .withProcedureName("check_files")
                .useInParameterNames()
                .declareParameters(
                        new SqlParameter("ip_files", OracleTypes.ARRAY),
                        new SqlOutParameter("op_cursor", OracleTypes.CURSOR)
                );

        loadFileSP = new SimpleJdbcCall(dataSource).withSchemaName("stg_owner")
                .withCatalogName("pkg_file_repo").withProcedureName("load_file")
                .useInParameterNames()
                .withoutProcedureColumnMetaDataAccess()
                .declareParameters(
                        new SqlParameter("ip_file_id", OracleTypes.NUMBER),
                        new SqlParameter("ip_file_image", OracleTypes.BLOB)
                );
    }


    /**
     * Get list of new files. Record with status NEW will be created.
     * It will call pkg_file_repo.check_files
     *
     * @param fileList   List of all files
     * @param returnType The type that will be populate with values from database and return as list of returned type
     * @return List of new files(returned types)
     */
    @Override
    public <T> List<T> checkFiles(List<TypeRecAllFiles> fileList, Class<T> returnType) throws SQLException {
        LOGGER.debug("Calling check files with file list {} ", fileList);
        checkFilesSP.returningResultSet("op_cursor", BeanPropertyRowMapper.newInstance(returnType));
        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_files", customOracleTypes.getRecAllFilesStructArray(checkFilesSP.getJdbcTemplate().getDataSource(), fileList));
        Map<String, Object> result = checkFilesSP.execute(inParams);
        return (List<T>) result.get("op_cursor");
    }


    @Override
    public void loadFile(FileRecord fileRecord, InputStream is) throws SQLException, IOException {
        Objects.requireNonNull(fileRecord, "fileRecord cannot be null");
        Objects.requireNonNull(is, "file content cannot be null");
        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_file_id", fileRecord.getId())
                .addValue("ip_file_image", new SqlLobValue(IOUtils.toByteArray(is)));
        loadFileSP.execute(inParams);
    }
}
