package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * The class that would be used for mapping from the json like this:
 * <pre>
 *     "_source" : {
 * "id" : zzzzz,
 * "guiKey" : "tmRec_gme_trader",
 * ....
 * "dto" : {
 * .....
 * },
 * "disclosureDataRows" : [ {
 * } ],
 * "disclosureDataEnrichments" : true,
 * "other" : { },
 * "actionType" : "EV_DISCLOSURE_DATA_ENRICHMENTS",
 * "actionTypes" : [ "EV_DISCLOSURE_DATA_ENRICHMENTS" ],
 * "allowedActions" : [ ]
 * }
 * </pre>
 * <p>
 * Created by tymkvit on 30/03/2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchHitDataSource {
    @JsonProperty("guiKey")
    private String guiKey;
    @JsonProperty("dto")
    private SearchHitDto dto;
    @JsonProperty("disclosureDataRows")
    private List<SearchHitDisclosureDataRow> disclosureDataRows;
}
