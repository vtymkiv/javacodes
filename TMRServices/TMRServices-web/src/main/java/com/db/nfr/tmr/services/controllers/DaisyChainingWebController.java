package com.db.nfr.tmr.services.controllers;

import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import com.db.nfr.tmr.services.common.DataType;
import com.db.nfr.tmr.services.common.LocalHost;
import com.db.nfr.tmr.services.common.MandateType;
import com.db.nfr.tmr.services.config.RestConfigProperties;

/**
 * Created by tymkvit on 15/12/2016.
 */
@Controller
public class DaisyChainingWebController {
    public static final String DAISY_CHAINING_STATUS_FOR_DEBT = "daisyChainingStatusForDebt";
    public static final String DAISY_CHAINING_STATUS_FOR_EQUITY = "daisyChainingStatusForEquity";
    public static final String LOCAL_HOST_URL = "localHostUrl";

    private static Logger LOGGER = LoggerFactory.getLogger(DaisyChainingWebController.class);

    @Autowired
    private LocalHost localHost;

    @Autowired
    private RestConfigProperties restConfigProperties;

    @Autowired
    private RestTemplate restTemplate;


    @RequestMapping(value = "/")
    public String index(Model model) {
        // Get status info about supervisor debt and equities
        setDaisyChainingStatModel(model);
        return "index";
    }

    @RequestMapping(value = "/daisychaining", method = RequestMethod.POST)
    public String daisyChaining(@RequestParam(name = "mandateType") String mandateType, @RequestParam(name = "daisyChainingURL") String daisyChainingURL, final RedirectAttributes
            redirectAttributes) {

        LOGGER.debug("Calling daisy chaining rest started with url [{}], mandateType [{}] and settings [{}] ...", daisyChainingURL, mandateType, restConfigProperties);

        String retVal = mandateType;

        StringBuilder sbUrl = new StringBuilder();
        sbUrl
                .append(daisyChainingURL)
                .append(restConfigProperties.getRestEndpoints().getBaseUrl())
                .append(restConfigProperties.getRestEndpoints().getDaisyChainingEndpoint().replace("${dataType}", DataType.SUPERVISOR.getName()).replace("${mandateType}",
                        MandateType.findByCode(mandateType).getCode()));

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(sbUrl.toString());
        // call rest that are located in tmr-to-gss-mail-feed
        HttpHeaders headers = getHttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.POST, entity, String.class);
        retVal += " Ok " + response.getBody();

        redirectAttributes.addFlashAttribute("dcStatus", retVal);

        LOGGER.debug("Daisy chaining ended.");

        // Redirect to index page
        return "redirect:/";
    }

    private void setDaisyChainingStatModel(Model model) {
        DaisyChainingStat daisyChainingStatusForDebt = getDaisyChainingStatus(MandateType.DEBT);
        DaisyChainingStat daisyChainingStatusForEquity = getDaisyChainingStatus(MandateType.EQUITY);
        model.addAttribute(DAISY_CHAINING_STATUS_FOR_DEBT, daisyChainingStatusForDebt);
        model.addAttribute(DAISY_CHAINING_STATUS_FOR_EQUITY, daisyChainingStatusForEquity);
        model.addAttribute(LOCAL_HOST_URL, getUrlToLocalHost(localHost.getLocalHostName()));
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE));
        headers.setAccept(Collections.singletonList(MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE)));
        return headers;
    }

    private DaisyChainingStat getDaisyChainingStatus(MandateType mandateType) {
        String localHostName = localHost.getLocalHostName();
        LOGGER.debug("Getting stat info calling daisy chaining rest started with url [{}] and settings [{}] ... for {}", localHostName, restConfigProperties, mandateType);

        StringBuilder sbUrl = getUrlToDaisyChainingStatusEndpoint(mandateType, localHostName);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(sbUrl.toString());
        // call rest that are located in tmr-to-gss-mail-feed
        ResponseEntity<DaisyChainingStat> response;
        HttpHeaders headers = getHttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);

        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, DaisyChainingStat.class);
        return response.getBody();
    }

    private StringBuilder getUrlToDaisyChainingStatusEndpoint(MandateType mandateType, String localHostName) {
        StringBuilder sbUrl = new StringBuilder();
        sbUrl
                .append(getUrlToLocalHost(localHostName))
                .append(restConfigProperties.getRestEndpoints().getBaseUrl())
                .append(restConfigProperties.getRestEndpoints().getDaisyChainingStatusEndpoint().replace("${dataType}", DataType.SUPERVISOR.getName()).replace("${mandateType}",
                        mandateType.getCode()));
        LOGGER.debug("getUrlToDaisyChainingStatusEndpoint {}", sbUrl);
        return sbUrl;
    }

    private StringBuilder getUrlToLocalHost(String localHostName) {
        StringBuilder sbUrl = new StringBuilder();
        sbUrl.append(restConfigProperties.getSchema())
                .append(restConfigProperties.getHost().replace("${localhost}", localHostName).replace("${port}", restConfigProperties.getPort()));
        LOGGER.debug("getUrlToLocalHost {}", sbUrl);
        return sbUrl;
    }
}
