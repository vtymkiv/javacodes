package com.db.nfr.tmr.services.disclosuredata.converters;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.common.CertificationPersonType;
import com.db.nfr.tmr.services.common.MandateType;
import com.db.nfr.tmr.services.disclosuredata.DisclosureData;
import com.db.nfr.tmr.services.disclosuredata.DisclosureStatus;
import com.db.nfr.tmr.services.disclosuredata.DtoStatus;


import static com.db.nfr.tmr.services.disclosuredata.converters.DateConverterServiceImpl.NEW_EVIDENCE_PROVISION_DATE_PATTERN;
import static com.db.nfr.tmr.services.disclosuredata.converters.DateConverterServiceImpl.NEW_MANDATE_DATE_PATTERN;
import static com.db.nfr.tmr.services.disclosuredata.converters.DateConverterServiceImpl.ORIGINAL_EVIDENCE_PROVISION_DATE_PATTERN;
import static com.db.nfr.tmr.services.disclosuredata.converters.DateConverterServiceImpl.ORIGINAL_MANDATE_DATE_PATTERN;

/**
 * Created by tymkvit on 28/03/2017.
 */
@Component
public class DisclosureDataTransformationServiceImpl implements DisclosureDataTransformationService {
    private static final String TRADER = "trader";
    private static final String SUPERVISOR = "supervisor";
    private static final String DEBT = "fic";
    private static final String EQT = "gme";

    @Autowired
    private DateConverterService dateConverterService;

    @Override
    public void setRecertificationAndMandateType(String guiKey, DisclosureData disclosureData) {
        if (guiKey.contains(TRADER)) {
            disclosureData.setCertificationType(CertificationPersonType.TRADER.getCode());
        } else if (guiKey.contains(SUPERVISOR)) {
            disclosureData.setCertificationType(CertificationPersonType.SUPERVISOR.getCode());
        } else {
            disclosureData.setCertificationType(CertificationPersonType.UNKNOWN.getCode());
        }

        if (guiKey.contains(DEBT)) {
            disclosureData.setMandateType(MandateType.DEBT.getCode());
        } else if (guiKey.contains(EQT)) {
            disclosureData.setMandateType(MandateType.EQUITY.getCode());
        } else {
            disclosureData.setMandateType(MandateType.UNKNOWN.getCode());
        }
    }

    @Override
    public void setDates(String evidenceProvisionDate, String mandateDate, DisclosureData disclosureData) {
        if (StringUtils.isNotBlank(evidenceProvisionDate)) {
            disclosureData.setProvisionDate(dateConverterService.convertDateTo(evidenceProvisionDate, ORIGINAL_EVIDENCE_PROVISION_DATE_PATTERN, NEW_EVIDENCE_PROVISION_DATE_PATTERN));
        }

        if (StringUtils.isNotBlank(mandateDate)) {
            disclosureData.setMandateDate(dateConverterService.convertDateTo(mandateDate, ORIGINAL_MANDATE_DATE_PATTERN, NEW_MANDATE_DATE_PATTERN));
        }
    }

    @Override
    public DisclosureStatus getStatus(DtoStatus dtoStatus) {
        switch (dtoStatus) {
            case CLOSED_OK:
                return DisclosureStatus.COMPLETE;
            case CLOSED_NOK:
                return DisclosureStatus.CLOSED;
            case PROVISION:
            case REMINDER:
            case ESCALATION:
                return DisclosureStatus.IN_PROGRESS;
            default:
                return DisclosureStatus.UNKNOWN;
        }
    }

    @Override
    public DisclosureStatus getStatus(String status) {
        DtoStatus dtoStatus = DtoStatus.findByCode(status);
        return getStatus(dtoStatus);
    }

}
