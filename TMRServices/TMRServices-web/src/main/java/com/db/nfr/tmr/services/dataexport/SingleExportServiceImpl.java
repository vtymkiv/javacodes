package com.db.nfr.tmr.services.dataexport;

import javax.sql.DataSource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.common.ExportSingleData;
import com.db.nfr.tmr.services.common.NoDataFoundException;
import com.db.nfr.tmr.services.common.SingleTrader;
import com.db.nfr.tmr.services.dbapi.PkgMandateGssRecertif;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by gasiork on 26/04/2016.
 */
@Slf4j
@Component
public class SingleExportServiceImpl implements SingleExportService {

    @Autowired
    @Qualifier("tmsAppDS")
    private DataSource tmsAppDataSource;

    @Autowired
    private PkgMandateGssRecertif pkgMandateGssRecertif;

    public ExportSingleData process(SingleTrader singleTraderJson) throws SQLException, IOException, NoDataFoundException {
        LOGGER.debug("Calling recertifyMandates started");
        try (Connection tmsAppDataSourceConnection = tmsAppDataSource.getConnection()) {
            ExportSingleData exportSingleData = pkgMandateGssRecertif.pExportMandateFile(singleTraderJson, tmsAppDataSourceConnection);
            LOGGER.debug("Calling recertifyMandates completed");
            return exportSingleData;
        }
    }
}