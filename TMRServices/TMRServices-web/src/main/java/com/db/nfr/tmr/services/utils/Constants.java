package com.db.nfr.tmr.services.utils;

public final class Constants {
    // Default private Constructor
    private Constants() {
    }

    public static final String HOST = "host";
    public static final String PORT = "port";
    public static final String PORT_PROPERTY = "mail.smtp.port";
    public static final String HOST_PROPERTY = "mail.smtp.host";
    public static final String TO = "to";
    public static final String TO_COUNT = "to.count";
    public static final String FROM = "from";
    public static final String SUBJECT = "subject";
    public static final String FILE_NAME = "file.name";
    public static final String FILE_NAME_EQT = "file.name.equity";
    public static final String FILE_NAME_DEBT = "file.name.debt";
    public static final String FILE_TYPE = "file.type";
    public static final String GSS_OUTPUT_FEED_CONFIG_ID = "gss.output.feed.config.id";

    //GSS XLS
    public static final String EXPORT_VERSION_COLUMN = "export.version.column";

    public static final String TIMER_TIMEZONE_ID = "Europe/London";
    //Version Status from DB
    public static final int VERSION_IS_NULL = -2;
    public static final int VERSION_IS_NOT_NUMBER = -1;
    public static final int NO_REPLY_FROM_DB = 0;
}
