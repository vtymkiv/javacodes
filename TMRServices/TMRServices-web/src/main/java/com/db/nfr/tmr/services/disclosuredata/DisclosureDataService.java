package com.db.nfr.tmr.services.disclosuredata;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.db.nfr.tmr.services.common.FileRecord;
import com.db.nfr.tmr.services.disclosuredata.converters.Converter;

/**
 * Created by tymkvit on 09/03/2017.
 */
public interface DisclosureDataService {
    /**
     * Recertificate mandate
     *
     * @param disclosureData
     */
    void recertifyMandates(DisclosureData disclosureData);

    void recertifyMandates(String guiKey, String msg);

    void recertifyMandates(String guiKey, String msg, Converter converter) throws IOException;

    FileRecord saveFile(FileRecord fileRecord, InputStream fileContent);

    /**
     * Save text as file in data storage
     *
     * @param s
     * @param text
     */
    FileRecord saveFile(String s, String text);

    List<String> getVersionIdList();

    void recertifyMandates(List<DisclosureData> disclosureDataList);
}
