package com.db.nfr.tmr.services.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.template.Configuration;

/**
 * Created by tymkvit on 15/12/2016.
 */
@ControllerAdvice
public class FreeMarkerControllerAdvice {

    @ModelAttribute
    public Model getGlobalAttr(Model model) {
        model.addAttribute("statics", new BeansWrapperBuilder(Configuration.getVersion()).build().getStaticModels());
        return model;
    }

    @ModelAttribute
    public Model getEnums(Model model) {
        model.addAttribute("enums", new BeansWrapperBuilder(Configuration.getVersion()).build().getEnumModels());
        return model;
    }

}
