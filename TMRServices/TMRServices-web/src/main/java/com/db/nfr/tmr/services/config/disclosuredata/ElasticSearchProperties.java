package com.db.nfr.tmr.services.config.disclosuredata;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.ToString;

/**
 * Created by tymkvit on 08/12/2016.
 */
@Data
@ToString
@Component
@ConfigurationProperties(prefix = "elastic")
public class ElasticSearchProperties {
    @NotBlank
    private String hostName;

    private int port;

    @NotBlank
    private String indexName;
    @NotBlank
    private String typeName;
    @NotBlank
    private String pathToRequest;
    @NotBlank
    private String schedule;

    private List<Setting> settings;

    @Data
    public static class Setting {
        private String name;
        private String value;
    }
}
