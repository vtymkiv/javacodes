package com.db.nfr.tmr.services.rest.appinfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db.nfr.tmr.services.config.appinfo.AppInfoProperties;

/**
 * This class represents application information rest service
 */
@Service
public class ApplicationInfoServiceImpl implements ApplicationInfoService {
    @Autowired
    private AppInfoProperties appInfoProperties;


    @Override
    public WsApplicationInfo getApplicationInfo() {
        AppInfoProperties.Build buildInfo = appInfoProperties.getBuild();
        AppInfoProperties.Commit commitInfo = appInfoProperties.getCommit();

        WsApplicationInfo info = new WsApplicationInfo();
        info.setTime(buildInfo.getTime());
        info.setArtifact(buildInfo.getArtifact());
        info.setGroup(buildInfo.getGroup());
        info.setName(buildInfo.getName());
        info.setVersion(buildInfo.getVersion());
        info.setBuildNumber(buildInfo.getNumber());
        info.setUserName(buildInfo.getUserName());
        info.setUserEmail(buildInfo.getUserEmail());
        info.setCommitTime(commitInfo.getTime());
        info.setCommitUserName(commitInfo.getUserName());
        info.setShortCommitId(commitInfo.getShortCommitId());
        info.setBranchName(commitInfo.getBranch());
        return info;
    }

    @Override
    public String toString() {
        return "ApplicationInfoServiceImpl{" +
                "appInfoProperties=" + appInfoProperties +
                '}';
    }
}
