package com.db.nfr.tmr.services.dbapi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author gasiork
 */
public class ProcessingStatus {
    private final List<String> traders = new ArrayList<>();
    private final int version;
    private final String status;

    public ProcessingStatus(int version, String status) {
        this.version = version;
        this.status = status;
    }

    public int getVersion() {
        return version;
    }

    public void addTrader(String trader) {
        traders.add(trader);
    }

    public List<String> getTraders() {
        return Collections.unmodifiableList(traders);
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Version = " + version + ", Status = " + status + ", Traders = " + traders;
    }
}
