package com.db.nfr.tmr.services.dbapi;

import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import oracle.jdbc.OracleConnection;

/**
 * Created by gasiork on 02/08/2016.
 */
public class TypeRecAllFiles {
    private int feedConfigId;
    private String fileName;
    private Date fileDate;
    private long fileSize;

    public TypeRecAllFiles(int feedConfigId, String fileName, Date fileDate, long fileSize) {
        this.feedConfigId = feedConfigId;
        this.fileName = fileName;
        this.fileDate = new Date(fileDate.getTime());
        this.fileSize = fileSize;
    }

    public Array getStructArray(Connection conn) throws SQLException {
        Object[] data = new Object[]{
                conn.createStruct("STG_OWNER.REC_ALL_FILES", new Object[]{
                        this.feedConfigId,
                        this.fileName,
                        (this.fileDate == null) ? null : new java.sql.Date(this.fileDate.getTime()),
                        this.fileSize
                })};
        OracleConnection oracleConnection = conn.unwrap(OracleConnection.class);
        return oracleConnection.createARRAY("STG_OWNER.T_ALL_FILES", data);
    }

    public int getFeedConfigId() {
        return feedConfigId;
    }

    public String getFileName() {
        return fileName;
    }

    public Date getFileDate() {
        return new Date(fileDate.getTime());
    }

    public long getFileSize() {
        return fileSize;
    }


}
