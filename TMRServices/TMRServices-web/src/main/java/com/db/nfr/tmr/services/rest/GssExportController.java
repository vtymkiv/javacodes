package com.db.nfr.tmr.services.rest;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.db.nfr.tmr.services.common.JsonOptions;
import com.db.nfr.tmr.services.common.Request;
import com.db.nfr.tmr.services.dataexport.GssExportUtils;
import com.db.nfr.tmr.services.dbapi.ProcessingStatus;

/**
 * @author gasiork
 */
@RestController
public class GssExportController {
    private static final Logger log = LoggerFactory.getLogger(GssExportController.class);
    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(1);
    private static final Lock lock = new ReentrantLock();

    @Autowired
    private GssExportUtils gssExportUtils;

    @RequestMapping(value = "/export/gss", method = {RequestMethod.GET, RequestMethod.POST})
    public String exportToGssByMail(String json) {
        log.info("GSS export started");
        lock.lock();
        log.info("GSS export locked");
        try {
            JsonOptions jsonData = new JsonOptions(json);
            final Request request = jsonData.getRequest();
            try {
                final ProcessingStatus ps = gssExportUtils.getVersionForTraders(request);
                request.leaveTraders(ps.getTraders());
                final int version = ps.getVersion();
                EXECUTOR.submit(new Runnable() {
                    @Override
                    public void run() {
                        log.info("Starting export of " + ps.toString());
                        gssExportUtils.exportByMail(request, version);
                    }
                });
                return Integer.toString(version);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                return "Error: " + ex.getClass().getName() + " with message '" + ex.getMessage() + "'";
            }
        } finally {
            lock.unlock();
            log.info("GSS export unlocked");
            log.info("GSS export finished");
        }
    }
}
