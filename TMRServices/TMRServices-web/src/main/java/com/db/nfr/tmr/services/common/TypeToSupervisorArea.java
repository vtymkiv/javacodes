package com.db.nfr.tmr.services.common;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

/**
 * Created by danyvik on 21/06/2016.
 */
public class TypeToSupervisorArea implements SQLData {
    private String sqlType;
    public String trader;
    public String trader_email;
    public String supervisor;
    public String supervisor_email;
    //    public String business_7;
    public String business_8;
    public String business_9;
    public String mandate_date;
    public String recertification_date;
    public String trader_status;
    public String is_new;
    public String supervisor_status;
    public String gss_comment;
    public String reason;
    public String prev_mandate_date;
    public String mandate_type;
    public String is_can_be_exported;
    public String gss_superv_comment;
    public String superv_reason;
    public String recertif_superv_date;

    public String[] toArray() {
        return new String[]{
                trader_email,
                supervisor_email,
                recertification_date,
                mandate_date,
                business_8,
                business_9,
                trader_status,
                supervisor_status,
                reason,
                gss_comment
        };
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return sqlType;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        sqlType = typeName;
        trader = stream.readString();
        trader_email = stream.readString();
        supervisor = stream.readString();
        supervisor_email = stream.readString();
//        business_7 = stream.readString();
        business_8 = stream.readString();
        business_9 = stream.readString();
        mandate_date = stream.readString();
        recertification_date = stream.readString();
        trader_status = stream.readString();
        is_new = stream.readString();
        supervisor_status = stream.readString();
        gss_comment = stream.readString();
        reason = stream.readString();
        prev_mandate_date = stream.readString();
        mandate_type = stream.readString();
        is_can_be_exported = stream.readString();
        gss_superv_comment = stream.readString();
        superv_reason = stream.readString();
        recertif_superv_date = stream.readString();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(trader);
        stream.writeString(trader_email);
        stream.writeString(supervisor);
        stream.writeString(supervisor_email);
//        stream.writeString(business_7);
        stream.writeString(business_8);
        stream.writeString(business_9);
        stream.writeString(mandate_date);
        stream.writeString(recertification_date);
        stream.writeString(trader_status);
        stream.writeString(is_new);
        stream.writeString(supervisor_status);
        stream.writeString(gss_comment);
        stream.writeString(reason);
        stream.writeString(prev_mandate_date);
        stream.writeString(mandate_type);
        stream.writeString(is_can_be_exported);
        stream.writeString(gss_superv_comment);
        stream.writeString(superv_reason);
        stream.writeString(recertif_superv_date);
    }
}

