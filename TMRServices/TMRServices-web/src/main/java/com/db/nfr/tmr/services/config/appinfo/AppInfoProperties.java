package com.db.nfr.tmr.services.config.appinfo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by tymkvit on 08/12/2016.
 */
@Component
@ConfigurationProperties(locations = {"classpath:TMRServices/application.yml"}, prefix = "info")
public class AppInfoProperties {
    private Build build;
    private Commit commit;

    public static class Build {
        private String number;
        private String time;
        private String artifact;
        private String group;
        private String name;
        private String version;
        private String userName;
        private String userEmail;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getArtifact() {
            return artifact;
        }

        public void setArtifact(String artifact) {
            this.artifact = artifact;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }

        @Override
        public String toString() {
            return "Build{" +
                    "number='" + number + '\'' +
                    ", time='" + time + '\'' +
                    ", artifact='" + artifact + '\'' +
                    ", group='" + group + '\'' +
                    ", name='" + name + '\'' +
                    ", version='" + version + '\'' +
                    ", userName='" + userName + '\'' +
                    ", userEmail='" + userEmail + '\'' +
                    '}';
        }
    }

    public static class Commit {
        private String time;
        private String userName;
        private String shortCommitId;
        private String branch;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getShortCommitId() {
            return shortCommitId;
        }

        public void setShortCommitId(String shortCommitId) {
            this.shortCommitId = shortCommitId;
        }

        public String getBranch() {
            return branch;
        }

        public void setBranch(String branch) {
            this.branch = branch;
        }

        @Override
        public String toString() {
            return "Commit{" +
                    "time='" + time + '\'' +
                    ", userName='" + userName + '\'' +
                    ", shortCommitId='" + shortCommitId + '\'' +
                    ", branch='" + branch + '\'' +
                    '}';
        }
    }

    public Build getBuild() {
        return build;
    }

    public void setBuild(Build build) {
        this.build = build;
    }

    public Commit getCommit() {
        return commit;
    }

    public void setCommit(Commit commit) {
        this.commit = commit;
    }

    @Override
    public String toString() {
        return "AppInfoProperties{" +
                "build=" + build +
                ", commit=" + commit +
                '}';
    }
}
