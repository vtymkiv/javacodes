package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch;

import java.net.InetSocketAddress;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.stereotype.Component;

/**
 * Created by tymkvit on 05/04/2017.
 */
@Component
public class ElasticSearchServiceImpl implements ElasticSearchService {
    @Override
    public Client getTransportClient(Settings settings, InetSocketAddress inetSocketAddress) {
        return TransportClient.builder()
                .settings(settings)
                .build()
                .addTransportAddress(new InetSocketTransportAddress(inetSocketAddress));
    }
}
