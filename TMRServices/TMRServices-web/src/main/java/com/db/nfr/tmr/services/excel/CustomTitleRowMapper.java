package com.db.nfr.tmr.services.excel;

/**
 * Created by gasiork on 05/07/2016.
 */
public abstract class CustomTitleRowMapper implements RowMapper {
    private boolean hasNext;

    public CustomTitleRowMapper() {
        hasNext = true;
    }

    @Override
    public boolean hasNextRow() {
        boolean hasNextRow = hasNext;
        hasNext = false;
        return hasNextRow;
    }

    @Override
    public abstract String getCellValue(int columnNumber);
}
