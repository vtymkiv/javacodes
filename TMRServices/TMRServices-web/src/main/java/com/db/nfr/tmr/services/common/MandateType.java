/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.nfr.tmr.services.common;

import lombok.Getter;

/**
 * @author tymkvit
 */

public enum MandateType {
    DEBT("DEBT"), EQUITY("EQT"), UNKNOWN("Unknown");
    @Getter
    private String code;

    MandateType(String code) {
        this.code = code;
    }

    public static MandateType findByCode(String code) {
        for (MandateType mandateType : values()) {
            if (mandateType.getCode().equalsIgnoreCase(code)) {
                return mandateType;
            }
        }
        return UNKNOWN;
    }
}
