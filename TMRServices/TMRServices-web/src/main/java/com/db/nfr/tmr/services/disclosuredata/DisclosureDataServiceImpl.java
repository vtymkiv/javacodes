package com.db.nfr.tmr.services.disclosuredata;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.common.FileRecord;
import com.db.nfr.tmr.services.dbapi.PkgFileRepoDAO;
import com.db.nfr.tmr.services.dbapi.PkgMandateRecertifDAO;
import com.db.nfr.tmr.services.dbapi.PkgStgMandatesRepoDAO;
import com.db.nfr.tmr.services.dbapi.TypeRecAllFiles;
import com.db.nfr.tmr.services.disclosuredata.converters.Converter;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by tymkvit on 09/03/2017.
 */
@Slf4j
@Component
public class DisclosureDataServiceImpl implements DisclosureDataService {
    private static final String IMAGE_TYPE = "JSON";
    @Autowired
    private Converter coneverter;

    @Autowired
    private PkgStgMandatesRepoDAO pkgStgMandatesRepoDAO;

    @Autowired
    private PkgMandateRecertifDAO pkgMandateRecertifDAO;

    @Autowired
    private PkgFileRepoDAO pkgFileRepoDAO;

    @Override
    public void recertifyMandates(DisclosureData disclosureData) {
        recertifyMandates(Arrays.asList(disclosureData));
    }

    @Override
    public void recertifyMandates(String guiKey, String msg) {
        try {
            recertifyMandates(guiKey, msg, coneverter);
        } catch (IOException ex) {
            LOGGER.error("Cannot recertificate mandate", ex);
            throw new RuntimeException("Cannot recertificate mandate", ex);
        }
    }

    @Override
    public void recertifyMandates(String guiKey, String msg, Converter converter) throws IOException {
        LOGGER.info("Recertification mandate for guiKey [{}] and json [{}]", guiKey, msg);
        Objects.requireNonNull(guiKey, "guiKey cannot be null");
        Objects.requireNonNull(msg, "json cannot be null");
        Objects.requireNonNull(converter, "converter cannot be null");
        FileRecord savedFileRecord = saveFile(guiKey, msg);
        DisclosureData disclosureData = converter.convertFrom(msg, DisclosureData.class);
        if (savedFileRecord != null) {
            disclosureData.setFileId(savedFileRecord.getId());
        } else {
            LOGGER.debug("The problem with saving file happened");
        }
        recertifyMandates(disclosureData);
    }


    @Override
    public FileRecord saveFile(FileRecord fileRecord, InputStream fileContent) {

        TypeRecAllFiles typeRecAllFiles = new TypeRecAllFiles(fileRecord.getFeedConfigId(), fileRecord.getFileName(), fileRecord.getDate(), fileRecord.getSize());
        FileRecord savedFileRec = null;
        try {
            LOGGER.debug("Start to check file {} ", typeRecAllFiles);
            List<FileRecord> fileRecordList = pkgFileRepoDAO.checkFiles(Arrays.asList(typeRecAllFiles), FileRecord.class);

            if (CollectionUtils.isNotEmpty(fileRecordList)) {
                savedFileRec = fileRecordList.get(0);
                LOGGER.debug("Loading file {} ", savedFileRec.getFileName());
                pkgFileRepoDAO.loadFile(savedFileRec, fileContent);
            } else {
                LOGGER.debug("Did not load file. check files returned empty list.");
            }
        } catch (SQLException ex) {
            LOGGER.error("Exception happened when saving file in database.", ex);
        } catch (IOException ex) {
            LOGGER.error("Exception happened when saving file.", ex);
        }
        return savedFileRec;
    }

    /**
     * Save text as file in data storage
     *
     * @param text
     */
    @Override
    public FileRecord saveFile(String guiKey, String text) {
        FileRecord savedFileRecord = null;
        try {
            File tempFile = File.createTempFile(guiKey + "-", "." + IMAGE_TYPE.toLowerCase());
            tempFile.deleteOnExit();
            if (tempFile.canWrite()) {
                Files.write(Paths.get(tempFile.getPath()), text.getBytes(Charset.defaultCharset()), StandardOpenOption.WRITE);
                FileRecord fileRecord = new FileRecord();
                fileRecord.setDate(new Date(tempFile.lastModified()));
                fileRecord.setFeedConfigId(113);
                fileRecord.setFileName(tempFile.getName());
                fileRecord.setImageType(IMAGE_TYPE);
                fileRecord.setSize(tempFile.length());

                try (InputStream is = new FileInputStream(tempFile)) {
                    LOGGER.debug("File record: [{}]", fileRecord);
                    savedFileRecord = saveFile(fileRecord, is);
                }
            }
        } catch (IOException e) {
            LOGGER.error("Exception happened during creating temporary file", e);
        }
        return savedFileRecord;
    }

    @Override
    public List<String> getVersionIdList() {
        List<String> versionIdList = new ArrayList<>();
        try {
            versionIdList.addAll(pkgMandateRecertifDAO.getRecertifPeriodVersions().stream().map(version -> String.valueOf(version)).collect(Collectors.toList()));
        } catch (SQLException ex) {
            LOGGER.error("Exception happened when saving file in database.", ex);
        }
        return Collections.unmodifiableList(versionIdList);
    }

    @Override
    public void recertifyMandates(List<DisclosureData> disclosureDataList) {
        Objects.requireNonNull(disclosureDataList, "disclosureData can not be null.");
        LOGGER.debug("Start to re-certify disclosure data. The total amount: {}", disclosureDataList.size());
        try {
            pkgStgMandatesRepoDAO.clean();
            pkgStgMandatesRepoDAO.populateMandates(disclosureDataList);
            pkgMandateRecertifDAO.recertifMandate();
            LOGGER.debug("Successfully finished recertification disclosure data");
        } catch (SQLException ex) {
            LOGGER.error("Exception happened when recertifying disclosure data.", ex);
        }
    }
}
