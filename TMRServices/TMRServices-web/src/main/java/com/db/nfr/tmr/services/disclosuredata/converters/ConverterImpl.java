package com.db.nfr.tmr.services.disclosuredata.converters;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by tymkvit on 10/03/2017.
 */
@Slf4j
@Component("jsonConverter")
public class ConverterImpl implements Converter {
    /**
     * Converts json represented in jsonSource as String into object represented in destinationType as type of
     * object.
     *
     * @param jsonSource
     * @param destinationType
     * @return
     */
    @Override
    public <T> T convertFrom(String jsonSource, Class<T> destinationType) throws IOException {
        Objects.requireNonNull(jsonSource, "jsonSource cannot be null");
        Objects.requireNonNull(destinationType, "destinationType cannot be null");

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonSource, destinationType);
    }

    /**
     * Converts json represented in jsonSource starting from pathToNode node into object represented in destinationType
     * as type of object.
     *
     * @param jsonSource
     * @param destinationType
     * @param pathToNode
     * @return
     * @throws IOException
     */
    @Override
    public <T> T convertFrom(String jsonSource, Class<T> destinationType, String pathToNode) throws IOException {
        Objects.requireNonNull(jsonSource, "jsonSource cannot be null");
        Objects.requireNonNull(destinationType, "destinationType cannot be null");
        Objects.requireNonNull(pathToNode, "pathToNode cannot be null");

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(jsonSource);
        JsonNode nodeEntry = root.at(pathToNode);
        return mapper.readValue(nodeEntry.traverse(), destinationType);
    }

    @Override
    public <T> List<T> convertFromArray(String jsonSource, Class<T> destinationType, String pathToNode) throws IOException {
        Objects.requireNonNull(jsonSource, "jsonSource cannot be null");
        Objects.requireNonNull(destinationType, "destinationType cannot be null");
        Objects.requireNonNull(pathToNode, "pathToNode cannot be null");

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(jsonSource);
        JsonNode nodeEntry = root.at(pathToNode);
        return mapper.readValue(nodeEntry.traverse(), mapper.getTypeFactory().constructCollectionType(List.class, destinationType));//Arrays.asList();
    }

    /**
     * Converts json into map
     *
     * @param jsonSource
     * @return
     * @throws IOException
     */
    @Override
    public <T extends Map<? extends String, ?>> T convertFrom(String jsonSource) throws IOException {
        Objects.requireNonNull(jsonSource, "jsonSource cannot be null");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonSource, new TypeReference<T>() {
        });
    }
}
