package com.db.nfr.tmr.services.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.nfr.tmr.services.excel.CustomResultSetRowMapper;

/**
 * Created by gasiork on 05/07/2016.
 */
public class TypeToSupervisorAreaRowMapper extends CustomResultSetRowMapper {
    private String[] dataArr;
    private JsonPojo jp;

    public TypeToSupervisorAreaRowMapper(ResultSet rs, JsonPojo jp) {
        super(rs);
        this.jp = jp;
    }

    @Override
    public boolean onNext() {
        try {
            TypeToSupervisorArea data = (TypeToSupervisorArea) getResultSet().getObject(1);
            //dataexport only selected or all traders
            if (jp.idList.size() > 0) {
                if (!jp.idList.contains(data.trader_email)) {
                    return hasNextRow();
                }
            }
            dataArr = data.toArray();
            return true;
        } catch (SQLException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public String getCellValue(int columnNumber) {
        return dataArr[columnNumber];
    }
}
