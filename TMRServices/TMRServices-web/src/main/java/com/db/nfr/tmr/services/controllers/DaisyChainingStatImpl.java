package com.db.nfr.tmr.services.controllers;

import com.db.nfr.tmr.services.common.MandateType;


/**
 * Created by tymkvit on 16/12/2016.
 */
public class DaisyChainingStatImpl implements DaisyChainingStat {
    private MandateType mandateType;
    private int tradersAmount;
    private int tradersChunkSize;


    @Override
    public MandateType getMandateType() {
        return mandateType;
    }

    @Override
    public int getTradersAmount() {
        return tradersAmount;
    }

    @Override
    public int getTradersChunkSize() {
        return tradersChunkSize;
    }

    @Override
    public void setMandateType(MandateType mandateType) {
        this.mandateType = mandateType;
    }

    @Override
    public void setTradersAmount(int tradersAmount) {
        this.tradersAmount = tradersAmount;
    }

    @Override
    public void setTradersChunkSize(int tradersChunkSize) {
        this.tradersChunkSize = tradersChunkSize;
    }
}
