package com.db.nfr.tmr.services.common;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

/**
 * Created by danyvik on 02/02/2017.
 */
@Data
public class SingleTrader {

    @NotBlank
    private final String user;
    @NotBlank
    private final String sourceSystem;
    @NotBlank
    private final String mandateType;
    @NotBlank
    private final String traderEmail;
    @NotBlank
    private final String mandateDate;

}
