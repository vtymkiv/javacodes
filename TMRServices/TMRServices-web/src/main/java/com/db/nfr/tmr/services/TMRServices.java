package com.db.nfr.tmr.services;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.web.WebApplicationInitializer;

/**
 * @author gasiork
 */
@SpringBootApplication
@EnableConfigServer
public class TMRServices extends SpringBootServletInitializer implements WebApplicationInitializer {
    private static final String EMAIL_PROPERTIES = "TMRServices/email.properties";
    private static final Logger LOGGER = LoggerFactory.getLogger(TMRServices.class);

    static {
        String property = System.getProperty("spring.config.location");
        System.setProperty("spring.config.location", StringUtils.isBlank(property) ? "classpath:TMRServices/" : property + ", classpath:TMRServices/");
        LOGGER.info("spring.config.location={}", System.getProperty("spring.config.location"));
    }

    /**
     * Get properties file as properties.
     *
     * @return
     */
    public static Properties getInstanceConfig() {
        try {
            Properties p = new Properties();
            p.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(EMAIL_PROPERTIES));
            return p;
        } catch (IOException ex) {
            LOGGER.error("Could not load properties file {}", EMAIL_PROPERTIES);
            throw new RuntimeException(ex);
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(TMRServices.class, args);
    }
}
