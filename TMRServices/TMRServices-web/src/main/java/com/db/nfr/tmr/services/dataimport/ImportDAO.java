package com.db.nfr.tmr.services.dataimport;

import javax.sql.DataSource;

import java.io.ByteArrayOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.dbapi.DbmsSession;
import com.db.nfr.tmr.services.dbapi.PkgFileRepo;
import com.db.nfr.tmr.services.dbapi.TypeRecAllFiles;
import com.db.nfr.tmr.services.utils.Constants;
import com.db.nfr.tmr.services.utils.Utils;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;


/**
 * @author DProkopiuk
 */
@Component
public class ImportDAO {

    @Autowired
    @Qualifier("tmsAppDS")
    private DataSource tmsAppDataSource;

    @Autowired
    @Qualifier("stgFeedDS")
    private DataSource stgFeedDataSource;

    public int saveFile(Workbook workbook, String fileName) throws Exception {
        Utils utils = new Utils();
        Properties p = utils.initProps();
        int feedConfigId = Integer.parseInt(p.getProperty(Constants.GSS_OUTPUT_FEED_CONFIG_ID));
        try (Connection conn = stgFeedDataSource.getConnection()) {
            DbmsSession.resetPackageState(conn);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            workbook.write(os);
            TypeRecAllFiles recAllFiles = new TypeRecAllFiles(feedConfigId, fileName, new Date(), os.size());
            os.close();
            final int fileId = PkgFileRepo.checkFiles(conn, recAllFiles);
            PkgFileRepo.loadFile(conn, fileId, workbook);
            return fileId;
        }
    }

    public void cleanup() {
        try (Connection c = stgFeedDataSource.getConnection()) {
            CallableStatement cs = c.prepareCall("{call  PKG_STG_MANDATES.p_clean_stg_mandates_extract()}");
            cs.execute();
            cs.close();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void recertifMandate() {
        try (Connection c = tmsAppDataSource.getConnection()) {
            CallableStatement cs = c.prepareCall("{call TMS_OWNER.PKG_MANDATE_GSS_RECERTIF.p_recertif_mandate()}");
            cs.execute();
            cs.close();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void persiste(GssImportUtils client, List<ImportData> dataList) {
        try {
            List<List<Object>> convertedList = new ArrayList();
            for (ImportData item : dataList) {
                List<Object> list = convert(item);
                convertedList.add(list);
            }
            call(convertedList, client.getBulkTypeName(), client.getTypeName(), client.getInsertQuery());
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void persisteAll(GssImportUtils client, List<List<ImportData>> dataList) {
        try {
            List<List<Object>> convertedList = new ArrayList();
            for (List<ImportData> list : dataList) {
                for (ImportData item : list) {
                    List<Object> converted = convert(item);
                    convertedList.add(converted);
                }
            }
            call(convertedList, client.getBulkTypeName(), client.getTypeName(), client.getInsertQuery());
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    private void call(List<List<Object>> list, String oracleBulkTypeName, String oracleTypeName, String sql) throws SQLException {
        try (Connection c = stgFeedDataSource.getConnection();
             CallableStatement cs = c.prepareCall(sql);) {
            ARRAY array = convert(list, oracleBulkTypeName, c, oracleTypeName);
            cs.setObject(1, array);
            cs.execute();
        }
    }

    private ARRAY convert(List<List<Object>> list, String oracleBulkTypeName, Connection c, String oracleTypeName) throws SQLException {
        ArrayDescriptor arrDesc = ArrayDescriptor.createDescriptor(oracleBulkTypeName, c);
        Object[] data = new Object[list.size()];
        for (int i = 0; i < list.size(); i++) {
            data[i] = convert(list.get(i), oracleTypeName, c);
        }
        return new ARRAY(arrDesc, c, data);
    }

    private STRUCT convert(List<Object> data, String oracleTypeName, Connection c) throws SQLException {
        for (int i = 0; i < data.size(); i++) {
            Object item = data.get(i);
            if (item != null && item.getClass() == String.class) {
                data.set(i, item.toString().replaceAll("\"", ""));
            }
        }
        StructDescriptor structdesc = StructDescriptor.createDescriptor(oracleTypeName, c);
        return new STRUCT(structdesc, c, data.toArray());
    }

    private List<Object> convert(ImportData data) {
        List<Object> list = new ArrayList();
        list.add(data.getTraderEmail());
        list.add(data.getSummary());
        list.add(data.getLocalVersion());
        list.add(data.getMandateType());
        list.add(data.getRecertificationType());
        list.add(data.getTradeStatus());
        list.add(data.getGssComment());
        list.add(data.getReason());
        list.add(data.getStatus());
        list.add(data.getProvisionDate());
        list.add(data.getCycleDate());
        list.add("".equals(data.getVersion()) ? null : data.getVersion());
        list.add(data.getFileId());
        return list;
    }
}
