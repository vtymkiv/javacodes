package com.db.nfr.tmr.services.dbapi;

import java.sql.SQLException;
import java.util.List;

import com.db.nfr.tmr.services.disclosuredata.DisclosureData;

/**
 * This class represents the data access object that would be
 * used for accessing to the database's pkg_stg_mandates package functions
 * <p>
 * Created by tymkvit on 01/02/2017.
 */
public interface PkgStgMandatesRepoDAO {
    /**
     * Clean up debts
     * It will call pkg_stg_mandates.p_clean_stg_mandates_debt
     *
     * @throws SQLException
     */
    void clean() throws SQLException;

    /**
     * Uploading mandates into STG_ table.
     * It will call pkg_stg_mandates.p_fill_stg_mandates_extract
     *
     * @param disclosureData
     * @throws SQLException
     */
    void populateMandates(List<DisclosureData> disclosureData) throws SQLException;

}
