/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.nfr.tmr.services.common;

import lombok.Getter;

/**
 * @author tymkvit
 */

public enum CertificationPersonType {
    TRADER("Trader"), SUPERVISOR("Supervisor"), UNKNOWN("Unknown");

    @Getter
    private String code;

    CertificationPersonType(String code) {
        this.code = code;
    }

}
