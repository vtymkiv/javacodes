package com.db.nfr.tmr.services.dbapi;

import javax.sql.DataSource;
import javax.sql.rowset.CachedRowSet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Struct;
import java.sql.Types;
import java.text.ParseException;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.common.ExportSingleData;
import com.db.nfr.tmr.services.common.NoDataFoundException;
import com.db.nfr.tmr.services.common.Request;
import com.db.nfr.tmr.services.common.ResultSetFunction;
import com.db.nfr.tmr.services.common.SingleTrader;
import com.db.nfr.tmr.services.excel.ExcelExport;
import com.db.nfr.tmr.services.excel.ResultSetRowMapper;
import com.db.nfr.tmr.services.excel.ResultSetTitleRowMapper;
import com.db.nfr.tmr.services.excel.TemporaryWorkbook;

import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleTypes;
import oracle.jdbc.rowset.OracleCachedRowSet;
import oracle.sql.BLOB;


import static com.db.nfr.tmr.services.utils.Constants.NO_REPLY_FROM_DB;
import static com.db.nfr.tmr.services.utils.Constants.VERSION_IS_NOT_NUMBER;
import static com.db.nfr.tmr.services.utils.Constants.VERSION_IS_NULL;

/**
 * @author gasiork on 02/08/2016.
 * @author Vitaliy Tymkiv.
 */
@Slf4j
@Component
@Deprecated
public class PkgMandateGssRecertif {

    @Autowired
    private CustomOracleTypes customOracleTypes;

    @Autowired
    @Qualifier("tmsAppDS")
    private DataSource tmsAppDataSource;

    public ProcessingStatus pGetVersion(Connection conn, Request request) throws Exception {
        LOGGER.debug("Calling pkg_mandate_gss_recertif.p_get_version(?,?,?) schema: {}", conn.getSchema());
        Struct obj = customOracleTypes.getVersionStruct(conn, request);
        String sql = "{call pkg_mandate_gss_recertif.p_get_version(?,?,?)}";
        DbmsSession.resetPackageState(conn);
        try (CallableStatement cs = conn.prepareCall(sql)) {
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.registerOutParameter(2, OracleTypes.CURSOR);
            cs.setObject(3, obj);
            cs.execute();
            try (ResultSet rset = (ResultSet) cs.getObject(2)) {
                if (rset.next()) {
                    Struct structObj = (Struct) rset.getObject(1);
                    Object[] objArr = structObj.getAttributes();
                    try {
                        if (objArr[0] == null) {
                            ProcessingStatus ps = new ProcessingStatus(VERSION_IS_NULL, "Version is null");
                            LOGGER.debug("Processing status: {}", ps);
                            return ps;
                        }
                        int version = Integer.parseInt(objArr[0].toString());
                        String status = objArr[1].toString();
                        Array tradersArr = (Array) objArr[2];
                        ProcessingStatus ps = new ProcessingStatus(version, status);
                        try (ResultSet rsTrader = tradersArr.getResultSet()) {
                            while (rsTrader.next()) {
                                Struct traderObj = (Struct) rsTrader.getObject(2);
                                Object[] traderAttr = traderObj.getAttributes();
                                ps.addTrader(traderAttr[0].toString());
                            }
                        }
                        LOGGER.debug("Processing status: {}", ps);
                        return ps;
                    } catch (NumberFormatException nfe) {
                        LOGGER.error("Could not parse version", nfe);
                        ProcessingStatus ps = new ProcessingStatus(VERSION_IS_NOT_NUMBER, "Version is not a number");
                        LOGGER.debug("Processing status: {}", ps);
                        return ps;
                    }
                } else {
                    ProcessingStatus ps = new ProcessingStatus(NO_REPLY_FROM_DB, "No reply from database");
                    LOGGER.debug("Processing status: {}", ps);
                    return ps;
                }
            }
        }
    }

    /**
     * Use this version in case of failure
     * ip_version in number
     * ip_action_status in varchar2 default 'FAILED'
     */
    public void pMarkStatusPendingFAILED(Connection conn, int version) throws Exception {
        String sql = "{call pkg_mandate_gss_recertif.p_mark_status_pending(?)}";
        LOGGER.debug("Calling {} schema: {}", sql, conn.getSchema());
        try (CallableStatement cs = conn.prepareCall(sql)) {
            cs.setInt(1, version);
            cs.execute();
        }
        LOGGER.debug("Calling {} complete", sql);
    }

    public void pMarkStatusPending(Connection conn, Request request, int version) throws Exception {
        LOGGER.debug("Calling pkg_mandate_gss_recertif.p_mark_status_pending(?,?)} schema: {}", conn.getSchema());
        String sql = "{call pkg_mandate_gss_recertif.p_mark_status_pending(?,?)}";
        try (CallableStatement cs = conn.prepareCall(sql)) {
            Array gssTraderStructArray = customOracleTypes.getGssTraderStructArray(conn, customOracleTypes.getTradersList(request));
            cs.setArray(1, gssTraderStructArray);
            cs.setInt(2, version);
            cs.execute();
        }
        LOGGER.debug("Calling call pkg_mandate_gss_recertif.p_mark_status_pending(?,?)} complete.");
    }

    public void pGetMandatesForRecertif(Connection conn, Request chunk, int version, ResultSetFunction fn) throws Exception {
        LOGGER.debug("Call p_get_mandates_for_recertif for version {}, schema {}, chunk {}", version, conn.getSchema(), chunk);
        DbmsSession.resetPackageState(conn);
        try (CallableStatement cs = conn.prepareCall("{call pkg_mandate_gss_recertif.p_get_mandates_for_recertif(?,?,?,?)}")) {
            Array gssTraderStructArray = customOracleTypes.getGssTraderStructArray(conn, customOracleTypes.getTradersList(chunk));
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.registerOutParameter(2, OracleTypes.CURSOR);
            cs.setArray(3, gssTraderStructArray);
            cs.setInt(4, version);
            cs.execute();
            try (ResultSet rs = (ResultSet) cs.getObject(2)) {
                fn.run(rs);
            }
        }
        LOGGER.debug("Call p_get_mandates_for_recertif {} completed", version);
    }

    public void pSaveTraders(Connection conn, Request request) throws Exception {
        LOGGER.debug("Calling  pkg_mandate_gss_recertif.p_save_traders(?,?) schema: {}", conn.getSchema());
        try (CallableStatement cs = conn.prepareCall("{call pkg_mandate_gss_recertif.p_save_traders(?,?)}")) {
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setArray(2, customOracleTypes.getGssTraderStructArray(conn, customOracleTypes.getTradersList(request)));
            cs.execute();
        }
        LOGGER.debug("Calling call pkg_mandate_gss_recertif.p_save_traders(?,?) complete.", conn.getSchema());
    }

    public void pDeleteTraders(Connection conn, Request request) throws Exception {
        LOGGER.debug("Calling pkg_mandate_gss_recertif.p_delete_traders(?,?) schema: {}", conn.getSchema());
        try (CallableStatement cs = conn.prepareCall("{call pkg_mandate_gss_recertif.p_delete_traders(?,?)}")) {
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setArray(2, customOracleTypes.getGssTraderStructArray(conn, customOracleTypes.getTradersList(request)));
            cs.execute();
        }
        LOGGER.debug("Calling pkg_mandate_gss_recertif.p_delete_traders(?,?) complete");
    }

    // TODO: Refactor the blob creation
    public void pSaveFile(Connection conn, Request request, Workbook workbook, int version) throws SQLException, ParseException, IOException {
        LOGGER.debug("Calling  pkg_mandate_gss_recertif.p_save_file(?,?,?,?,?) schema: {}", conn.getSchema());
        BLOB workbookBlob = BLOB.createTemporary(conn, false, BLOB.DURATION_SESSION);
        try (OutputStream os = workbookBlob.setBinaryStream(0L)) {
            workbook.write(os);
        }
        DbmsSession.resetPackageState(conn);
        try (CallableStatement cs = conn.prepareCall("{call pkg_mandate_gss_recertif.p_save_file(?,?,?,?,?)}")) {
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setBlob(2, workbookBlob);
            cs.setString(3, request.getUser());
            cs.setInt(4, version);
            cs.setArray(5, customOracleTypes.getGssTraderStructArray(conn, customOracleTypes.getTradersList(request)));
            cs.execute();
        }
        LOGGER.debug("Calling  pkg_mandate_gss_recertif.p_save_file(?,?,?,?,?) complete.", conn.getSchema());
    }

    public ExportSingleData pExportMandateFile(SingleTrader singleTraderJson, Connection connection) throws SQLException {
        LOGGER.debug("pExportMandateFile started");
        ExportSingleData exportSingleData = new ExportSingleData();
        final String SQL = "{call pkg_mandate_gss_recertif.p_export_mandate_file(?,?,?,?,?,?,?,?,?)}";
        byte[] bytes = null;
        DbmsSession.resetPackageState(connection);
        try (CallableStatement callableStatement = connection.prepareCall(SQL)) {
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.registerOutParameter(2, Types.BLOB);
            callableStatement.registerOutParameter(3, OracleTypes.VARCHAR);
            callableStatement.registerOutParameter(4, OracleTypes.VARCHAR);
            callableStatement.setString(5, singleTraderJson.getUser().trim());
            callableStatement.setString(6, singleTraderJson.getSourceSystem().trim());
            callableStatement.setString(7, singleTraderJson.getTraderEmail().trim());
            callableStatement.setString(8, singleTraderJson.getMandateDate().trim());
            callableStatement.setString(9, singleTraderJson.getMandateType().trim());

            callableStatement.executeQuery();
            Blob callableStatementBlob = callableStatement.getBlob(2);

            exportSingleData.setFileName(callableStatement.getString(3));
            exportSingleData.setContentType(callableStatement.getString(4));

            if (callableStatementBlob == null) {
                ResultSet resultset = (ResultSet) callableStatement.getObject(1);
                resultset.setFetchSize(500);
                TemporaryWorkbook temporaryWorkbook = generateXLS(resultset);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                temporaryWorkbook.getWorkbook().write(byteArrayOutputStream);
                exportSingleData.setFileImage(byteArrayOutputStream.toByteArray());
                temporaryWorkbook.close();
            } else {
                exportSingleData.setFileImage(IOUtils.toByteArray(callableStatementBlob.getBinaryStream()));
            }
        } catch (SQLException e) {
            LOGGER.error(e.toString());
            throw new NoDataFoundException("Exception happened during call to Database");
        } catch (IOException e) {
            LOGGER.error(e.toString());
        } catch (Exception e) {
            LOGGER.error(e.toString());
        }
        LOGGER.debug("pExportMandateFile  completed");
        return exportSingleData;
    }

    private TemporaryWorkbook generateXLS(ResultSet rs) throws SQLException, IOException {
        LOGGER.info("generateXLS started");
        CachedRowSet cachedRowSet = new OracleCachedRowSet();
        CachedRowSet cachedRowSetTitle = new OracleCachedRowSet();
        cachedRowSet.populate(rs);
        cachedRowSetTitle.populate(rs);
        ExcelExport ee = new ExcelExport();
        ee.setSheetName("DCA Data")
                .setTitleRowMapper(new ResultSetTitleRowMapper(cachedRowSetTitle))
                .setRowMapper(new ResultSetRowMapper(cachedRowSet))
                .setColumnCount(rs.getMetaData().getColumnCount());
        LOGGER.info("generateXLS completed");
        return ee.getTemporaryWorkbook();
    }
}
