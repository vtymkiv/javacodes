package com.db.nfr.tmr.services.disclosuredata.converters;

/**
 * Created by tymkvit on 28/03/2017.
 */
public interface DateConverterService {
    String convertDateTo(String originalDateAsStr, String originalFormat, String newFormat);
}
