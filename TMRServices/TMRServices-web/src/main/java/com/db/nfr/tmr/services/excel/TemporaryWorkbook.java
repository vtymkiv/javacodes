package com.db.nfr.tmr.services.excel;

import java.io.File;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author gasiork
 */
public class TemporaryWorkbook implements AutoCloseable {
    private static final Logger log = LoggerFactory.getLogger(TemporaryWorkbook.class);

    private final File tempFile;
    private final SXSSFWorkbook workbook;

    public TemporaryWorkbook(File tempFile, SXSSFWorkbook workbook) {
        this.tempFile = tempFile;
        this.workbook = workbook;
    }

    @Override
    public void close() throws Exception {
        workbook.close();
        if (tempFile.exists()) {
            tempFile.delete();
            log.info("Deleted temp file '" + tempFile.getAbsolutePath() + "'");
        }
    }

    public Workbook getWorkbook() {
        return workbook;
    }
}