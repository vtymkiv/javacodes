package com.db.nfr.tmr.services.rest;

import java.io.InputStream;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.db.nfr.tmr.services.dataimport.GssImportUtils;

/**
 * @author gasiork
 */
@RestController
public class GssImportController {
    private static final Logger log = LoggerFactory.getLogger(GssImportController.class);
    private static final Lock lock = new ReentrantLock();

    @Autowired
    private GssImportUtils gssImportUtils;

    @RequestMapping(value = "/import/gss", method = {RequestMethod.POST})
    public String downloadExcel(@RequestPart("file") MultipartFile file) {
        String retVal = "OK";
        log.info("GSS import Started");
        lock.lock();
        log.info("GSS import Locked");
        try {
            if (file != null) {
                InputStream in = file.getInputStream();
                String fileName = file.getOriginalFilename();
                log.info("Processing file '" + fileName + "' Started");
                fileName = fileName.substring(fileName.lastIndexOf('\\') + 1);
                gssImportUtils.process(in, fileName);
                log.info("Processing file '" + fileName + "' Finished");
            } else {
                log.error("GSS file was not uploaded");
            }
        } catch (Exception ex) {
            log.error("GSS import Error", ex);
            retVal = ex.getMessage();
        } finally {
            lock.unlock();
            log.info("GSS import Unlocked");
        }
        log.info("GSS import Finished");
        return retVal;
    }

    public static boolean isProcessing() {
        if (lock.tryLock()) {
            try {
                return false;
            } finally {
                lock.unlock();
            }
        } else {
            return true;
        }
    }
}
