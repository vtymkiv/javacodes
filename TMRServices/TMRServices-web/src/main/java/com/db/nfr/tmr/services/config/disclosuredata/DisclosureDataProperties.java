package com.db.nfr.tmr.services.config.disclosuredata;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.ToString;

/**
 * Created by tymkvit on 08/12/2016.
 */
@Data
@Component
@ToString
@ConfigurationProperties(prefix = "disclosure-data")
public class DisclosureDataProperties {
    /**
     * List of hosts that Disclosure Data Service could be run on.
     */
    private List<String> allowedHosts;
}
