package com.db.nfr.tmr.services.dbapi;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

/**
 * This class represents the data access object that would be
 * used for accessing to the database's pkg_mandate_gss_recertif package functions
 * <p>
 * Created by tymkvit on 01/02/2017.
 */
public interface PkgMandateRecertifDAO {
    /**
     * Call to re-certificate mandate
     * It will call pkg_mandate_gss_recertif.p_recertif_mandate
     *
     * @throws SQLException
     */
    void recertifMandate() throws SQLException;

    /**
     * Call to get versions available for recertification
     * It will call pkg_mandate_gss_recertif.p_get_submitted stored procedure
     *
     * @throws SQLException
     */
    List<BigDecimal> getRecertifPeriodVersions() throws SQLException;
}
