package com.db.nfr.tmr.services.rest.appinfo;

/**
 * Created by tymkvit on 27/10/2016.
 */

public class WsApplicationInfo {
    private String version;
    private String time;
    private String artifact;
    private String group;
    private String name;
    private String buildNumber;
    private String userName;
    private String userEmail;
    private String shortCommitId;
    private String commitTime;
    private String commitUserName;
    private String branchName;
    ;

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setArtifact(String artifact) {
        this.artifact = artifact;
    }

    public String getArtifact() {
        return artifact;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroup() {
        return group;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setCommitTime(String commitTime) {
        this.commitTime = commitTime;
    }

    public String getCommitTime() {
        return commitTime;
    }

    public void setCommitUserName(String commitUserName) {
        this.commitUserName = commitUserName;
    }

    public String getCommitUserName() {
        return commitUserName;
    }

    public void setShortCommitId(String shortCommitId) {
        this.shortCommitId = shortCommitId;
    }

    public String getShortCommitId() {
        return shortCommitId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @Override
    public String toString() {
        return "WsApplicationInfo{" +
                "version='" + version + '\'' +
                ", time='" + time + '\'' +
                ", artifact='" + artifact + '\'' +
                ", group='" + group + '\'' +
                ", name='" + name + '\'' +
                ", buildNumber='" + buildNumber + '\'' +
                ", userName='" + userName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", shortCommitId='" + shortCommitId + '\'' +
                ", commitTime='" + commitTime + '\'' +
                ", commitUserName='" + commitUserName + '\'' +
                ", branchName='" + branchName + '\'' +
                '}';
    }
}
