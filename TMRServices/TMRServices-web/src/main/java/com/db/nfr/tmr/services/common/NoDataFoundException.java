package com.db.nfr.tmr.services.common;

import java.sql.SQLException;

/**
 * Created by danyvik on 03/02/2017.
 */
public class NoDataFoundException extends SQLException {

    public NoDataFoundException() {
        super();
    }

    public NoDataFoundException(String message) {
        super(message);
    }

}
