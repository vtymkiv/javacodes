package com.db.nfr.tmr.services.common;

import java.sql.ResultSet;

/**
 * Created by gasiork on 02/08/2016.
 */
public interface ResultSetFunction {
    void run(ResultSet rs) throws Exception;
}
