package com.db.nfr.tmr.services.disclosuredata.converters;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Converter of json objects into POJOs.
 * Created by tymkvit on 10/03/2017.
 */
public interface Converter {
    /**
     * Converts json represented in jsonSource as String into object represented in destinationType as type of
     * object.
     *
     * @param jsonSource
     * @param destinationType
     * @param <T>
     * @return
     */
    <T> T convertFrom(String jsonSource, Class<T> destinationType) throws IOException;

    /**
     * Converts json represented in jsonSource starting from pathToNode node into object represented in destinationType
     * as type of object.
     *
     * @param jsonSource
     * @param destinationType
     * @param pathToNode
     * @param <T>
     * @return
     * @throws IOException
     */
    <T> T convertFrom(String jsonSource, Class<T> destinationType, String pathToNode) throws IOException;

    /**
     * Converts json represented in jsonSource starting from pathToNode node into list of objects represented in destinationType
     * as type of object. The pathToNode should point to the array object in json hierarchy.
     *
     * @param jsonSource
     * @param destinationType
     * @param pathToNode
     * @param <T>
     * @return
     * @throws IOException
     */
    <T> List<T> convertFromArray(String jsonSource, Class<T> destinationType, String pathToNode) throws IOException;

    /**
     * Converts json into map
     *
     * @param jsonSource
     * @param <T>
     * @return
     * @throws IOException
     */
    <T extends Map<? extends String, ?>> T convertFrom(String jsonSource) throws IOException;

}
