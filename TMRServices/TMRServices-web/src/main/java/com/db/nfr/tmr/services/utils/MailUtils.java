package com.db.nfr.tmr.services.utils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.util.ByteArrayDataSource;

import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.db.nfr.tmr.services.common.MandateType;


import static org.apache.commons.lang3.time.DateFormatUtils.format;

/**
 * This was deprecated. Consider this {@link com.db.nfr.tmr.services.mail.MailUtils}.
 *
 * @author prokden
 */
@Component
@Deprecated
public class MailUtils {
    private static Logger LOGGER = LoggerFactory.getLogger(MailUtils.class);

    private Utils utils = new Utils();

    public Session initSession() {
        Properties props = System.getProperties();
        Properties properties = utils.initProps();
        String host = properties.getProperty(Constants.HOST);
        String port = properties.getProperty(Constants.PORT, "");
        props.setProperty(Constants.HOST_PROPERTY, host);
        if (!StringUtils.isEmpty(port)) {
            props.setProperty(Constants.PORT_PROPERTY, port);
        }
        return Session.getDefaultInstance(props, null);
    }

    public InternetAddress initFrom() throws AddressException {
        String from = utils.initProps().getProperty(Constants.FROM);
        return new InternetAddress(from);
    }

    public String initSubject() {
        return utils.initProps().getProperty(Constants.SUBJECT);
    }

    public BodyPart initBodyPart(byte[] data) throws MessagingException {
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setFileName(initFileName());
        DataSource byteArrayDataSource = new ByteArrayDataSource(data, initFileType());
        messageBodyPart.setDataHandler(new DataHandler(byteArrayDataSource));
        return messageBodyPart;
    }

    public String initFileName() {
        return utils.initProps().getProperty(Constants.FILE_NAME);
    }

    public String initFileType() {
        return utils.initProps().getProperty(Constants.FILE_TYPE);
    }

    public InternetAddress[] initTo(MandateType mandateType) throws AddressException {
        Properties props = utils.initProps();
        int n = Integer.parseInt(props.getProperty(Constants.TO_COUNT + "." + mandateType.name(), "0"));
        InternetAddress[] array = new InternetAddress[n];
        for (int i = 0; i < n; i++) {
            String to = props.getProperty(Constants.TO + "." + mandateType.name() + "." + i);
            array[i] = new InternetAddress(to.trim());
        }
        return array;
    }

    public String initSubject(MandateType mandateType) {
        return utils.initProps().getProperty(Constants.SUBJECT + "." + mandateType.name());
    }

    public String getFileName(MandateType mandateType) {
        LOGGER.debug("Getting file name for mandate type {}", mandateType);
        Properties properties = utils.initProps();

        switch (mandateType) {
            case DEBT:
                return formatFileName(properties.getProperty(Constants.FILE_NAME_DEBT));
            case EQUITY:
                return formatFileName(properties.getProperty(Constants.FILE_NAME_EQT));
            default:
                return properties.getProperty(Constants.FILE_NAME);
        }
    }

    /**
     * Format the name of the file based on the format specified in file name like:
     * testFile{MMddYY}.txt. The current date would be used as actual value for formal parameters
     * specified in file name: {MMddYY}.
     *
     * @param fileName
     * @return
     */
    public String formatFileName(String fileName) {
        return formatFileName(fileName, new Date());

    }

    /**
     * Format the name of the file based on the format specified in file name. The pattern should be
     * reside inside of curly braces {dateFormat} in name of the file.
     * The current date would be used as value for date format specified in the name of the file.
     *
     * @param fileName
     * @return
     */
    public String formatFileName(String fileName, Date date) {
        LOGGER.debug("File name for formatting [{}]", fileName);
        Assert.hasText(fileName, "fileName cannot be empty");

        if (fileName.indexOf("{") < 0 || fileName.indexOf("}") < 0) {
            LOGGER.debug("No file datetime pattern  like {ddMMyyyy_HH_mm_ss} was found in {} so original file name will be used.", fileName);
            return fileName;
        }

        String fileNameFormat = fileName.substring(fileName.indexOf("{") + 1, fileName.indexOf("}"));
        //SimpleDateFormat dateFormat = new SimpleDateFormat(fileNameFormat);
        String formattedTxt = format(date, fileNameFormat, TimeZone.getTimeZone(Constants.TIMER_TIMEZONE_ID));
        return fileName.replaceAll("\\{.*?\\}", formattedTxt);
    }
}
