package com.db.nfr.tmr.services.common;

import java.util.Map;

import com.db.nfr.tmr.services.excel.CustomTitleRowMapper;

/**
 * Created by gasiork on 05/07/2016.
 */
public class TypeToSupervisorAreaTitleMapper extends CustomTitleRowMapper {
    private final String[] titles;

    public TypeToSupervisorAreaTitleMapper(Map<String, String> columnTitles) {
        titles = columnTitles.values().toArray(new String[columnTitles.size()]);
    }

    @Override
    public String getCellValue(int columnNumber) {
        return titles[columnNumber];
    }
}
