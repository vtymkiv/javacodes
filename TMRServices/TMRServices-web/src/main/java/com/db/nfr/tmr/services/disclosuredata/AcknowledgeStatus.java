package com.db.nfr.tmr.services.disclosuredata;

import lombok.Getter;

/**
 * Created by tymkvit on 15/03/2017.
 */
public enum AcknowledgeStatus {
    YES("Yes"),
    NO("No"),
    UNDEFINED("UNDEFINED");

    @Getter
    private String code;

    public static AcknowledgeStatus findByCode(String code) {
        for (AcknowledgeStatus dtoStatus : values()) {
            if (dtoStatus.getCode().equalsIgnoreCase(code)) {
                return dtoStatus;
            }
        }
        return UNDEFINED;
    }

    AcknowledgeStatus(String code) {
        this.code = code;
    }
}
