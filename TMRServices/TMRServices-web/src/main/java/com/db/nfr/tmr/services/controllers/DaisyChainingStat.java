package com.db.nfr.tmr.services.controllers;


import com.db.nfr.tmr.services.common.MandateType;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by tymkvit on 16/12/2016.
 */
@JsonDeserialize(as = DaisyChainingStatImpl.class)
public interface DaisyChainingStat {
    MandateType getMandateType();

    int getTradersAmount();

    int getTradersChunkSize();

    void setMandateType(MandateType mandateType);

    void setTradersAmount(int tradersAmount);

    void setTradersChunkSize(int tradersChunkSize);
}
