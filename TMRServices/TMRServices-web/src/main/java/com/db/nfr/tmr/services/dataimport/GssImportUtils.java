package com.db.nfr.tmr.services.dataimport;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author gasiork
 */
@Component
public class GssImportUtils {
    private static final Logger log = LoggerFactory.getLogger(GssImportUtils.class);

    @Autowired
    private ImportDAO dao;

    public static final String EvProvId = "EvProvId";
    public static final String ControlTitle = "Control Title";
    public static final String AssociatedWSP = "Associated WSP";
    public static final String Name = "Name";
    public static final String Owner = "Owner";
    public static final String OwnerEmail = "Owner Email";
    public static final String Delegate = "Delegate";
    public static final String DelegateEmail = "Delegate Email";
    public static final String Cycle = "Cycle";
    public static final String Status = "Status";
    public static final String CycleDate = "Cycle Date";
    public static final String DueDate = "Due Date";
    public static final String ProvisionDate = "Provision Date";
    public static final String Completed = "Completed";
    public static final String Trader = "Trader";
    //TODO consume COLUMNNAMES from DatabaseMetadata
    public static final String BusinessUBR7 = "Business UBR 8";
    public static final String BusinessUBR8 = "Business UBR 9";
    public static final String Supervisor = "Supervisor";
    public static final String Country = "Country";
    public static final String Region = "Region";
    public static final String MandateDate = "Mandate Date";
    public static final String Summary = "Summary";
    public static final String jsonObject = "jsonObject";
    public static final String Comments = "Comments";
    public static final String Acknowledge = "Acknowledge";
    public static final String Reason = "Reason";
    public static final String Version = "Version";

    public void process(InputStream in, String fileName) {
        try {
            log.info("Opening workbook " + fileName);
            XSSFWorkbook myWorkbook = new XSSFWorkbook(in);
            final int fileId = saveFile(myWorkbook, fileName);
            cleanup();
            XSSFSheet sheet = myWorkbook.getSheetAt(0);
            Iterator<Row> rows = sheet.iterator();
            List<ColumnInfo> columnInfoList = null;
            List<List<ImportData>> allList = new ArrayList<>();
            long rowNum = 0;
            while (rows.hasNext()) {
                rowNum++;
                try {
                    if (columnInfoList == null) {
                        columnInfoList = getColumnInfoList(rows.next());
                    } else {
                        allList.add(getImportData(rows.next(), columnInfoList, fileId));
                    }
                } catch (Exception ex) {
                    log.error("Error when importing row " + rowNum, ex);
                }
            }
            processAll(allList);
            recertifMandate();
        } catch (Exception ex) {
            log.error("GSS import processing error", ex);
            throw new RuntimeException(ex);
        }
    }

    public List<ColumnInfo> getColumnInfoList(Row titleRow) {
        List<String> columns = new ArrayList<>();
        columns.add(EvProvId);
        columns.add(ControlTitle);
        columns.add(AssociatedWSP);
        columns.add(Name);
        columns.add(Owner);
        columns.add(OwnerEmail);
        columns.add(Delegate);
        columns.add(DelegateEmail);
        columns.add(Cycle);
        columns.add(Status);
        columns.add(CycleDate);
        columns.add(DueDate);
        columns.add(ProvisionDate);
        columns.add(Completed);
        columns.add(Trader);
        columns.add(BusinessUBR7);
        columns.add(BusinessUBR8);
        columns.add(Supervisor);
        columns.add(Country);
        columns.add(Region);
        columns.add(MandateDate);
        columns.add(Summary);
        columns.add(jsonObject);
        columns.add(Comments);
        columns.add(Acknowledge);
        columns.add(Reason);
        columns.add(Version);

        List<ColumnInfo> result = new ArrayList<>();
        Iterator<Cell> cols = titleRow.cellIterator();
        int i = 0;
        while (cols.hasNext()) {
            String colName = process(cols.next());
            if (columns.contains(colName)) {
                result.add(new ColumnInfo(colName, i));
            }
            i++;
        }
        return result;
    }

    public void process(Row row) {
        Iterator<Cell> itr = row.cellIterator();
        while (itr.hasNext()) {
            process(itr.next());
        }
    }

    public String process(Cell cell) {
        return getCellValue(cell);
    }

    public String getCellValue(Cell cell) {
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue();
            case Cell.CELL_TYPE_NUMERIC:
                return String.valueOf(cell.getNumericCellValue());
            case Cell.CELL_TYPE_BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case Cell.CELL_TYPE_BLANK:
                return "";
            default:
                throw new RuntimeException("Incorrect type of Cell");
        }
    }

    public List<ImportData> getImportData(Row row, List<ColumnInfo> columnInfoList, final int fileId) throws Exception {
        Iterator<Cell> itr = row.cellIterator();
        Map<String, String> dataMap = new HashMap<>();
        int i = 0;
        while (itr.hasNext()) {
            String dataStr = process(itr.next());
            for (ColumnInfo columnInfo : columnInfoList) {
                if (columnInfo.getIndex() == i) {
                    dataMap.put(columnInfo.getName(), dataStr);
                    break;
                }
            }
            i++;
        }
        return transform(dataMap, fileId);
    }

    protected List<ImportData> transform(Map<String, String> dataMap, final int fileId) throws Exception {
        List<ImportData> list = new ArrayList();
        ImportData importData = new ImportData();
        list.add(importData);
        enrich(importData, dataMap);
        importData.setFileId(fileId);
        return list;
    }

    public void enrich(ImportData importData, Map<String, String> fieldMap) throws Exception {
        String controlTitle = fieldMap.get(ControlTitle);
        String status = fieldMap.get(Status);
        String cycleDate = fieldMap.get(CycleDate);
        String provisionDate = fieldMap.get(ProvisionDate);
        String trader = fieldMap.get(Trader);
        String businessUbr8 = fieldMap.get(BusinessUBR7);
        String businessUbr9 = fieldMap.get(BusinessUBR8);
        String supervisor = fieldMap.get(Supervisor);
        String country = fieldMap.get(Country);
        String region = fieldMap.get(Region);
        String mandateDate = fieldMap.get(MandateDate);
        String summary = fieldMap.get(Summary);
        String acknowledge = fieldMap.get(Acknowledge);
        String reason = fieldMap.get(Reason);

        String version = fieldMap.get(Version);
        if (version != null && !"".equals(version)) {
            version = Integer.toString((int) Double.parseDouble(version));
        }

        String comments = fieldMap.get(Comments);
        if (comments.length() > 4000) {
            comments = comments.substring(0, 4000);
        }

        importData.setTraderEmail(trader);
        importData.setBusiness8(businessUbr8);
        importData.setBusiness9(businessUbr9);
        importData.setSupervisor(supervisor);
        importData.setTraderCountry(country);
        importData.setTraderRegion(region);
        importData.setSummary(summary);
        //importData.setGlobalVersion(defineGlobalVersion(mandateDate));
        importData.setLocalVersion(defineMandateDate(mandateDate));
        importData.setMandateType(defineMandateType(controlTitle));
        importData.setTradeStatus(acknowledge);
        importData.setSupervisorStatus(acknowledge);
        importData.setRecertificationType(defineRecertifiactionType(controlTitle));
        importData.setGssComment(comments);
        importData.setStatus(status);
        importData.setReason(reason);
        importData.setProvisionDate(provisionDate);
        importData.setCycleDate(cycleDate);
        importData.setVersion(version);
    }

    private String defineMandateType(String controlTitle) {
        if (controlTitle != null) {
            if (controlTitle.contains("Equities") || controlTitle.contains("equities")) {
                return "Eqt";
            }
            if (controlTitle.contains("Debt") || controlTitle.contains("debt")) {
                return "Debt";
            }
        }
        return null;
    }

    private String defineRecertifiactionType(String controlTitle) {
        if (controlTitle != null) {
            if (controlTitle.contains("Supervisor") || controlTitle.contains("supervisor")) {
                return "Supervisor";
            }
            if (controlTitle.contains("Trader") || controlTitle.contains("trader")) {
                return "Trader";
            }
        }
        return null;
    }

    private String defineMandateDate(String mandateDateValue) throws Exception {
        if (mandateDateValue.length() < 10) {
            mandateDateValue = ImportData.parseExcelDate(mandateDateValue);
        }
        try {
            validateMandateDate(mandateDateValue, "yyyy-MM-dd HH:mm:ss");
            return mandateDateValue;
        } catch (Exception ex) {
            //log.error("Could not parse '" + mandateDateValue + "' with format '" + "yyyy-MM-dd HH:mm:ss" + "'");
            try {
                return defineMandateDate(mandateDateValue, "dd-MMM-yyyy", "yyyy-MM-dd HH:mm:ss");
            } catch (Exception ex1) {
                throw ex1;
            }
        }
    }

    private void validateMandateDate(String mandateDateValue, String datePattern) throws Exception {
        DateFormat df = new SimpleDateFormat(datePattern);
        df.parse(mandateDateValue);
    }

    private String defineMandateDate(String mandateDateValue, String actualDatePattern, String expectedDatePattern) throws Exception {
        DateFormat df = new SimpleDateFormat(actualDatePattern);
        Date date = df.parse(mandateDateValue);
        df = new SimpleDateFormat(expectedDatePattern);
        return df.format(date);
    }

    public void processAll(List<List<ImportData>> list) {
        if (list != null && !list.isEmpty()) {
            dao.persisteAll(this, list);
        }
    }

    public void cleanup() {
        dao.cleanup();
    }

    public int saveFile(Workbook workbook, String fileName) throws Exception {
        return dao.saveFile(workbook, fileName);
    }

    public void recertifMandate() {
        dao.recertifMandate();
    }

    public String getTypeName() {
        return "TO_STG_MANDATES_EXTRACT";
    }

    public String getBulkTypeName() {
        return "TT_STG_MANDATES_EXTRACT".toUpperCase();
    }

    public String getInsertQuery() {
        //  return "{call PKG_STG_MANDATES_EXTRACT.p_fill_stg_mandates_extract(?)}";
        return "{call PKG_STG_MANDATES.p_fill_stg_mandates_extract(?)}";
    }
}
