package com.db.nfr.tmr.services.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by gasiork on 05/07/2016.
 */
public class ExcelExport {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelExport.class);

    private static final String TEMP_DIR = System.getProperty("java.io.tmpdir");
    private static final Logger log = LoggerFactory.getLogger(ExcelExport.class);
    private String sheetName = "Export";
    private int columnCount = 0;
    private RowMapper titleRowMapper = null;
    private RowMapper rowMapper = null;

    private File createEmptyTempWorkbook(String fileName) throws IOException {
        File tempDirectory = new File(TEMP_DIR);
        if (!tempDirectory.exists()) {
            tempDirectory.mkdirs();
        }
        File file = new File(tempDirectory.getAbsolutePath() + File.separator + fileName);
        XSSFWorkbook wb_template = new XSSFWorkbook();
        try (FileOutputStream fs = new FileOutputStream(file)) {
            wb_template.write(fs);
        }
        log.info("Generated temp file '" + file.getAbsolutePath() + "'");
        return file;
    }

    public ExcelExport setSheetName(String sheetName) {
        this.sheetName = sheetName;
        return this;
    }

    public ExcelExport setColumnCount(int columnCount) {
        this.columnCount = columnCount;
        return this;
    }

    public ExcelExport setTitleRowMapper(RowMapper mapper) {
        this.titleRowMapper = mapper;
        return this;
    }

    public ExcelExport setRowMapper(RowMapper mapper) {
        this.rowMapper = mapper;
        return this;
    }

    public XSSFWorkbook getWorkbook() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(sheetName);
        int rownum = 0;
        //title row
        if (titleRowMapper != null) {
            XSSFCellStyle style = workbook.createCellStyle();
            XSSFFont font = workbook.createFont();
            font.setBold(true);
            style.setFont(font);

            while (titleRowMapper.hasNextRow()) {
                XSSFRow row = sheet.createRow(rownum++);
                for (int colNum = 0; colNum < columnCount; colNum++) {
                    XSSFCell cell = row.createCell(colNum, CellType.STRING);
                    cell.setCellValue(titleRowMapper.getCellValue(colNum));
                    cell.setCellStyle(style);
                }
            }
        }
        //data rows
        if (rowMapper != null) {
            while (rowMapper.hasNextRow()) {
                XSSFRow row = sheet.createRow(rownum++);
                for (int colNum = 0; colNum < columnCount; colNum++) {
                    XSSFCell cell = row.createCell(colNum, CellType.STRING);
                    String s = rowMapper.getCellValue(colNum);
                    cell.setCellValue(s == null ? "" : s);
                }
            }
        }
        return workbook;
    }

    public TemporaryWorkbook getTemporaryWorkbook() throws IOException {
        LOGGER.info("Starting to export data to Excel");
        //create temp file
        final String fileName = Long.toString(new Date().getTime()) + Long.toString(Thread.currentThread().getId()) + ".xlsx";
        final File tempFile = createEmptyTempWorkbook(fileName);

        //prepare workbook
        XSSFWorkbook template;
        try (FileInputStream fs = new FileInputStream(tempFile)) {
            template = new XSSFWorkbook(fs);
        }
        SXSSFWorkbook workbook = new SXSSFWorkbook(template);
        workbook.setCompressTempFiles(true);

        SXSSFSheet sheet = workbook.createSheet(sheetName);
        sheet.setRandomAccessWindowSize(1000);

        int rownum = 0;
        //title row
        if (titleRowMapper != null) {
            CellStyle style = workbook.createCellStyle();
            XSSFFont font = template.createFont();
            font.setBold(true);
            style.setFont(font);

            while (titleRowMapper.hasNextRow()) {
                Row row = sheet.createRow(rownum++);
                for (int colNum = 0; colNum < columnCount; colNum++) {
                    Cell cell = row.createCell(colNum, CellType.STRING);
                    cell.setCellValue(titleRowMapper.getCellValue(colNum));
                    cell.setCellStyle(style);
                }
            }
        }
        //data rows
        if (rowMapper != null) {
            while (rowMapper.hasNextRow()) {
                Row row = sheet.createRow(rownum++);
                for (int colNum = 0; colNum < columnCount; colNum++) {
                    Cell cell = row.createCell(colNum, CellType.STRING);
                    String s = rowMapper.getCellValue(colNum);
                    cell.setCellValue(s == null ? "" : s);
                }
            }
        }
        return new TemporaryWorkbook(tempFile, workbook);
    }
}
