package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * Created by tymkvit on 30/03/2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchHitDisclosureDataRow {
    private static final int EMAIL_MAX_SIZE = 254;


    @JsonProperty("mandateDate")
    private String mandateDate;
    @JsonProperty("versionId")
    private String versionId;

    @JsonPropertyLength(max = EMAIL_MAX_SIZE)
    @JsonDeserialize(using = JsonTextFieldDeserializer.class)
    @JsonProperty("trader")
    private String traderEmail;

    @JsonProperty("declaration")
    private String acknowledge;

    @JsonPropertyLength
    @JsonDeserialize(using = JsonTextFieldDeserializer.class)
    @JsonProperty("rating")
    private String reason;

    @JsonProperty("comment")
    @JsonPropertyLength
    @JsonDeserialize(using = JsonTextFieldDeserializer.class)
    private String comment;
}
