package com.db.nfr.tmr.services.dbapi;


import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

import com.db.nfr.tmr.services.common.FileRecord;

/**
 * This class represents the data access object that would be
 * used for accessing to the database's pkg_file_repo package functions
 * Created by tymkvit on 01/02/2017.
 */
public interface PkgFileRepoDAO {
    /**
     * Get list of new files. Record with status NEW will be created.
     * It will call pkg_file_repo.check_files
     *
     * @return List of new files
     */
    <T> List<T> checkFiles(List<TypeRecAllFiles> fileList, Class<T> returnType) throws SQLException;


    /**
     * Master file will be loaded(saved) into database.
     *
     * @param fileRecord
     * @throws SQLException
     */
    void loadFile(FileRecord fileRecord, InputStream is) throws SQLException, IOException;
}
