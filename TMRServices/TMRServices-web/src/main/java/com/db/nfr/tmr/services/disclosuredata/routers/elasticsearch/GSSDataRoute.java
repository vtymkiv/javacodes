package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.admin.AdminService;
import com.db.nfr.tmr.services.config.disclosuredata.ElasticSearchProperties;
import com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.processors.ConvertSearchResultsProcessor;
import com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.processors.SearchDisclosureDataProcessor;

import lombok.extern.slf4j.Slf4j;

/**
 * Responsible for getting data from GSS Elastic Search
 * and putting it into the TMR repository
 * <p>
 * Created by tymkvit on 07/03/2017.
 */
@Slf4j
@Component
public class GSSDataRoute extends RouteBuilder {
    static final String IS_ELIGIBLE_FOR_RUNNING = "isEligibleForRunning";

    @Autowired
    private ElasticSearchProperties elasticSearchProperties;

    @Autowired
    private SearchDisclosureDataProcessor searchDisclosureDataProcessor;

    @Autowired
    private ConvertSearchResultsProcessor convertSearchResultsProcessor;

    @Autowired
    private AdminService adminService;

    /**
     * <b>Called on initialization to build the routes using the fluent builder syntax.</b>
     * <p/>
     * This is a central method for RouteBuilder implementations to implement
     * the routes using the Java fluent builder syntax.
     *
     * @throws Exception can be thrown during configuration
     */
    @Override
    public void configure() throws Exception {
        LOGGER.debug("Schedule: {}", elasticSearchProperties.getSchedule());
        from(elasticSearchProperties.getSchedule())
                .process(exchange -> {
                    exchange.getOut().setHeader(IS_ELIGIBLE_FOR_RUNNING, adminService.isEligibleForRunning() ? Boolean.TRUE : Boolean.FALSE);
                })
                .choice()
                .when(header(IS_ELIGIBLE_FOR_RUNNING).isNotEqualTo(Boolean.TRUE))
                .log("The host is not eligible for running")
                .otherwise()
                .process(searchDisclosureDataProcessor)
                .process(convertSearchResultsProcessor)
                .routeId("GSSDataRoute")
                .end();
    }
}
