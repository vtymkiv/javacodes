package com.db.nfr.tmr.services.mail;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * Utility for sending email
 *
 * @author Vitaliy Tymkiv
 */
@Component("newMailUtils")
public class MailUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailUtils.class);

    public List<InternetAddress> convertToAddressList(List<String> recipients) throws AddressException {
        LOGGER.debug("Initializing list of recipients: {}", recipients);
        Assert.notEmpty(recipients, "Empty list of recipients");
        int n = recipients.size();
        List<InternetAddress> internetAddressList = new ArrayList<>(n);
        for (String recipient : recipients) {
            internetAddressList.add(convertToAddress(recipient));
        }
        return internetAddressList;
    }

    public InternetAddress convertToAddress(String recipient) throws AddressException {
        LOGGER.info("Initializing recipient: {}", recipient);
        Assert.hasText(recipient, "Empty recipient");
        return new InternetAddress(recipient.trim());
    }
}
