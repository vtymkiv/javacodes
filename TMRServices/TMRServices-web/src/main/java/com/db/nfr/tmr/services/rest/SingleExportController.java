package com.db.nfr.tmr.services.rest;

import javax.servlet.http.HttpServletResponse;

import java.sql.SQLException;

import org.springframework.expression.ParseException;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.common.SingleTrader;

/**
 * Created by tymkvit on 31/01/2017.
 */
@Component
public interface SingleExportController {
    void downloadSingleMandateExport(SingleTrader singleTraderJson, HttpServletResponse response) throws ParseException, SQLException;
}
