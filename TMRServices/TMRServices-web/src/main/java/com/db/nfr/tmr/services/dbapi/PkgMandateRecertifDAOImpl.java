package com.db.nfr.tmr.services.dbapi;

import javax.sql.DataSource;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleTypes;

/**
 * Created by tymkvit on 09/03/2017.
 */
@Slf4j
@Repository
public class PkgMandateRecertifDAOImpl implements PkgMandateRecertifDAO {
    private SimpleJdbcCall recertifMandateSP;
    private SimpleJdbcCall recertifPeriodVersionsSP;

    @Autowired
    public PkgMandateRecertifDAOImpl(@Qualifier("tmsAppDS") DataSource tmsAppDataSource) {
        createStoredProcedures(tmsAppDataSource);
    }

    private void createStoredProcedures(@Qualifier("tmsAppDS") DataSource tmsAppDataSource) {
        recertifMandateSP = new SimpleJdbcCall(tmsAppDataSource)
                .withSchemaName("tms_owner")
                .withCatalogName("pkg_mandate_gss_recertif")
                .withProcedureName("p_recertif_mandate");

        recertifPeriodVersionsSP = new SimpleJdbcCall(tmsAppDataSource)
                .withSchemaName("tms_owner")
                .withCatalogName("pkg_mandate_gss_recertif")
                .withProcedureName("p_get_submitted")
                .withoutProcedureColumnMetaDataAccess()
                .declareParameters(
                        new SqlOutParameter("op_versions", OracleTypes.ARRAY, "TMS_OWNER.NUMARRAY")
                );
    }

    /**
     * Call to re-certificate mandate
     * It will call pkg_mandate_gss_recertif.p_recertif_mandate
     *
     * @throws SQLException
     */
    @Override
    public void recertifMandate() throws SQLException {
        LOGGER.debug("Calling stored procedure: {}.{}.{} ...", recertifMandateSP.getSchemaName(), recertifMandateSP.getCatalogName(), recertifMandateSP.getProcedureName());
        recertifMandateSP.execute();
        LOGGER.debug("Stored procedure: {}.{}.{} is finished.", recertifMandateSP.getSchemaName(), recertifMandateSP.getCatalogName(), recertifMandateSP.getProcedureName());
    }

    /**
     * Call to get versions available for recertification
     * It will call pkg_mandate_gss_recertif.p_get_submitted stored procedure
     *
     * @throws SQLException
     */
    @Override
    public List<BigDecimal> getRecertifPeriodVersions() throws SQLException {
        LOGGER.debug("Calling stored procedure: {}.{}.{} ...", recertifPeriodVersionsSP.getSchemaName(), recertifPeriodVersionsSP.getCatalogName(), recertifPeriodVersionsSP
                .getProcedureName());
        Map<String, Object> result = recertifPeriodVersionsSP.execute();
        LOGGER.debug("Stored procedure: {}.{}.{} is finished.", recertifPeriodVersionsSP.getSchemaName(), recertifPeriodVersionsSP.getCatalogName(), recertifPeriodVersionsSP
                .getProcedureName());
        Object op_versions = ((Array) result.get("op_versions")).getArray();
        BigDecimal[] versions = (BigDecimal[]) op_versions;
        return Arrays.asList(versions);
    }
}


