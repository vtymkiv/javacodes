package com.db.nfr.tmr.services.dataimport;

import java.util.Objects;

/**
 * Created by gasiork on 11/04/2016.
 */
public class ColumnInfo {
    private final String name;
    private final int index;

    public ColumnInfo(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public boolean is(String name) {
        return Objects.equals(this.name, name);
    }

    public int getIndex() {
        return this.index;
    }

    public String getName() {
        return this.name;
    }
}
