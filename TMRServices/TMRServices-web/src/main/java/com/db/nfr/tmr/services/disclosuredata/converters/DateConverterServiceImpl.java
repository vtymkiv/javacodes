package com.db.nfr.tmr.services.disclosuredata.converters;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by tymkvit on 28/03/2017.
 */
@Slf4j
@Component
public class DateConverterServiceImpl implements DateConverterService {
    static final String ORIGINAL_MANDATE_DATE_PATTERN = "dd-MMM-yyyy";
    static final String ORIGINAL_EVIDENCE_PROVISION_DATE_PATTERN = "yyyy/MM/dd HH:mm:ss";
    static final String NEW_MANDATE_DATE_PATTERN = "yyyy-MM-dd";
    static final String NEW_EVIDENCE_PROVISION_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

    @Override
    public String convertDateTo(String originalDateAsStr, String originalFormat, String newFormat) {
        Assert.hasText(originalDateAsStr, "originalDateAsStr can not be null or empty");
        Assert.hasText(originalFormat, "originalFormat can not be null or empty");
        Assert.hasText(newFormat, "newFormat can not be null or empty");

        DateTimeFormatter originalFormatter = new DateTimeFormatterBuilder().parseCaseInsensitive().appendPattern(originalFormat).toFormatter();
        TemporalAccessor temporalAccessor = originalFormatter.parse(originalDateAsStr);

        DateTimeFormatter newFormatter = DateTimeFormatter.ofPattern(newFormat);
        if (temporalAccessor.isSupported(ChronoField.MINUTE_OF_HOUR)) {
            LocalDateTime localDateTime = LocalDateTime.from(temporalAccessor);
            return newFormatter.format(localDateTime);
        } else {
            LocalDate localDate = LocalDate.from(temporalAccessor);


            return newFormatter.format(localDate);
        }

    }
}
