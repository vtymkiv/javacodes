package com.db.nfr.tmr.services.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by gasiork on 22/04/2016.
 */
public class Request {
    private final List<RequestItem> items = new ArrayList<>();
    private final String user;

    public Request(String user) {
        this.user = user;
    }

    public Request getRequestFilteredByType(MandateType type) {
        Request retVal = new Request(this.user);
        for (RequestItem item : items) {
            //find indexes of entries of required mandate type
            if (type == MandateType.findByCode(item.getMandate())) {
                retVal.items.add(item);
            }
        }
        return retVal;
    }

    public List<Request> getRequestInChunks(int chunkSize) {
        List<Request> chunkList = new ArrayList<>();
        Request r = null;
        for (int i = 0; i < items.size(); i++) {
            if (r == null) {
                r = new Request(this.user);
            }
            r.items.add(items.get(i));
            if (r.items.size() == chunkSize) {
                chunkList.add(r);
                r = null;
                continue;
            }
        }
        if (r != null) {
            //add tail chunk - if tail size is less than chunk size
            chunkList.add(r);
        }
        return chunkList;
    }

    public void leaveTraders(List<String> traders) {
        Iterator<RequestItem> iter = items.iterator();
        while (iter.hasNext()) {
            RequestItem trader = iter.next();
            if (!traders.contains(trader.getEmpId())) {
                iter.remove();
            }
        }
    }

    public List<RequestItem> getItems() {
        return items;
    }

    public String getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "Items = " + items.toString() + ", User = " + user;
    }
}
