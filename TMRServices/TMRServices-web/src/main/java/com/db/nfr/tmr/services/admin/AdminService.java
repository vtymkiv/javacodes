package com.db.nfr.tmr.services.admin;

/**
 * Created by tymkvit on 29/03/2017.
 */
public interface AdminService {
    boolean isEligibleForRunning();
}
