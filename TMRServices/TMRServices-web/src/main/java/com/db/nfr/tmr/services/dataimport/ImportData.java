package com.db.nfr.tmr.services.dataimport;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import org.apache.poi.ss.usermodel.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author prokden
 */
public class ImportData implements Serializable {
    private static final Logger log = LoggerFactory.getLogger(ImportData.class);

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final long serialVersionUID = 1L;

    private String traderEmail;
    private String business8;
    private String business9;
    private String supervisor;
    private String traderCountry;
    private String traderRegion;
    private String summary;
    private String bookFamily;
    private String bookId;
    private String productFamily;
    private String product;
    //   private String globalVersion;
    private String localVersion;
    private String mandateType;
    private String tradeStatus;
    private String supervisorStatus;
    private String recertificationType;
    private String gssComment;
    private String status;
    private String reason;
    private String provisionDate;
    private String cycleDate;
    private String version;
    private int fileId;

    public static String parseExcelDate(String cellValue) {
        try {
            return sdf.format(DateUtil.getJavaDate(Double.parseDouble(cellValue)));
        } catch (NumberFormatException ex) {
            log.error(ex.getMessage(), ex);
            return cellValue;
        }
    }

    public String getCycleDate() {
        return cycleDate;
    }

    public void setCycleDate(String cycleDate) {
        this.cycleDate = parseExcelDate(cycleDate);
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTraderEmail() {
        return traderEmail;
    }

    public void setTraderEmail(String traderEmail) {
        this.traderEmail = traderEmail;
    }

    public String getBusiness8() {
        return business8;
    }

    public void setBusiness8(String business9) {
        this.business8 = business9;
    }

    public String getBusiness9() {
        return business9;
    }

    public void setBusiness9(String business9) {
        this.business9 = business9;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getTraderCountry() {
        return traderCountry;
    }

    public void setTraderCountry(String traderCountry) {
        this.traderCountry = traderCountry;
    }

    public String getTraderRegion() {
        return traderRegion;
    }

    public void setTraderRegion(String traderRegion) {
        this.traderRegion = traderRegion;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getBookFamily() {
        return bookFamily;
    }

    public void setBookFamily(String bookFamily) {
        this.bookFamily = bookFamily;
    }

    public String getProductFamily() {
        return productFamily;
    }

    public void setProductFamily(String productFamily) {
        this.productFamily = productFamily;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

//    public String getGlobalVersion() {
//        return globalVersion;
//    }

//    public void setGlobalVersion(String globalVersion) {
//        this.globalVersion = globalVersion;
//    }

    public String getLocalVersion() {
        return localVersion;
    }

    public void setLocalVersion(String localVersion) {
        this.localVersion = localVersion;
    }

    public String getMandateType() {
        return mandateType;
    }

    public void setMandateType(String mandateType) {
        this.mandateType = mandateType;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getRecertificationType() {
        return recertificationType;
    }

    public void setRecertificationType(String recertificationDate) {
        this.recertificationType = recertificationDate;
    }

    public String getGssComment() {
        return gssComment;
    }

    public void setGssComment(String gssComment) {
        this.gssComment = gssComment;
    }

    public String getSupervisorStatus() {
        return supervisorStatus;
    }

    public void setSupervisorStatus(String supervisorStatus) {
        this.supervisorStatus = supervisorStatus;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getProvisionDate() {
        return provisionDate;
    }

    public void setProvisionDate(String provisionDate) {
        this.provisionDate = parseExcelDate(provisionDate);
    }

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }
}
