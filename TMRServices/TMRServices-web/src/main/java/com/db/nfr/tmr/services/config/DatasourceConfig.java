package com.db.nfr.tmr.services.config;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jndi.JndiObjectFactoryBean;

/**
 * Created by tymkvit on 22/09/2016.
 */
@Configuration
@EnableConfigurationProperties
public class DatasourceConfig {
    @Bean(name = "stgFeedDS", destroyMethod = "")
    public DataSource stgFeedDataSource() throws NamingException {
        JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
        jndiObjectFactoryBean.setJndiName(jndiStagePropertyHolder().getJndiName());
        jndiObjectFactoryBean.setProxyInterface(DataSource.class);
        jndiObjectFactoryBean.setLookupOnStartup(false);
        jndiObjectFactoryBean.afterPropertiesSet();
        return (DataSource) jndiObjectFactoryBean.getObject();
    }

    @Primary
    @Bean(name = "tmsAppDS", destroyMethod = "")
    public DataSource tmsAppDataSource() throws NamingException {
        JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
        jndiObjectFactoryBean.setJndiName(jndiTmsPropertyHolder().getJndiName());
        jndiObjectFactoryBean.setProxyInterface(DataSource.class);
        jndiObjectFactoryBean.setLookupOnStartup(false);
        jndiObjectFactoryBean.afterPropertiesSet();
        return (DataSource) jndiObjectFactoryBean.getObject();
    }

    @ConfigurationProperties(prefix = "data-sources.stage")
    @Bean
    public JndiPropertyHolder jndiStagePropertyHolder() {
        return new JndiPropertyHolder();
    }


    @ConfigurationProperties(prefix = "data-sources.tms")
    @Bean
    public JndiPropertyHolder jndiTmsPropertyHolder() {
        return new JndiPropertyHolder();
    }

    @Bean
    public JdbcTemplate tmsJdbcTemplate() throws NamingException {
        return new JdbcTemplate(tmsAppDataSource());
    }

    @Bean
    public JdbcTemplate stageJdbcTemplate() throws NamingException {
        return new JdbcTemplate(stgFeedDataSource());
    }


    private static class JndiPropertyHolder {
        private String jndiName;

        public String getJndiName() {
            return jndiName;
        }

        public void setJndiName(String jndiName) {
            this.jndiName = jndiName;
        }
    }

}