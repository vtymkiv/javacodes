package com.db.nfr.tmr.services.utils;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.common.MandateType;

/**
 * @author gasiork
 */
@Component
@Deprecated
public class MailSender {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailSender.class);

    @Autowired
    private MailUtils mailUtils;

    public void sendEmail(Workbook data, MandateType mandateType) throws IOException, MessagingException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            data.write(out);
            Session session = mailUtils.initSession();
            MimeMessage message = new MimeMessage(session);
            switch (mandateType) {
                case DEBT: {
                    message.setRecipients(Message.RecipientType.TO, mailUtils.initTo(mandateType));
                    message.setSubject(mailUtils.initSubject(mandateType));
                    break;
                }
                case EQUITY: {
                    message.setRecipients(Message.RecipientType.TO, mailUtils.initTo(mandateType));
                    message.setSubject(mailUtils.initSubject(mandateType));
                    break;
                }
                default: {
                    throw new RuntimeException("Invalid mandate type");
                }
            }
            message.setFrom(mailUtils.initFrom());
            BodyPart bodyPart = mailUtils.initBodyPart(out.toByteArray());
            bodyPart.setFileName(mailUtils.getFileName(mandateType));


            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(bodyPart);
            message.setContent(multipart);
            LOGGER.debug("Sending email with attached file [{}] for mandate type {}.", bodyPart.getFileName(), mandateType.name());
            Transport.send(message);
        }
    }

}
