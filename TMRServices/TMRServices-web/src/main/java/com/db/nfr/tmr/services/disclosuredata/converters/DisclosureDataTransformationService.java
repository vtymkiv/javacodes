package com.db.nfr.tmr.services.disclosuredata.converters;

import com.db.nfr.tmr.services.disclosuredata.DisclosureData;
import com.db.nfr.tmr.services.disclosuredata.DisclosureStatus;
import com.db.nfr.tmr.services.disclosuredata.DtoStatus;

/**
 * Created by tymkvit on 28/03/2017.
 */
public interface DisclosureDataTransformationService {
    /**
     * Set re-certification type and mandate type based on guiKey
     *
     * @param guiKey
     * @param disclosureData
     */
    void setRecertificationAndMandateType(String guiKey, DisclosureData disclosureData);

    /**
     * Set dates for disclosureData object by some rules
     *
     * @param evidenceProvisionDate
     * @param mandateDate
     * @param disclosureData
     */
    void setDates(String evidenceProvisionDate, String mandateDate, DisclosureData disclosureData);

    /**
     * Get disclosure data status based on dto status
     *
     * @param dtoStatus
     * @return DisclosureStatus
     */
    DisclosureStatus getStatus(DtoStatus dtoStatus);

    /**
     * Get disclosure data status based on status that came from elastic search response dto.status object
     *
     * @param status
     * @return
     */
    DisclosureStatus getStatus(String status);
}
