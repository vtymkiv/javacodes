package com.db.nfr.tmr.services.dbapi;

import javax.sql.DataSource;

import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Struct;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.common.Request;
import com.db.nfr.tmr.services.common.RequestItem;
import com.db.nfr.tmr.services.disclosuredata.DisclosureData;

import oracle.jdbc.OracleConnection;

/**
 * Create custom oracle types for use when call stored procedure methods.
 */
// TODO: Add integration tests
@Component
public class CustomOracleTypes {


    public Array getGssTraderStructArray(Connection conn, List<List> traders) throws SQLException {
        if (CollectionUtils.isEmpty(traders)) {
            throw new IllegalArgumentException("List of traders is empty.");
        }
        Struct[] structureList = new Struct[traders.size()];
        for (int i = 0; i < structureList.length; i++) {
            structureList[i] = getGssTraderStruct(conn, traders.get(i));
        }

        if (conn.isWrapperFor(OracleConnection.class)) {
            return conn.unwrap(OracleConnection.class).createOracleArray("TMS_OWNER.TT_GSS_TRADER", structureList);
        }
        return conn.createArrayOf("TMS_OWNER.TT_GSS_TRADER", structureList);
    }

    public Struct getGssTraderStruct(Connection conn, List traderRec) throws SQLException {
        return conn.createStruct("TMS_OWNER.TO_GSS_TRADER", new Object[]{traderRec.get(0), traderRec.get(1), traderRec.get(2), traderRec.get(3)});
    }


    public Struct getVersionStruct(Connection conn, Request request) throws SQLException, ParseException {
        return conn.createStruct("TMS_OWNER.TO_GET_VERSION", new Object[]{"", getGssTraderStructArray(conn, getTradersList(request))});
    }


    public Struct getStruct(Connection conn, RequestItem item, boolean swapDates) throws SQLException, ParseException {
        if (swapDates) {
            return getGssTraderStruct(conn, Arrays.asList(item.getEmpId(), item.getNextLocalVersion(), item.getMandate(), item.getLocalVersion()));
        } else {
            return getGssTraderStruct(conn, Arrays.asList(item.getEmpId(), item.getLocalVersion(), item.getMandate(), item.getNextLocalVersion()));
        }
    }

    public Struct getExportStruct(Connection conn, RequestItem item, boolean swapDates) throws SQLException, ParseException {
        return conn.createStruct("TMS_OWNER.TO_EXPORT_PART_MANDATE", new Object[]{"", getStruct(conn, item, swapDates)});
    }


    // TODO: This should be moved to Utils class
    public List<List> getTradersList(Request request) {
        List<List> traders = new ArrayList<>();
        for (RequestItem requestItem : request.getItems()) {
            traders.add(Arrays.asList(requestItem.getEmpId(), requestItem.getLocalVersion(), requestItem.getMandate(), requestItem.getNextLocalVersion()));
        }
        return traders;
    }

    public Array getMandatesStructArray(DataSource dataSource, List<DisclosureData> disclosureDataList) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            return getMandatesStructArray(conn, disclosureDataList);
        }

    }

    private Array getMandatesStructArray(Connection conn, List<DisclosureData> disclosureDataList) throws SQLException {
        Struct[] structureList = new Struct[disclosureDataList.size()];
        for (int i = 0; i < structureList.length; i++) {
            structureList[i] = getMandatesStruct(conn, disclosureDataList.get(i));
        }

        OracleConnection oracleConnection = conn.unwrap(OracleConnection.class);
        return oracleConnection.createOracleArray("STG_OWNER.TT_STG_MANDATES_EXTRACT", structureList);
    }

    private Struct getMandatesStruct(Connection conn, DisclosureData disclosureData) throws SQLException {
        return conn.createStruct("STG_OWNER.TO_STG_MANDATES_EXTRACT", new Object[]{
                disclosureData.getTraderEmail(),
                disclosureData.getSummary(),
                disclosureData.getMandateDate(),
                disclosureData.getMandateType(),
                disclosureData.getCertificationType(),
                disclosureData.getAcknowledge(),
                disclosureData.getComment(),
                disclosureData.getReason(),
                disclosureData.getStatus(),
                disclosureData.getProvisionDate(),
                disclosureData.getCycleDate(),
                disclosureData.getVersionId(),
                disclosureData.getFileId()
        });
    }

    public Array getRecAllFilesStructArray(DataSource dataSource, List<TypeRecAllFiles> typeRecAllFiles) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            return getRecAllFilesStructArray(conn, typeRecAllFiles);
        }
    }

    private Array getRecAllFilesStructArray(Connection conn, List<TypeRecAllFiles> typeRecAllFiles) throws SQLException {
        Struct[] structureList = new Struct[typeRecAllFiles.size()];
        for (int i = 0; i < structureList.length; i++) {
            structureList[i] = getRecAllFilesStruct(conn, typeRecAllFiles.get(i));
        }

        OracleConnection oracleConnection = conn.unwrap(OracleConnection.class);
        return oracleConnection.createOracleArray("STG_OWNER.T_ALL_FILES", structureList);
    }

    private Struct getRecAllFilesStruct(Connection conn, TypeRecAllFiles item) throws SQLException {
        return conn.createStruct("STG_OWNER.REC_ALL_FILES", new Object[]{item.getFeedConfigId(), item.getFileName(), new java.sql.Date(item.getFileDate().getTime()), item
                .getFileSize()});
    }
/*
    public SqlParameterValue getBlobValueForSqlParam(byte[] blobValue) {
        return new SqlParameterValue(Types.BLOB, new SqlLobValue(blobValue));
    }*/

}

