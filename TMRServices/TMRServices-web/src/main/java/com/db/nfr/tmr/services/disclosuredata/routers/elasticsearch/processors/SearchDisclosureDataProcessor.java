package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.processors;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.config.disclosuredata.ElasticSearchProperties;
import com.db.nfr.tmr.services.disclosuredata.DisclosureDataService;
import com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.ElasticSearchService;
import com.google.common.io.CharStreams;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RefreshScope
public class SearchDisclosureDataProcessor implements Processor {

    private static final String VERSION_ID_LIST_PLACEHOLDER = "${versionIdList}";

    @Autowired
    private ElasticSearchProperties elasticSearchProperties;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private DisclosureDataService disclosureDataService;

    @Autowired
    private ElasticSearchService elasticSearchService;

    @Override
    public void process(Exchange exchange) throws Exception {
        Settings.Builder settingsBuilder = Settings.settingsBuilder();
        elasticSearchProperties.getSettings().forEach(setting ->
                settingsBuilder.put(setting.getName(), setting.getValue())
        );

        Client client = elasticSearchService.getTransportClient(settingsBuilder.build(),
                new InetSocketAddress(elasticSearchProperties.getHostName(), elasticSearchProperties.getPort()));

        try {
            List<String> versionIdList = disclosureDataService.getVersionIdList();
            StringBuilder queryString = new StringBuilder();
            String originalQueryString = getQueryString();
            if (CollectionUtils.isNotEmpty(versionIdList) && originalQueryString.contains(VERSION_ID_LIST_PLACEHOLDER)) {
                queryString.append(originalQueryString.replace(VERSION_ID_LIST_PLACEHOLDER, versionIdList.stream().collect(Collectors.joining(", "))));
            } else {
                queryString.append(originalQueryString.replace(VERSION_ID_LIST_PLACEHOLDER, StringUtils.EMPTY));
            }

            LOGGER.debug("Json query string for elastic search : {}", queryString);

            SearchResponse response = client.prepareSearch(elasticSearchProperties.getIndexName())
                    .setTypes(elasticSearchProperties.getTypeName())
                    .setSource(queryString.toString())
                    .execute()
                    .actionGet();

            if (response == null || response.getHits() == null || response.getHits().totalHits() == 0) {
                stopRoute(exchange);
                throw new RuntimeException("Document not found");
            }
            exchange.getIn().setBody(response);
        } finally {
            if (client != null) {
                client.close();
            }
        }

    }

    private void stopRoute(Exchange exchange) {
        // mark the exchange to stop continue routing
        exchange.setProperty(Exchange.ROUTE_STOP, Boolean.TRUE);
    }

    private String getQueryString() throws IOException {
        try (InputStream inputStream = resourceLoader.getResource(elasticSearchProperties.getPathToRequest()).getInputStream()) {
            try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"))) {
                return CharStreams.toString(inputStreamReader);
            }
        }
    }

}
