package com.db.nfr.tmr.services.dbapi;

import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import oracle.jdbc.OracleTypes;
import oracle.sql.BLOB;

/**
 * Created by gasiork on 02/08/2016.
 */
@Deprecated
public class PkgFileRepo {
    private static Logger LOGGER = LoggerFactory.getLogger(PkgFileRepo.class);

    public static int checkFiles(Connection conn, TypeRecAllFiles recAllFiles) throws Exception {
        LOGGER.debug("checkFiles Schema {}", conn.getSchema());
        try (CallableStatement cs = conn.prepareCall("{call stg_owner.pkg_file_repo.check_files(?,?)}")) {
            cs.registerOutParameter(2, OracleTypes.CURSOR);
            cs.setArray(1, recAllFiles.getStructArray(conn));
            cs.execute();
            try (ResultSet rs = (ResultSet) cs.getObject(2)) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        }
        return 0;
    }

    public static void loadFile(Connection conn, int id, Workbook workbook) throws Exception {
        BLOB workbookBlob = BLOB.createTemporary(conn, false, BLOB.DURATION_SESSION);
        try (OutputStream os = workbookBlob.setBinaryStream(0L)) {
            workbook.write(os);
        }
        try (CallableStatement cs = conn.prepareCall("{call stg_owner.pkg_file_repo.load_file(?,?)}")) {
            cs.setInt(1, id);
            cs.setBlob(2, workbookBlob);
            cs.execute();
        }
    }
}
