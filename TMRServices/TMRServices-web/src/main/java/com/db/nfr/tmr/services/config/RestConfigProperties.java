package com.db.nfr.tmr.services.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by tymkvit on 08/12/2016.
 */
@Component
@ConfigurationProperties(prefix = "tmr-to-gss")
public class RestConfigProperties {
    private String schema;
    private String host;
    private String port;
    private RestEndpoints restEndpoints;

    public static class RestEndpoints {
        private String baseUrl;
        private String daisyChainingEndpoint;
        private String daisyChainingStatusEndpoint;

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public String getDaisyChainingEndpoint() {
            return daisyChainingEndpoint;
        }

        public void setDaisyChainingEndpoint(String daisyChainingEndpoint) {
            this.daisyChainingEndpoint = daisyChainingEndpoint;
        }

        public String getDaisyChainingStatusEndpoint() {
            return daisyChainingStatusEndpoint;
        }

        public void setDaisyChainingStatusEndpoint(String daisyChainingStatusEndpoint) {
            this.daisyChainingStatusEndpoint = daisyChainingStatusEndpoint;
        }

        @Override
        public String toString() {
            return "RestEndpoints{" +
                    "baseUrl='" + baseUrl + '\'' +
                    ", daisyChainingEndpoint='" + daisyChainingEndpoint + '\'' +
                    ", daisyChainingStatusEndpoint='" + daisyChainingStatusEndpoint + '\'' +
                    '}';
        }
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public RestEndpoints getRestEndpoints() {
        return restEndpoints;
    }

    public void setRestEndpoints(RestEndpoints restEndpoints) {
        this.restEndpoints = restEndpoints;
    }

    @Override
    public String toString() {
        return "RestConfigProperties{" +
                "schema='" + schema + '\'' +
                ", host='" + host + '\'' +
                ", port='" + port + '\'' +
                ", restEndpoints=" + restEndpoints +
                '}';
    }
}
