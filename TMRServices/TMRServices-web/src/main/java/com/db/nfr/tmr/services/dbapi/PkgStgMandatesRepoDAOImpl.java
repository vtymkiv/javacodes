package com.db.nfr.tmr.services.dbapi;

import javax.sql.DataSource;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.db.nfr.tmr.services.disclosuredata.DisclosureData;

import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleTypes;

/**
 * Created by tymkvit on 07/02/2017.
 */
@Slf4j
@Repository
public class PkgStgMandatesRepoDAOImpl implements PkgStgMandatesRepoDAO {
    @Autowired
    private CustomOracleTypes customOracleTypes;
    private SimpleJdbcCall cleanStgMandatesSP;
    private SimpleJdbcCall fillStgMandatesSP;

    @Autowired
    public PkgStgMandatesRepoDAOImpl(@Qualifier("stgFeedDS") DataSource stgFeedDataSource) {

        cleanStgMandatesSP = new SimpleJdbcCall(stgFeedDataSource)
                .withSchemaName("stg_owner")
                .withCatalogName("pkg_stg_mandates")
                .withProcedureName("p_clean_stg_mandates_extract");

        fillStgMandatesSP = new SimpleJdbcCall(stgFeedDataSource)
                .withSchemaName("stg_owner")
                .withCatalogName("pkg_stg_mandates")
                .withProcedureName("p_fill_stg_mandates_extract")
                .declareParameters(
                        new SqlParameter("ip_record", OracleTypes.ARRAY)
                );
    }

    /**
     * Clean up debts
     * It will call pkg_stg_mandates.p_clean_stg_mandates_extract
     *
     * @throws SQLException
     */
    @Override
    public void clean() throws SQLException {
        LOGGER.debug("Calling the stored procedure: {}.{}.{} ...", cleanStgMandatesSP.getSchemaName(), cleanStgMandatesSP.getCatalogName(), cleanStgMandatesSP.getProcedureName());
        cleanStgMandatesSP.execute();
        LOGGER.debug("Stored procedure: {}.{}.{} is finished.", cleanStgMandatesSP.getSchemaName(), cleanStgMandatesSP.getCatalogName(), cleanStgMandatesSP.getProcedureName());
    }

    /**
     * Uploading mandates into STG_ table.
     * It will call pkg_stg_mandates.p_fill_stg_mandates_extract
     *
     * @param disclosureDataList
     * @throws SQLException
     */
    @Override
    public void populateMandates(List<DisclosureData> disclosureDataList) throws SQLException {
        Objects.requireNonNull(disclosureDataList, "List of mandates cannot be null.");
        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("ip_record", customOracleTypes.getMandatesStructArray(fillStgMandatesSP.getJdbcTemplate().getDataSource(), disclosureDataList));
        LOGGER.debug("Calling the stored procedure: {}.{}.{} ...", fillStgMandatesSP.getSchemaName(), fillStgMandatesSP.getCatalogName(), fillStgMandatesSP.getProcedureName());
        fillStgMandatesSP.execute(inParams);
        LOGGER.debug("Stored procedure: {}.{}.{} is finished.", fillStgMandatesSP.getSchemaName(), fillStgMandatesSP.getCatalogName(), fillStgMandatesSP.getProcedureName());
    }

}
