package com.db.nfr.tmr.services.disclosuredata;

import lombok.Data;

/**
 * Created by tymkvit on 09/03/2017.
 */
@Data
public class DisclosureData {
    private static final int UNDEFINED_FILE_ID = -1;
    private String traderEmail;
    private String summary;
    /**
     * Local version
     */
    private String mandateDate;
    private String mandateType;
    private String certificationType;
    private String acknowledge;
    private String comment;
    private String reason;
    private String status;
    private String provisionDate;
    private String cycleDate;
    private String versionId;
    private int fileId = UNDEFINED_FILE_ID;
}
