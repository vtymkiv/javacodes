package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.fasterxml.jackson.annotation.JacksonAnnotation;

/**
 * Annotation used for setting the max length of property.
 * Must be applied only for text properties
 * Created by tymkvit on 07/04/2017.
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotation
@interface JsonPropertyLength {
    int max() default 3999;
}
