package com.db.nfr.tmr.services.common;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * Created by danyvik on 21/06/2016.
 */
public class JsonPojo {
    @SerializedName("businessArea")
    public List<String> businessAreaList;

    @SerializedName("locationsArea")
    public List<String> locationAreaList;

    @SerializedName("traderStatus")
    public List<String> traderStatusList;

    @SerializedName("supervisorStatus")
    public List<String> supervisorStatusList;

    @SerializedName("ids")
    public List<String> idList;

    @SerializedName("businessAreaLevel")
    public String ubrLevel;

    @SerializedName("user")
    public String user;
}
