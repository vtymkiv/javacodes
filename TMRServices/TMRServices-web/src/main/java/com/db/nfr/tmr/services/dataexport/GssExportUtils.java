package com.db.nfr.tmr.services.dataexport;

import javax.sql.DataSource;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.common.MandateType;
import com.db.nfr.tmr.services.common.Request;
import com.db.nfr.tmr.services.common.ResultSetFunction;
import com.db.nfr.tmr.services.dbapi.PkgMandateGssRecertif;
import com.db.nfr.tmr.services.dbapi.ProcessingStatus;
import com.db.nfr.tmr.services.utils.Constants;
import com.db.nfr.tmr.services.utils.MailSender;
import com.db.nfr.tmr.services.utils.Utils;

/**
 * @author gasiork
 */
@Component
public class GssExportUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(GssExportUtils.class);
    public static final String Version = "Version";
    private static final Utils utils = new Utils();
    private static final int POOL_THREAD_COUNT = 10;
    private static final int REQUEST_CHUNK_COUNT = 10;
    private static final ExecutorService executor = Executors.newFixedThreadPool(POOL_THREAD_COUNT);

    @Autowired
    @Qualifier("tmsAppDS")
    private DataSource tmsAppDataSource;

    @Autowired
    private PkgMandateGssRecertif pkgMandateGssRecertif;

    @Autowired
    @Qualifier("oldMailSender")
    private MailSender mailSender;

    public static class ExportWorkbook implements AutoCloseable {
        public Workbook workbook;
        public File tempFile;

        public void close() {
            if (tempFile == null) {
                return;
            }
            if (tempFile.exists()) {
                tempFile.delete();
            }
        }
    }

    public void exportByMail(Request request, int version) {
        try {
            //DEBTs
            Request debtRequest = request.getRequestFilteredByType(MandateType.DEBT);
            LOGGER.info("Exporting DEBTs {}", debtRequest);
            sendEmailByType(debtRequest, MandateType.DEBT, version);
            //EQTs
            Request eqtRequest = request.getRequestFilteredByType(MandateType.EQUITY);
            LOGGER.info("Exporting EQTs {}", eqtRequest);
            sendEmailByType(eqtRequest, MandateType.EQUITY, version);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            //remove locked traders
            removeTradersInProgress(request);
        }
    }

    public ProcessingStatus getVersionForTraders(Request request) {
        LOGGER.info("Get version for traders with request data: {} ", request);
        try (Connection conn = tmsAppDataSource.getConnection()) {
            return pkgMandateGssRecertif.pGetVersion(conn, request);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    private void removeTradersInProgress(Request request) {
        LOGGER.info("Delete traders with request data: {}", request);
        try (Connection conn = tmsAppDataSource.getConnection()) {
            pkgMandateGssRecertif.pDeleteTraders(conn, request);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private void sendEmailByType(Request request, MandateType mandateType, int version) throws Exception {
        LOGGER.info("Preparing to send {}, with request {}", mandateType, request);
        if (request.getItems().isEmpty()) {
            LOGGER.info("Empty request found");
            return;
        }
        try (ExportWorkbook exportWorkbook = getExportDataForMail(tmsAppDataSource, request, mandateType, version)) {
            if (exportWorkbook.workbook.getSheetAt(0).getLastRowNum() < 1) {
                LOGGER.info("Empty excel file found");
                return;
            }
            persistGeneratedFile(request, exportWorkbook.workbook, version);
            try {
                //send file to GSS
                mailSender.sendEmail(exportWorkbook.workbook, mandateType);
            } catch (Exception ex) {
                LOGGER.error("Sending file to gss failed with exception.", ex);
                updateStatusFailed(version);
                throw ex;
            }
            updateStatusSuccess(request, version);
        }
    }

    private void persistGeneratedFile(Request request, Workbook workbook, int version) throws Exception {
        LOGGER.info("Save file for version {} with request data: {}", version, request);
        try (Connection conn = tmsAppDataSource.getConnection()) {
            pkgMandateGssRecertif.pSaveFile(conn, request, workbook, version);
        }
    }

    private void updateStatusSuccess(Request request, int version) throws Exception {
        LOGGER.info("Mark status success for version {} with request data: {}", version, request.toString());
        try (Connection conn = tmsAppDataSource.getConnection()) {
            pkgMandateGssRecertif.pMarkStatusPending(conn, request, version);
        }
    }

    private void updateStatusFailed(int version) throws Exception {
        LOGGER.info("Mark status failed for version {}", version);
        try (Connection conn = tmsAppDataSource.getConnection()) {
            pkgMandateGssRecertif.pMarkStatusPendingFAILED(conn, version);
        }
    }

    private String getSheetName(MandateType mandateType) {
        if (mandateType == null) {
            return "Export";
        } else {
            switch (mandateType) {
                case DEBT:
                    return "TM REC FIC";
                case EQUITY:
                    return "TM REC GME";
                default:
                    return "Export";
            }
        }
    }

    public ExportWorkbook getExportDataForMail(DataSource tmsAppDataSource, Request request, MandateType mandateType, int version) throws Exception {
        try {
            LOGGER.info("getExportDataForMail with request: {}", request.getItems().size());
            // TODO: Change on SXSSFWorkbook. But the SXSSFWorkbook write method when called twice will throw an exception.
            Workbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet(getSheetName(mandateType));
            //parallel calls
            List<Future> futureList = new ArrayList<>();
            List<Request> chunkList = request.getRequestInChunks(REQUEST_CHUNK_COUNT);
            for (Request chunk : chunkList) {
                //create and submit runnable tasks
                futureList.add(executor.submit(getRunnable(tmsAppDataSource, chunk, sheet, version)));
            }

            LOGGER.debug("futureList {}.", futureList);
            for (Future future : futureList) {
                //wait for all tasks to finish

                future.get();
            }
            LOGGER.info("DataTransformer::getExportDataForMail END");
            //return workbook
            ExportWorkbook exportWorkbook = new ExportWorkbook();
            exportWorkbook.workbook = workbook;
            return exportWorkbook;
        } catch (Exception ex) {
            LOGGER.error("Exception happened during getExportDataForMail.", ex);
            throw ex;
        }
    }

    private Runnable getRunnable(final DataSource tmsAppDataSource, final Request chunk, final Sheet sheet, final int version) {
        return new Runnable() {
            @Override
            public void run() {
                try (Connection conn = tmsAppDataSource.getConnection()) {
                    LOGGER.debug("Calling  pGetMandatesForRecertif...");
                    pkgMandateGssRecertif.pGetMandatesForRecertif(conn, chunk, version, new ResultSetFunction() {
                        @Override
                        public void run(ResultSet rs) throws Exception {
                            synchronized (sheet) {
                                addDataToSheet(rs, sheet);
                            }
                        }
                    });
                } catch (Exception ex) {
                    LOGGER.error("Failed call to pGetMandatesForRecertif", ex);
                    throw new RuntimeException(ex);
                }
            }
        };
    }

    private void addDataToSheet(ResultSet rs, Sheet sheet) throws Exception {
        LOGGER.debug("Start addDataToSheet...");
        boolean exportVersionColumn = !"0".equals(utils.initProps().getProperty(Constants.EXPORT_VERSION_COLUMN, "0"));
        //get metadata
        ResultSetMetaData metadata = rs.getMetaData();
        //create title row
        int rowNum = sheet.getLastRowNum();
        Row row;
        if (rowNum == 0) {
            row = sheet.createRow(rowNum);
            int colNum = 0;
            for (int i = 0; i < metadata.getColumnCount(); i++) {
                String columnName = metadata.getColumnName(i + 1);
                if (columnName.equals(Version) && !exportVersionColumn) {
                    continue;
                }
                Cell cell = row.createCell(colNum++, Cell.CELL_TYPE_STRING);
                cell.setCellValue(columnName);
            }
        }
        //create data rows
        while (rs.next()) {
            row = sheet.createRow(++rowNum);
            int colNum = 0;
            for (int i = 0; i < metadata.getColumnCount(); i++) {
                String columnName = metadata.getColumnName(i + 1);
                if (columnName.equals(Version) && !exportVersionColumn) {
                    continue;
                }
                Cell cell = row.createCell(colNum++, Cell.CELL_TYPE_STRING);
                cell.setCellValue(rs.getString(i + 1));
            }
        }
        LOGGER.debug("End addDataToSheet.");
    }

}
