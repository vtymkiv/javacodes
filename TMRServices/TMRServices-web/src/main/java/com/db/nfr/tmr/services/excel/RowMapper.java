package com.db.nfr.tmr.services.excel;

/**
 * Created by gasiork on 05/07/2016.
 */
public interface RowMapper {
    boolean hasNextRow();

    String getCellValue(int columnNumber);
}
