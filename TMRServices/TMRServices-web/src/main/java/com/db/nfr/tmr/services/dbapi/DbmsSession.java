package com.db.nfr.tmr.services.dbapi;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by danyvik on 18/08/2016.
 */
public class DbmsSession {
    public static void resetPackageState(Connection connection) throws SQLException {
        try (CallableStatement cs = connection.prepareCall("{call sys.dbms_session.reset_package()}")) {
            cs.execute();
        }
    }
}
