package com.db.nfr.tmr.services.rest;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import java.io.IOException;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.db.nfr.tmr.services.common.ExportSingleData;
import com.db.nfr.tmr.services.common.SingleTrader;
import com.db.nfr.tmr.services.dataexport.SingleExportService;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gasiork
 */
@Slf4j
@RestController
public class SingleExportControllerImpl implements SingleExportController {

    @Autowired
    private SingleExportService singleExportService;

    @RequestMapping(value = "/exportservice/single", method = {RequestMethod.POST}, consumes = "application/json")
    public void downloadSingleMandateExport(@RequestBody @Valid SingleTrader singleTraderJson, HttpServletResponse response) throws ParseException, SQLException {
        LOGGER.debug("downloadSingleMandateExport started with the following data: {}", singleTraderJson.toString());
        exportData(singleTraderJson, response);
    }

    @RequestMapping(value = "/exportservice/getsingle", method = {RequestMethod.GET})
    public void downloadSingleMandateExportGet(@Valid String json, HttpServletResponse response) throws ParseException, SQLException, IOException {
        LOGGER.debug("downloadSingleMandateExportGet started with the following JsonString: {}" + json);
        ObjectMapper mapper = new ObjectMapper();
        SingleTrader singleTraderJson = mapper.readValue(json, SingleTrader.class);
        exportData(singleTraderJson, response);
        //TODO should have only one method for the Single Mandate Export Rest API
    }

    private void exportData(@RequestBody @Valid SingleTrader singleTraderJson, HttpServletResponse response) throws SQLException {
        try {
            ExportSingleData exportSingleData = singleExportService.process(singleTraderJson);
            response.setContentType(exportSingleData.getContentType());
            response.addHeader("Content-Disposition", "attachment; filename=" + exportSingleData.getFileName());
            response.getOutputStream().write(exportSingleData.getFileImage());
        } catch (IOException e) {
            LOGGER.error("No data found for, {}", singleTraderJson.getMandateType().toUpperCase());
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}