package com.db.nfr.tmr.services.dataexport;

import java.io.IOException;
import java.sql.SQLException;

import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.common.ExportSingleData;
import com.db.nfr.tmr.services.common.SingleTrader;

/**
 * Created by tymkvit on 31/01/2017.
 */
@Component
public interface SingleExportService {
    ExportSingleData process(SingleTrader singleTraderJson) throws SQLException, IOException;
}
