package com.db.nfr.tmr.services.disclosuredata;

import lombok.Getter;

/**
 * Created by tymkvit on 15/03/2017.
 */
public enum DtoStatus {
    CLOSED_NOK("CLOSED_NOK"),
    PROVISION("PROVISION"),
    REMINDER("REMINDER"),
    ESCALATION("ESCALATION"),
    CLOSED_OK("CLOSED_OK"),
    UNDEFINED("UNDEFINED");

    @Getter
    private String code;

    public static DtoStatus findByCode(String code) {
        for (DtoStatus dtoStatus : values()) {
            if (dtoStatus.getCode().equalsIgnoreCase(code)) {
                return dtoStatus;
            }
        }
        return UNDEFINED;
    }

    DtoStatus(String code) {
        this.code = code;
    }
}
