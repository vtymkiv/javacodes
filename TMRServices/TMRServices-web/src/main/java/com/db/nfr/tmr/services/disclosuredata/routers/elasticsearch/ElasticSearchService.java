package com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch;

import java.net.InetSocketAddress;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;

/**
 * Interface for accessing to elastic search services
 * Created by tymkvit on 05/04/2017.
 */
public interface ElasticSearchService {
    Client getTransportClient(Settings settings, InetSocketAddress inetSocketAddress);

}
