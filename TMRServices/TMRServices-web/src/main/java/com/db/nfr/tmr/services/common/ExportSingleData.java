package com.db.nfr.tmr.services.common;

import lombok.Data;

/**
 * Created by danyvik on 23/02/2017.
 */
@Data
public class ExportSingleData {
    private String fileName;
    private String contentType;
    private byte[] fileImage;
}
