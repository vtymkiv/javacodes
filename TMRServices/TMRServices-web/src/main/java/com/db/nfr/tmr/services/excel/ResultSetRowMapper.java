package com.db.nfr.tmr.services.excel;

import javax.sql.rowset.CachedRowSet;

import java.sql.SQLException;

/**
 * Created by gasiork on 05/07/2016.
 */
public class ResultSetRowMapper implements RowMapper {
    private CachedRowSet cachedRowSet;

    public ResultSetRowMapper(CachedRowSet cachedRowSet) throws SQLException {
        this.cachedRowSet = cachedRowSet;//new CachedRowSetImpl();
    }

    @Override
    public boolean hasNextRow() {
        try {
            return cachedRowSet.next();
        } catch (SQLException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public String getCellValue(int columnNumber) {
        try {
            //columnNumber start from 0, but resultset column index starts from 1
            return cachedRowSet.getString(columnNumber + 1);
        } catch (SQLException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
}
