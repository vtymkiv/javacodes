package com.db.nfr.tmr.services.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Created by tymkvit on 08/12/2016.
 */
@Configuration
public class RestConfig {
    @Bean
    public RestTemplate restTemplate() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        return customize(restTemplateBuilder.build());
    }

    private RestTemplate customize(RestTemplate restTemplate) {
        // You can add customization here
        return restTemplate;
    }
}
