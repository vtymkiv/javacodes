package com.db.nfr.tmr.services.config.disclosuredata;

import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.db.nfr.tmr.services.disclosuredata.routers.elasticsearch.GSSDataRoute;

/**
 * Created by vtymkiv on 22.05.2017.
 */
@Configuration
@RefreshScope
public class ESRouterConfig {

    @Bean
    public GSSDataRoute gssDataRoute() {
        return new GSSDataRoute();
    }
}
