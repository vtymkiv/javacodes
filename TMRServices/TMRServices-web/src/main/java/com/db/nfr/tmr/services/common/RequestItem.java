package com.db.nfr.tmr.services.common;

/**
 * Created by gasiork on 22/04/2016.
 */
public class RequestItem {
    private final String email;
    private final String empId;
    private final String localVersion;
    private final String mandate;
    private final String nextLocalVersion;

    public RequestItem(String email, String empId, String localVersion, String mandate, String nextLocalVersion) {
        this.email = email;
        this.empId = empId;
        this.localVersion = localVersion;
        this.mandate = mandate;
        this.nextLocalVersion = nextLocalVersion;
    }

    public String getEmail() {
        return email;
    }

    public String getEmpId() {
        return empId;
    }

    public String getLocalVersion() {
        return localVersion;
    }

    public String getMandate() {
        return mandate;
    }

    public String getNextLocalVersion() {
        return nextLocalVersion;
    }

    @Override
    public String toString() {
        return "{email: " + email + ", empId: " + empId + ", localVersion: " + localVersion + ", mandate: " + mandate + ", nextLocalVersion: " + nextLocalVersion + "}";
    }
}
