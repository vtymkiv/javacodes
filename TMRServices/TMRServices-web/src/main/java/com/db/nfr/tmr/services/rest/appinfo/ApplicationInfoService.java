package com.db.nfr.tmr.services.rest.appinfo;

import org.springframework.stereotype.Component;

/**
 * Responsible for presenting information about application.
 * Created by tymkvit on 27/10/2016.
 */
@Component
public interface ApplicationInfoService {
    /**
     * Get application info as JSON object:
     * <p>
     * "time": "2016-12-01 16:29:39.184",
     * "artifact": "services-web",
     * "group": "com.db.nfr.tmr",
     * "name": "GTMRServices-web"
     *
     * @return
     */
    WsApplicationInfo getApplicationInfo();
}
