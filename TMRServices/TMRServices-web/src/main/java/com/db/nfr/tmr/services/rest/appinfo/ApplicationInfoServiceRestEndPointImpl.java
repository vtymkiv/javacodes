package com.db.nfr.tmr.services.rest.appinfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class represents application information rest service
 */
@Component
@RestController
public class ApplicationInfoServiceRestEndPointImpl {
    public static final String APPLICATION_INFO = "/appInfo";

    @Autowired
    private ApplicationInfoService applicationInfoService;

    @RequestMapping(value = APPLICATION_INFO, method = RequestMethod.GET)
    public WsApplicationInfo getApplicationInfo() {
        return applicationInfoService.getApplicationInfo();
    }
}
