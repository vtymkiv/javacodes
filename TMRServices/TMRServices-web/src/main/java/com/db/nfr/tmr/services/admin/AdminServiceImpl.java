package com.db.nfr.tmr.services.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import com.db.nfr.tmr.services.common.LocalHost;
import com.db.nfr.tmr.services.config.disclosuredata.DisclosureDataProperties;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by tymkvit on 29/03/2017.
 */
@Slf4j
@Component
@RefreshScope
public class AdminServiceImpl implements AdminService {
    @Autowired
    private LocalHost localHost;

    @Autowired
    private DisclosureDataProperties disclosureDataProperties;

    @Override
    public boolean isEligibleForRunning() {
        String localHostName = localHost.getLocalHostName();
        List<String> allowedHosts = disclosureDataProperties.getAllowedHosts();
        LOGGER.info("Checking if host [{}] is eligible for running TMR services. Here is the list of allowed hosts for running TMR services: [{}]", localHostName, allowedHosts);
        return allowedHosts.contains(localHostName) || allowedHosts.get(0).equalsIgnoreCase("*");
    }
}
