package com.db.nfr.tmr.services.excel;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by gasiork on 05/07/2016.
 */
public abstract class CustomResultSetRowMapper implements RowMapper {
    private final ResultSet rs;

    public CustomResultSetRowMapper(ResultSet rs) {
        this.rs = rs;
    }

    public boolean onNext() {
        return true;
    }

    @Override
    public boolean hasNextRow() {
        try {
            boolean hasNext = rs.next();
            if (hasNext) {
                return onNext();
            }
            return hasNext;
        } catch (SQLException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public abstract String getCellValue(int columnNumber);

    protected ResultSet getResultSet() {
        return rs;
    }
}
