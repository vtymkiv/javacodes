package com.db.nfr.tmr.services.common;


import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by tymkvit on 13/10/2016.
 */
@Component
public class LocalHost {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocalHost.class);
    private CharSequence port;

    /**
     * Get local host name from inet version
     *
     * @return
     */
    public String getLocalHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            LOGGER.error("Getting local host address failed with exception", ex);
        }
        return StringUtils.EMPTY;
    }
}

