[#ftl]
[#import "/spring.ftl" as spring /]
[#assign stat = statics['com.db.nfr.tmr.services.rest.GssImportController'] /]
[#assign mandateType = enums['com.db.nfr.tmr.services.common.MandateType'] /]

<script type="text/javascript">
    function setMandateType(type) {
        //Alternately, you can access the button by its Id
        document.forms["daisyChainingForm"].elements["mandateType"].value = type;
    }
</script>

<html>
<body>
[#if stat.isProcessing()]
    <p>GSS import is in progress, please wait.</p>
[#else]
    <form method="POST" action="/TMRServices-web/import/gss" enctype="multipart/form-data">
        <fieldset>
            <legend>GSS Disclosure files loading</legend>
            <p>Select file:<br>
                <input type="file" name="file"/>
            </p>
            <input type="submit"/>
        </fieldset>
    </form>
[/#if]
<!-- Daisy chaining -->
<form method="POST" action="/TMRServices-web/daisychaining" id="daisyChainingForm" name="daisyChainingForm">
    <input type="hidden" name="mandateType" id="mandateType">
    <fieldset>

        <legend>Daisy chaining:</legend>
        <p>URL: <input type="text" name="daisyChainingURL" value="${localHostUrl}">

            [#if daisyChainingStatusForDebt??]
        <p>Mandate type: ${daisyChainingStatusForDebt.getMandateType()}, Number of
            traders: ${daisyChainingStatusForDebt.getTradersAmount()}, Number of traders
            to process at one time:
            ${daisyChainingStatusForDebt.getTradersChunkSize()}
        <p><input type="submit" value="Run DEBT" id="submitDEBT"
                  onClick="setMandateType('${mandateType.DEBT.getCode()}')"/></p>

        <p>Mandate type: ${daisyChainingStatusForEquity.getMandateType()}, Number of
            traders: ${daisyChainingStatusForEquity.getTradersAmount()}, Number of
            traders to process at one time: ${daisyChainingStatusForEquity.getTradersChunkSize()}
        <p><input type="submit" value="Run EQT" id="submitEQT"
                  onClick="setMandateType('${mandateType.EQUITY.getCode()}')"/></p>
        [#else]
            Error: Could not get info about how many traders exist from ${localHostUrl!"undefined"}. Please check, also see log files for details.
        [/#if]
    </fieldset>

    <fieldset>
        Call status: [#if dcStatus?has_content]${dcStatus}[#else]Daisy chaining has not been called.[/#if]
    </fieldset>
</form>
</body>
</html>