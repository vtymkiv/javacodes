package com.citi.alr.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class AlrConfig {
    @JsonProperty("pathToFile")
    private String pathToFile;

    @JsonProperty("root")
    private String root;
}
