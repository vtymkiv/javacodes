package com.citi.alr.service;

import com.citi.alr.config.AlrConfig;
import com.citi.alr.model.CashSnu;
import com.citi.alr.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.*;

@Slf4j
public class AlrServiceImpl implements AlrService {

    public void run() {
        System.out.println("Running alr service");
    }

    public static void main(String[] args) {
//        readYaml();
        runSqlSpark();
//        runRDDSpark();

    }

    public static void readYaml() {
        String pathToConfig = System.getProperty("alr.config.location", "./src/main/resources/application.yaml");
        ConfigurationLoaderService configurationLoaderService = new ConfigurationLoaderServiceImpl();
        AlrConfig config = configurationLoaderService.loadConfiguration(new File(
                pathToConfig), AlrConfig.class);
    }

    public static void runSqlSpark() {
        SparkSession session = SparkSession.builder()
                .master("local[*]")
                .appName("Spark CSV Reader")
                .getOrCreate();

       /* Dataset<Row> csv = session
                .read()
                .format("csv")
                .option("header", true)
                .load("src/main/resources/cash_snu_inventory.csv");

        csv.show();*/

        //Convert data from file into map or list of objects
        SQLContext sqlContext = new SQLContext(session);
        Dataset<Row> cashSnuDFRaw = sqlContext
                .read()
                .format("csv")
                .option("header", true)
                .load("src/main/resources/cash_snu_inventory.csv");


        //Build data set with renamed columns
        Dataset<Row> cashSnuDF = cashSnuDFRaw.withColumnRenamed("bus_date", "busDate")
                .withColumnRenamed("day_number", "dayNumber");

       /* Dataset<Integer> years = file8Data.map(( MapFunction<Row, Integer>) row -> row.<Integer>getAs("YEAR"), Encoders.INT());
        Dataset<Integer> newYears = years.flatMap((FlatMapFunction<Integer, Integer>) year -> {
            return Arrays.asList(year + 1, year + 2).iterator();
        }, Encoders.INT());*/

        //Encoder<CashSnu> cashSnuEncoder = Encoders.bean(CashSnu.class);

        cashSnuDFRaw = castDatasetRow(cashSnuDFRaw);
        List<CashSnu> cashSnuList = cashSnuDFRaw.map((MapFunction<Row, CashSnu>) CashSnu::rowMapper, Encoders.bean(CashSnu.class))
                .collectAsList();

        Map<String, List<CashSnu>> cashSnuMap = cashSnuList.stream().collect(Collectors.groupingBy(CashSnu::getAssetClassSegment));

        // Write list of results to file
        List<Result> resultList = new ArrayList<>();
        resultList.add(new Result("2020-03-04", "CBNA-NY", "France", "CusipF1", 1));
        resultList.add(new Result("2020-03-04", "CBNA-NY", "France", "CusipF2", 2));
        resultList.add(new Result("2020-03-04", "CBNA-NY", "Germany", "CusipG1", 1));
        resultList.add(new Result("2020-03-04", "CBNA-NY", "Germany", "CusipG2", 2));
        resultList.add(new Result("2020-03-04", "CBNA-NY", null, "CusipG2", 2));

        Dataset<Row> dataFrame = session.createDataFrame(resultList, Result.class);
        Dataset<Row> assetClassSegment = dataFrame.repartition(col("assetClassSegment"));

        /*dataFrame.createOrReplaceTempView("tempViewTest");
        Dataset<Row> sqlDF = sqlContext.sql("SELECT 'CBNA-NY-TEST' as columnToUpdate, assetClassSegment, busDate, cusip, dayNumber FROM tempViewTest WHERE assetClassSegment='France'");
        sqlDF.show();
        writeToOrcFile(sqlDF, "src/main/resources/alr_report/orc/sqlDf");*/


        // Expected input is:
        // --columnToUpdate= --condition= --columNewValue=
        String columnToUpdate = "legalEntity";
        Column condition = expr( "assetClassSegment is null");
        Object columnOldValue = col(columnToUpdate);
        Object columnNewValue = lit("CBNA-NY-TEST");

        Dataset<Row> dfWithUpdate = dataFrame.withColumn(columnToUpdate
                , when(condition, columnNewValue).otherwise(columnOldValue)
        );

        dfWithUpdate.show();
        writeToOrcFile(dfWithUpdate, "src/main/resources/alr_report/orc/dataFrame");
        writeToFile(assetClassSegment, "src/main/resources/alr_report_asset_class");

        session.close();
    }


    private static Column getConditionColumn(String columnName, String operator, String value) {
        Column colCondition = col(columnName);
        switch (operator) {
            case "==":
                colCondition = col(columnName).equalTo(value);
            break;
            case "<":
                colCondition = col(columnName).lt(value);
                break;
            case ">":
                colCondition = col(columnName).gt(value);
                break;

            default:
                throw new RuntimeException("The " + operator + " in expression " + columnName + operator + colCondition + " is not supported.");
        }
        return colCondition;
    }


    private static Dataset<Row> castDatasetRow(Dataset<Row> rowDataset) {
        return rowDataset
                .withColumn("day_number", col("day_number").cast(DataTypes.IntegerType))
                .toDF();
    }

    private static void writeToFile(Dataset<Row> dataFrame) {
        writeToFile(dataFrame, "src/main/resources/alr_report");
    }

    private static void writeToFile(Dataset<Row> dataFrame, String pathToFile) {
        dataFrame
                .write()
                .format("csv")
                .mode(SaveMode.Overwrite)
                .option("header", true)
                .save(pathToFile);
    }

    private static void writeToOrcFile(Dataset<Row> dataFrame, String pathToFile) {
        dataFrame
                .coalesce(1)
                //.repartition(1)
                .write()
                //.format("orc")
                .mode(SaveMode.Overwrite)
                .option("header", true)
                .orc(pathToFile);
                //.save(pathToFile)
        ;
    }


    private static void writeToZipFile(Dataset<Row> dataFrame, String pathToFile) {
        dataFrame
                .write()
                .format("csv")
                .mode(SaveMode.Overwrite)
                .option("header", true)
                .save(pathToFile);
    }

    public static void runRDDSpark() {
        SparkConf sparkConf = new SparkConf().setMaster("local[*]").setAppName("alr calculation");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        JavaRDD<String> stringJavaRDD = sc.textFile("src/main/resources/cash_snu_inventory.csv");
        stringJavaRDD.foreach(x -> log.info(x));

        // Write list of results to file
        sc.close();
    }


}

