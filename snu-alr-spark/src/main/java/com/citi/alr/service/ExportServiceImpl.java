package com.citi.alr.service;

import lombok.AllArgsConstructor;
import lombok.val;
import org.apache.spark.sql.SparkSession;

@AllArgsConstructor
public class ExportServiceImpl implements ExportService {
    private SparkSession sparkSession;

    @Override
    public <S, D> void exportTo(S s, D d) {

        val jdbcDF = sparkSession
                .sqlContext()
                .read()
                .format("jdbc")
                .option("driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver")
                .option("url", "jdbc:sqlserver://XXXXX.com:port;databaseName=xxx")
                .option("dbtable", "(SELECT * FROM xxxx) tmp")
                .option("user", "xxx")
                .option("password", "xxx")
                .load();

    }
}
