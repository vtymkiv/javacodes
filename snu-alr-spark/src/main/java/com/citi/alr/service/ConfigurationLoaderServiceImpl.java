package com.citi.alr.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.apache.commons.text.StringSubstitutor;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.util.Map;

public class ConfigurationLoaderServiceImpl implements ConfigurationLoaderService {
    private final ObjectMapper objectMapper;
    private final StringSubstitutor stringSubstitutor;

    public ConfigurationLoaderServiceImpl() {
        this.objectMapper = new ObjectMapper(new YAMLFactory());
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.stringSubstitutor = new StringSubstitutor();
        this.stringSubstitutor.setEnableSubstitutionInVariables(true);
    }

    @Override
    public <T> T loadConfiguration(File config, Class<T> cls) {
        try {
            Map<String, Object> map = this.objectMapper.readValue(config, new TypeReference<Map<String, Object>>() {
            });
            String contents =
                    StringSubstitutor.replace(new String(Files.readAllBytes(config.toPath())), map,
                            "{{",
                            "}}"
                    );


            return this.objectMapper.readValue(contents, cls);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }


}
