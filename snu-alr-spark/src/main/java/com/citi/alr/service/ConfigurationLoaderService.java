package com.citi.alr.service;

import java.io.File;

public interface ConfigurationLoaderService {
    <T> T loadConfiguration(File config, Class<T> cls);
}
