package com.citi.alr.service;

public interface ExportService {
    <S, D> void exportTo(S s, D d);
}
