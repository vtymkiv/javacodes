package com.citi.alr.service;

import com.citi.alr.model.Result;

import java.util.List;

public interface CalculationService {
    List<Result> calculate();
}
