package com.citi.alr.model;

import lombok.Value;

@Value
public class Result {
    final private String busDate;
    final private String legalEntity;
    final private String assetClassSegment;
    final private String cusip;
    final private int dayNumber;
}
