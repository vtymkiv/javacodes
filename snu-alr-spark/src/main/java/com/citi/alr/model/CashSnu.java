package com.citi.alr.model;

import lombok.*;
import org.apache.spark.sql.Row;

import java.io.Serializable;
import java.sql.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode
public class CashSnu implements Serializable {
    private Date busDate;
    private String assetClassSegment;
    private int dayNumber;
    private String cusip;

    public static CashSnu rowMapper(Row row) {
        return new CashSnu(
                getDateFromString(row.getString(row.fieldIndex("bus_date"))),
                row.getString(row.fieldIndex("assetClassSegment")),
                row.getInt(row.fieldIndex("day_number")),
                row.getString(row.fieldIndex("cusip"))
        );
    }

    private  static Date getDateFromString(String busDate) {
        return java.sql.Date.valueOf(busDate);
    }
}
