package test;

public class LoadingExample_1 {
    public LoadingExample_1() {
        System.out.println("Default constructor");
    }

    public LoadingExample_1(int num) {
        System.out.println("One argument constructor with argument " + num);
    }

    static {
        System.out.println("1st static init");
    }

    {
        System.out.println("1st instance init");
    }

    {
        System.out.println("2nd instance init");
    }

    static {
        System.out.println("2nd static init");
    }

    public static void main(String[] args) {
        new LoadingExample_1();
        new LoadingExample_1(8);
    }
}
