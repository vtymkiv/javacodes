package test;

import java.io.Serializable;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReducingExample_1 {
    public static void main(String[] args) {
        Stream<Integer> nums = Stream.of(5, 10, 20, 50);
        Function<Integer, String> multiplyBy2ToString = i -> String.valueOf(i * 2);
        String collect = nums.collect(Collectors.reducing("", multiplyBy2ToString, (x, y) -> x + " " + y));
        System.out.println(collect);
    }
}
