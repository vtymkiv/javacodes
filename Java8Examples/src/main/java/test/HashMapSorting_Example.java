package test;

import java.util.HashMap;
import java.util.TreeMap;

public class HashMapSorting_Example {
    public static void main(String[] args) {
        // let's create a map with Java releases and their code names
        HashMap<String, String> codeNameList = new HashMap<String, String>();

        codeNameList.put("JDK 1.1.4", "Sparkler");
        codeNameList.put("J2SE 1.2", "Playground");
        codeNameList.put("J2SE 1.3", "Kestrel");
        codeNameList.put("J2SE 1.4", "Merlin");
        codeNameList.put("J2SE 5.0", "Tiger");
        codeNameList.put("Java SE 6", "Mustang");
        codeNameList.put("Java SE 7", "Dolphin");

        System.out.println("HashMap before sorting, random order ");
        codeNameList.forEach((key, value) -> System.out.println(key + " ==> " + value));

        System.out.println("\nHashMap after sorting by keys in ascending order ");
        sortByKey(codeNameList);

    }

    private static void sortByKey(HashMap<String, String> mapToSort) {
        TreeMap<String, String> sortedByKey = new TreeMap<>(mapToSort);
        sortedByKey.forEach((key, value) -> System.out.println(key + " ==> " + value));
    }

    private static void sortByValue(HashMap<String, String> mapToSort) {

    }
}
