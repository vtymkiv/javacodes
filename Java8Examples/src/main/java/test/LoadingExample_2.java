package test;

public class LoadingExample_2 {
    static {
        initialize();
    }

    private static int sum;

    public static int getSum() {
        initialize();
        return sum;
    }

    private static boolean initialized = false;

    private static void initialize() {
        if (!initialized) {
            for (int i = 0; i < 4; i++) {
                sum += i;
            }
            initialized = true;
        }
    }

    public static void main(String[] args) {
        System.out.println(LoadingExample_2.getSum());
    }
}
