package test;

import java.util.HashMap;

public class HashMapExample_1 {
    public static void main(String[] args) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(null, "Value");
        // What we would have here? Exception, null, ...?
        System.out.println("Value for null key: " + hashMap.get(null));
    }
}
