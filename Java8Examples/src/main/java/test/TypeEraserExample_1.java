package test;

public class TypeEraserExample_1<T> {
    private T[] content;

    public TypeEraserExample_1(int capacity) {
        this.content = (T[]) new Object[capacity];
    }

    public void putToContent(T data) {
        // ..
    }

    public T getFromContent() {
        // ..
        return null;
    }


}
