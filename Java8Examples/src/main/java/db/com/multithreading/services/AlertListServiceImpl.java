package db.com.multithreading.services;

import java.util.Arrays;
import java.util.List;

/**
 * Created by vtymkiv on 14.09.2016.
 */
public class AlertListServiceImpl implements AlertListService {
    public List<String> getAlertNameList() {
        return Arrays.asList("Alert 1", "Alert 2");
    }
}
