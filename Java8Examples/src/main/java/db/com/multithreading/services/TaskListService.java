package db.com.multithreading.services;

import java.util.List;

/**
 * Created by vtymkiv on 14.09.2016.
 */
public interface TaskListService {
    List<String> getTaskNameList();

}
