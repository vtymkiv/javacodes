package db.com.multithreading.services;

import java.util.Arrays;
import java.util.List;

/**
 * Created by vtymkiv on 14.09.2016.
 */
public class TaskListServiceImpl implements TaskListService {

    public List<String> getTaskNameList() {
        return Arrays.asList("Task 1", "Task 2");
    }
}
