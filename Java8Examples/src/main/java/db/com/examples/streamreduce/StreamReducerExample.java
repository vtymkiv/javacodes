package db.com.examples.streamreduce;

import java.util.Arrays;
import java.util.List;

public class StreamReducerExample {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
        int result = numbers.stream()
                .reduce(0, (subtotal, element) -> subtotal + element);
        System.out.println(result);
        joinStrings();
    }

    private static void joinStrings() {
        List<String> stringList = Arrays.asList("", "", "a", "", "", "b", "c", "d");
        String result = stringList.stream().reduce("", (prevStr, currentStr) ->
                {
                    return prevStr.equals("") ? currentStr : prevStr + "," + currentStr;
                }

        );
        System.out.println(result);
    }
}
