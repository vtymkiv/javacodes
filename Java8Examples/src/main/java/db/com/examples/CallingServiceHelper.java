package db.com.examples;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by vtymkiv on 14.09.2016.
 */
public class CallingServiceHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(CallingServiceHelper.class);

    public void callRemoteService() {
        String wsResult = null;

        int currentAttempt = 0;
        int numOfAttempts = 5;

        while (wsResult == null && currentAttempt < numOfAttempts) {
            currentAttempt++;
            try {
                // Call to ws
                if (currentAttempt == 3) {
                    wsResult = "Call to ws";
                }

                LOGGER.info("{}", wsResult);

                Thread.sleep(10);
            } catch (Exception ex) {

                LOGGER.error("{}", ex);

            }
        }

        if (wsResult == null) {
            // Log info: Calling WS service num of times without success
            LOGGER.debug("Failed to call {}", wsResult);
            return;
        }

        // Successfully call service

        LOGGER.info("Success {}", wsResult);
    }
}
