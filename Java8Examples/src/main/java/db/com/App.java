package db.com;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.TreeMap;

import lombok.extern.slf4j.Slf4j;

/**
 * Examples of Java8
 */
@Slf4j
public class App {
    public static void main(String[] args) {
//        testTreeMap();
        //testDateTime();
        // CallingServiceHelper callingServiceHelper = new CallingServiceHelper();
        // callingServiceHelper.callRemoteService();
    }

//    private static void testTreeMap() {
//        TreeMap<String, String> treeMap = new TreeMap<>((o1, o2) -> 0);
//
//        treeMap.put("Vasya", "Vasya");
//        treeMap.put("Petya", "Petya");
//    }
//
//    private static void testDateTime() {
//        LocalDate localDate = LocalDate.now();
//        LOGGER.info("local date: {}", localDate);
//
//        LocalDate localDateByDefaultTZ = LocalDate.now(Clock.systemDefaultZone());
//        LOGGER.info("localDateByDefaultTZ: {}", localDateByDefaultTZ);
//
//    }
//
//    private static void findWhenMeetingStart(){
//        long hours = 6;
//        long minutes = 30;
//        LocalTime localTime = LocalTime.now();
//        LOGGER.info("Current time is: {}, My meeting start at: {}", localTime, localTime.plusHours(hours).plusMinutes
//            (minutes));
//    }

}
