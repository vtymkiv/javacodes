package db.com.zipfile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import lombok.extern.slf4j.Slf4j;

/**
 * Create zip for given file.
 * <p>
 * <p>
 * Created by vtymkiv on 19.05.2017.
 */
@Slf4j
public class ZipFileServiceImpl {
    private static final int CHUNK = 1024; // to help copy chunks of 1KB

    public void createZip(String fileName, String zipFileName) {
        // buffer is the temporary byte buffer used for copying data
        // from one stream to another stream
        byte[] buffer = new byte[CHUNK];
        // these stream constructors can throw FileNotFoundException
        try (ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(zipFileName));
             FileInputStream fileIn = new FileInputStream(fileName)) {
            zipFile.putNextEntry(new ZipEntry(fileName)); // putNextEntry can throw IOException
            int lenRead; // the variable to keep track of number of bytes sucessfully read
            // copy the contents of the input file into the zip file
            while ((lenRead = fileIn.read(buffer)) > 0) { // read can throw IOException
                zipFile.write(buffer, 0, lenRead); // write can throw IOException
            }
            // the streams will be closed automatically because they are within try-with-
            // resources statement
        } catch (FileNotFoundException e) {
            LOGGER.error("Not found file.", e);
        } catch (IOException e) {
            LOGGER.error(e.toString());
        }
    }
}
