package java8;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import lombok.extern.slf4j.Slf4j;

/**
 * Unit test for simple App.
 */
@Slf4j
public class AppTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    public void testLocalTime() {
        long hours = 6;
        long minutes = 30;
        LocalTime localTime = LocalTime.now();
        LOGGER.info("Current time is: {}, My meeting start at: {}", localTime, localTime.plusHours(hours).plusMinutes
                (minutes));
    }

    public void testLocalTimeWithTimeZone() {

        LocalTime localTimeWithDefaultTimeZone = LocalTime.now(Clock.systemDefaultZone());
        LocalTime localTimeWithTokioTimeZone = LocalTime.now(ZoneId.of("Asia/Tokyo"));
        LOGGER.info("Current time with default time zone: {}, Current time with Asia/Tokyo time zone: {}, date: {} ",
                localTimeWithDefaultTimeZone, localTimeWithTokioTimeZone, LocalDate.now(ZoneId.of("Asia/Tokyo")));
    }

    public void testLocalDateTime() {
        LocalDateTime localDateTime = LocalDateTime.now();
        LOGGER.info("Current date and time with default time zone: {}", localDateTime);
    }

    public void testLocalDateTimeIsAfter() {
        LocalDateTime christmas = LocalDateTime.of(2015, 12, 25, 0, 0);
        LocalDateTime newYear = LocalDateTime.of(2016, 1, 1, 0, 0);
        LOGGER.info("New Year 2016 comes after Christmas 2015? {}", newYear.isAfter(christmas));
    }

    public void testPeriod() {
        LocalDate christmas = LocalDate.of(2017, Month.DECEMBER, 25);
        LocalDate today = LocalDate.now();
        Period period = Period.between(today, christmas);
        LOGGER.info("For christmas is left {} months, {} days", period.getMonths(), period.getDays());
    }
}
