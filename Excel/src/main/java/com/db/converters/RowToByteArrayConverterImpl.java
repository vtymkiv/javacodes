package com.db.converters;

import java.io.DataOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.db.config.PoiAutoconfigure;

/**
 * Created by vtymkiv on 21.09.2016.
 */
public class RowToByteArrayConverterImpl extends RowToByteArrayConverter {
    Logger LOGGER = LoggerFactory.getLogger(PoiAutoconfigure.class);

    @Override
    protected void parseResultSet(DataOutputStream dataOutputStream, ResultSet rs) {
        LOGGER.info("Parsing result set...");
        try {
            if (rs == null) {
                LOGGER.info("Result set is null");
                return;
            }

            while (rs.next()) {
                String col1 = rs.getString(0);
                String col2 = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
