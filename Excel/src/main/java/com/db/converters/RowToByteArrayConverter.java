package com.db.converters;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.sql.ResultSet;

/**
 * Created by vtymkiv on 21.09.2016.
 */
public abstract class RowToByteArrayConverter {
    protected DataOutputStream dataOutputStream;
    protected ByteArrayOutputStream byteArrayOutputStream;

    public byte[] rowToByteArray(ResultSet resultSet) {
        parseResultSet(dataOutputStream, resultSet);
        return byteArrayOutputStream.toByteArray();
    }

    public RowToByteArrayConverter() {
        dataOutputStream = new DataOutputStream(byteArrayOutputStream);
    }


    protected abstract void parseResultSet(DataOutputStream dataOutputStream, ResultSet rs);
}
