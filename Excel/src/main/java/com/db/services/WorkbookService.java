package com.db.services;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.db.workbook.Workbook;
import com.db.workbook.exceptions.WorkbookException;

/**
 * Created by vtymkiv on 21.09.2016.
 */
@Service
public interface WorkbookService {
    XSSFWorkbook createWorkbook(Workbook workbook) throws WorkbookException;
}
