package com.db.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by vtymkiv on 21.09.2016.
 */
@ConfigurationProperties("poi.yml")
@Component
public class PoiProperties {
    public static final int DEFAULT_FLUSH_SIZE = 20;

    private Integer flushSize = DEFAULT_FLUSH_SIZE;

    public Integer getFlushSize() {
        return flushSize;
    }

    public void setFlushSize(Integer flushSize) {
        this.flushSize = flushSize;
    }
}
