package com.db.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.db.workbook.ExcelWriter;

/**
 * Created by vtymkiv on 21.09.2016.
 */
@Configuration
@EnableConfigurationProperties(PoiProperties.class)
public class PoiAutoconfigure {
    Logger LOGGER = LoggerFactory.getLogger(PoiAutoconfigure.class);

    @Autowired
    PoiProperties poiProperties;

    @Bean
    public ExcelWriter excelWriter() {
        LOGGER.info("Trying to init {} by auto-configuration.", ExcelWriter.class.getSimpleName());
        return new ExcelWriter(poiProperties.getFlushSize());
    }

}
