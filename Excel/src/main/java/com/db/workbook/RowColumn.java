package com.db.workbook;

/**
 * Created by vtymkiv on 21.09.2016.
 */
public class RowColumn {
    private String columnName;
    private int index = 0;
    private String value;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof RowColumn))
            return false;

        RowColumn rowColumn = (RowColumn) o;

        if (getIndex() != rowColumn.getIndex())
            return false;
        if (!getColumnName().equals(rowColumn.getColumnName()))
            return false;
        return getValue() != null ? getValue().equals(rowColumn.getValue()) : rowColumn.getValue() ==
                null;

    }

    @Override
    public int hashCode() {
        int result = getColumnName().hashCode();
        result = 31 * result + getIndex();
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
        return result;
    }
}
