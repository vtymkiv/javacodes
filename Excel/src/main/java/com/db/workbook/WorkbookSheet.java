package com.db.workbook;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vtymkiv on 21.09.2016.
 */
public class WorkbookSheet {
    private String name = "COI Register";

    private List<SheetRow> rowList = new ArrayList<>();
    private List<RowColumn> headers = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SheetRow> getRowList() {
        return rowList;
    }

    public List<RowColumn> getHeaders() {
        return headers;
    }

    public void setHeaders(List<RowColumn> headers) {
        this.headers = headers;
    }
}
