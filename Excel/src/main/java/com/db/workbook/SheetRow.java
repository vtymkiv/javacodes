package com.db.workbook;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vtymkiv on 21.09.2016.
 */
public class SheetRow {
    List<RowColumn> rowColumnList = new ArrayList<>();

    public List<RowColumn> getRowColumnList() {
        return rowColumnList;
    }

    public void setRowColumnList(List<RowColumn> rowColumnList) {
        this.rowColumnList = rowColumnList;
    }

    public RowColumn getRowColumn(String columnName) {
        for (RowColumn rowColumn : rowColumnList) {
            if (rowColumn.getColumnName().equals(columnName)) {
                return rowColumn;
            }
        }
        return null;
    }

    public RowColumn getRowColumn(int columnIndex) {
        for (RowColumn rowColumn : rowColumnList) {
            if (rowColumn.getIndex() == columnIndex) {
                return rowColumn;
            }
        }
        return null;
    }

    public void addColumn(RowColumn rowColumn) {
        if (!rowColumnList.contains(rowColumn)) {
            rowColumnList.add(rowColumn);
        }
    }
}
