package com.db.workbook;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the excel workbook structure like:
 * Workbook --
 * | Sheet 1
 * |        | Row1
 * |              | Col1 | Col2
 * |        |
 * |        | Row2
 * |        |    | Col1 | Col2
 * |
 * | Sheet 2
 * | Row1
 * | Row2
 * <p>
 * <p>
 * Created by vtymkiv on 21.09.2016.
 */
public class Workbook {
    private String name;
    private List<WorkbookSheet> sheetList = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<WorkbookSheet> getSheetList() {
        return sheetList;
    }

}
