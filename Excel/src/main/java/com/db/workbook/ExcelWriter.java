package com.db.workbook;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 * Created by vtymkiv on 21.09.2016.
 */
@Component
public class ExcelWriter {
    private static final int ROW_HEADER_IDX = 0;
    private Logger LOGGER = LoggerFactory.getLogger(ExcelWriter.class);


    /**
     * Rows in memory, exceeding rows will be flushed to disk
     */
    private int flushSize;

    public ExcelWriter(int flushSize) {
        this.flushSize = flushSize;
    }


    /**
     * Could be interesting for spring batch
     *
     * @param inputResource  source of the data read by lines
     * @param outputResource generated .xlsx resource
     */
    public void process(Resource inputResource, Resource outputResource) {
        SXSSFWorkbook wb = new SXSSFWorkbook(flushSize);
        try {
            wb.setCompressTempFiles(true); // temp files will be gzipped
            final Sheet sh = wb.createSheet();

            InputStreamReader is = new InputStreamReader(inputResource.getInputStream());

            BufferedReader br = new BufferedReader(is);
            final CreationHelper createHelper = sh.getWorkbook().getCreationHelper();

            br.lines().forEachOrdered(string -> {
                        Row row = sh.createRow(sh.getPhysicalNumberOfRows());
                        createCells(sh, row);
                    }
            );

            FileOutputStream out = new FileOutputStream(outputResource.getFile());

            wb.write(out);
            IOUtils.closeQuietly(out);
        } catch (IOException e) {
            LOGGER.error("IOE", e);
        } finally {
            if (wb != null) {
                // dispose of temporary files backing this workbook on disk
                wb.dispose();
            }
        }
    }

    protected void createColumnHeaders(final Sheet sh, final List<RowColumn> columnHeaders) {
        // Assert that sh not null and column headers are not empty
        Row row = sh.createRow(ROW_HEADER_IDX);
        CellStyle columnHeaderStyle = createCellStyle(sh);
        setColumnHeaderStyle(columnHeaderStyle);
        for (int i = 0; i < columnHeaders.size(); i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(columnHeaders.get(i).getColumnName());
            cell.setCellStyle(columnHeaderStyle);
        }
        sh.createFreezePane(0, columnHeaders.size());
    }

    private CellStyle createCellStyle(Sheet sheet) {
        return sheet.getWorkbook().createCellStyle();
    }

    private void setColumnHeaderStyle(final CellStyle style) {
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.GREEN.getIndex());
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLUE.getIndex());
        style.setBorderTop(CellStyle.BORDER_MEDIUM_DASHED);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
    }

    public SXSSFWorkbook
    createWorkBookWithSheets(Workbook workbook) {
        SXSSFWorkbook wb = new SXSSFWorkbook(flushSize);
        wb.setCompressTempFiles(true); // temp files will be gzipped
        List<WorkbookSheet> sheetList = workbook.getSheetList();
        for (WorkbookSheet workbookSheet : sheetList) {
            String safeName = WorkbookUtil.createSafeSheetName(workbookSheet.getName());
            createColumnHeaders(wb.createSheet(safeName), workbookSheet.getHeaders());
        }
        return wb;
    }


    protected Cell createCell(Sheet sh) {
        return creteRow(sh).createCell(1);
    }

    protected List<Cell> createCells(Sheet sh, Row row) {
        List<Cell> cellList = new ArrayList<>();
        for (int i = 0; i < 42; i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue
                    ("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
            cellList.add(cell);

        }
        return cellList;
    }

    protected Row creteRow(Sheet sh) {
        return sh.createRow(sh.getPhysicalNumberOfRows());
    }

    public int getFlushSize() {
        return flushSize;
    }

    public void setFlushSize(int flushSize) {
        this.flushSize = flushSize;
    }

    public void processWorkbook(Workbook workbook, Resource destinationSource) {
        SXSSFWorkbook wb = createWorkBookWithSheets(workbook);
        try {
            FileOutputStream out = new FileOutputStream(destinationSource.getFile());
            wb.write(out);
            IOUtils.closeQuietly(out);
        } catch (IOException e) {
            LOGGER.error("IOE", e);
        } finally {
            if (wb != null) {
                // dispose of temporary files backing this workbook on disk
                wb.dispose();
            }
        }
    }
}

