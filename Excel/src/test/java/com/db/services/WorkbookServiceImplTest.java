package com.db.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.db.workbook.Workbook;
import com.db.workbook.RowColumn;
import com.db.workbook.SheetRow;
import com.db.workbook.WorkbookSheet;

/**
 * Created by vtymkiv on 21.09.2016.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WorkbookServiceImplTest {

    @Autowired
    private WorkbookService workbookService;

    private Workbook workbook;

    @Before
    public void setUp() throws Exception {
        workbook = new Workbook();
        workbook.setName("TestWorkbook");

        WorkbookSheet sheet = new WorkbookSheet();

        SheetRow rowForNames = new SheetRow();

        RowColumn workbookCol = new RowColumn();
        workbookCol.setColumnName("Col1");
        rowForNames.getRowColumnList().add(workbookCol);
        sheet.getRowList().add(rowForNames);

        workbook.getSheetList().add(sheet);

    }

    @Test
    public void createWorkbook() throws Exception {
        workbookService.createWorkbook(workbook);
    }

}