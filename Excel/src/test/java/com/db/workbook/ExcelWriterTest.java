package com.db.workbook;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

/**
 * Created by vtymkiv on 21.09.2016.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ExcelWriterTest {

    private static final int ROWS_TO_GENERATE = 5000;
    private File destination_file;
    private File sourceFile;

    @Autowired
    private ExcelWriter poiExcelWriter;

    @Before
    public void setUp() throws IOException {
        destination_file = File.createTempFile("test-poi-spring-boot", ".xlsx", new File("."));
        destination_file.createNewFile();
        //destination_file.deleteOnExit();

        sourceFile = File.createTempFile("test-poi-spring-boot-source", ".txt");
        sourceFile.createNewFile();

        FileOutputStream fos = new FileOutputStream(sourceFile);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        for (int i = 0; i < ROWS_TO_GENERATE; i++) {
            bw.write((UUID.randomUUID() + " rewtyewt").toString());
            bw.newLine();
        }
        bw.close();

    }

    @Test
    public void process() throws Exception {
        StringBuilder sb = new StringBuilder();
        //Resource inputSource =  new ByteArrayResource(sb.append("1|").append("2|").append("3")
    /*Resource inputSource =  new ByteArrayResource(sb.append("1|").append("2|").append("3")
        .toString().getBytes());*/
        Resource inputSource = new FileSystemResource(sourceFile);
        final Resource destinationSource = new FileSystemResource(destination_file);
        poiExcelWriter.process(inputSource, destinationSource);
        assertThat(destination_file.length(), greaterThan(Long.valueOf(0)));
    }


    @Test
    public void processWorkbook() throws Exception {
        final Resource destinationSource = new FileSystemResource(destination_file);
        poiExcelWriter.processWorkbook(createWorkbookTestFixture(), destinationSource);
        assertThat(destination_file.length(), greaterThan(Long.valueOf(0)));
    }

    private Workbook createWorkbookTestFixture() {
        Workbook workbook = new Workbook();
        workbook.setName("coi");

        WorkbookSheet sheet = new WorkbookSheet();
        sheet.setName("COI Register");

        List<RowColumn> headers = new ArrayList<>();
        for (int i = 0; i < 60; i++) {
            RowColumn header = new RowColumn();
            header.setColumnName("Column_" + i);
            headers.add(header);
        }
        sheet.getHeaders().addAll(headers);
        List<SheetRow> rows = new ArrayList<>();

        for (int i = 0; i < 6000; i++) {

            SheetRow row = new SheetRow();

            for (int j = 0; j < 60; j++) {
                RowColumn column = new RowColumn();
                column.setColumnName("Column_" + j);
                column.setIndex(j);
                column.setValue("Data of column " + j);
                row.addColumn(column);
            }
            rows.add(row);
        }

        sheet.getRowList().addAll(rows);
        workbook.getSheetList().add(sheet);
        return workbook;
    }
}