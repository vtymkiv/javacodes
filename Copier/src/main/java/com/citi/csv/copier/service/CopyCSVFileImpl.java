package com.citi.csv.copier.service;

import com.citi.csv.copier.service.config.CopyCSVFileConfigProperties;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Slf4j
public class CopyCSVFileImpl implements CopyCSVFile {
    public static final String COLUMN_LIST_TO_COPY_VALUES_HAVE_NOT_BEEN_SPECIFIED = "The values of property column-list-to-copy have not been specified.";
    private final CopyCSVFileConfigProperties copyCSVFileConfig;
    private static final String COMMA = ",";

    @Autowired
    public CopyCSVFileImpl(CopyCSVFileConfigProperties copyCSVFileConfig) {
        this.copyCSVFileConfig = copyCSVFileConfig;
    }

    @Override
    public void copyByColumnList(String sourceFile, String destinationFile, Map<Integer, Integer> columnLisToCopy) throws IOException {
        File outputFile = geOrCreateOutputFile(destinationFile);

        try (FileOutputStream outputStream = new FileOutputStream(outputFile);
             Stream<String> lines = Files.lines(Paths.get(sourceFile))
        ) {
            lines
                    .map(line -> line.split(COMMA))
                    .map(dataRow -> {
                        List<String> newDataRow = columnLisToCopy.keySet()
                                .stream()
                                .map(colIdx -> dataRow[colIdx]).collect(Collectors.toList());
                        return StringUtils.collectionToDelimitedString(newDataRow, COMMA)
                                .concat(System.lineSeparator())
                                .getBytes();

                    })
                    .forEach(
                            b -> {
                                try {
                                    outputStream.write(b);
                                } catch (IOException e) {
                                    log.error("Exception happened during the writing data to another file", e);
                                }
                            }
                    );
        }
    }

    private File geOrCreateOutputFile(String destinationFile) throws IOException {
        File outputFile = new File(destinationFile);
        if (outputFile.exists()) {
            return outputFile;
        } else if (outputFile.createNewFile()) {
            return outputFile;
        } else {
            throw new IOException(String.format("Unable to create  file %s", destinationFile));
        }
    }

    public void copyAllColumns(String source, String dest) throws IOException {
        File outputFile = geOrCreateOutputFile(dest);
        try (FileOutputStream outputStream = new FileOutputStream(outputFile);
             Stream<String> lines = Files.lines(Paths.get(source))
        ) {
            lines
                    .map(line -> line.split(COMMA))
                    .map(dataRow -> StringUtils.collectionToDelimitedString(Arrays.asList(dataRow), COMMA)
                            .concat(System.lineSeparator())
                            .getBytes())
                    .forEach(
                            b -> {
                                try {
                                    outputStream.write(b);
                                } catch (IOException e) {
                                    log.error("Exception happened during the writing data to another file", e);
                                }
                            }
                    );
        }
    }

    @Override
    public void copy() throws IOException {
        String source = this.copyCSVFileConfig.getPathToSourceFile();
        String dest = this.copyCSVFileConfig.getPathToDestinationFile();
        val columnListToCopy = this.copyCSVFileConfig.getColumnListToCopy();

        log.info("Copying from file [{}] into file [{}] columns: [{}]",
                source,
                dest,
                StringUtils.collectionToDelimitedString(columnListToCopy.keySet(), COMMA));

        if (columnListToCopy.isEmpty()) {
            log.error(COLUMN_LIST_TO_COPY_VALUES_HAVE_NOT_BEEN_SPECIFIED);
            throw new IllegalArgumentException(COLUMN_LIST_TO_COPY_VALUES_HAVE_NOT_BEEN_SPECIFIED);
        } else if (columnListToCopy.containsKey("all")) {
            this.copyAllColumns(source, dest);
        } else {
            this.copyByColumnList(source, dest, this.copyCSVFileConfig.getOrderedColumnList());
        }
    }
}
