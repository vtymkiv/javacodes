package com.citi.csv.copier.service.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.AbstractMap;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.math.NumberUtils.isCreatable;

@Configuration
@ConfigurationProperties(prefix = "copy")
@Data
public class CopyCSVFileConfigProperties {
    private String pathToSourceFile;
    private String pathToDestinationFile;
    private Map<String, String> columnListToCopy;

    public Map<Integer, Integer> getOrderedColumnList() {
        Predicate<Map.Entry<String, String>> isNumber = entry -> isCreatable(entry.getKey()) && isCreatable(entry.getValue());
        return columnListToCopy.entrySet()
                .stream()
                .filter(isNumber)
                .map(entry -> new AbstractMap.SimpleImmutableEntry<>(Integer.parseInt(entry.getKey()), Integer.parseInt(entry.getValue())))
                .sorted(Comparator.comparing(AbstractMap.SimpleImmutableEntry::getValue))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

}
