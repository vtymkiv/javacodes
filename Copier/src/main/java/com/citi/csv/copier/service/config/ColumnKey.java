package com.citi.csv.copier.service.config;

import lombok.Value;

@Value
public class ColumnKey {
    private int columnIdx;
    private String columnName;
}
