package com.citi.csv.copier.service;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

@Service
public interface CopyCSVFile {
    void copyByColumnList(String sourceFile, String destinationFile, Map<Integer, Integer> columnLisToCopy) throws IOException;

    void copyAllColumns(String sourceFile, String destinationFile) throws IOException;

    void copy() throws IOException;
}
