package com.citi.csv.copier;

import com.citi.csv.copier.service.CopyCSVFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class CopierApplication implements CommandLineRunner {

    private final CopyCSVFile copyCSVFile;

    public static void main(String[] args) {
        SpringApplication.run(CopierApplication.class, args);
    }

    @Autowired
    public CopierApplication(CopyCSVFile copyCSVFile) {
        this.copyCSVFile = copyCSVFile;
    }

    @Override
    public void run(String... args) throws Exception {
        copyCSVFile.copy();
    }
}
