package com.citi.csv.copier.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class CopyCSVFileImplTest {
    @Autowired
    private CopyCSVFile copyCSVFile;

    @Test
    void copyCsvFiles() throws IOException {
        Map<Integer, Integer> columnList = new HashMap<Integer, Integer>() {{
            put(5, 0);
            put(6, 1);
        }};
        copyCSVFile.copyByColumnList("src/test/resources/test1.csv", "src/test/resources/test2.csv", columnList);
    }

}