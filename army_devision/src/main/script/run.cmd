REM ********************************************************************
REM *** vtymkiv. All rights reserved.
REM ********************************************************************
REM ***  $file:   run.bat  $
REM ********************************************************************

@echo off
REM java -DpathToExcel=army_state_on_2022_05_24.xlsx -DpathToTemplateDoc=description_template.docx -DpatchToNewDoc=generated_from_template.docx -DexcelSheetName=батальйон -jar army_devision-1.0-SNAPSHOT.jar "315,488" "315,488" 2022-08-09 10
SET JAVA_PATH=java
SET HEALTHY_LIST=%1
SET DEPART_LIST=%2
SET ORDER_DATE=%3
SET ORDER_NUM=%4

SET JAVA_OPTS=" -DpathToExcel=army_state_on_2022_05_24.xlsx -DexcelSheetName=батальйон -DpathToTemplateDoc=description_template.docx -DpatchToNewDoc=generated_from_template.docx"
SET EXEC_JAR="army_devision-1.0-SNAPSHOT.jar"
%JAVA_PATH%  %JAVA_OPTS% -jar %EXEC_JAR% %HEALTHY_LIST% %DEPART_LIST% %ORDER_DATE% %ORDER_NUM%

echo Successfully executed

