package org.army.model;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class OutputRowImpl implements OutputRow {
    private String output;
}
