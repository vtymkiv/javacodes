package org.army.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.With;
import org.army.mapper.DataRow;

//@Data
@Builder
@ToString
@Getter
public class ExcelRow implements DataRow {
    private int orderNum;
    private String subDivision;
    private String order;
    private String position;
    private String positionDescription;
    private String lastName;
    private String firstName;
    private String middleName;
    // This is a date in format: dd.mm.yy
    private String arrived;
    @With
    private String armyPartDescription;

    public String getFullName() {
        return (getLastName() + " " + getFirstName() + " " + getMiddleName()).trim();
    }
}
