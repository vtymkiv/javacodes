package org.army.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

@Builder
@Getter
@ToString
public class DocumentContext {
    private Pair<String, List<ExcelRow>> healthList;
    private Pair<String, List<ExcelRow>> departList;
    private Pair<String, String> docDate;
    private Pair<String, String> orderNum;
}
