package org.army.model;

public interface OutputRow {
    String getOutput();
}
