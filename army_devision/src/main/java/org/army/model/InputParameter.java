package org.army.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Builder
@Getter
@ToString
public class InputParameter {
    //@NotNull
    private List<Integer> healthyOrderNumList;

    //@NotNull
    private  List<Integer> departOrderNumList;

    //@NotNull
    private  String docDate;

    private String orderNum;

    //@NotNull
    private String pathToTemplateDoc;

    //@NotNull
    private  String pathToExcel;

    //@NotNull
    private  String pathToNewDoc;

    private String  excelSheetName;

}
