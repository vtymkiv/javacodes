package org.army;

import org.army.model.InputParameter;
import org.army.service.ReportGenerator;
import org.army.service.ReportGeneratorImpl;
import org.army.service.docx.DocxServiceImpl;
import org.army.service.excel.ExcelServiceImpl;
import org.army.service.parameter.ParameterException;
import org.army.service.parameter.ParameterServiceImpl;

public class ArmyDevCLI {
    public static void main(String[] args) throws ParameterException {
        // Get the parameters and run with them
        // The parameters are:
        // 1. List of combatants that will go to improve health
        // 2. List of combatants that will depart
        // 3. Document/report date

        // Consider using params file for these data
        // 4. Template document
        // 5. Excel where data that would be used in report should be taken
        // 6. The path to the document that will be generated
        new ArmyDevCLI().run(args);
    }

    public void run(String[] args) {
        InputParameter inputParameter;
        try {
            inputParameter = new ParameterServiceImpl().loadInputParameter(args);
        } catch (ParameterException e) {
            throw new RuntimeException(e);
        }
        ReportGenerator reportGenerator = ReportGeneratorImpl.builder()
                .excelService(new ExcelServiceImpl(inputParameter.getPathToExcel()))
                .docxService(new DocxServiceImpl())
                .build();
        reportGenerator.generateReport(inputParameter);
    }
}