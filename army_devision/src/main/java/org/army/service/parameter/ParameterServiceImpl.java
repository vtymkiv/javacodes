package org.army.service.parameter;

import org.apache.commons.lang3.StringUtils;
import org.army.model.InputParameter;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

public class ParameterServiceImpl implements ParameterService<InputParameter> {
    private final DateTimeFormatter asOfDateDocParser;

    public ParameterServiceImpl() {
        this.asOfDateDocParser = DateTimeFormatter.ISO_LOCAL_DATE;
    }

    public InputParameter loadInputParameter(String[] args) throws ParameterException {
        return parseAndGetInputParameter(args);
    }

    private InputParameter parseAndGetInputParameter(String[] args) throws ParameterException {
        AtomicInteger argNum = new AtomicInteger();
        String errorMsg = "The parameter number %s for %s has not been specified";
        List<Integer> healthOrderNumList = Arrays.stream(ofNullable(args)
                        .filter(cliArgs -> cliArgs.length > argNum.get())
                        .map(cliArgs -> cliArgs[argNum.get()])
                        .orElseThrow(() -> new ParameterException( String.format(errorMsg, argNum.get(), "health list") ))
                        .split(","))
                .map(Integer::parseInt)
                .collect(Collectors.toList());


        List<Integer> departOrderNumList = Arrays.stream(ofNullable(args)
                        .filter(cliArgs -> cliArgs.length > argNum.incrementAndGet())
                        .map(cliArgs -> cliArgs[argNum.get()])
                        .orElseThrow(() -> new ParameterException( String.format(errorMsg, argNum.get(), "depart list") ))
                        .split(","))
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        String docDate = ofNullable(args)
                .filter((cliArgs -> cliArgs.length > argNum.incrementAndGet()))
                .map(cliArgs -> cliArgs[argNum.get()])
                .orElseThrow(() -> new ParameterException( String.format(errorMsg, argNum.get(), "doc date")));

        String orderNum = ofNullable(args)
                .filter((cliArgs -> cliArgs.length > argNum.incrementAndGet()))
                .map(cliArgs -> cliArgs[argNum.get()])
                .orElseThrow(() -> new ParameterException( String.format(errorMsg,argNum.get(), "order num")));

        String pathToExcel = System.getProperty("pathToExcel", "");
        String patchToNewDoc = System.getProperty("patchToNewDoc", "");
        String pathToTemplateDoc = System.getProperty("pathToTemplateDoc", "");
        String excelSheetName = System.getProperty("excelSheetName", "");

        validateParameters(pathToExcel, patchToNewDoc, pathToTemplateDoc);


        return InputParameter.builder()
                .healthyOrderNumList(healthOrderNumList)
                .departOrderNumList(departOrderNumList)
                .docDate(docDate)
                .orderNum(orderNum)
                .excelSheetName(excelSheetName)
                .pathToExcel(pathToExcel)
                .pathToNewDoc(patchToNewDoc)
                .pathToTemplateDoc(pathToTemplateDoc)
                .build();
    }

    private  void validateParameters(String pathToExcel, String patchToNewDoc, String pathToTemplateDoc) throws ParameterException {
        validateFile(pathToExcel);
        validateFile(patchToNewDoc);
        validateFile(pathToTemplateDoc);
    }

    private  void validateFile(String pathToExcel) throws ParameterException {
        if(StringUtils.isBlank(pathToExcel) || !Files.exists(Paths.get(pathToExcel))) {
            throw  new ParameterException(String.format("File %s does not exist oe not specified", pathToExcel));
        }
    }

    private String getUsageDescription() {
        return "Usage: ";
    }


}
