package org.army.service.parameter;

import org.army.model.InputParameter;

public interface ParameterService<T> {
    T loadInputParameter(String[] args) throws ParameterException;
}
