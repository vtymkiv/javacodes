package org.army.service.docx;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.army.model.DocumentContext;
import org.army.model.ExcelRow;
import org.army.service.converter.ConverterServiceImpl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.file.Files.newInputStream;

@Slf4j
@AllArgsConstructor
public class DocxServiceImpl implements DocxService {
    @Override
    public void generateDocFromTemplate(String pathToTemplateDoc, String pathToOutputDoc, DocumentContext updateContext) {
        try (InputStream is = newInputStream(Paths.get(pathToTemplateDoc));
             XWPFDocument doc = new XWPFDocument(is)) {

            List<XWPFParagraph> xwpfParagraphList = doc.getParagraphs();
            //Iterate over paragraph list and check for the replaceable text in each paragraph
            for (XWPFParagraph xwpfParagraph : xwpfParagraphList) {
                List<XWPFRun> xwpfParagraphRunList = xwpfParagraph.getRuns().stream()
                        .filter(xwpfRun -> StringUtils.isNotBlank(xwpfRun.text()))
                        .collect(Collectors.toList());

                for (XWPFRun xwpfRun : xwpfParagraphRunList) {
                    String docText = xwpfRun.getText(0);

                    //replacement and setting position
                    if(updateContext.getHealthList() != null && docText.contains(updateContext.getHealthList().getKey())) {
                        String style = xwpfRun.getStyle();
                        docText = docText.replace(updateContext.getHealthList().getKey(),
                                convertToStr(updateContext.getHealthList().getValue()));
                        xwpfRun.setText(docText, 0);
                        xwpfRun.setItalic(true);
                        xwpfRun.setStyle(style);
                    } else if(updateContext.getDepartList() != null && docText.contains(updateContext.getDepartList().getKey())) {
                        String style = xwpfRun.getStyle();
                        docText = docText.replace(updateContext.getDepartList().getKey(),
                                convertToStr(updateContext.getDepartList().getValue()));
                        xwpfRun.setText(docText, 0);
                        xwpfRun.setItalic(true);
                        xwpfRun.setStyle(style);
                    } else if(updateContext.getDocDate() != null && docText.contains(updateContext.getDocDate().getKey())) {
                        docText = docText.replace(updateContext.getDocDate().getKey(),
                                updateContext.getDocDate().getValue());
                        xwpfRun.setText(docText, 0);
                    }  else if(updateContext.getOrderNum() != null && docText.contains(updateContext.getOrderNum().getKey())) {
                        docText = docText.replace(updateContext.getOrderNum().getKey(),
                                updateContext.getOrderNum().getValue());
                        xwpfRun.setText(docText, 0);
                    }
                }
            }

            // save the docs
            try (FileOutputStream out = new FileOutputStream(pathToOutputDoc)) {
                doc.write(out);
            }

        } catch (IOException e) {
            log.error("Exception happened when updating g document", e);
            throw new RuntimeException(e);
        }
    }

    private  String convertToStr(final List<ExcelRow> excelRowList) {
        // Convert to healthy list
        ConverterServiceImpl converterServiceToHealth = new ConverterServiceImpl();
        return converterServiceToHealth.convertToString(excelRowList);
    }
}
