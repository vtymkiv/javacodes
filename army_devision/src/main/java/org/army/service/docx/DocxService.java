package org.army.service.docx;

import org.army.model.DocumentContext;

public interface DocxService {
    void generateDocFromTemplate(String pathToTemplateDoc, String pathToOutputDoc, DocumentContext updateContext);
}