package org.army.service.converter;

import org.army.model.ExcelRow;
import org.army.model.OutputRow;

import java.util.List;
import java.util.function.Function;

public interface ConverterService<T> {
    List<OutputRow> convertTo(List<ExcelRow> objectList, Function<ExcelRow, OutputRow> outputRowFunction);
    String convertToString(List<ExcelRow> excelRowList);
}
