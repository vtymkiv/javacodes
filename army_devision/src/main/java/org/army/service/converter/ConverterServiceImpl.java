package org.army.service.converter;

import org.army.model.ExcelRow;
import org.army.model.OutputRow;
import org.army.model.OutputRowImpl;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ConverterServiceImpl implements ConverterService<ExcelRow> {
    @Override
    public List<OutputRow> convertTo(List<ExcelRow> objectList, Function<ExcelRow, OutputRow> outputRowFunction) {
        return objectList.stream()
                .map(outputRowFunction)
                .collect(Collectors.toList());
    }

    public String convertToString(List<ExcelRow> excelRowList) {
        List<OutputRow> outputHealthyRowList = convertTo(
                excelRowList
                , excelRow ->
                        OutputRowImpl.builder()
                                .output((
                                        excelRow.getPosition()
                                        + ". " + excelRow.getFullName()
                                        + " " + excelRow.getPositionDescription()
                                        + " " + excelRow.getArmyPartDescription()
                                        )
                                )
                                .build()

        );

        return outputHealthyRowList.stream()
                .map(OutputRow::getOutput)
                .collect(Collectors.joining("\n"));
    }
}
