package org.army.service.excel;

import org.army.model.ExcelRow;

import java.util.List;

public interface ExcelService<T> {
    List<T> readExcelSheet(String pathToExcelFile);
}
