package org.army.service.excel;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.army.mapper.RowMapperImpl;
import org.army.mapper.RowSetterImpl;
import org.army.model.ExcelRow;
import org.army.service.excel.ExcelService;

import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@AllArgsConstructor
public class ExcelServiceImpl implements ExcelService<ExcelRow> {

    private final String pathToExcelFile;

    @Override
    public List<ExcelRow> readExcelSheet(final String excelSheetName) {
        final List<ExcelRow> excelRowList = new ArrayList<>();
        try (FileInputStream excelFile = new FileInputStream(pathToExcelFile)) {
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = workbook.getSheet(excelSheetName);

            //Row row = sheet.getRow(ROW_SHIFT);
            //TODO: Read in parallel
            excelRowList.addAll( new RowMapperImpl<ExcelRow>().mapRowListToBeanList(sheet, new RowSetterImpl()));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        return excelRowList;
    }
}
