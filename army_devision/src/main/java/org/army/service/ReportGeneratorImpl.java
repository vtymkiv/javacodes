package org.army.service;

import lombok.Builder;
import lombok.var;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;
import org.army.model.*;
import org.army.service.docx.DocxService;
import org.army.service.excel.ExcelService;
import org.army.service.parameter.ParameterService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Builder
public class ReportGeneratorImpl implements ReportGenerator {
    @Required
    private ExcelService<ExcelRow> excelService;
    @Required
    private DocxService docxService;

    @Override
    public Status generateReport(final InputParameter inputParameter) {
        //final InputParameter inputParameter = parameterService.getInputParameter();
        List<ExcelRow> excelRowList = excelService.readExcelSheet(inputParameter.getExcelSheetName());
        // Get list of  combatants for health
        List<Integer> includedOrderNum4Health = inputParameter.getHealthyOrderNumList();
        List<ExcelRow> healthList = excelRowList.stream()
                .filter(excelRow -> includedOrderNum4Health.contains(excelRow.getOrderNum()))
                .collect(Collectors.toList());

        // Get list of  combatants for depart
        List<Integer> includedOrderNum4Depart = inputParameter.getDepartOrderNumList();
        List<ExcelRow> departList = excelRowList.stream()
                .filter(excelRow -> includedOrderNum4Depart.contains(excelRow.getOrderNum()))
                .collect(Collectors.toList());
        // Get document date

        var updateContext = DocumentContext.builder()
                .healthList(new ImmutablePair<>("${healthy_list}", healthList))
                .departList(new ImmutablePair<>("${depart_list}", departList))
                .docDate(new ImmutablePair<>("${order_date}", inputParameter.getDocDate()))
                .orderNum(new ImmutablePair<>("${order_num}", inputParameter.getOrderNum()))
                .build();
        // Create update context
        docxService.generateDocFromTemplate(inputParameter.getPathToTemplateDoc(), inputParameter.getPathToNewDoc(), updateContext);

        return Status.SUCCESS;
    }
}
