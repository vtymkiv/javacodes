package org.army.service;

import org.army.model.InputParameter;
import org.army.model.Status;

public interface ReportGenerator {
    Status generateReport(InputParameter inputParameter);
}
