package org.army.mapper;

import org.apache.poi.ss.usermodel.Row;

public interface RowSetter<T extends DataRow> {
    T set(Row row);
}
