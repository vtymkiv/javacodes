package org.army.mapper;

import org.apache.poi.ss.usermodel.Sheet;

import java.util.List;

public interface RowMapper<T extends DataRow> {
    List<T> mapRowListToBeanList(Sheet sheet, RowSetter<T> rowSetter);
}
