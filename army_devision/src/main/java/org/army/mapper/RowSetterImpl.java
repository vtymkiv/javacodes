package org.army.mapper;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellAddress;
import org.army.model.ExcelRow;

@Slf4j
public class RowSetterImpl implements RowSetter<ExcelRow> {
    @Override
    public ExcelRow set(Row row) {
        log.info("\n===================== Row: [{}] ====================", row.getRowNum());
        for (int i = 0; i < row.getLastCellNum(); i++) {
            Cell cell = row.getCell(i);
            if(cell == null) continue;
            log.info("Cell num {}, Cell value: {}", i,
                    cell.getCellType() == CellType.STRING ? cell.getStringCellValue() : "Type of cell is " + cell.getCellType());

        }
        return ExcelRow.builder()
                .orderNum(StringUtils.isEmpty(getCellValue(row.getCell(0))) ? -1 : Double.valueOf(getCellValue(row.getCell(0))).intValue())
                .position(getCellValue(row.getCell(29)))
                .lastName(getCellValue(row.getCell(30)))
                .firstName(getCellValue(row.getCell(31)))
                .middleName(getCellValue(row.getCell(32)))
                .arrived(getCellValue(row.getCell(34)))
                .positionDescription(getCellValue(row.getCell(87)))
                .armyPartDescription(getArmyPartDescription(row))
                .build();
    }

    private String getArmyPartDescription(Row row) {
        // ВІЙСЬКОВОЇ ЧАСТИНИ А 4125
        Sheet sheet = row.getSheet();
        CellAddress cellAddress = new CellAddress("CN1");
        Row rowZero = sheet.getRow(cellAddress.getRow());
        Cell cell = rowZero.getCell(cellAddress.getColumn());

        String armyPartDescription = "";
        FormulaEvaluator evaluator = sheet.getWorkbook().getCreationHelper().createFormulaEvaluator();
        if (cell.getCellType() == CellType.FORMULA) {
            switch (evaluator.evaluateFormulaCell(cell)) {
                case BOOLEAN:
                    armyPartDescription = String.valueOf(cell.getBooleanCellValue());
                    break;
                case NUMERIC:
                    armyPartDescription = String.valueOf(cell.getNumericCellValue());
                    break;
                case STRING:
                    armyPartDescription = cell.getRichStringCellValue().getString();
                    break;
            }
            log.info("Cell with formula: {}", armyPartDescription);
        }
        return armyPartDescription.toLowerCase().replace(" a ", "A");
    }

    private String getCellValue(Cell cell) {
        if (cell == null) return "";
        String cellVal;
        switch (cell.getCellType()) {
            case NUMERIC:
                cellVal = String.valueOf(cell.getNumericCellValue());
                break;
            case STRING:
                cellVal = StringUtils.isBlank(cell.getStringCellValue()) ? "" : cell.getStringCellValue();
                break;
            case BOOLEAN:
                cellVal = String.valueOf(cell.getBooleanCellValue());
            default:
                cellVal = "";
                break;
        }
        return cellVal;
    }
}
