package org.army.mapper;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class RowMapperImpl<T extends DataRow> implements RowMapper<T> {
    private static final int ROW_SHIFT = 6;
    @Override
    public List<T> mapRowListToBeanList(Sheet sheet, RowSetter<T> rowSetter) {
        List<T> dataRowList = new ArrayList<>();


       //TODO: Read in parallel
        int lastRowNum = sheet.getLastRowNum();
        for(int rowNum = ROW_SHIFT; rowNum < lastRowNum; rowNum++) {
            Row row = sheet.getRow(rowNum);
            if(row.getCell(0) == null
                    || row.getCell(0).getCellType() == CellType._NONE
                    || row.getCell(0).getCellType() == CellType.BLANK) {
                log.debug("Skipped row: {}", row.getRowNum());
                continue;
            }
            T dataRow = rowSetter.set(row);
            dataRowList.add(dataRow);
        }
        return dataRowList;
    }


}
