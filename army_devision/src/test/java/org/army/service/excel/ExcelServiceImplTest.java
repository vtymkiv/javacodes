package org.army.service.excel;


import lombok.extern.slf4j.Slf4j;
import org.army.model.ExcelRow;
import org.army.service.excel.ExcelService;
import org.army.service.excel.ExcelServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

@Slf4j
public class ExcelServiceImplTest {
private ExcelService<ExcelRow> excelService;
    final String pathToExcelFile = "src/test/resources/army_state/army_state_on_2022_05_24.xlsx";
    @Before
    public void setUp() {
        excelService = new ExcelServiceImpl(pathToExcelFile);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testReadExcelSheet() {
        List<ExcelRow> excelRowList = this.excelService.readExcelSheet("батальйон");
        excelRowList.forEach(excelRow -> log.info("Row : {}", excelRow));
    }
}