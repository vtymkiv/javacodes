package org.army.service.docx;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.army.model.DocumentContext;
import org.army.model.ExcelRow;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class DocxServiceImplTest {
    final String pathToDocxFile = "src/test/resources/army_report/description_template_1.docx";
    final String pathToNewDocxFile = "src/test/resources/army_report/doc_from_template.docx";
    private DocxService docxService;

    @Before
    public void setUp() {
        docxService = new DocxServiceImpl();
    }

    @Test
    public void testGenerateDocument() {
        List<ExcelRow> healthyList = new ArrayList<>();
        healthyList.add(ExcelRow.builder()
                .firstName("First Name 1")
                .lastName("Last Name 1")
                .middleName("Middle Name 1")
                .position("Position 1")
                .build());

        healthyList.add(ExcelRow.builder()
                .firstName("First Name 2")
                .lastName("Last Name 2")
                .middleName("Middle Name 2")
                .position("Position 2")
                .build());

List<ExcelRow> departList = new ArrayList<>();
        departList.add(ExcelRow.builder()
                .firstName("Depart First Name 1")
                .lastName("Depart Last Name 1")
                .middleName("Depart Middle Name 1")
                .position("Depart Position 1")
                .build());

        departList.add(ExcelRow.builder()
                .firstName("Depart First Name 2")
                .lastName("Depart Last Name 2")
                .middleName("Depart Middle Name 2")
                .position("Depart Position 2")
                .build());

        DocumentContext documentContext = DocumentContext.builder()
                .healthList(new ImmutablePair<>("${healthy_list}", healthyList))
                .docDate(new ImmutablePair<>("${order_date}", "2022-01-01"))
                .orderNum(new ImmutablePair<>("${order_num}", "100000"))
                .departList(new ImmutablePair<>("${leaving_list}", departList))
                .build();
        docxService.generateDocFromTemplate(pathToDocxFile, pathToNewDocxFile, documentContext);
    }
}