package com.example;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * Created by vtymkiv on 23.03.2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class JmsObject {
    @JsonProperty("Abbrev")
    private String abbrev;

    //@JsonDeserialize
    @JsonProperty("Acronym")
    private String acronym;

    @JsonProperty("title")
    private String title;


}
