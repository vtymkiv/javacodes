package com.example.converters;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.java.Log;

/**
 * Created by vtymkiv on 23.03.2017.
 */
@Log
@Component
public class Converter {

    /**
     * Converts json represented in jsonSource as String into object represented in destinationType as type of
     * object.
     *
     * @param jsonSource
     * @param destinationType
     * @param <T>
     * @return
     */
    public <T> T convertFrom(String jsonSource, Class<T> destinationType) throws IOException {
        Objects.requireNonNull(jsonSource, "jsonSource cannot be null");
        Objects.requireNonNull(destinationType, "destinationType cannot be null");

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonSource, destinationType);
    }

    /**
     * Converts json represented in jsonSource starting from pathToNode node into object represented in destinationType
     * as
     * type of
     * object.
     *
     * @param jsonSource
     * @param destinationType
     * @param pathToNode
     * @param <T>
     * @return
     * @throws IOException
     */
    public <T> T convertFrom(String jsonSource, Class<T> destinationType, String pathToNode) throws IOException {
        Objects.requireNonNull(jsonSource, "jsonSource cannot be null");
        Objects.requireNonNull(destinationType, "destinationType cannot be null");
        Objects.requireNonNull(pathToNode, "pathToNode cannot be null");

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(jsonSource);
        JsonNode nodeEntry = root.at(pathToNode);
        return mapper.readValue(nodeEntry.traverse(), destinationType);
    }

    /**
     * Converts json into map
     *
     * @param jsonSource
     * @param <T>
     * @return
     * @throws IOException
     */
    public <T extends Map> T convertFrom(String jsonSource) throws IOException {
        Objects.requireNonNull(jsonSource, "jsonSource cannot be null");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonSource, new TypeReference<T>() {
        });
    }

    public <T extends Map, V> V convertTo(T mapSource, Class<V> destinationType) throws IOException {
        Objects.requireNonNull(mapSource, "jsonSource cannot be null");
        ObjectMapper mapper = new ObjectMapper();
        StringWriter stringWriter = new StringWriter(mapSource.size());
        mapper.writeValue(stringWriter, mapSource);
        return convertFrom(stringWriter.toString(), destinationType);
    }
}
