package com.example;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.converters.Converter;


import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by vtymkiv on 23.03.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ConverterTest {
    @Autowired
    private Converter converter;

    @Test
    public void convertFromGivenJmsObjectAndPathToNode() throws Exception {
        JmsObject jmsObject = converter.convertFrom("{\n" +
                "    \"title\": \"example glossary\",\n" +
                "    \"GlossDiv\": {\n" +
                "      \"title\": \"S\",\n" +
                "      \"GlossList\": {\n" +
                "        \"GlossEntry\": {\n" +
                "          \"ID\": \"SGML\",\n" +
                "          \"SortAs\": \"SGML\",\n" +
                "          \"GlossTerm\": \"Standard Generalized Markup Language\",\n" +
                "          \"Acronym\": \"SGML\",\n" +
                "          \"Abbrev\": \"ISO 8879:1986\",\n" +
                "          \"GlossDef\": {\n" +
                "            \"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",\n" +
                "            \"GlossSeeAlso\": [\n" +
                "              \"GML\",\n" +
                "              \"XML\"\n" +
                "            ]\n" +
                "          },\n" +
                "          \"GlossSee\": \"markup\"\n" +
                "        }\n" +
                "      }\n" +
                "    }\n" +
                "}\n", JmsObject.class, "/GlossDiv");
        assertThat(jmsObject, notNullValue());
    }

    @Test
    public void convertFromGivenElasticSearchObject() throws Exception {
        ElasticSearchObject elasticSearchObject = converter.convertFrom("", ElasticSearchObject.class);
    }

    @Test
    public void convertFromGivenMap() throws Exception {
        Map<String, Object> mapOfValues = converter.convertFrom("{\n" +
                "  \"title\":\"test\",\n" +
                "  \"name\":\"My name\",\n" +
                "  \"anotherOb\" : {\n" +
                "    \"anotherName\":\"anotherName\",\n" +
                "    \"anotherTitle\":\"anotherTitle\"\n" +
                "  }\n" +
                "}");
        assertThat(mapOfValues, notNullValue());
    }

}
