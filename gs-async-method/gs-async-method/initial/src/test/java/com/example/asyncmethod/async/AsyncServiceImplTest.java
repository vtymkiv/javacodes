package com.example.asyncmethod.async;

import com.example.asyncmethod.async.config.AsynServiceImplTestConfig;
import com.example.asyncmethod.async.config.AsyncServiceConfig;
import com.example.asyncmethod.common.domain.ReportFilters;
import com.example.asyncmethod.common.domain.config.JobConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        AsynServiceImplTestConfig.class,
        ReportFilters.class,
        AsyncServiceConfig.class,
        JobConfig.class})

public class AsyncServiceImplTest {
    @Autowired
    private ReportFilters reportFilters;

    @Autowired
    @Qualifier("threadPoolTaskExecutor")
    private Executor threadPoolTaskExecutor;

    private String soeId;
    private String soeId1;

    @Autowired
    @Qualifier("testAsyncService")
    private AsyncService asyncService;

    @Before
    public void setUp() throws Exception {
        this.soeId = "soeId_1";
        this.soeId1 = "soeId_2";
        reportFilters.setModelCode("ICSGML_MODEL_HJG");
    }

    @Test
    public void execute() throws InterruptedException {
        asyncService.execute(soeId, reportFilters);
        asyncService.execute(soeId1, reportFilters);
    }
}