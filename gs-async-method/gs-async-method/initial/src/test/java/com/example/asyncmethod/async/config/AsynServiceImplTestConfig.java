package com.example.asyncmethod.async.config;

import com.example.asyncmethod.async.AsyncService;
import com.example.asyncmethod.async.AsyncServiceImpl;
import com.example.asyncmethod.service.ModelRunner;
import com.example.asyncmethod.service.ModelRunnerImpl;
import com.example.asyncmethod.service.ModelRunnerService;
import com.example.asyncmethod.service.ModelRunnerServiceImpl;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@EnableAsync
public class AsynServiceImplTestConfig {

    @Bean
    @Qualifier("testAsyncService")
    public AsyncService asyncService() {
        return new AsyncServiceImpl();
    }

    @Bean
    public ModelRunnerService modelRunnerService() {
        return new ModelRunnerServiceImpl();
    }

    @Bean
    public ModelRunner modelRunner() {
        return new ModelRunnerImpl();
    }

}
