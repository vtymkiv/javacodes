package com.example.asyncmethod.common.domain.config;

import com.example.asyncmethod.common.domain.Job;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.UUID;

@Configuration
public class JobConfig {
    static final String INITIATED = "initiated";

    @Bean
    @Scope(value = "prototype")
    public Job newJob() {
        Job job = new Job();
        job.setJobId(UUID.randomUUID().toString());
        job.setJobMessage(INITIATED);
        job.setJobStatus(Job.JobStatus.QUEUED);
        return job;
    }
}
