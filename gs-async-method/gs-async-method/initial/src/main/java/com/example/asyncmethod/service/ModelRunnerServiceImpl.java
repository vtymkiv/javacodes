package com.example.asyncmethod.service;

import com.example.asyncmethod.common.domain.Job;
import com.example.asyncmethod.common.domain.ReportFilters;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;

@Slf4j
public class ModelRunnerServiceImpl implements ModelRunnerService {
    @Autowired
    @Qualifier("threadPoolTaskExecutor")
    private Executor taskExecutor;

    @Autowired
    private ModelRunner modelRunner;

    @Autowired
    @Qualifier("newJob")
    private Job newJob;


    @Async("threadPoolTaskExecutor")
    @Override
    public void run(String soeId, ReportFilters reportFilters) {
        this.setUpJobParams(soeId, reportFilters);
        this.modelRunner.runModel(reportFilters);
    }

    private void setUpJobParams(String soeId, ReportFilters reportFilters) {
        newJob.setSoeId(soeId);
        newJob.setJobId(UUID.randomUUID().toString());
        newJob.setScalarRun(reportFilters.isScalarRun());
        newJob.setJobTimeStamp(String.valueOf(Instant.now().toEpochMilli()));
        newJob.setJobName(formatJobName(reportFilters));
        newJob.setParentJobId(getParentJobId(reportFilters));
    }

    private String getParentJobId(ReportFilters reportFilters) {
        if (!reportFilters.isScalarRun()) {
            return null;
        }

        String parentJobId = null;
        if (ObjectUtils.isNotEmpty(reportFilters.getKeyScalarPairs())) {
            parentJobId = this.newJob.getJobId();
        } else if (StringUtils.isNotBlank(reportFilters.getParentJobId())) {
            parentJobId = reportFilters.getParentJobId();
        }
        return parentJobId;
    }

    //TODO: Put description how format for job name work, also move it into another class like Utility
    private String formatJobName(ReportFilters reportFilters) {
        return reportFilters.getModelCode().split("-")[0]
                + "_" + reportFilters.getStressPeriod()
                + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yy_HH:mm"));
    }
}
