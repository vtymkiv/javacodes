package com.example.asyncmethod.service;

import com.example.asyncmethod.common.domain.ReportFilters;

@FunctionalInterface
public interface ModelRunner {
    void runModel(ReportFilters reportFilters);
}
