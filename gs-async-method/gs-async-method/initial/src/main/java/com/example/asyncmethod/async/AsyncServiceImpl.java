package com.example.asyncmethod.async;

import com.example.asyncmethod.common.domain.Job;
import com.example.asyncmethod.common.domain.ReportFilters;
import com.example.asyncmethod.service.ModelRunnerService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * Service that will be used to call async service method
 */
@Slf4j
@Service
public class AsyncServiceImpl implements AsyncService {
    @Autowired
    @Qualifier("threadPoolTaskExecutor")
    private Executor taskExecutor;

    @Autowired
    private ModelRunnerService modelRunnerService;


    @Override
    public void execute(@Valid String soeId, ReportFilters reportFilters) {
        LOGGER.debug("Start to run execute method with soeId: {} and report filters: {}", soeId, reportFilters);
        // Validate all required values
        Objects.requireNonNull(soeId, "soeId can not be null.");
        Objects.requireNonNull(reportFilters, "reportFilters can not be null.");

        LOGGER.debug("Running task with soeId: {}, and reportFilters: {}", soeId, reportFilters);
        if (StringUtils.isBlank(reportFilters.getModelCode())) {
            throw new IllegalArgumentException("No model code was specified");
        }
        // We should get Future object in order to cancel job
        modelRunnerService.run(soeId, reportFilters);

        //return CompletableFuture.completedFuture();
    }


}
