package com.example.asyncmethod.async;

import com.example.asyncmethod.common.domain.ReportFilters;

@FunctionalInterface
public interface AsyncService {
    void execute(String soeId, ReportFilters reportFilters);
}
