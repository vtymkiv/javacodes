package com.example.asyncmethod.common.domain;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;
import org.springframework.stereotype.Component;

@Component
@Data
@ToString
public final class Job {
    private String jobId;
    private String parentJobId;
    private String jobName;
    private String jobMessage;
    private String jobTimeStamp;
    private String soeId;
    private boolean scalarRun;
    private JobStatus jobStatus;

    @Getter
    @ToString(onlyExplicitlyIncluded = true)
    public static enum JobStatus {
        QUEUED(1, "Queued"),
        RUNNING(2, "Running"),
        COMPLETED(3, "Completed"),
        CANCELED(4, "Canceled"),
        ERROR(5, "Error"),
        CANCELLING(6, "Cancelling");

        private final int id;
        @ToString.Include
        private final String desc;

        JobStatus(int id, String desc) {
            this.id = id;
            this.desc = desc;
        }
    }
}
