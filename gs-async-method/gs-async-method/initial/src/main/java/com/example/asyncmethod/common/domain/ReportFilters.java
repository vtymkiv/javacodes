package com.example.asyncmethod.common.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportFilters {

    @JsonIgnore
    private final String YES = "yes";

    private String scenario;
    private String busDate;
    private String modelCode;
    private String parentJobId;
    private int stressPeriod;
    private boolean scalarRun;

    private List<KeyScalarPair> keyScalarPairs;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private String lockupOOOId;
}
