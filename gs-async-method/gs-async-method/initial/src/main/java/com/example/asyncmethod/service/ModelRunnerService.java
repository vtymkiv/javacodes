package com.example.asyncmethod.service;

import com.example.asyncmethod.common.domain.ReportFilters;

import java.util.concurrent.Future;

public interface ModelRunnerService {
    void run(String soeId, ReportFilters reportFilters);
    //Future<?> run(ReportFilters reportFilters);
}
