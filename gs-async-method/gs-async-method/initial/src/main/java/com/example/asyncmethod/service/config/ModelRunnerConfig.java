package com.example.asyncmethod.service.config;

import com.example.asyncmethod.service.ModelRunner;
import com.example.asyncmethod.service.ModelRunnerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelRunnerConfig {

    @Bean
    public ModelRunner modelRunner() {
        return new ModelRunnerImpl();
    }


}
