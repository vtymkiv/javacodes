package com.example.asyncmethod.async.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Data
@Slf4j
@Configuration
@EnableAsync
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "spring.task.execution.pool")
public class AsyncServiceConfig {
    private int coreSize;
    private int maxSize;
    private int queueCapacity;


    @Bean(name = "threadPoolTaskExecutor")
    public Executor threadPool() {
        ThreadPoolTaskExecutor threadPoolExecutor = new ThreadPoolTaskExecutor();
        threadPoolExecutor.setCorePoolSize(coreSize);
        threadPoolExecutor.setMaxPoolSize(maxSize);
        threadPoolExecutor.setQueueCapacity(queueCapacity);
        threadPoolExecutor.setThreadNamePrefix("Async-");
        return threadPoolExecutor;
    }

/*
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return (ex, method, params) -> {
            LOGGER.error(ex.getMessage(), ex);
        };
    }
*/

}
