package com.example.asyncmethod.service;

import com.example.asyncmethod.common.domain.ReportFilters;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
public class ModelRunnerImpl implements ModelRunner {
    @Override
    public void runModel(ReportFilters reportFilters) {
        Objects.requireNonNull(reportFilters, "reportFilters should exist.");

        LOGGER.debug("Running model with filters: {}", reportFilters);
    }
}
