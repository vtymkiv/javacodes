package com.example.asyncmethod.service.config;

import com.example.asyncmethod.service.ModelRunnerService;
import com.example.asyncmethod.service.ModelRunnerServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelRunnerServiceConfig {

    @Bean
    public ModelRunnerService modelRunnerService() {
        return new ModelRunnerServiceImpl();
    }
}
