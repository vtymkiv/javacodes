package com.example.asyncmethod.common.domain;

import lombok.Data;

@Data
public class KeyScalarPair {
    private String key;
    private Double scalar;
    private Double increase;
    private String assetCode;
    private String liabilityCode;
}
