package com.example.datetime;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class DemoApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Test
    public void timeZone() {
        ZoneId zoneId = ZoneId.of("Asia/Kolkata");
        LocalDateTime localDateTime = LocalDateTime.now();
        ZonedDateTime zonedDateTime = localDateTime.atZone(zoneId);
        ZoneOffset zoneOffset = zonedDateTime.getOffset();
        LOGGER.info("Local data time {}, zoned data time {}, zone offset {}", localDateTime, zonedDateTime, zoneOffset);
    }
}
