package com.example.datetime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by vtymkiv on 27.07.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CustomDatePatternsService.class})
@Slf4j
public class CustomDatePatternsServiceTest {
    @Autowired
    private CustomDatePatternsService customDatePatternsService;

    @Test
    public void showCustomPatternsWithDescription() throws Exception {
        customDatePatternsService.showCustomPatternsWithDescription();
    }

}