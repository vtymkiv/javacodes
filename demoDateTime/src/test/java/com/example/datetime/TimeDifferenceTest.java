package com.example.datetime;

import java.time.ZoneId;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by vtymkiv on 21.07.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TimeDifference.class})
@Slf4j
public class TimeDifferenceTest {
    @Autowired
    private TimeDifference timeDifference;

    @Test
    public void showCurrentTimeDifferenceBetween() throws Exception {

        ZoneId kyivZoneId = ZoneId.of("Europe/Kiev");
        // US/Central -> Chicago
        ZoneId chicagoZoneId = ZoneId.of("US/Central");
        long timeDifference = this.timeDifference.showCurrentTimeDifferenceBetween(kyivZoneId, chicagoZoneId);
        LOGGER.info("The difference between {} and {} is {} hours", kyivZoneId, chicagoZoneId,
                timeDifference);
    }

    @Test
    public void getZoneIdList() {
        LOGGER.info("Zone id list: {}", timeDifference.getZoneIdList().stream().filter(s -> s.contains("Kiev") || s
                .contains("Central")).collect(Collectors.toList()));
    }
}