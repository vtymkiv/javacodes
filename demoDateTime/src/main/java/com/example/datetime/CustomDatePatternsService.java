package com.example.datetime;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by vtymkiv on 27.07.2017.
 */
@Service
@Slf4j
public class CustomDatePatternsService {
    public void showCustomPatternsWithDescription() {
        LocalDateTime now = LocalDateTime.now();
        for (CustomDatePattern customDatePattern : CustomDatePattern.values()) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(customDatePattern.getPattern());
            LOGGER.info("Pattern {} is {}", customDatePattern.getPattern(), dateTimeFormatter.format(now));
        }

    }
}
