package com.example.datetime;

/**
 * Created by vtymkiv on 27.07.2017.
 */
public enum CustomDatePattern {
    DAY_MONTH_YEAR("dd-MM-yyyy", "d is day (in month), M is month, y is year"), DAY_OF_THE_YEAR("d '('E')' MMM, YYYY", "E is name of the day (in week), Y is year"), WEEK_OF_THE_YEAR("w'th week of' YYYY", "w is the week of the year"), DAY_IN_THE_WEEK("EEEE, dd'th' MMMM, YYYY", "E is day name in the week");
    private String pattern;
    private String description;

    CustomDatePattern(String pattern, String description) {
        this.pattern = pattern;
        this.description = description;
    }

    public String getPattern() {
        return pattern;
    }

    public String getDescription() {
        return description;
    }
}
