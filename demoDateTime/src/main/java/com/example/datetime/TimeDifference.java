package com.example.datetime;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

/**
 * Created by vtymkiv on 21.07.2017.
 */
@Service
public class TimeDifference {

    public long showCurrentTimeDifferenceBetween(ZoneId sourceZoneId, ZoneId destinationZoneId) {
        ZonedDateTime sourceZonedDateTime = ZonedDateTime.now(sourceZoneId);
        ZonedDateTime destinationZonedDateTime = sourceZonedDateTime.withZoneSameInstant(destinationZoneId);

        Duration timeDifference = Duration.between(sourceZonedDateTime.toLocalTime(), destinationZonedDateTime
                .toLocalTime());
        return timeDifference.toHours();
    }

    public List<String> getZoneIdList() {
        List<String> zoneIdList = new ArrayList<>(ZoneId.getAvailableZoneIds());
        return zoneIdList.stream().sorted().collect(Collectors.toList());
    }


}
