package interview;

import interview.codingquestions.SingleLinkedList;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

public class SingleLinkedListTest {
    private SingleLinkedList<String> singleLinkedList;

    @Before
    public void setUp() {
        this.singleLinkedList = new SingleLinkedList<>();
    }

    @Test
    public void add() {
        String[] studentNameList = {"Petya", "Vasya", "Igor"};
        this.singleLinkedList.add(studentNameList[0]);
        this.singleLinkedList.add(studentNameList[1]);
        this.singleLinkedList.add(studentNameList[2]);

        //assertThat(this.singleLinkedList.getSize(), is(studentNameList.length));
        this.singleLinkedList.printAll();

    }
}