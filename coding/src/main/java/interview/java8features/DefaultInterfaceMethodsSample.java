package interview.java8features;

interface Fly {
    default void takeOff() {
        System.out.println("Fly::takeOff");
    }

    default void turnOn() {
        System.out.println("Fly::turnOn");
    }

    default void cruise() {
        System.out.println("Fly::cruise");
    }

    default void land() {
        System.out.println("Fly::land");
    }
}

class FlyImpl implements Fly {

}

public class DefaultInterfaceMethodsSample {
    public static void main(String[] args) {
        Fly fly = new FlyImpl();
        fly.takeOff();
    }
}
