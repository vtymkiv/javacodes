package interview.java8features;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Sample {
    public static void main(String[] args) {
        List<String> nameList = Arrays.asList("Tom", "Jerry", "Jane", "Jack");
        System.out.println(nameList.stream().map(String::toUpperCase).collect(Collectors.joining(", ")));
    }
}
