package interview.java8features.mapreduce;

import interview.Gender;
import interview.Person;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DemoMapReduce {
    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        Person person = new Person("Test", Gender.Male, 45).withSalary(new BigDecimal(100.0)).withLC("Y");
        Person person1 = new Person("Test2", Gender.Male, 45).withSalary(new BigDecimal(200.0)).withLC("");
        Person person2 = new Person("Test3", Gender.Female, 45).withSalary(new BigDecimal(300.0));
        Person person3 = new Person("Test4", Gender.Female, 45).withSalary(new BigDecimal(150.0));
        people.add(person);
        people.add(person1);
        people.add(person2);
        people.add(person3);

        double asDouble = people.stream().mapToInt(prsn -> person1.getAge()).average().getAsDouble();
        BigDecimal salary = people.stream().map(prsn -> prsn.getSalary()).reduce(BigDecimal.ZERO, BigDecimal::add);
        Map<Gender, Optional<BigDecimal>> collect = people.stream().collect(Collectors.groupingBy(prs1 -> prs1.getGender(), Collectors.mapping(prsn -> prsn.getSalary(), Collectors.reducing(BigDecimal::add))));//map(prsn->prsn.getSalary()).reduce(BigDecimal.ZERO, BigDecimal::add);
        Map<Gender, Optional<BigDecimal>> collect1 = people.stream().filter(prs1 -> "Y".equals(prs1.getLegalCertainty())).collect(Collectors.groupingBy(prs1 -> prs1.getGender(), Collectors.mapping(prsn -> prsn.getSalary(), Collectors.reducing(BigDecimal::add))));//map(prsn->prsn.getSalary()).reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(String.format("%s %s", asDouble, salary));
        System.out.println(collect);
        System.out.println(collect1);
        System.out.println("Y".equals(null));


    }
}
