package interview.java8features;

interface Util {
    static int numberOfCores() {
        return Runtime.getRuntime().availableProcessors();
    }
}

public class InterfacesSample {
    public static void main(String[] args) {
        System.out.println(Util.numberOfCores());
    }
}
