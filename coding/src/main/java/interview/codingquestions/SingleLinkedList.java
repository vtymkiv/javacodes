package interview.codingquestions;

import interview.codingquestions.Node;

public class SingleLinkedList<T> {
    private Node<T> head;
    private int size;
    private Node<T> currentNode;

    public SingleLinkedList() {
        this.head = null;
        this.size = 0;
    }

    public void add(T element) {
        Node<T> newNode = new Node<>(element, null);

        if (this.isEmpty()) {
            head = currentNode = newNode;
        } else {
            currentNode.setNextNode(newNode);
            currentNode = newNode;
        }
        size++;
    }

    public void removeElement() {
        size--;
    }

    public int getSize() {
        return this.size;
    }

    public void printAll() {
        if (this.isEmpty()) {
            System.out.println("The list is empty.");
            return;
        }

        Node<T> node = head;
        while (node != null) {
            System.out.println(node.getElement());
            node = node.getNextNode();
        }

    }


    private boolean isEmpty() {
        return this.getSize() == 0;
    }

    public Node<T> getHead() {
        return head;
    }

    public void reverse(Node head) {
        Node<T> current = head;
        Node<T> previous = null;
        Node<T> next;


        while (current != null) {
            next = current.getNextNode();
            current.setNextNode(previous);
            previous = current;
            current = next;

        }
        this.head = previous;
    }

    public boolean hasLoop(Node head) {
        Node<T> slow = head;
        Node<T> fast = head;
        while (slow != null && fast != null && fast.getNextNode() != null) {
            slow = slow.getNextNode();
            fast = fast.getNextNode().getNextNode();
            if (slow == fast) {
                return true;
            }
        }
        return false;
    }
}
