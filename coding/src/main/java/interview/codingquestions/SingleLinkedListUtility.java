package interview.codingquestions;

public class SingleLinkedListUtility {
    public static void main(String[] args) {
        SingleLinkedList<String> linkedList = new SingleLinkedList();
        linkedList.add("1");
        linkedList.add("2");
        linkedList.add("3");
        linkedList.add("2");
        System.out.println("Before reverse");
        linkedList.printAll();
        linkedList.reverse(linkedList.getHead());
        System.out.println("After reverse");
        linkedList.printAll();

        Node<String> node1 = new Node<>("1", null);
        Node<String> node2 = new Node<>("2", null);
        /*Node<String> node3 = new Node<>("3", null);
        Node<String> node4 = new Node<>("4", null);
        Node<String> node5 = new Node<>("5", null);*/
        node1.setNextNode(node2);
        node2.setNextNode(node1);
//        node3.setNextNode(node4);
//        node4.setNextNode(node5);
//        node5.setNextNode(node2);

        System.out.println("Has loop " + linkedList.hasLoop(node1));
    }
//
//    public static <T> SingleLinkedList<T> reverseList(SingleLinkedList<T> linkedList) {
//        Node<T> head = linkedList.getHead();
//        Node<T> previous = null;
//        Node<T> current;
//        Node<T> next;
//
//        current = head;
//
//        while(current != null) {
//            next = current.getNextNode();
//            current.setNextNode(previous);
//            previous = current;
//            current = next;
//
//        }
//
//        linkedList.
//    }
}
