package interview.codingquestions.streams;

import interview.Gender;
import interview.Person;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class FindFirstUse1 {
    public static void main(String[] args) {
        /*Method[] methodList = Stream.class.getMethods();
        Optional<String> methodName = Arrays.stream(methodList).map(method -> method.getName()).filter(name -> name.endsWith("Match")).sorted().findFirst();
        System.out.println("Result: " + methodName.orElse("No method found."));*/
        groupByExamples();
    }

    public static void groupByExamples() {
        List<Person> persons = new ArrayList<>();

        persons.add(new Person("A", Gender.Male, 10));
        persons.add(new Person("B", Gender.Male, 10));
        persons.add(new Person("C", Gender.Male, 10));
        persons.add(new Person("A1", Gender.Female, 10));
        persons.add(new Person("B1", Gender.Female, 10));

        final Map<Gender, List<Person>> petsGroupedByCity = persons.stream().collect(
                Collectors.groupingBy(
                        Person::getGender
                )
        );
    }
}
