package interview.codingquestions.algoritms;

public class Factorial {

    long recursiveFactorial(long n) {
        if (n <= 1) {
            return 1;
        }
        return n * recursiveFactorial(n - 1);
    }

    public static void main(String[] args) {
        Factorial fact = new Factorial();
        System.out.println(fact.recursiveFactorial(3));
        System.out.println(fact.recursiveFactorial(4));
        System.out.println(fact.factorial(3));
        System.out.println(fact.factorial(4));
    }

    long factorial(long n) {
        long factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial *= i;
        }
        return factorial;
    }
}
