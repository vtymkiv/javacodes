package interview.codingquestions.algoritms;

public class PoweOfTwo {

    public static boolean isPowerOf2(int n) {
        boolean isPowerOf2 = false;
        if (n == 2) {
            return true;
        }
        if (n % 2 == 0) {
            isPowerOf2 = true;
            return isPowerOf2 && isPowerOf2(n / 2);
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(isPowerOf2(3));
        System.out.println(isPowerOf2(1));
        System.out.println(isPowerOf2(5));
        System.out.println(isPowerOf2(18));
        System.out.println(isPowerOf2(4));
        System.out.println(isPowerOf2(8));
    }
}
