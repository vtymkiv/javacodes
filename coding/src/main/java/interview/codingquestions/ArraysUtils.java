package interview.codingquestions;

import java.util.stream.IntStream;

public class ArraysUtils {
    public static void main(String[] args) {
        int i[] = {1, 2, 3, 4};

        printAll(i);
        System.out.println("===============");
        printAll(reverse(i));
    }

    public static int[] reverse(int[] array) {
        for (int i = 0, j = array.length - 1; i < j; i++, j--) {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }

        return array;
    }

    public static void printAll(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }
}
