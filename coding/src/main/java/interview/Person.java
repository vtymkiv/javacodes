package interview;


import java.math.BigDecimal;

public class Person {
    private String name;
    private Gender gender;
    private int age;
    private BigDecimal salary = BigDecimal.ZERO;
    private String legalCertainty;

    public Person(String name, Gender gender, int age) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public Person withSalary(BigDecimal salary) {
        this.salary = salary;
        return this;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", gender=" + gender +
                ", age=" + age +
                '}';
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public String getLegalCertainty() {
        return legalCertainty;
    }

    public Person withLC(String legalCertainty) {
        this.legalCertainty = legalCertainty;
        return this;
    }
}
