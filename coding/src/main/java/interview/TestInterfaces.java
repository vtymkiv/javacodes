package interview;

interface Util {
    static int numberOfCourses() {
        return Runtime.getRuntime().availableProcessors();
    }
}

interface Fly {
    default void takeOff() {
        System.out.println("takeOff");
    }

    default void turn() {
        System.out.println("turn");
    }

    default void cruise() {
        System.out.println("cruise");
    }

    default void land() {
        System.out.println("land");
    }
}

interface FastFly extends Fly {
    default void turn() {
        System.out.println("FastFly::turn");
    }

}

interface Sail {
    default void cruise() {
        System.out.println("Sail::cruise");
    }
}

class Vehicle {
    public void land() {
        System.out.println("Vehicle::land");
    }
}

class SeaPlane extends Vehicle implements FastFly, Sail {

    @Override
    public void cruise() {
        System.out.println("SeaPlane::cruise");
        FastFly.super.cruise();
    }
}

public class TestInterfaces {
    public static void main(String[] args) {
        System.out.println(Util.numberOfCourses());
        SeaPlane seaPlane = new SeaPlane();
        seaPlane.turn();
    }
}
