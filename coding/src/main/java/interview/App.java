package interview;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.lang.System.*;

/**
 * Hello world!
 */
public class App {
    public static List<Person> createPeople() {
        return Arrays.asList(
                new Person("Sara", Gender.Female, 20),
                new Person("Sara", Gender.Female, 22),
                new Person("Bob", Gender.Male, 20),
                new Person("Paula", Gender.Female, 32),
                new Person("Paul", Gender.Male, 32),
                new Person("Jack", Gender.Male, 2),
                new Person("Jack", Gender.Male, 72),
                new Person("Jill", Gender.Female, 12)

        );
    }

    public static void main(String[] args) {

        /*List<String> nameList = Arrays.asList("Tom", "Jerry", "Jane");

        List<String> convertedNames = nameList.stream().map(String::toUpperCase).collect(Collectors.toList());
        String convertedToString = convertedNames.stream().collect(Collectors.joining(","));
        out.println(convertedNames);
        out.println(convertedToString);*/
        workWithPeople();

    }

    public static void workWithPeople() {
        Predicate<Integer> gt5 = i -> i > 5;
        printSorted(createPeople(), Comparator.comparing(Person::getName).thenComparing(Person::getAge));
        printGrouped(createPeople());

    }

    public static void printSorted(List<Person> personList, Comparator comparartor) {
        personList.stream().sorted(comparartor).forEach(out::println);
    }

    public static void printGrouped(List<Person> personList) {
        Map<Gender, List<Person>> collect = personList.stream().collect(Collectors.groupingBy(Person::getGender));
        Map<Integer, List<Person>> groupedByAge = personList.stream().collect(Collectors.groupingBy(person -> person.getAge()));
        out.println(groupedByAge);
    }
}
