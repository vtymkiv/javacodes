package com.db;

/**
 * Created by vtymkiv on 16.11.2016.
 */
public interface FileWatcherService {
    void watchFile(String filePath);
}
