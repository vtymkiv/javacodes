package com.db.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;


import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks class or method as logging, so every invocation will be logged
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD, ElementType.TYPE})
public @interface Logging {

}
