package com.db.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

/**
 * Logger aspect logs start and end of every method
 */

@Service
@Aspect
public class LoggingAspect {

    /**
     * To catch all methods with annotation Loggable
     */
    @Pointcut("execution(@com.db.annotations.Logging * *(..))")
    public void loggedMethod() {
    }

    /**
     * To catch all classes with annotation Loggable
     */
    @Pointcut("within(@com.db.annotations.Logging *)")
    public void loggedBean() {
    }

    /**
     * Logging start message
     *
     * @param joinPoint
     */
    @Before(value = "(loggedMethod() || loggedBean())")
    public void logBefore(JoinPoint joinPoint) {
        String name = joinPoint.getSignature().getName();
        Logger typeLogger = AspectUtils.getLogger(joinPoint);
        typeLogger.debug("|started| {}", name);
    }

    /**
     * Logging end of the message execution
     *
     * @param joinPoint
     */
    @AfterReturning(value = "(loggedMethod() || loggedBean())", returning = "result")
    public void logAfter(JoinPoint joinPoint, Object result) {
        String name = joinPoint.getSignature().getName();
        Logger typeLogger = AspectUtils.getLogger(joinPoint);
        typeLogger.debug("|ended| {}", name);
    }

    /**
     * Logging of exception in method execution
     *
     * @param joinPoint
     * @param e         - occurred exception
     */
    @AfterThrowing(value = "(loggedMethod() || loggedBean())", throwing = "e")
    public void logException(JoinPoint joinPoint, Exception e) {
        String name = joinPoint.getSignature().getName();
        Logger typeLogger = AspectUtils.getLogger(joinPoint);
        typeLogger.error("|ended| " + name + " | with exception", e);
    }
}
