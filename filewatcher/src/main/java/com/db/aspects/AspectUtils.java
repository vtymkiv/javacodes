package com.db.aspects;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Utilities for working with aspects
 */
@Component
public final class AspectUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(AspectUtils.class);

    private AspectUtils() {
    }

    /**
     * Get logger from specified class
     *
     * @param joinPoint
     * @return
     */
    public static Logger getLogger(org.aspectj.lang.JoinPoint joinPoint) {
        Class<?> declaringType = null;
        try {
            declaringType = joinPoint.getSignature().getDeclaringType();
            Field loggerField = declaringType.getDeclaredField("LOGGER");
            loggerField.setAccessible(true);
            return (Logger) loggerField.get(null);
        } catch (NoSuchFieldException e) {
            if (declaringType != null) {
                LOGGER.error("No logger declared for type: " + declaringType.getName(), e);
            }
        } catch (Exception e) {//NOSONAR //NOPMD //We can't know which exceptions may occur, but this logic should be protected
            if (declaringType != null) {
                LOGGER.error("Exception occured trying to access logger in" + declaringType.getName(), e);
            }
        }
        return LOGGER;
    }
}

