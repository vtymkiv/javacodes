package com.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.db.annotations.Logging;

/**
 * Created by vtymkiv on 16.11.2016.
 */
@Service
@Configuration
@Logging
public class FileWatcherServiceImpl implements FileWatcherService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileWatcherServiceImpl.class);

    @Override
    public void watchFile(String filePath) {
        System.out.println("watch file " + filePath);
    }


    public static void main(String args[]) {
        FileWatcherServiceImpl fileWatcherService = new FileWatcherServiceImpl();
        fileWatcherService.watchFile("test file.");
    }
}
